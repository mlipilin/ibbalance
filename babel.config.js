module.exports = function(api) {
    api.cache(true);

    const presets = ['@babel/preset-env', '@babel/preset-react'];
    const plugins = [
        ['@babel/plugin-proposal-decorators', { legacy: true }],
        '@babel/proposal-class-properties',
        '@babel/plugin-syntax-dynamic-import',
        [
            '@babel/plugin-transform-runtime',
            {
                regenerator: true,
            },
        ],
        [
            'prismjs',
            {
                languages: ['javascript', 'jsx', 'sass', 'markdown'],
                plugins: ['line-numbers', 'show-language'],
                css: true,
            },
        ],
    ];

    return {
        presets,
        plugins,
    };
};
