import React, { useContext } from 'react';

// Components
import ThemeChangerComponent from '../../components/ThemeChanger';

import ThemeContext from '../../theme-context';

const ThemeChanger = () => {
    const context = useContext(ThemeContext);
    const { name: themeName, change: changeTheme } = context;
    return <ThemeChangerComponent value={themeName} onChange={changeTheme} />;
};

export default ThemeChanger;
