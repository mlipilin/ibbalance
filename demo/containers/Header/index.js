import React from 'react';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
// import PropTypes from 'prop-types';

// Components
import Logo from '../../components/Logo';

// Containers
import ThemeChanger from '../ThemeChanger';

// Utils
import url from '../../../src/utils/url';

import packageJson from '../../../package.json';

import styles from './styles.sass';

const Header = props => {
    const handleLogoClick = () => {
        const { history } = props;
        history.push(url.route.about());
    };

    return (
        <div className={styles.Header}>
            <div className={styles.Header__Container}>
                <div className={styles.Header__Center}>
                    <div className={styles.Header__Logo}>
                        <Logo onClick={handleLogoClick} />
                    </div>
                    <div className={styles.Header__Version}>v{packageJson.version}</div>
                </div>
                <div className={styles.Header__ThemeChanger}>
                    <ThemeChanger />
                </div>
            </div>
        </div>
    );
};

Header.propTypes = {
    history: ReactRouterPropTypes.history.isRequired,
};

export default withRouter(Header);
