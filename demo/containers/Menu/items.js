// Constants
import { COMPONENTS_DEMO_BLACK_LIST } from './constants';

// Constructors
import { createMenuGroup, createMenuItem } from '../../constructors/menu';

// Utils
import url from '../../../src/utils/url';

// All components from /src/components folder
const componentsMenuGroupItems = require
    .context('../../../src/components', true, /^\.\/[a-zA-Z]+\/$/)
    .keys()
    .filter(key => !COMPONENTS_DEMO_BLACK_LIST.some(c => `./${c}/` === key))
    .map(key => {
        const title = key.replace(/\.|\//g, '');
        return createMenuItem({
            title,
            url: url.route.viewSubitem({ item: 'components', subitem: title }),
        });
    });

export default [
    createMenuItem({
        title: 'Что такое iBalance?',
        url: url.route.about(),
    }),
    createMenuItem({
        title: 'Начало работы',
        url: url.route.gettingStarted(),
    }),
    createMenuItem({
        title: 'Какие бывают компоненты?',
        url: url.route.kindOfComponents(),
    }),
    createMenuGroup({
        title: 'Компоненты',
        items: componentsMenuGroupItems,
    }),
    createMenuItem({
        title: 'Константы и миксины',
        url: url.route.constants(),
    }),
    createMenuGroup({
        title: 'Типографика',
        items: [
            createMenuItem({
                title: 'Заголовки',
                url: url.route.viewSubitem({ item: 'typography', subitem: 'H' }),
            }),
            createMenuItem({
                title: 'Текст',
                url: url.route.viewSubitem({ item: 'typography', subitem: 'Text' }),
            }),
        ],
    }),
    createMenuItem({
        title: 'Обертки',
        url: url.route.wrappers(),
    }),
    createMenuItem({
        title: 'Сетка',
        url: url.route.grid(),
    }),
    createMenuItem({
        title: 'Разметка страницы',
        url: url.route.layout(),
    }),
    createMenuItem({
        title: 'Создание компонента',
        url: url.route.createComponent(),
    }),
];
