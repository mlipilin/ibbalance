import { applyActiveOpenStatesToMenu } from './helpers';

// Constructors
import { createMenuGroup, createMenuItem } from '../../constructors/menu';

describe('demo/containers/Menu/helpers.js', () => {
    describe('applyActiveOpenStatesToMenu', () => {
        it('menu items', () => {
            const input = [
                [
                    createMenuItem({ title: 'Item 1', url: 'http://ibalance.com/item1' }),
                    createMenuItem({ title: 'Item 2', url: 'http://ibalance.com/item2' }),
                    createMenuItem({ title: 'Item 3', url: 'http://ibalance.com/item3' }),
                ],
                'http://ibalance.com/item2',
            ];
            const output = [
                createMenuItem({
                    active: false,
                    title: 'Item 1',
                    url: 'http://ibalance.com/item1',
                }),
                createMenuItem({ active: true, title: 'Item 2', url: 'http://ibalance.com/item2' }),
                createMenuItem({
                    active: false,
                    title: 'Item 3',
                    url: 'http://ibalance.com/item3',
                }),
            ];
            expect(applyActiveOpenStatesToMenu(...input)).toEqual(output);
        });
        it('menu groups', () => {
            const input = [
                [
                    createMenuGroup({
                        title: 'Group 1',
                        items: [
                            createMenuItem({
                                title: 'Item 1',
                                url: 'http://ibalance.com/group1/item1',
                            }),
                            createMenuItem({
                                title: 'Item 2',
                                url: 'http://ibalance.com/group1/item2',
                            }),
                            createMenuItem({
                                title: 'Item 3',
                                url: 'http://ibalance.com/group1/item3',
                            }),
                        ],
                    }),
                    createMenuGroup({
                        title: 'Group 2',
                        items: [
                            createMenuItem({
                                title: 'Item 4',
                                url: 'http://ibalance.com/group2/item4',
                            }),
                            createMenuItem({
                                title: 'Item 5',
                                url: 'http://ibalance.com/group2/item5',
                            }),
                            createMenuItem({
                                title: 'Item 6',
                                url: 'http://ibalance.com/group2/item6',
                            }),
                        ],
                    }),
                ],
                'http://ibalance.com/group2/item5',
            ];
            const output = [
                createMenuGroup({
                    open: false,
                    title: 'Group 1',
                    items: [
                        createMenuItem({
                            active: false,
                            title: 'Item 1',
                            url: 'http://ibalance.com/group1/item1',
                        }),
                        createMenuItem({
                            active: false,
                            title: 'Item 2',
                            url: 'http://ibalance.com/group1/item2',
                        }),
                        createMenuItem({
                            active: false,
                            title: 'Item 3',
                            url: 'http://ibalance.com/group1/item3',
                        }),
                    ],
                }),
                createMenuGroup({
                    open: true,
                    title: 'Group 2',
                    items: [
                        createMenuItem({
                            active: false,
                            title: 'Item 4',
                            url: 'http://ibalance.com/group2/item4',
                        }),
                        createMenuItem({
                            active: true,
                            title: 'Item 5',
                            url: 'http://ibalance.com/group2/item5',
                        }),
                        createMenuItem({
                            active: false,
                            title: 'Item 6',
                            url: 'http://ibalance.com/group2/item6',
                        }),
                    ],
                }),
            ];
            expect(applyActiveOpenStatesToMenu(...input)).toEqual(output);
        });
    });
});
