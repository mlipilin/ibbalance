import urlUtil from '../../../src/utils/url';

export const applyActiveOpenStatesToMenu = (menuItems, url) => {
    return menuItems.map(item => {
        if (item.items) {
            let open = false;

            const items = item.items.map(subitem => {
                if (subitem.url === url) {
                    open = true;
                    return { ...subitem, active: true };
                }
                return subitem;
            });

            return { ...item, items, open };
        }

        let active = item.url === url;
        if (url === '/' && item.url === urlUtil.route.about()) {
            active = true;
        }

        return { ...item, active };
    });
};
