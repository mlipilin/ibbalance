import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
// import PropTypes from 'prop-types';

// Components
import Menu from '../../components/Menu';

// Constants
import ITEMS from './items';

// Helpers
import { applyActiveOpenStatesToMenu } from './helpers';

import styles from './styles.sass';

@withRouter
class MenuContainer extends Component {
    handleItemClick = url => {
        const { history } = this.props;
        history.push(url);
    };

    render() {
        const {
            match: { url },
        } = this.props;
        const items = applyActiveOpenStatesToMenu(ITEMS, url);

        return (
            <div className={styles.MenuContainer}>
                <Menu items={items} onItemClick={this.handleItemClick} />
            </div>
        );
    }
}

MenuContainer.propTypes = {
    history: ReactRouterPropTypes.history,
    match: ReactRouterPropTypes.match,
};

MenuContainer.defaultProps = {
    history: undefined,
    match: undefined,
};

export default MenuContainer;
