# Создание компонента

Как было сказано в [одном из предыдущих разделов](/kind-of-components), компоненты в *iB*alance бывают четырех типов: простые компоненты, компоненты-обёртки, типографика и структурные компоненты. В данном разделе будет рассмотрено создание простого компонента, остальные создаются по аналогичной схеме.

## Storybook

Подразумевается, что разработка компонента будет вестись через [Storybook](https://storybook.js.org/docs/guides/guide-react/), поэтому работу лучше начинать со следующей команды:

```bash
npm run storybook
```

После этого все имеющиеся компоненты с "историями" будут доступны по адресу [http://localhost:8013](http://localhost:8013)

## Создание простого компонента

Для каждого **простого** компонента создается папка: `src/components/ComponentName`, где `ComponentName` - название компонента.

Структура папки с компонентом выглядит следующим образом:

<img src="/public/demo/create_component_folder_structure.png" alt="Структура папки с компонентом" style="width: 325px;">

-   `ComponentName/index.js` - файл с кодом компонента
-   `ComponentName/stories/index.js` - файл с "историями" для Storybook
-   `ComponentName/readme.md` - файл, содержащий информацию о компоненте для разработчика:
    -   примеры использования
    -   описание PropTypes
    -   все возможные CSS-селекторы, с которыми работает компонент

### ComponentName/index.js (базовый код)

<!-- prettier-ignore -->
```jsx
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import { withTheme } from '../../theme-provider';

@withTheme
class ComponentName extends Component {
  render() {
    const { className, applyTheme, ...otherProps } = this.props;

    const componentClass = applyTheme(cn('ComponentName'), className);

    const innerDivClass = applyTheme(cn('ComponentName__InnerDiv'));

    return (
      <div className={componentClass} {...otherProps}>
        <div className={innerDivClass}>my component</div>
      </div>
    );
  }
}

ComponentName.propTypes = {
  className: PropTypes.string,
  applyTheme: PropTypes.func,
};

ComponentName.defaultProps = {
  applyTheme: _ => _,
};

export default ComponentName;
```

### ComponentName/stories/index.js (базовый код)

<!-- prettier-ignore -->
```jsx
import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';

import ComponentName from '../index';

import notes from '../readme.md';

let stories = storiesOf('ComponentName', module);

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories.add('Default', () => <ComponentName />, { notes });
```

### ComponentName/readme.md (базовый код)

<img src="/public/demo/component_readme_sample.png" alt="ComponentName/readme.md" style="width: 325px;">

Далее работаем с перечисленными файлами:

-   описываем логику работы компонента в файле `ComponentName/index.js`
-   добавляем **истории** компонента в файл `ComponentName/stories/index.js`
-   докуменируем интерфейс созданного компонента в `ComponentName/readme.md`

Параллельно с этим нужно настраивать внешний вид компонента — для этого есть темы 🙂

## Настройка темы компонента

Все темы расположены в папке `src/themes`, стили конкретной темы - в `src/themes/concrete-theme`.

Структура папки с темой выглядит следующим образом:

<img src="/public/demo/theme_folder_structure.png" style="width: 244px">

В каждую из тем, за исключением _demo_, нужно добавить следующий файл: `src/themes/concrete-theme/components/ComponentName/index.sass`. Этот файл должен содержать все возможные CSS(SASS)-селекторы компонента, описанные в файле `src/components/ComponentName/readme.md`. При этом стили могут быть не у каждого селектора (например в одной теме кнопка может быть любого из возможных размеров, а в другой — только трёх)

Пример файла с темой:

<!-- prettier-ignore -->
```sass
@import ../../mixins/index

.Spin
  animation: spin_animation 0.8s linear infinite
  border: 2px solid transparent
  border-radius: 50%
  box-sizing: border-box

  // Size
  &_size_xs
    @include size(12px)
    border-width: 1px
  &_size_s
    @include size(16px)
    border-width: 1px
  &_size_m
    @include size(24px)
  &_size_l
    @include size(32px)
  &_size_xl
    @include size(40px)
  &_size_xxl
    @include size(48px)

  // Type
  &_type_default
    border-top-color: $color-default
  &_type_primary
    border-top-color: $color-primary
  &_type_secondary
    border-top-color: $color-secondary
  &_type_danger
    border-top-color: $color-danger
  &_type_success
    border-top-color: $color-success
  &_type_info
    border-top-color: $color-info
  &_type_warning
    border-top-color: $color-warning
  &_type_black
    border-top-color: $color-black
  &_type_white
    border-top-color: $color-white

// Animation
@keyframes spin_animation
  0%
    transform: rotate(0deg)
  100%
    transform: rotate(360deg)
```

**Важно!** Для того, чтобы файлы со стилями компонента подгрузились в общий билд, и разрабатываемый компонент засиял всеми нужными красками, необходимо импортировать стили компонента в файл `src/themes/concrete-theme/components/index.sass` следующим образом:

```sass
@import Switch/index
```
