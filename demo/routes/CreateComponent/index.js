import React from 'react';

// iBalance
import { FeatherIcon, LContainer, LContent, LHeader, LSidebar } from '../../../src';

// Containers
import Header from '../../containers/Header';
import Menu from '../../containers/Menu';

// Demo
import ReadmeView from '../../components/ReadmeView';

// Wrappers
import WContent from '../../wrappers/WContent';

import readme from './readme.md';

const CreateComponent = () => {
    return (
        <LContainer hasHeader sidebarSide="left">
            <LHeader>
                <Header />
            </LHeader>
            <LSidebar hasHeader toggleIcon={<FeatherIcon name="menu" />}>
                <Menu />
            </LSidebar>
            <LContent>
                <WContent>
                    <ReadmeView>{readme}</ReadmeView>
                </WContent>
            </LContent>
        </LContainer>
    );
};

export default CreateComponent;
