import React, { useContext, useState, useEffect, useMemo } from 'react';
import Prism from 'prismjs';

import { FeatherIcon, LContainer, LContent, LHeader, LSidebar } from '../../../src';

// Components
import CodeHighlight from '../../components/CodeHighlight';

// Containers
import Header from '../../containers/Header';
import Menu from '../../containers/Menu';

// Wrappers
import WContent from '../../wrappers/WContent';
import H from '../../../src/typography/H';
import { getPairs } from './helper';

// Theme
import ThemeContext from '../../theme-context';
import ReadmeView from '../../components/ReadmeView';
import readme from './readme.md';

import styles from './styles.sass';

const typesConstant = ['colors', 'breakpoints', 'font', 'layout', 'z-index'];
const titleConstant = {
    colors: 'Цвета',
    breakpoints: ' Breakpoints',
    font: 'Шрифт',
    layout: 'Layout',
    'z-index': 'Z-index',
};

const typesMixins = ['block', 'breakpoints', 'fonts', 'textarea', 'tooltip'];
const titleMixins = {
    block: 'Блоки',
    breakpoints: 'Breakpoints',
    fonts: 'Шрифт',
    textarea: 'Textarea',
    tooltip: 'Tooltip',
};

const ConstantsAndMixins = () => {
    const [state, setState] = useState({
        constants: {
            colors: {},
            font: {},
            'z-index': {},
            breakpoints: {},
            layout: {},
        },
        mixins: { block: {}, breakpoints: {}, fonts: {}, textarea: {}, tooltip: {} },
    });

    const theme = useContext(ThemeContext);

    const getConstants = () => {
        if (theme.name) {
            const requests = typesConstant.map(constant =>
                import(`../../../src/base/ThemeConstants/${constant}/${theme.name}`),
            );
            Promise.all(requests).then((responses = []) => {
                const newState = { ...state };
                responses.forEach((response, index) => {
                    if (response && response.default) {
                        newState.constants[typesConstant[index]] = response.default;
                    }
                });
                setState(newState);
                setTimeout(Prism.highlightAll);
            });
        }
    };

    const getMixins = () => {
        if (theme.name) {
            const requests = typesMixins.map(mixin =>
                import(`../../../src/base/ThemeMixins/${mixin}/${theme.name}`),
            );
            Promise.all(requests).then((responses = []) => {
                const newState = { ...state };
                responses.forEach((response, index) => {
                    if (response && response.default) {
                        newState.mixins[typesMixins[index]] = response.default;
                    }
                });
                setState(newState);
                setTimeout(Prism.highlightAll);
            });
        }
    };
    useEffect(() => {
        getConstants();
        getMixins();
    }, [theme.name]);

    const allColors = useMemo(() => {
        let colors = {};
        const keys = Object.keys(state.constants.colors);
        keys.forEach(key => {
            colors = { ...colors, ...state.constants.colors[key] };
        });
        return colors;
    }, [state.constants.colors]);

    return (
        <LContainer hasHeader sidebarSide="left">
            <LHeader>
                <Header />
            </LHeader>
            <LSidebar hasHeader toggleIcon={<FeatherIcon name="menu" />}>
                <Menu />
            </LSidebar>
            <LContent>
                <WContent>
                    <ReadmeView>{readme}</ReadmeView>

                    <H size="2" className={styles.h2}>
                        Константы
                    </H>
                    {typesConstant.map(type => (
                        <div key={type} style={{ marginBottom: '60px' }}>
                            <H size="3" className={styles.h3}>
                                {titleConstant[type]}
                            </H>
                            {getPairs(state.constants[type]).map(item => (
                                <div key={item.name} style={{ marginBottom: '30px' }}>
                                    <H size="4" className={styles.h4}>
                                        {item.name}
                                    </H>
                                    {item.data.map(d => {
                                        const isColors =
                                            type === 'colors' && d[1].indexOf('(') === -1;
                                        let backgroundColor = '';

                                        if (isColors) {
                                            backgroundColor =
                                                d[1][0] === '$' ? allColors[d[1].slice(1)] : d[1];
                                        }

                                        return (
                                            <div key={d[0]} className={styles.Prop}>
                                                <div className={styles.Prop__Key}>
                                                    {`$${d[0]}:`}
                                                </div>
                                                <div className={styles.Prop__Value}>
                                                    {isColors && (
                                                        <div
                                                            className={styles.Prop__ValueSquare}
                                                            style={{ backgroundColor }}
                                                        />
                                                    )}
                                                    {d[1]}
                                                </div>
                                            </div>
                                        );
                                    })}
                                </div>
                            ))}
                        </div>
                    ))}

                    <H size="2" className={styles.h2}>
                        Миксины
                    </H>
                    {typesMixins.map(type => (
                        <div key={type} style={{ marginBottom: '50px' }}>
                            <H size="3" className={styles.h3}>
                                {titleMixins[type]}
                            </H>
                            {getPairs(state.mixins[type]).map(item => (
                                <div key={item.name} style={{ marginBottom: '30px' }}>
                                    <H size="4" className={styles.h4}>
                                        {item.name}
                                    </H>
                                    <div className={styles.CodeBlock}>
                                        <CodeHighlight code={item.data} language="sass" />
                                    </div>
                                </div>
                            ))}
                        </div>
                    ))}
                </WContent>
            </LContent>
        </LContainer>
    );
};
export default ConstantsAndMixins;
