export const getPairs = obj => {
    const res = [];
    Object.keys(obj)
        .filter(item => Object.keys(obj[item]).length > 0)
        .forEach(item => {
            res.push({
                name: item,
                data:
                    typeof obj[item] === 'object'
                        ? Object.entries(obj[item])
                        : `@mixin ${item}\n${obj[item]}`,
            });
        });

    return res;
};
