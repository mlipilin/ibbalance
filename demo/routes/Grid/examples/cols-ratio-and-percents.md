# Фиксированный размер ячейки в процентах

Массив `cols` может также содержать строки с размером в процентах

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Row, Col } from 'ibalance';

ReactDOM.render(
    <Row cols={['20%', 2, 1]}>
        <Col>20%</Col>
        <Col>(100% - 20%) * 2 / 3</Col>
        <Col>(100% - 20%) * 1 / 3</Col>
    </Row>,
    document.getElementById('root'),
);
```
