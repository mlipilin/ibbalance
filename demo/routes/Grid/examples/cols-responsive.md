# Адаптивный размер ячеек

Свойство `cols` также может принять объект `responsiveObject`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Row, Col, WMobile, WTab, WDesktop, WLargeDesktop } from 'ibalance';

ReactDOM.render(
    <Row
        cols={{
            mobile: [1],
            tab: [1, 1],
            desktop: [1, 1, 1],
            largeDesktop: [1, 1, 1, 1, 1, 1],
        }}
    >
        <Col>
            <WMobile>100%</WMobile>
            <WTab>1/2</WTab>
            <WDesktop>1/3</WDesktop>
            <WLargeDesktop>1/6</WLargeDesktop>
        </Col>
        <Col>
            <WMobile>100%</WMobile>
            <WTab>1/2</WTab>
            <WDesktop>1/3</WDesktop>
            <WLargeDesktop>1/6</WLargeDesktop>
        </Col>
        <Col>
            <WMobile>100%</WMobile>
            <WTab>1/2</WTab>
            <WDesktop>1/3</WDesktop>
            <WLargeDesktop>1/6</WLargeDesktop>
        </Col>
        <Col>
            <WMobile>100%</WMobile>
            <WTab>1/2</WTab>
            <WDesktop>1/3</WDesktop>
            <WLargeDesktop>1/6</WLargeDesktop>
        </Col>
        <Col>
            <WMobile>100%</WMobile>
            <WTab>1/2</WTab>
            <WDesktop>1/3</WDesktop>
            <WLargeDesktop>1/6</WLargeDesktop>
        </Col>
        <Col>
            <WMobile>100%</WMobile>
            <WTab>1/2</WTab>
            <WDesktop>1/3</WDesktop>
            <WLargeDesktop>1/6</WLargeDesktop>
        </Col>
    </Row>,
    document.getElementById('root'),
);
```
