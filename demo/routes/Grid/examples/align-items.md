# Выравнивание ячеек в строке

Свойство `alignItems` принимает 3 значения: `start`, `center` и `end`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Row, Col } from 'ibalance';

ReactDOM.render(
    <Row alignItems="center">
        <Col>
            <div style={{ height: '100px' }}>100px</div>
        </Col>
        <Col>
            <div style={{ height: '50px' }}>50px</div>
        </Col>
        <Col>
            <div style={{ height: '150px' }}>150px</div>
        </Col>
    </Row>,
    document.getElementById('root'),
);
```
