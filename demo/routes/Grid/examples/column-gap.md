# Расстояние между ячейками

В свойство`gap` можно передать строку со величиной расстояния

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Row, Col } from 'ibalance';

ReactDOM.render(
    <Row gap="20px">
        <Col>1/3</Col>
        <Col>1/3</Col>
        <Col>1/3</Col>
    </Row>,
    document.getElementById('root'),
);
```
