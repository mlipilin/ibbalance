# Расстояние между ячейками (в обоих направлениях)

Также в свойство `gap` можно передать массив с двумя величинами: `["vertical", "horizontal"]`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Row, Col } from 'ibalance';

ReactDOM.render(
    <Row cols={[1, 1, 1]} gap={['20px', '15px']}>
        <Col>1/3</Col>
        <Col>1/3</Col>
        <Col>1/3</Col>
        <Col>1/3</Col>
        <Col>1/3</Col>
        <Col>1/3</Col>
    </Row>,
    document.getElementById('root'),
);
```
