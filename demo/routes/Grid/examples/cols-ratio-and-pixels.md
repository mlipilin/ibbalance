# Фиксированный размер ячейки в пикселях

Массив `cols` кроме чисел может содержать строки с размером в пикселях

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Row, Col } from 'ibalance';

ReactDOM.render(
    <Row cols={['200px', 2, 1]}>
        <Col>200px</Col>
        <Col>(100% - 200px) * 2 / 3</Col>
        <Col>(100% - 200px) * 1 / 3</Col>
    </Row>,
    document.getElementById('root'),
);
```
