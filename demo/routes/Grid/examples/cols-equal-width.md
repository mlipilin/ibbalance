# Ячейки одинакового размера

Простейшее использование сетки

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Row, Col } from 'ibalance';

ReactDOM.render(
    <Row>
        <Col>1/3</Col>
        <Col>1/3</Col>
        <Col>1/3</Col>
    </Row>,
    document.getElementById('root'),
);
```
