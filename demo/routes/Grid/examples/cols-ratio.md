# Соотношение ячеек (пропорции)

В свойство `cols` можно передать массив чисел

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Row, Col } from 'ibalance';

ReactDOM.render(
    <Row cols={[1, 2, 1]}>
        <Col>1/4</Col>
        <Col>2/4</Col>
        <Col>1/4</Col>
    </Row>,
    document.getElementById('root'),
);
```
