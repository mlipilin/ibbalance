# Адаптивное расстояние между ячейками

Ну и само собой свойство `gap` принимает `responsiveObject`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Row, Col } from 'ibalance';

ReactDOM.render(
    <Row
        cols={[1, 1, 1]}
        gap={{
            mobile: '10px',
            tab: '15px',
            desktop: ['25px', '15px'],
            largeDesktop: ['30px', '20px'],
        }}
    >
        <Col>1/3</Col>
        <Col>1/3</Col>
        <Col>1/3</Col>
        <Col>1/3</Col>
        <Col>1/3</Col>
        <Col>1/3</Col>
    </Row>
,
    document.getElementById('root'),
);
```
