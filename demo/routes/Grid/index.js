import React from 'react';

// iBalance
import PropTypes from 'prop-types';
import { FeatherIcon, LContainer, LContent, LHeader, LSidebar } from '../../../src';

// Containers
import Header from '../../containers/Header';
import Menu from '../../containers/Menu';

// Demo
import ReadmeView from '../../components/ReadmeView';
import DemoComponent from '../../components/DemoComponent';
import Example from '../../components/Example';

// Wrappers
import WContent from '../../wrappers/WContent';
import { Col, Row } from '../../../src/wrappers/WGrid';
import { WMobile, WTab, WDesktop, WLargeDesktop } from '../../../src/wrappers/WResponsive';

import readme from './readme.md';

import styles from './styles.sass';

import alignItems from './examples/align-items.md';
import alignItemsResponsive from './examples/align-items-responsive.md';
import colsEqualWidth from './examples/cols-equal-width.md';
import colsRatio from './examples/cols-ratio.md';
import colsRatioAndPercents from './examples/cols-ratio-and-percents.md';
import colsRatioAndPixels from './examples/cols-ratio-and-pixels.md';
import colsResponsive from './examples/cols-responsive.md';
import columnGap from './examples/column-gap.md';
import columnGapBothDirections from './examples/column-gap-both-directions.md';
import columnGapResponsive from './examples/column-gap-responsive.md';

const Cell = ({ children, ...otherProps }) => (
    <div className={styles.Cell} {...otherProps}>
        {children}
    </div>
);

const Grid = () => {
    return (
        <LContainer hasHeader sidebarSide="left">
            <LHeader>
                <Header />
            </LHeader>
            <LSidebar hasHeader toggleIcon={<FeatherIcon name="menu" />}>
                <Menu />
            </LSidebar>
            <LContent>
                <WContent>
                    <DemoComponent>
                        <ReadmeView>{readme}</ReadmeView>

                        <div id="example">
                            <Example readme={colsEqualWidth}>
                                <Row>
                                    <Col>
                                        <Cell>1/3</Cell>
                                    </Col>
                                    <Col>
                                        <Cell>1/3</Cell>
                                    </Col>
                                    <Col>
                                        <Cell>1/3</Cell>
                                    </Col>
                                </Row>
                            </Example>

                            <Example readme={colsRatio}>
                                <Row cols={[1, 2, 1]}>
                                    <Col>
                                        <Cell>1/4</Cell>
                                    </Col>
                                    <Col>
                                        <Cell>2/4</Cell>
                                    </Col>
                                    <Col>
                                        <Cell>1/4</Cell>
                                    </Col>
                                </Row>
                            </Example>

                            <Example readme={colsRatioAndPixels}>
                                <Row cols={['200px', 2, 1]}>
                                    <Col>
                                        <Cell>200px</Cell>
                                    </Col>
                                    <Col>
                                        <Cell>(100% - 200px) * 2 / 3</Cell>
                                    </Col>
                                    <Col>
                                        <Cell>(100% - 200px) * 1 / 3</Cell>
                                    </Col>
                                </Row>
                            </Example>

                            <Example readme={colsRatioAndPercents}>
                                <Row cols={['20%', 2, 1]}>
                                    <Col>
                                        <Cell>20%</Cell>
                                    </Col>
                                    <Col>
                                        <Cell>(100% - 20%) * 2 / 3</Cell>
                                    </Col>
                                    <Col>
                                        <Cell>(100% - 20%) * 1 / 3</Cell>
                                    </Col>
                                </Row>
                            </Example>

                            <Example readme={colsResponsive}>
                                <Row
                                    cols={{
                                        mobile: [1],
                                        tab: [1, 1],
                                        desktop: [1, 1, 1],
                                        largeDesktop: [1, 1, 1, 1, 1, 1],
                                    }}
                                >
                                    <Col>
                                        <Cell>
                                            <WMobile>100%</WMobile>
                                            <WTab>1/2</WTab>
                                            <WDesktop>1/3</WDesktop>
                                            <WLargeDesktop>1/6</WLargeDesktop>
                                        </Cell>
                                    </Col>
                                    <Col>
                                        <Cell>
                                            <WMobile>100%</WMobile>
                                            <WTab>1/2</WTab>
                                            <WDesktop>1/3</WDesktop>
                                            <WLargeDesktop>1/6</WLargeDesktop>
                                        </Cell>
                                    </Col>
                                    <Col>
                                        <Cell>
                                            <WMobile>100%</WMobile>
                                            <WTab>1/2</WTab>
                                            <WDesktop>1/3</WDesktop>
                                            <WLargeDesktop>1/6</WLargeDesktop>
                                        </Cell>
                                    </Col>
                                    <Col>
                                        <Cell>
                                            <WMobile>100%</WMobile>
                                            <WTab>1/2</WTab>
                                            <WDesktop>1/3</WDesktop>
                                            <WLargeDesktop>1/6</WLargeDesktop>
                                        </Cell>
                                    </Col>
                                    <Col>
                                        <Cell>
                                            <WMobile>100%</WMobile>
                                            <WTab>1/2</WTab>
                                            <WDesktop>1/3</WDesktop>
                                            <WLargeDesktop>1/6</WLargeDesktop>
                                        </Cell>
                                    </Col>
                                    <Col>
                                        <Cell>
                                            <WMobile>100%</WMobile>
                                            <WTab>1/2</WTab>
                                            <WDesktop>1/3</WDesktop>
                                            <WLargeDesktop>1/6</WLargeDesktop>
                                        </Cell>
                                    </Col>
                                </Row>
                            </Example>

                            <Example readme={alignItems}>
                                <Row alignItems="center">
                                    <Col>
                                        <Cell style={{ height: '100px' }}>100px</Cell>
                                    </Col>
                                    <Col>
                                        <Cell style={{ height: '50px' }}>50px</Cell>
                                    </Col>
                                    <Col>
                                        <Cell style={{ height: '150px' }}>150px</Cell>
                                    </Col>
                                </Row>
                            </Example>

                            <Example readme={alignItemsResponsive}>
                                <Row
                                    alignItems={{
                                        tab: 'start',
                                        desktop: 'center',
                                        largeDesktop: 'end',
                                    }}
                                >
                                    <Col>
                                        <Cell style={{ height: '100px' }}>100px</Cell>
                                    </Col>
                                    <Col>
                                        <Cell style={{ height: '50px' }}>50px</Cell>
                                    </Col>
                                    <Col>
                                        <Cell style={{ height: '150px' }}>150px</Cell>
                                    </Col>
                                </Row>
                            </Example>

                            <Example readme={columnGap}>
                                <Row gap="20px">
                                    <Col>
                                        <Cell>1/3</Cell>
                                    </Col>
                                    <Col>
                                        <Cell>1/3</Cell>
                                    </Col>
                                    <Col>
                                        <Cell>1/3</Cell>
                                    </Col>
                                </Row>
                            </Example>

                            <Example readme={columnGapBothDirections}>
                                <Row cols={[1, 1, 1]} gap={['20px', '15px']}>
                                    <Col>
                                        <Cell>1/3</Cell>
                                    </Col>
                                    <Col>
                                        <Cell>1/3</Cell>
                                    </Col>
                                    <Col>
                                        <Cell>1/3</Cell>
                                    </Col>
                                    <Col>
                                        <Cell>1/3</Cell>
                                    </Col>
                                    <Col>
                                        <Cell>1/3</Cell>
                                    </Col>
                                    <Col>
                                        <Cell>1/3</Cell>
                                    </Col>
                                </Row>
                            </Example>

                            <Example readme={columnGapResponsive}>
                                <Row
                                    cols={[1, 1, 1]}
                                    gap={{
                                        mobile: '10px',
                                        tab: '15px',
                                        desktop: ['25px', '15px'],
                                        largeDesktop: ['30px', '20px'],
                                    }}
                                >
                                    <Col>
                                        <Cell>1/3</Cell>
                                    </Col>
                                    <Col>
                                        <Cell>1/3</Cell>
                                    </Col>
                                    <Col>
                                        <Cell>1/3</Cell>
                                    </Col>
                                    <Col>
                                        <Cell>1/3</Cell>
                                    </Col>
                                    <Col>
                                        <Cell>1/3</Cell>
                                    </Col>
                                    <Col>
                                        <Cell>1/3</Cell>
                                    </Col>
                                </Row>
                            </Example>
                        </div>
                    </DemoComponent>
                </WContent>
            </LContent>
        </LContainer>
    );
};

export default Grid;

Cell.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

Cell.defaultProps = {
    children: null,
};
