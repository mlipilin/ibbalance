import React from 'react';

// iBalance
import { FeatherIcon, LContainer, LContent, LHeader, LSidebar } from '../../../src';

import Image from '../../../src/components/Image';
import { Col, Row } from '../../../src/wrappers/WGrid';
import DemoComponent from '../../components/DemoComponent';
import Example from '../../components/Example';

// Containers
import Header from '../../containers/Header';
import Menu from '../../containers/Menu';

// Demo
import ReadmeView from '../../components/ReadmeView';

// Examples
import contentFooter from './examples/content_footer.md';
import headerContent from './examples/header_content.md';
import headerContentFooter from './examples/header_content_footer.md';
import headerSidebarLeftContentFooterV1 from './examples/header_sidebarLeft_content_footer_v1.md';
import headerSidebarLeftContentFooterV2 from './examples/header_sidebarLeft_content_footer_v2.md';
import headerSidebarLeftContentV1 from './examples/header_content_sidebarLeft_v1.md';
import headerSidebarLeftContentV2 from './examples/header_content_sidebarLeft_v2.md';
import headerSidebarLeftSidebarRightContentFooterV1 from './examples/header_sidebarLeft_sidebarRight_content_footer_v1.md';
import headerSidebarLeftSidebarRightContentFooterV2 from './examples/header_sidebarLeft_sidebarRight_content_footer_v2.md';
import headerSidebarLeftSidebarRightContentV1 from './examples/header_content_sidebarLeft_sidebarRight_v1.md';
import headerSidebarLeftSidebarRightContentV2 from './examples/header_content_sidebarLeft_sidebarRight_v2.md';
import headerSidebarRightContentFooterV1 from './examples/header_sidebarRight_content_footer_v1.md';
import headerSidebarRightContentFooterV2 from './examples/header_sidebarRight_content_footer_v2.md';
import headerSidebarRightContentV1 from './examples/header_content_sidebarRight_v1.md';
import headerSidebarRightContentV2 from './examples/header_content_sidebarRight_v2.md';
import sidebarLeftContent from './examples/sidebarLeft_content.md';
import sidebarLeftContentSidebarRight from './examples/sidebarLeft_content_sidebarRight.md';
import sidebarRightContent from './examples/sidebarRight_content.md';

// Wrappers
import WContent from '../../wrappers/WContent';

// Images
import contentFooterImg from './images/Content_Footer.svg';
import headerContentFooterImg from './images/Header_Content_Footer.svg';
import headerContentImg from './images/Header_Content.svg';
import headerSidebarLeftContentFooterV1Img from './images/Header_SidebarLeft_Content_Footer_v1.svg';
import headerSidebarLeftContentFooterV2Img from './images/Header_SidebarLeft_Content_Footer_v2.svg';
import headerSidebarLeftContentSidebarRightV1Img from './images/Header_SidebarLeft_SidebarRight_Content_v1.svg';
import headerSidebarLeftContentSidebarRightV2Img from './images/Header_SidebarLeft_SidebarRight_Content_v2.svg';
import headerSidebarLeftContentV1Img from './images/header_sidebarLeft_content_v1.svg';
import headerSidebarLeftContentV2Img from './images/header_sidebarLeft_content_v2.svg';
import headerSidebarLeftSidebarRightContentFooterV1Img from './images/Header_SidebarLeft_SidebarRight_Content_Footer_v1.svg';
import headerSidebarLeftSidebarRightContentFooterV2Img from './images/Header_SidebarLeft_SidebarRight_Content_Footer_v2.svg';
import headerSidebarRightContentFooterV1Img from './images/Header_SidebarRight_Content_Footer_v1.svg';
import headerSidebarRightContentFooterV2Img from './images/Header_SidebarRight_Content_Footer_v2.svg';
import headerSidebarRightContentV1Img from './images/header_sidebarRight_content_v1.svg';
import headerSidebarRightContentV2Img from './images/header_sidebarRight_content_v2.svg';
import sidebarLeftContentImg from './images/SidebarLeft_Content.svg';
import sidebarLeftContentSidebarRightImg from './images/SidebarLeft_Content_SidebarRight.svg';
import sidebarRightContentImg from './images/SidebarRight_Content.svg';

import readme from './readme.md';

const Layout = () => {
    return (
        <LContainer hasHeader sidebarSide="left">
            <LHeader>
                <Header />
            </LHeader>
            <LSidebar hasHeader toggleIcon={<FeatherIcon name="menu" />}>
                <Menu />
            </LSidebar>
            <LContent>
                <WContent>
                    <DemoComponent>
                        <ReadmeView>{readme}</ReadmeView>

                        <div id="example">
                            <Row
                                cols={{
                                    mobile: [1],
                                    tab: [1, 1],
                                    desktop: [1],
                                    largeDesktop: [1, 1],
                                }}
                                gap="20px"
                            >
                                <Col style={{ overflow: 'hidden' }}>
                                    <Example readme={headerContent}>
                                        <Image size="contain" src={headerContentImg} />
                                    </Example>

                                    <Example readme={headerContentFooter}>
                                        <Image
                                            size="contain"
                                            src={headerContentFooterImg}
                                            alt="header_content_footer"
                                        />
                                    </Example>

                                    <Example readme={sidebarRightContent}>
                                        <Image
                                            size="contain"
                                            src={sidebarRightContentImg}
                                            alt="sidebarRight_content"
                                        />
                                    </Example>

                                    <Example readme={headerSidebarLeftContentV1}>
                                        <Image
                                            size="contain"
                                            src={headerSidebarLeftContentV1Img}
                                            alt="header_sidebarLeft_content_v1"
                                        />
                                    </Example>

                                    <Example readme={headerSidebarLeftSidebarRightContentV1}>
                                        <Image
                                            size="contain"
                                            src={headerSidebarLeftContentSidebarRightV1Img}
                                            alt="header_sidebarLeft_content_sidebarRight__v1"
                                        />
                                    </Example>

                                    <Example readme={headerSidebarRightContentV1}>
                                        <Image
                                            size="contain"
                                            src={headerSidebarRightContentV1Img}
                                            alt="header_sidebarRight_content_v1"
                                        />
                                    </Example>

                                    <Example readme={headerSidebarLeftContentFooterV1}>
                                        <Image
                                            size="contain"
                                            src={headerSidebarLeftContentFooterV1Img}
                                            alt="header_sidebarLeft_content_footer_v1"
                                        />
                                    </Example>

                                    <Example readme={headerSidebarRightContentFooterV1}>
                                        <Image
                                            size="contain"
                                            src={headerSidebarRightContentFooterV1Img}
                                            alt="header_sidebarRight_content_footer_v1"
                                        />
                                    </Example>

                                    <Example readme={headerSidebarLeftSidebarRightContentFooterV1}>
                                        <Image
                                            size="contain"
                                            src={headerSidebarLeftSidebarRightContentFooterV1Img}
                                            alt="header_sidebarLeft_sidebarRight_content_footer_v1"
                                        />
                                    </Example>
                                </Col>
                                <Col style={{ overflow: 'hidden' }}>
                                    <Example readme={contentFooter}>
                                        <Image
                                            size="contain"
                                            src={contentFooterImg}
                                            alt="content_footer"
                                        />
                                    </Example>

                                    <Example readme={sidebarLeftContent}>
                                        <Image
                                            size="contain"
                                            src={sidebarLeftContentImg}
                                            alt="sidebarLeft_content"
                                        />
                                    </Example>

                                    <Example readme={sidebarLeftContentSidebarRight}>
                                        <Image
                                            size="contain"
                                            src={sidebarLeftContentSidebarRightImg}
                                            alt="sidebarLeft_content_sideBarRight"
                                        />
                                    </Example>

                                    <Example readme={headerSidebarLeftContentV2}>
                                        <Image
                                            size="contain"
                                            src={headerSidebarLeftContentV2Img}
                                            alt="header_sidebarLeft_content_v2"
                                        />
                                    </Example>

                                    <Example readme={headerSidebarLeftSidebarRightContentV2}>
                                        <Image
                                            size="contain"
                                            src={headerSidebarLeftContentSidebarRightV2Img}
                                            alt="header_sidebarLeft_content_sidebarRight__v2"
                                        />
                                    </Example>

                                    <Example readme={headerSidebarRightContentV2}>
                                        <Image
                                            size="contain"
                                            src={headerSidebarRightContentV2Img}
                                            alt="header_sidebarRight_content_v2"
                                        />
                                    </Example>

                                    <Example readme={headerSidebarLeftContentFooterV2}>
                                        <Image
                                            size="contain"
                                            src={headerSidebarLeftContentFooterV2Img}
                                            alt="header_sidebarLeft_content_footer_v2"
                                        />
                                    </Example>

                                    <Example readme={headerSidebarRightContentFooterV2}>
                                        <Image
                                            size="contain"
                                            src={headerSidebarRightContentFooterV2Img}
                                            alt="header_sidebarRight_content_footer_v2"
                                        />
                                    </Example>

                                    <Example readme={headerSidebarLeftSidebarRightContentFooterV2}>
                                        <Image
                                            size="contain"
                                            src={headerSidebarLeftSidebarRightContentFooterV2Img}
                                            alt="header_sidebarLeft_sidebarRight_content_footer_v2"
                                        />
                                    </Example>
                                </Col>
                            </Row>
                        </div>
                    </DemoComponent>
                </WContent>
            </LContent>
        </LContainer>
    );
};

export default Layout;
