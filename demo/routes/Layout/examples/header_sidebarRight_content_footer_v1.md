# Header + Sidebar Right + Content + Footer (V1)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { LContainer, LHeader, LContent, LSidebar, LFooter } from 'ibalance';

ReactDOM.render(
    <LContainer hasHeader sidebarSide="right">
        <LHeader sidebarSide="right">Header</LHeader>
        <LSidebar side="right">Sidebar Right</LSidebar>
        <LContent>
            <div>Content</div>
        </LContent>
        <LFooter>Footer</LFooter>
    </LContainer>,
    document.getElementById('root'),
);
```
