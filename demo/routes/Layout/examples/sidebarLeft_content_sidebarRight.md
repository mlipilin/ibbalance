# Sidebar Left + Content + Sidebar Right

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { LContainer, LContent, LSidebar } from 'ibalance';

ReactDOM.render(
    <LContainer sidebarSide="both">
        <LSidebar side="left">Sidebar</LSidebar>
        <LSidebar side="right">Sidebar</LSidebar>
        <LContent>
            <div>Content</div>
        </LContent>
    </LContainer>,
    document.getElementById('root'),
);
```
