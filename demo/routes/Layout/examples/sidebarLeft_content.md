# Sidebar Left + Content

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { LContainer, LContent, LSidebar } from 'ibalance';

ReactDOM.render(
    <LContainer sidebarSide="left">
        <LSidebar>Sidebar</LSidebar>
        <LContent>
            <div>Content</div>
        </LContent>
    </LContainer>,
    document.getElementById('root'),
);
```
