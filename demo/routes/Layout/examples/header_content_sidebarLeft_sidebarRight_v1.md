# Header + Sidebar Left + Sidebar Right + Content (V1)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { LContainer, LHeader, LContent, LSidebar } from 'ibalance';

ReactDOM.render(
    <LContainer hasHeader sidebarSide="both">
        <LHeader sidebarSide="both">Header</LHeader>
        <LSidebar side="left">Sidebar</LSidebar>
        <LSidebar side="right">Sidebar</LSidebar>
        <LContent>
            <div>Content</div>
        </LContent>
    </LContainer>,
    document.getElementById('root'),
);
```
