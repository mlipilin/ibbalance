# Header + Sidebar Left + Sidebar Right + Content + Footer (V2)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { LContainer, LHeader, LContent, LSidebar, LFooter } from 'ibalance';

ReactDOM.render(
    <LContainer hasHeader sidebarSide="both">
        <LHeader>Header</LHeader>
        <LSidebar hasHeader side="left">
            Sidebar Left
        </LSidebar>
        <LSidebar hasHeader side="right">
            Sidebar Right
        </LSidebar>
        <LContent>
            <div>Content</div>
        </LContent>
        <LFooter>Footer</LFooter>
    </LContainer>,
    document.getElementById('root'),
);
```
