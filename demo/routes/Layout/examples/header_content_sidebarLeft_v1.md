# Header + Sidebar Left + Content (V1)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { LContainer, LHeader, LContent, LSidebar } from 'ibalance';

ReactDOM.render(
    <LContainer hasHeader sidebarSide="left">
        <LHeader sidebarSide="left">Header</LHeader>
        <LSidebar>Sidebar</LSidebar>
        <LContent>
            <div>Content</div>
        </LContent>
    </LContainer>,
    document.getElementById('root'),
);
```
