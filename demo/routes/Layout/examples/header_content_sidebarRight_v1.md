# Header + Sidebar Right + Content (V1)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { LContainer, LHeader, LContent, LSidebar } from 'ibalance';

ReactDOM.render(
    <LContainer hasHeader sidebarSide="right">
        <LHeader sidebarSide="right">Header</LHeader>
        <LSidebar side="right">Sidebar Right</LSidebar>
        <LContent>
            <div>Content</div>
        </LContent>
    </LContainer>,
    document.getElementById('root'),
);
```
