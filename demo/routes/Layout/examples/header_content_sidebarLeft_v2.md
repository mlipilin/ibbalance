# Header + Sidebar Left + Content (V2)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { LContainer, LHeader, LContent, LSidebar } from 'ibalance';

ReactDOM.render(
    <LContainer hasHeader sidebarSide="left">
        <LHeader>Header</LHeader>
        <LSidebar hasHeader>Sidebar</LSidebar>
        <LContent>
            <div>Content</div>
        </LContent>
    </LContainer>,
    document.getElementById('root'),
);
```
