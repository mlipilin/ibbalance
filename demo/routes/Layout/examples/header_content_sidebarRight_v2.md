# Header + Sidebar Right + Content (V2)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { LContainer, LHeader, LContent, LSidebar } from 'ibalance';

ReactDOM.render(
    <LContainer hasHeader sidebarSide="right">
        <LHeader>Header FullWidth</LHeader>
        <LSidebar hasHeader side="right">
            Sidebar Right
        </LSidebar>
        <LContent>
            <div>Content</div>
        </LContent>
    </LContainer>,
    document.getElementById('root'),
);
```
