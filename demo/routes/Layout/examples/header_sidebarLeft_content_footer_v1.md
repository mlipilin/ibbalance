# Header + Sidebar Left + Content + Footer (V1)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { LContainer, LHeader, LContent, LSidebar, LFooter} from 'ibalance';

ReactDOM.render(
    <LContainer hasHeader sidebarSide="left">
        <LHeader sidebarSide="left">Header</LHeader>
        <LSidebar>Sidebar Left</LSidebar>
        <LContent>
            <div>Content</div>
        </LContent>
        <LFooter>Footer</LFooter>
    </LContainer>,
    document.getElementById('root'),
);
```
