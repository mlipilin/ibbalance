# Header+Content

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { LContainer, LHeader, LContent } from 'ibalance';

ReactDOM.render(
    <LContainer hasHeader>
        <LHeader>Header</LHeader>
        <LContent>
            <div>Content</div>
        </LContent>
    </LContainer>,
    document.getElementById('root'),
);
```
