# Header + Sidebar Left + Sidebar Left + Content (V2)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { LContainer, LHeader, LContent, LSidebar } from 'ibalance';

ReactDOM.render(
    <LContainer hasHeader sidebarSide="both">
        <LHeader>Header</LHeader>
        <LSidebar hasHeader side="left">
            Sidebar
        </LSidebar>
        <LSidebar hasHeader side="right">
            Sidebar
        </LSidebar>
        <LContent>
            <div>Content</div>
        </LContent>
    </LContainer>,
    document.getElementById('root'),
);
```
