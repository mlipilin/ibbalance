# Header+Content+Footer

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { LContainer, LHeader, LContent, LFooter } from 'ibalance';

ReactDOM.render(
    <LContainer hasHeader>
        <LHeader>Header</LHeader>
        <LContent>
            <div>Content</div>
        </LContent>
        <LFooter>Footer</LFooter>
    </LContainer>,
    document.getElementById('root'),
);
```
