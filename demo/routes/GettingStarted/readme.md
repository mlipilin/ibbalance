# Начало работы

*iB*alance состоит из набора переиспользуемых **React-компонентов**, которые можно подключить в проект. Также в распоряжении разработчика есть несколько заготовленных **тем**, стилизующих компоненты.

## Быстрый старт

Для того, чтобы быстро развернуть новый проект, в котором будет предустановлена *iB*alance, можно воспользоваться утилитой [bpl](https://gitlab.infra.b-pl.pro/mortgage/bpl):

```bash
> npx git+ssh://git@gitlab.infra.b-pl.pro:mortgage/bpl.git create myCoolProjectName
> cd ./myCoolProjectName
> npm run install-modules
> npm start
```

Готово! Созданный проект доступен по адресу http://0.0.0.0:8001

Если же нужно интегрировать *iB*alance в текущий проект, то следуйте дальнейшим инструкциям ⇩

## Установка iBalance

Для того, чтобы установить *iB*alance в текущий проект, выполните команду:

```bash
> GIT_SSL_NO_VERIFY=true npm install git+https://gitlab.infra.b-pl.pro/lib/ibalance.git
```

## Конфигурация Webpack

Так как *iB*alance содержит **только исходные файлы** (без "билдов"), то для использования компонентов и их стилизации нужно соответствующим образом сконфигурировать Webpack. Настройку Webpack'а можно разбить на 4 шага:

### 1. Настроить работу с декораторами

*iB*alance активно использует js-декораторы, поэтому необходимо установить следующий лоадер:

```bash
> npm i -D @babel/plugin-proposal-decorators
```

И рассказать о нем **"Бабелю"**, добавив следующий код в `babel.config.js`:

<!-- prettier-ignore -->
```javascript
module.exports = function() {
  const plugins = [
    // ...
    ['@babel/plugin-proposal-decorators', { legacy: true }],
    // ...
  ];

  return {
    //...
    plugins,
  };
};
```

### 2. Настроить работу со стилями

Про стили в *iB*alance можно сказать 2 вещи:

1. они написаны на **SASS**
2. подключаются в компоненты как **CSS-модули**

Поэтому для работы со стилями нужно установить соответствующие **"Webpack-лоадеры"**:

```bash
> npm install style-loader css-loader node-sass sass-loader
```

И настроить их использование в `webpack.config.js`:

<!-- prettier-ignore -->
```javascript
module.exports = {
  module: {
    rules: [
      {
        test: /\.sass$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            // Для CSS-модулей
            options: {
              modules: true,
              importLoaders: 1,
              localIdentName: '[name]_[local]_[hash:base64]',
              sourceMap: true,
            },
          },
          'sass-loader',
        ],
      },
    ],
  },
};
```

### 3. Добавить в исключение исходные файлы iBalance, лежащие в папке node_modules

Обычно по умолчанию папку `node_modules` в конфигурационном файле помечают флагом `exclude`, чтобы не включать в сборку лишние файлы:

<!-- prettier-ignore -->
```javascript
module.exports = {
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules\/.*/,
      },
    ],
  },
};
```

Для папки `node_modules/ibalance` нужно сделать исключение, так как там лежат все необходимые исходные файлы, которые должны в сборку войти:

<!-- prettier-ignore -->
```javascript
module.exports = {
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules\/(?!(\ibalance)\/).*/,
      },
    ],
  },
};
```

### 4. Настроить alias для темы

Чтобы было удобнее импортировать `.sass` файлы одной из тем *iB*alance, рекомендуется настроить `alias` для выбранной темы:

<!-- prettier-ignore -->
```javascript
module.exports = {
  resolve: {
    alias: {
      ibalance: path.resolve(__dirname, '../node_modules/ibalance/src/themes/выбранная_тема'),
    },
  },
};
```

Итого, после всех вышеперечисленных манипуляций файл `webpack.config.js` должен выглядеть следующим образом:

<!-- prettier-ignore -->
```javascript
module.exports = {
  // ...
  module: {
    rules: [
      // ...
      {
        test: /\.jsx?$/,
        exclude: /node_modules\/(?!(\ibalance)\/).*/,
        // ...
      },
      // ...
      {
        test: /\.sass$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 1,
              localIdentName: '[name]_[local]_[hash:base64]',
              sourceMap: true,
            },
          },
          'sass-loader',
        ],
      },
      // ...
    ],
  },
  // ...
  resolve: {
    // ...
    alias: {
      theme: path.resolve(__dirname, '../node_modules/ibalance/src/themes/название_темы'),
    },
  },
  // ...
};
```

## Подключение компонента в проект

После того, как сконфигурирован Webpack, можно подключать компоненты в проект.

<!-- prettier-ignore -->
```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Button } from 'ibalance';

ReactDOM.render(
  <>
    <Button type="default" size="l">
      Первая кнопка
    </Button>{' '}
    <Button type="primary" size="m">
      Вторая кнопка
    </Button>{' '}
    <Button type="secondary" size="s">
      Третья кнопка
    </Button>
  </>,
  document.getElementById('root'),
);
```

## Подключение темы

Компоненты подключены, но вместо красивых кнопок на странице находятся кнопки со стандартными стилями? 😁

<p>
  <img src="/public/demo/buttons_without_theme.png" alt="Кнопки без стилей" style="width: 300px" >
</p>

Все правильно, осталось подключить тему 👌

<!-- prettier-ignore -->
```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Button, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/alfa/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Button type="default" size="l">
      Первая кнопка
    </Button>{' '}
    <Button type="primary" size="m">
      Вторая кнопка
    </Button>{' '}
    <Button type="secondary" size="s">
      Третья кнопка
    </Button>
  </ThemeProvider>,
  document.getElementById('root'),
);
```

Voilà!

<p>
  <img src="/public/demo/buttons_with_theme.png" alt="Кнопки с темой 'Альфа'" style="width: 400px">
</p>
