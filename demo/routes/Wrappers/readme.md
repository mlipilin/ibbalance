# Обертки

В данный момент *iB*alance включает в себя три компонента-обертки:

-   WFormRow
-   WGrid
-   WResponsive

## WFormRow

Данный компонент используется в качестве контейнера для любого контрола на форме (Input, SelectBox и т.д.), задавая при этом нижний отступ.

<!-- prettier-ignore -->
```jsx
<form>
  <WFormRow>
    <Input label="Логин" />
  </WFormRow>
  <WFormRow>
    <Input label="Пароль" type="password" />
  </WFormRow>
  <WFormRow>
    <Button block htmlType="submit" type="primary">Войти</Button>
  </WFormRow>
</form>
```

Результат:

<img src="/public/demo/wrappers_w_form_row.png" alt="WFormRow" style="width: 325px;">

## WGrid

Наша реализация простой сетки, описание можно найти [здесь](/grid)

## WResponsive

Содержит в себе обертки, отображающие дочерние узлы только при определенном разрешении экрана:

| Обертка           | Разрешение экрана |
| ----------------- | ----------------- |
| **WMobile**       | 320px...767px     |
| **WTab**          | 768px...1024px    |
| **WDesktop**      | 1025px...1440px   |
| **WLargeDesktop** | от 1441px         |
| **WFromTab**      | от 768px          |
| **WFromDesktop**  | от 1025px         |
| **WToTab**        | 320px...1024px    |
| **WToDesktop**    | 320px...1440px    |

<!-- prettier-ignore -->
```jsx
<WMobile>Контент для смартфонов</WMobile>
<WTab>Контент для планшетов</WTab>
<WDesktop>Контент для десктопов</WDesktop>
<WLargeDesktop>Контент для больших десктопов</WLargeDesktop>
<WFromTab>Контент для планшетов, десктопов и больших десктопов</WFromTab>
<WFromDesktop>Контент для десктопов и больших десктопов</WFromDesktop>
<WToTab>Контент для смартфонов и планшетов</WToTab>
<WToDesktop>Контент для смартфонов, планшетов и десктопов</WToDesktop>
```
