export const getItemAndSubitem = (params = {}) => {
    let { item, subitem } = params;
    if (!item && !subitem) {
        item = 'components';
        subitem = 'Button';
    }

    item = item || '';
    subitem = subitem || '';

    return { item, subitem };
};
