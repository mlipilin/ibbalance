import React, { useContext } from 'react';

// Theme context
import ThemeContext from '../../theme-context';

export default View => {
    const ViewDecorator = props => {
        const context = useContext(ThemeContext);
        return <View {...props} themeName={context.name} />; // eslint-disable-line
    };

    return ViewDecorator;
};
