import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactRouterPropTypes from 'react-router-prop-types';

// iBalance
import { FeatherIcon, LContainer, LContent, LHeader, LSidebar } from '../../../src';

// Components
import NoReadmeComponent from './components/NoReadmeComponent';

// Containers
import Header from '../../containers/Header';
import Menu from '../../containers/Menu';

// Decorators
import ViewDecorator from './decorator';

// Helpers
import { getItemAndSubitem } from './helpers';

// Wrappers
import WContent from '../../wrappers/WContent';

@ViewDecorator
class View extends Component {
    constructor(props) {
        super(props);
        this.state = { DemoViewComponent: NoReadmeComponent };
    }

    componentDidMount() {
        setTimeout(() => {
            this.importViewItem();
        });
    }

    componentDidUpdate(prevProps) {
        const { match, themeName } = this.props;
        if (prevProps.match.url !== match.url) {
            this.importViewItem();
        }

        if (prevProps.themeName !== themeName) {
            this.importViewItem();
        }
    }

    importViewItem = () => {
        const { themeName, match } = this.props;
        const { item, subitem } = getItemAndSubitem(match.params);
        import(`../../../src/${item}/${subitem}/demo/${themeName}/index.js`)
            .then(({ default: DemoViewComponent }) => {
                this.setState({ DemoViewComponent });
            })
            .catch(error => {
                console.log('error', error);
                this.setState({ DemoViewComponent: NoReadmeComponent });
            });
    };

    render() {
        const { DemoViewComponent } = this.state;

        return (
            <LContainer hasHeader sidebarSide="left">
                <LHeader>
                    <Header />
                </LHeader>
                <LSidebar hasHeader toggleIcon={<FeatherIcon name="menu" />}>
                    <Menu />
                </LSidebar>
                <LContent>
                    <WContent>
                        <DemoViewComponent />
                    </WContent>
                </LContent>
            </LContainer>
        );
    }
}

export default View;

View.propTypes = {
    themeName: PropTypes.string,
    match: ReactRouterPropTypes.match.isRequired,
};

View.defaultProps = {
    themeName: null,
};
