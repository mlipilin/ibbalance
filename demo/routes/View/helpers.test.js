import { getItemAndSubitem } from './helpers';

describe('demo/routes/View/helpers.js', () => {
    describe('getItemAndSubitem', () => {
        it('NO params -> components/Button', () => {
            const output = { item: 'components', subitem: 'Button' };
            expect(getItemAndSubitem()).toEqual(output);
        });
        it('NO item, NO subitem -> components/Button', () => {
            const input = {};
            const output = { item: 'components', subitem: 'Button' };
            expect(getItemAndSubitem(input)).toEqual(output);
        });
        it('HAS item, NO subitem -> item', () => {
            const input = { item: 'components' };
            const output = { item: 'components', subitem: '' };
            expect(getItemAndSubitem(input)).toEqual(output);
        });
        it('NO item, HAS subitem -> item', () => {
            const input = { subitem: 'Checkbox' };
            const output = { item: '', subitem: 'Checkbox' };
            expect(getItemAndSubitem(input)).toEqual(output);
        });
        it('HAS item, HAS subitem -> item/Subitem', () => {
            const input = { item: 'components', subitem: 'Checkbox' };
            const output = { item: 'components', subitem: 'Checkbox' };
            expect(getItemAndSubitem(input)).toEqual(output);
        });
    });
});
