import React from 'react';

// Demo
import ReadmeView from '../../../../components/ReadmeView';

import readme from './readme.md';

const NoReadmeComponent = () => {
    return <ReadmeView>{readme}</ReadmeView>;
};

export default NoReadmeComponent;
