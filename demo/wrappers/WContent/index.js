import React from 'react';
import PropTypes from 'prop-types';

// Wrappers
import WContainer from '../WContainer';

import styles from './styles.sass';

const WContent = props => {
    const { children } = props;
    return (
        <div className={styles.WContent}>
            <WContainer>{children}</WContainer>
        </div>
    );
};

export default WContent;

WContent.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

WContent.defaultProps = {
    children: null,
};
