import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles.sass';

const WContainer = props => {
    const { children } = props;
    return <div className={styles.WContainer}>{children}</div>;
};

export default WContainer;

WContainer.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

WContainer.defaultProps = {
    children: null,
};
