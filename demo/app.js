import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

// Constants
import { LOCAL_STORAGE_DEMO_THEME_KEY } from '../src/constants/local-storage';
import { THEMES } from '../src/constants/themes';

// Constructors
import { createTheme } from './constructors/theme';

// Contexts
import ImageViewer from '../src/components/ImageViewer';

// Routes
import About from './routes/About';
import CreateComponent from './routes/CreateComponent';
import GettingStarted from './routes/GettingStarted';
import Grid from './routes/Grid';
import KindOfComponents from './routes/KindOfComponents';
import Layout from './routes/Layout';
import Wrappers from './routes/Wrappers';
import View from './routes/View';

// Theme context
import ThemeContext from './theme-context';

// Theme provider
import ThemeProvider from '../src/theme-provider';

// Themes
import alfaTheme from '../src/themes/alfa/theme.sass';
import kycTheme from '../src/themes/kyc/theme.sass';
import kycLandingTheme from '../src/themes/kyc_landing/theme.sass';
import absolutTheme from '../src/themes/absolut/theme.sass';
import defaultTheme from '../src/themes/default/theme.sass';
import ozonTheme from '../src/themes/ozon/theme.sass';
import sbermarketTheme from '../src/themes/sbermarket/theme.sass';
import spbTheme from '../src/themes/spb/theme.sass';
import tkbTheme from '../src/themes/tkb/theme.sass';
import tkbLkzTheme from '../src/themes/tkb_lkz/theme.sass';
import vkusvillTheme from '../src/themes/vkusvill/theme.sass';
import demoTheme from '../src/themes/demo/theme.sass';

// Utils
import url from '../src/utils/url';
import Constants from './routes/Constants';

const THEMES_DATA = {
    [THEMES.ABSOLUT]: absolutTheme,
    [THEMES.ALFA]: alfaTheme,
    [THEMES.KYC]: kycTheme,
    [THEMES.KYC_LANDING]: kycLandingTheme,
    [THEMES.DEFAULT]: defaultTheme,
    [THEMES.OZON]: ozonTheme,
    [THEMES.SBERMARKET]: sbermarketTheme,
    [THEMES.SPB]: spbTheme,
    [THEMES.TKB]: tkbTheme,
    [THEMES.TKB_LKZ]: tkbLkzTheme,
    [THEMES.VKUSVILL]: vkusvillTheme,
    [THEMES.DEMO]: demoTheme,
};

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            themeName: null,
        };
    }

    componentDidMount() {
        const themeName = localStorage.getItem(LOCAL_STORAGE_DEMO_THEME_KEY) || THEMES.ABSOLUT;
        this.setState({ themeName });
    }

    changeTheme = themeName => {
        this.setState({ themeName }, () => {
            localStorage.setItem(LOCAL_STORAGE_DEMO_THEME_KEY, themeName);
        });
    };

    render() {
        const { themeName } = this.state;

        const theme = createTheme({
            name: themeName,
            theme: THEMES_DATA[themeName],
            change: this.changeTheme,
        });

        return (
            <ThemeProvider theme={demoTheme}>
                <ThemeContext.Provider value={theme}>
                    <ImageViewer.Provider>
                        <Router>
                            <Switch>
                                <Route path="/" exact component={About} />
                                <Route path={url.route.about()} component={About} />
                                <Route path={url.route.grid()} component={Grid} />
                                <Route
                                    path={url.route.gettingStarted()}
                                    component={GettingStarted}
                                />
                                <Route
                                    path={url.route.kindOfComponents()}
                                    component={KindOfComponents}
                                />
                                <Route
                                    path={url.route.createComponent()}
                                    component={CreateComponent}
                                />
                                <Route path={url.route.wrappers()} component={Wrappers} />
                                <Route path={url.route.layout()} component={Layout} />
                                <Route path={url.route.viewSubitem()} component={View} />
                                <Route path={url.route.viewItem()} component={View} />
                                <Route path={url.route.constants()} component={Constants} />
                            </Switch>
                        </Router>
                    </ImageViewer.Provider>
                </ThemeContext.Provider>
            </ThemeProvider>
        );
    }
}

export default App;
