import React from 'react';

// Constructors
import { createTheme } from './constructors/theme';

export default React.createContext(createTheme());
