export const createTheme = ({ name = null, theme = null, change = _ => _ } = {}) => ({
    name,
    theme,
    change,
});
