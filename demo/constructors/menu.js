export const createMenuItem = ({ active = false, title = '', url = '' } = {}) => ({
    active,
    title,
    url,
});

export const createMenuGroup = ({ items = [], open = false, title = '' } = {}) => ({
    items,
    open,
    title,
});
