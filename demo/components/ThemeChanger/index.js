import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Constants
import { THEMES, THEMES_NAMES } from '../../../src/constants/themes';

import styles from './styles.sass';

class ThemeChanger extends Component {
    handleChange = e => {
        const { onChange } = this.props;
        onChange(e.target.value);
    };

    render() {
        const { value } = this.props;

        const filteredThemes = Object.values(THEMES).filter(theme => theme !== THEMES.DEMO);

        return (
            <div className={styles.ThemeChanger}>
                <div className={styles.ThemeChanger__Label}>Тема:</div>
                <select
                    className={styles.ThemeChanger__Select}
                    value={value || ''}
                    onChange={this.handleChange}
                >
                    {filteredThemes.map(theme => (
                        <option key={theme} value={theme}>
                            {THEMES_NAMES[theme]}
                        </option>
                    ))}
                </select>
            </div>
        );
    }
}

ThemeChanger.propTypes = {
    value: PropTypes.oneOf(Object.values(THEMES)),
    onChange: PropTypes.func,
};

ThemeChanger.defaultProps = {
    value: null,
    onChange: _ => _,
};

export default ThemeChanger;
