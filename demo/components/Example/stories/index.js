import React from 'react';
import { storiesOf } from '@storybook/react';

import Example from '../index';

import readme from './readme.md';

const stories = storiesOf('Example', module);

stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories.add('Default', () => (
    <Example readme={readme}>
        <button type="button">Button 1</button>
        <button type="button">Button 2</button>
        <button type="button">Button 3</button>
    </Example>
));
