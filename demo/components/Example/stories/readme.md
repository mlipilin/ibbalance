# Type

Свойство type может принимать одно из следующих значений: `default`, `primary` и `secondary`

```jsx
<Button type="default">Default button</Button>
<Button type="primary">Primary button</Button>
<Button type="secondary">Secondary button</Button>
```
