import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import Prism from 'prismjs';

// Components
import { FeatherIcon } from '../../../src/index'; //eslint-disable-line
import CodeHighlight from '../CodeHighlight';

// Demo
import ReadmeView from '../ReadmeView';

// Theme
import { ThemeProvider } from '../../../src'; //eslint-disable-line
import ThemeContext from '../../theme-context';

import styles from './styles.sass';

class Example extends Component {
    constructor(props) {
        super(props);
        this.state = { code: null, isCodeVisible: false };
    }

    componentDidMount() {
        const codeNode = this.readme.querySelector('pre');
        if (codeNode) {
            const code = codeNode ? codeNode.innerText : null;
            codeNode.remove();
            this.setState({ code });
        } else {
            this.setState({ code: null });
        }

        setTimeout(Prism.highlightAll);
    }

    handleActionCodeClick = e => {
        e.preventDefault();
        this.setState(prevState => ({
            isCodeVisible: !prevState.isCodeVisible,
        }));
    };

    render() {
        const { readme, children } = this.props;
        const { theme } = this.context;

        const { code, isCodeVisible } = this.state;

        const actionCodeClass = cn(styles.ActionCode, {
            [styles.ActionCode_active]: isCodeVisible,
        });

        const readmeCodeClass = cn(styles.Readme__Code, {
            [styles.Readme__Code_visible]: isCodeVisible,
        });

        return (
            <section className={styles.Example}>
                <div className={styles.Children}>
                    <ThemeProvider theme={theme}>{children}</ThemeProvider>
                </div>
                <div
                    className={styles.Readme}
                    ref={el => {
                        this.readme = el;
                        return this.readme;
                    }}
                >
                    <div className={styles.Readme__Text}>
                        <ReadmeView>{readme}</ReadmeView>
                    </div>
                    <div className={styles.Readme__Actions}>
                        <div
                            className={actionCodeClass}
                            title={isCodeVisible ? 'Скрыть код' : 'Посмотреть код'}
                            onClick={this.handleActionCodeClick}
                        >
                            <FeatherIcon name="code" />
                        </div>
                    </div>
                    <div className={readmeCodeClass}>
                        {!!code && <CodeHighlight code={code} language="jsx" />}
                    </div>
                </div>
            </section>
        );
    }
}

Example.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    readme: PropTypes.string,
};

Example.defaultProps = {
    children: null,
    readme: null,
};

Example.contextType = ThemeContext;

export default Example;
