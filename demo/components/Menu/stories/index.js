import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean } from '@storybook/addon-knobs';

import MenuGroup from '../components/MenuGroup';
import MenuItem from '../components/MenuItem';
import Menu from '../index';

const stories = storiesOf('Menu', module);

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories
    .add('MenuItem', () => (
        <MenuItem active={boolean('Active', false)} small={boolean('Small', false)} url="#">
            Главная
        </MenuItem>
    ))
    .add('MenuGroup', () => (
        <MenuGroup open={boolean('Open', false)} title="Компоненты">
            <MenuItem small>Button</MenuItem>
            <MenuItem small>Checkbox</MenuItem>
            <MenuItem small>Confirm</MenuItem>
        </MenuGroup>
    ))
    .add('Menu', () => (
        <Menu
            items={[
                {
                    title: 'Компоненты',
                    url: '#',
                    items: [
                        {
                            title: 'Button',
                            url: '#',
                        },
                        {
                            title: 'Checkbox',
                            url: '#',
                        },
                        {
                            title: 'Confirm',
                            url: '#',
                        },
                    ],
                },
                { title: 'Структура страницы', url: '#' },
                {
                    title: 'Обертки',
                    url: '#',
                    items: [
                        {
                            title: 'Заголовки',
                            url: '#',
                        },
                        {
                            title: 'Текст',
                            url: '#',
                        },
                    ],
                },
            ]}
        />
    ));
