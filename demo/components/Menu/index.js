import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Components
import MenuGroup from './components/MenuGroup';
import MenuItem from './components/MenuItem';

import styles from './styles.sass';

class Menu extends Component {
    handleItemClick = url => e => {
        e.preventDefault();
        const { onItemClick } = this.props;
        onItemClick(url);
    };

    render() {
        const { items } = this.props;

        return (
            <nav className={styles.Menu}>
                <div className={styles.Items}>
                    {items.map(item => {
                        if (item.items) {
                            return (
                                <MenuGroup open={item.open} title={item.title} key={item.title}>
                                    {item.items.map(subItem => (
                                        <MenuItem
                                            active={subItem.active}
                                            small
                                            key={subItem.title}
                                            onClick={this.handleItemClick(subItem.url)}
                                        >
                                            {subItem.title}
                                        </MenuItem>
                                    ))}
                                </MenuGroup>
                            );
                        }
                        return (
                            <MenuItem
                                active={item.active}
                                key={item.title}
                                onClick={this.handleItemClick(item.url)}
                            >
                                {item.title}
                            </MenuItem>
                        );
                    })}
                </div>
            </nav>
        );
    }
}

Menu.propTypes = {
    items: PropTypes.arrayOf(
        PropTypes.oneOfType([
            // MenuItem
            PropTypes.shape({
                active: PropTypes.bool,
                title: PropTypes.string,
                url: PropTypes.string,
            }),
            // MenuGroup
            PropTypes.shape({
                items: PropTypes.arrayOf(
                    PropTypes.shape({
                        active: PropTypes.bool,
                        title: PropTypes.string,
                        url: PropTypes.string,
                    }),
                ),
                open: PropTypes.bool,
                title: PropTypes.string,
            }),
        ]),
    ),
    onItemClick: PropTypes.func,
};

Menu.defaultProps = {
    items: [],
    onItemClick: _ => _,
};

export default Menu;
