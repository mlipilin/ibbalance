import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import styles from './styles.sass';

const MenuItem = props => {
    const { active, small, url, onClick, children } = props;

    const componentClass = cn(styles.MenuItem, {
        [styles.MenuItem_active]: active,
        [styles.MenuItem_small]: small,
    });

    return (
        <div className={componentClass}>
            <a href={url} onClick={onClick}>
                {children}
            </a>
        </div>
    );
};

MenuItem.propTypes = {
    active: PropTypes.bool,
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    small: PropTypes.bool,
    url: PropTypes.string,
    onClick: PropTypes.func,
};

MenuItem.defaultProps = {
    active: false,
    children: null,
    small: false,
    url: null,
    onClick: _ => _,
};

export default MenuItem;
