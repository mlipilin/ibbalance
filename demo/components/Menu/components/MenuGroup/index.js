import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

// Components
import { FeatherIcon } from '../../../../../src';
import MenuItem from '../MenuItem';

import styles from './styles.sass';

class MenuGroup extends Component {
    constructor(props) {
        super(props);
        this.state = { open: false };
    }

    componentDidMount() {
        const { open } = this.props;
        this.setState({ open });
    }

    componentDidUpdate(prevProps) {
        const { open } = this.props;
        if (prevProps.open !== open) {
            this.setState({ open }); //eslint-disable-line
        }
    }

    handleTitleClick = e => {
        e.preventDefault();
        this.setState(prevState => ({
            open: !prevState.open,
        }));
    };

    render() {
        const { title, children } = this.props;
        const { open } = this.state;

        const toggleIconClass = cn(styles.ToggleIcon, {
            [styles.ToggleIcon_open]: open,
        });

        const childrenClass = cn(styles.Children, {
            [styles.Children_open]: open,
        });

        return (
            <div className={styles.MenuGroup}>
                <header className={styles.Header}>
                    <div className={styles.Title}>
                        <MenuItem onClick={this.handleTitleClick}>{title}</MenuItem>
                    </div>
                    <div className={toggleIconClass}>
                        <FeatherIcon name="chevron-down" />
                    </div>
                </header>

                {/* Children */}
                <div className={childrenClass}>{children}</div>
            </div>
        );
    }
}

MenuGroup.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    open: PropTypes.bool,
    title: PropTypes.string,
};

MenuGroup.defaultProps = {
    children: null,
    open: false,
    title: '',
};

export default MenuGroup;
