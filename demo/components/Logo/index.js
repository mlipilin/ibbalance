import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './styles.sass';

class Logo extends Component {
    handleClick = e => {
        e.preventDefault();
        const { onClick } = this.props;
        onClick();
    };

    render() {
        return (
            <>
                <a href="/" className={styles.Logo} onClick={this.handleClick}>
                    <b>iB</b>
                    alance
                </a>
            </>
        );
    }
}

Logo.propTypes = {
    onClick: PropTypes.func,
};
Logo.defaultProps = {
    onClick: _ => _,
};

export default Logo;
