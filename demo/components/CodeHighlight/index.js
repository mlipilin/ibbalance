import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles.sass';

const CodeHighlight = props => {
    const { code, language } = props;

    return (
        <pre className={styles.Highlight}>
            <code className={`language-${language}`}>{code}</code>
        </pre>
    );
};

CodeHighlight.propTypes = {
    code: PropTypes.string,
    language: PropTypes.oneOf(['jsx', 'javascript', 'sass']),
};

CodeHighlight.defaultProps = {
    code: null,
    language: null,
};

export default CodeHighlight;
