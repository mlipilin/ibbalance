import React from 'react';
import { storiesOf } from '@storybook/react';

import CodeHighlight from '../index';

const stories = storiesOf('CodeHighlight', module);

stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories
    .add('JSX', () => (
        <CodeHighlight
            code={`
import { Button } from 'antd';

ReactDOM.render(
    <div>
        <Button type="primary">Primary</Button>
        <Button>Default</Button>
        <Button type="dashed">Dashed</Button>
        <Button type="danger">Danger</Button>
        <Button type="link">Link</Button>
    </div>,
    mountNode,
);
`.trim()}
            language="jsx"
        />
    ))
    .add('JavaScript', () => (
        <CodeHighlight
            code={`
function sum(a, b) {
    if (b === undefined) {
        return function(c) {
            return a + c;
        }
    }
    return a + b;
}

sum(1, 2); // 3
sum(1)(2); // 3
);
`.trim()}
            language="javascript"
        />
    ));
