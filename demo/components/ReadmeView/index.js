import React, { useEffect } from 'react';
import Prism from 'prismjs';
import PropTypes from 'prop-types';

import styles from './styles.sass';

const ReadmeView = props => {
    useEffect(() => {
        setTimeout(Prism.highlightAll);
    }, []);

    const { children } = props;

    return <div className={styles.ReadmeView} dangerouslySetInnerHTML={{ __html: children }} />;
};

ReadmeView.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

ReadmeView.defaultProps = {
    children: null,
};

export default ReadmeView;
