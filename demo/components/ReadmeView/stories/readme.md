# H1 Заголовок
## H2 Заголовок
### H3 Заголовок

Первый абзац

Второй абзац

Третий абзац

## Неупорядоченный список:
* элемент 1
* элемент 2
* элемент 3

## Упорядоченный список:
1. элемент 1
2. элемент 2
3. элемент 3

## Таблица:
| #        | ФИО           | Должность | Дата        |
| -------- | ------------- | --------- | ----------- |
| 12345678 | Иванов И.И.   | Директор  | 10 авг 2019 |
| 23456789 | Петров П.П.   | Менеджер  | 11 авг 2019 |
| 34567890 | Васильев В.В. | Инженер  | 12 авг 2019 |

## Код:
<!-- prettier-ignore -->
```jsx
import React from 'react';
import ReactDOM from 'react-dom';
import { Button, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/alfa/theme';
ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Button type="default" size="l">
      Первая кнопка
    </Button>{' '}
    <Button type="primary" size="m">
      Вторая кнопка
    </Button>{' '}
    <Button type="secondary" size="s">
      Третья кнопка
    </Button>
  </ThemeProvider>,
  document.getElementById('root'),
);
```

<!-- prettier-ignore -->
```javascript
module.exports = {
  resolve: {
    alias: {
      ibalance: path.resolve(__dirname, '../node_modules/ibalance/src/themes/выбранная_тема'),
    },
  },
};
```
