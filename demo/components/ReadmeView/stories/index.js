import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';

import ReadmeView from '../index';

import readme from './readme.md';

const stories = storiesOf('ReadmeView', module);

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories.add('Default', () => <ReadmeView>{readme}</ReadmeView>);
