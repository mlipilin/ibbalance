# Button

Простая реализация кнопки

## Когда использовать

- Случай 1
- Случай 2
- Случай 3

## Примеры использования

## API

Prop name | Prop type | Default value
--- | --- | ---
**block**|*bool*|`false`
**disabled**|*bool*|`false`
**iconLeft**|*object*|`null`
**iconRight**|*object*|`null`
**processing**|*bool*|`false`
**size**|`xs`, `s`, `m`, `l`, `xl`, `xxl`|`m`
**type**|`default`, `primary`, `secondary`, `danger`, `success`, `info`, `warning`|`default`
