import React from 'react';
import { storiesOf } from '@storybook/react';

import DemoComponent from '../index';

// Components
import { Button } from '../../../../src';

import Example from '../../Example';
import ReadmeView from '../../ReadmeView';

import example from './example.md';
import readme from './readme.md';

const stories = storiesOf('DemoComponent', module);

stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories.add('Default', () => (
    <DemoComponent>
        <ReadmeView>{readme}</ReadmeView>
        <Example readme={example}>
            <Button type="default">Default button</Button>{' '}
            <Button type="primary">Primary button</Button>{' '}
            <Button type="secondary">Secondary button</Button>
        </Example>
    </DemoComponent>
));
