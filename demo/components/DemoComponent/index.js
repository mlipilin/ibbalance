import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './styles.sass';

class DemoComponent extends Component {
    componentDidMount() {
        const exampleHeader = this.block.querySelector('#примеры-использования');
        const exampleSection = this.block.querySelector('#example');
        if (exampleHeader && exampleSection) {
            exampleHeader.after(exampleSection);
        }
    }

    render() {
        const { children } = this.props;
        return (
            <div
                className={styles.DemoComponent}
                ref={el => {
                    this.block = el;
                    return this.block;
                }}
            >
                {children}
            </div>
        );
    }
}

DemoComponent.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

DemoComponent.defaultProps = {
    children: null,
};

export default DemoComponent;
