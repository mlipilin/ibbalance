import React from 'react';

import { configure, addDecorator, addParameters } from '@storybook/react';

// Constants
import { LOCAL_STORAGE_THEME_KEY } from '../src/constants/local-storage';
import { THEMES } from '../src/constants/themes';

import StoreWithTheme from './StoreWithTheme';

addParameters({
    options: {
        name: 'iBalance DS',
        panelPosition: 'right',
    },
});

addDecorator(store => <StoreWithTheme store={store} />);

const req = require.context('../src', true, /stories\/[\S]+\.js$/);
function loadStories() {
    req.keys().forEach(filename => req(filename));
}

configure(() => {
    loadStories();
    setTimeout(() => {
        const themeId = localStorage.getItem(LOCAL_STORAGE_THEME_KEY) || THEMES.DEFAULT;

        const parentDocument = window.parent.document;
        if (parentDocument.getElementById('themeChanger')) {
            parentDocument.getElementById('themeChanger').remove();
        }

        const form = window.parent.document.querySelector('.simplebar-content form');

        let select = document.createElement('select');

        Object.values(THEMES).forEach(theme => {
            let option = document.createElement('option');
            option.value = theme;
            option.innerText = theme;
            select.appendChild(option);
        });

        select.value = themeId;
        select.addEventListener('change', e => {
            localStorage.setItem(LOCAL_STORAGE_THEME_KEY, e.target.value);
            window.location.reload();
            select.disabled = true;
        });

        let div = document.createElement('div');
        div.id = 'themeChanger';
        div.innerText = 'Theme: ';
        div.style.position = 'absolute';
        div.style.top = '40px';
        div.appendChild(select);

        form.style.marginBottom = '45px';
        form.appendChild(div);
    }, 1000);
}, module);
