import React, { Component } from 'react';

// Theme
import { ThemeProvider } from '../src/index';

// Constants
import { LOCAL_STORAGE_THEME_KEY } from '../src/constants/local-storage';
import { THEMES } from '../src/constants/themes';

class StoreWithTheme extends React.Component {
    state = { theme: null, themeId: null };

    componentDidMount() {
        const themeId = localStorage.getItem(LOCAL_STORAGE_THEME_KEY) || THEMES.DEFAULT;
        import(`../src/themes/${themeId}/theme`)
            .then(({ default: theme }) => {
                this.setState({ theme, themeId });
            })
            .catch(error => {
                console.log('error', error);
            });
    }

    render() {
        const { theme, themeId } = this.state;

        if (!themeId) return null;

        return <ThemeProvider theme={theme}>{this.props.store()}</ThemeProvider>;
    }
}

export default StoreWithTheme;
