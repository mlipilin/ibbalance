import React from 'react';

import { configure, addDecorator, addParameters } from '@storybook/react';

// Theme
import { ThemeProvider } from '../src/index';
import themeDemo from '../src/themes/demo/theme';

addParameters({
    options: {
        name: 'iBalance Demo',
        panelPosition: 'right',
    },
});

addDecorator(store => <ThemeProvider theme={themeDemo}>{store()}</ThemeProvider>);

const req = require.context('../demo', true, /stories\/[\S]+\.js$/);
function loadStories() {
    req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
