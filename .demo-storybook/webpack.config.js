const path = require('path');

module.exports = async ({ config, mode }) => {
    // `mode` has a value of 'DEVELOPMENT' or 'PRODUCTION'
    // You can change the configuration based on that.
    // 'PRODUCTION' is used when building the static version of storybook.

    config.module.rules.push({
        test: /\.sass$/,
        use: [
            'style-loader',
            {
                loader: 'css-loader',
                options: {
                    modules: {
                        localIdentName: '[name]_[local]_[hash:base64]',
                    },
                    importLoaders: 1,
                    sourceMap: true,
                },
            },
            'resolve-url-loader',
            'sass-loader',
        ],
    });

    config.module.rules.push({
        test: /\.md$/,
        use: ['markdown-loader'],
    });

    config.resolve = {
        ...config.resolve,
        alias: {
            theme: path.resolve(__dirname, '../src/themes/demo/expose'),
        },
        extensions: ['.js', '.sass'],
    };

    return config;
};
