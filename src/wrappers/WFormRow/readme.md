## Usage

```jsx
<form>
    <WFormRow>
        <Input label="Логин" />
    </WFormRow>
    <WFormRow>
        <Input label="Пароль" type="password" />
    </WFormRow>
    <WFormRow>
        <Button>Отправить</Button>
    </WFormRow>
</form>
```

## Props

—

## CSS Selectors

```css
.WFormRow {}
```
