import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';

// Compinents
import Button from '../../../components/Button';
import Input from '../../../components/Input';

// HOC
import { withState } from '../../../hoc';

import WFormRow from '../index';

import notes from '../readme.md';

const InputWithState = withState(Input);

const stories = storiesOf('WFormRow', module);

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories.add(
    'Default',
    () => (
        <form>
            <WFormRow>
                <InputWithState label="Логин" />
            </WFormRow>
            <WFormRow>
                <InputWithState label="Пароль" type="password" />
            </WFormRow>
            <WFormRow>
                <Button block type="primary">
                    Отправить
                </Button>
            </WFormRow>
        </form>
    ),
    { notes },
);
