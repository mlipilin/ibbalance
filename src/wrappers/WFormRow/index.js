import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import { withTheme } from '../../theme-provider';

@withTheme
class WFormRow extends Component {
    render() {
        const { children, className, applyTheme, ...otherProps } = this.props;

        const componentClass = applyTheme(cn('WFormRow'), className);

        return (
            <div className={componentClass} {...otherProps}>
                {children}
            </div>
        );
    }
}

WFormRow.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    className: PropTypes.string,
    applyTheme: PropTypes.func,
};

WFormRow.defaultProps = {
    children: null,
    className: '',
    applyTheme: _ => _,
};

export default WFormRow;
