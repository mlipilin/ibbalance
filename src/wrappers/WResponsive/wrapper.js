import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

// Constants
import { DISPLAY, SIZE, SIZE_BLOCK_CLASS } from './constants';

import { withTheme } from '../../theme-provider';

@withTheme
class Wrapper extends Component {
    render() {
        const { children, className, display, size, applyTheme, ...otherProps } = this.props;

        const blockClass = SIZE_BLOCK_CLASS[size];
        if (!blockClass) return children;

        const componentClass = applyTheme(
            cn(blockClass, `${blockClass}_display_${display}`),
            className,
        );

        const isInline =
            display === DISPLAY.INLINE ||
            display === DISPLAY.INLINE_BLOCK ||
            display === DISPLAY.INLINE_FLEX;

        return isInline ? (
            <span className={componentClass} {...otherProps}>
                {children}
            </span>
        ) : (
            <div className={componentClass} {...otherProps}>
                {children}
            </div>
        );
    }
}

Wrapper.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    className: PropTypes.string,
    display: PropTypes.oneOf(Object.values(DISPLAY)),
    size: PropTypes.oneOf(Object.values(SIZE)).isRequired,
    applyTheme: PropTypes.func,
};

Wrapper.defaultProps = {
    children: null,
    className: '',
    display: DISPLAY.BLOCK,
    applyTheme: _ => _,
};

export default Wrapper;
