import React from 'react';

// Constants
import { SIZE } from './constants';

import Wrapper from './wrapper';

/* eslint-disable */
const WMobile = ({ children, ...otherProps }) => (
    <Wrapper {...otherProps} size={SIZE.MOBILE}>
        {children}
    </Wrapper>
);

const WTab = ({ children, ...otherProps }) => (
    <Wrapper {...otherProps} size={SIZE.TAB}>
        {children}
    </Wrapper>
);

const WDesktop = ({ children, ...otherProps }) => (
    <Wrapper {...otherProps} size={SIZE.DESKTOP}>
        {children}
    </Wrapper>
);

const WLargeDesktop = ({ children, ...otherProps }) => (
    <Wrapper {...otherProps} size={SIZE.LARGE_DESKTOP}>
        {children}
    </Wrapper>
);

const WFromTab = ({ children, ...otherProps }) => (
    <Wrapper {...otherProps} size={SIZE.FROM_TAB}>
        {children}
    </Wrapper>
);

const WFromDesktop = ({ children, ...otherProps }) => (
    <Wrapper {...otherProps} size={SIZE.FROM_DESKTOP}>
        {children}
    </Wrapper>
);

const WToTab = ({ children, ...otherProps }) => (
    <Wrapper {...otherProps} size={SIZE.TO_TAB}>
        {children}
    </Wrapper>
);

const WToDesktop = ({ children, ...otherProps }) => (
    <Wrapper {...otherProps} size={SIZE.TO_DESKTOP}>
        {children}
    </Wrapper>
);
/* eslint-enable */

export { WMobile, WTab, WDesktop, WLargeDesktop, WFromTab, WFromDesktop, WToTab, WToDesktop };
export default {
    WMobile,
    WTab,
    WDesktop,
    WLargeDesktop,
    WFromTab,
    WFromDesktop,
    WToTab,
    WToDesktop,
};
