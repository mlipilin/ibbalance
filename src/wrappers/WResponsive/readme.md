## Usage

```jsx
<WMobile>Some content that is visible only on mobile screen size</WMobile>
```

## Props

—

## CSS Selectors

```css
.Mobile {}
.Tab {}
.Desktop {}
.LargeDesktop {}
.FromTab {}
.FromDesktop {}
.ToTab {}
.ToDesktop {}
```
