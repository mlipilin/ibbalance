import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, select } from '@storybook/addon-knobs';

// Constants
import { DISPLAY } from '../constants';

import {
    WMobile,
    WTab,
    WDesktop,
    WLargeDesktop,
    WFromTab,
    WFromDesktop,
    WToTab,
    WToDesktop,
} from '../index';

import notes from '../readme.md';

const stories = storiesOf('WResponsive', module);

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories.add(
    'Default',
    () => {
        const display = select('Display', Object.values(DISPLAY), DISPLAY.BLOCK);
        const props = { display };
        return (
            <>
                <WMobile {...props}>
                    <p>
                        <b>&lt;WMobile&gt;</b>
                        visible on Mobile
                        <b>&lt;/WMobile&gt;</b>
                    </p>
                </WMobile>
                <WTab {...props}>
                    <p>
                        <b>&lt;WTab&gt;</b>
                        visible on Tab
                        <b>&lt;/WTab&gt;</b>
                    </p>
                </WTab>
                <WDesktop {...props}>
                    <p>
                        <b>&lt;WDesktop&gt;</b>
                        visible on Desktop
                        <b>&lt;/WDesktop&gt;</b>
                    </p>
                </WDesktop>
                <WLargeDesktop {...props}>
                    <p>
                        <b>&lt;WLargeDesktop&gt;</b>
                        visible on LargeDesktop
                        <b>&lt;/WLargeDesktop&gt;</b>
                    </p>
                </WLargeDesktop>
                <WFromTab {...props}>
                    <p>
                        <b>&lt;WFromTab&gt;</b>
                        visible on Tab, Desktop, and LargeDesktop
                        <b>&lt;/WFromTab&gt;</b>
                    </p>
                </WFromTab>
                <WFromDesktop {...props}>
                    <p>
                        <b>&lt;WFromDesktop&gt;</b>
                        visible on Desktop, and LargeDesktop
                        <b>&lt;/WFromDesktop&gt;</b>
                    </p>
                </WFromDesktop>
                <WToTab {...props}>
                    <p>
                        <b>&lt;WToTab&gt;</b>
                        visible on Mobile and Tab
                        <b>&lt;/WToTab&gt;</b>
                    </p>
                </WToTab>
                <WToDesktop {...props}>
                    <p>
                        <b>&lt;WToDesktop&gt;</b>
                        visible on Mobile, Tab and Desktop
                        <b>&lt;/WToDesktop&gt;</b>
                    </p>
                </WToDesktop>
            </>
        );
    },
    { notes },
);
