export const DISPLAY = {
    BLOCK: 'block',
    FLEX: 'flex',
    INLINE: 'inline',
    INLINE_BLOCK: 'inline-block',
    INLINE_FLEX: 'inline-flex',
};

export const SIZE = {
    MOBILE: 'mobile',
    TAB: 'tab',
    DESKTOP: 'desktop',
    LARGE_DESKTOP: 'large-desktop',
    // from
    FROM_TAB: 'from-tab',
    FROM_DESKTOP: 'from-desktop',
    // to
    TO_TAB: 'to-tab',
    TO_DESKTOP: 'to-desktop',
};

export const SIZE_BLOCK_CLASS = {
    [SIZE.MOBILE]: 'WMobile',
    [SIZE.TAB]: 'WTab',
    [SIZE.DESKTOP]: 'WDesktop',
    [SIZE.LARGE_DESKTOP]: 'WLargeDesktop',
    [SIZE.FROM_TAB]: 'WFromTab',
    [SIZE.FROM_DESKTOP]: 'WFromDesktop',
    [SIZE.TO_TAB]: 'WToTab',
    [SIZE.TO_DESKTOP]: 'WToDesktop',
};
