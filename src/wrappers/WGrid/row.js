import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

// Helpers
import { getRowStyleAlignItems, getRowStyleCols, getRowStyleGap } from './helpers';

// Hooks
import useScreenSize from '../../hooks/use-screen-size';

import { withTheme } from '../../theme-provider';

function Row(props) {
    const { alignItems, children, className, cols, gap, style, applyTheme, ...otherProps } = props;

    // Hooks
    const screenSize = useScreenSize();

    // Render props
    const componentClass = applyTheme(cn('Row'), className);

    const fullStyle = useMemo(
        () => ({
            ...style,
            ...getRowStyleAlignItems(alignItems, screenSize),
            ...getRowStyleCols(cols, Array.isArray(children) ? children.length : 0, screenSize),
            ...getRowStyleGap(gap, screenSize),
        }),
        [alignItems, cols, children.length, screenSize, gap],
    );

    return (
        <div className={componentClass} {...otherProps} style={fullStyle}>
            {children}
        </div>
    );
}

Row.propTypes = {
    alignItems: PropTypes.oneOfType([
        PropTypes.oneOf(['start', 'center', 'end']),
        PropTypes.shape({
            mobile: PropTypes.oneOf(['start', 'center', 'end']),
            tab: PropTypes.oneOf(['start', 'center', 'end']),
            desktop: PropTypes.oneOf(['start', 'center', 'end']),
            largeDesktop: PropTypes.oneOf(['start', 'center', 'end']),
        }),
    ]),
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]),
    className: PropTypes.string,
    cols: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
        PropTypes.shape({
            mobile: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
            tab: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
            desktop: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
            largeDesktop: PropTypes.arrayOf(
                PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
            ),
        }),
    ]),
    gap: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.arrayOf(PropTypes.string),
        PropTypes.shape({
            mobile: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)]),
            tab: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)]),
            desktop: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)]),
            largeDesktop: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.arrayOf(PropTypes.string),
            ]),
        }),
    ]),
    style: PropTypes.shape({}),
    applyTheme: PropTypes.func,
};
Row.defaultProps = {
    alignItems: null,
    children: null,
    className: null,
    cols: null,
    gap: null,
    style: {},
    applyTheme: _ => _,
};

export default withTheme(Row);
