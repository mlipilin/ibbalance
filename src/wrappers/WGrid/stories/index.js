import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, select } from '@storybook/addon-knobs';
import PropTypes from 'prop-types';

import { Col, Row } from '../index';

import notes from '../readme.md';

import styles from './styles.sass';

const Cell = ({ children }) => <div className={styles.Cell}>{children}</div>;

const stories = storiesOf('WGrid', module);

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories.add(
    'Default',
    () => (
        <Row
            alignItems={select(
                'alignItems',
                { start: 'start', center: 'center', end: 'end' },
                'start',
            )}
            cols={select(
                'cols',
                {
                    '[1]': [1],
                    '[1,1]': [1, 1],
                    '[1,1,1]': [1, 1, 1],
                    '[1,1,1,1,1,1]': [1, 1, 1, 1, 1, 1],
                },
                [1],
            )}
            gap={select(
                'gap',
                {
                    '10px': '10px',
                    '20px': '20px',
                    '[10px, 10px]': ['10px', '10px'],
                    '[20px, 20px]': ['20px', '20px'],
                },
                '10px',
            )}
        >
            <Col>
                <Cell>Cell</Cell>
            </Col>
            <Col>
                <Cell>Cell</Cell>
            </Col>
            <Col>
                <Cell>Cell</Cell>
            </Col>
            <Col>
                <Cell>Cell</Cell>
            </Col>
            <Col>
                <Cell>Cell</Cell>
            </Col>
            <Col>
                <Cell>Cell</Cell>
            </Col>
        </Row>
    ),
    { notes },
);

Cell.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

Cell.defaultProps = {
    children: null,
};
