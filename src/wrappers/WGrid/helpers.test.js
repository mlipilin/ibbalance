import { getRowStyleAlignItems, getRowStyleCols, getRowStyleGap } from './helpers';

// Constants
import { SCREEN_SIZES } from '../../constants/props';

describe('src/wrappers/WGrid/helpers.js', () => {
    describe('getRowStyleAlignItems', () => {
        describe('string', () => {
            it('no value', () => {
                expect(getRowStyleAlignItems()).toEqual({});
            });
            it('with value', () => {
                expect(getRowStyleAlignItems('center')).toEqual({
                    alignItems: 'center',
                });
            });
        });
        describe('responsive', () => {
            it('screenSize=mobile', () => {
                expect(
                    getRowStyleAlignItems(
                        {
                            mobile: 'center',
                            tab: 'start',
                            desktop: 'center',
                            largeDesktop: 'end',
                        },
                        SCREEN_SIZES.MOBILE,
                    ),
                ).toEqual({ alignItems: 'center' });
            });
            it('screenSize=tab', () => {
                expect(
                    getRowStyleAlignItems(
                        {
                            mobile: 'center',
                            tab: 'start',
                            desktop: 'center',
                            largeDesktop: 'end',
                        },
                        SCREEN_SIZES.TAB,
                    ),
                ).toEqual({ alignItems: 'start' });
            });
            it('screenSize=desktop', () => {
                expect(
                    getRowStyleAlignItems(
                        {
                            mobile: 'center',
                            tab: 'start',
                            desktop: 'center',
                            largeDesktop: 'end',
                        },
                        SCREEN_SIZES.DESKTOP,
                    ),
                ).toEqual({ alignItems: 'center' });
            });
            it('screenSize=largeDesktop', () => {
                expect(
                    getRowStyleAlignItems(
                        {
                            mobile: 'center',
                            tab: 'start',
                            desktop: 'center',
                            largeDesktop: 'end',
                        },
                        SCREEN_SIZES.LARGE_DESKTOP,
                    ),
                ).toEqual({ alignItems: 'end' });
            });
        });
    });
    describe('getRowStyleCols', () => {
        describe('NO cols', () => {
            it('No children -> NO styles', () => {
                expect(getRowStyleCols()).toEqual({});
            });
            it('One child -> 1fr', () => {
                expect(getRowStyleCols(null, 1)).toEqual({
                    gridTemplateColumns: '1fr',
                });
            });
            it('Many children -> 1fr...1fr', () => {
                expect(getRowStyleCols(null, 3)).toEqual({
                    gridTemplateColumns: '1fr 1fr 1fr',
                });
            });
        });
        describe('cols is array', () => {
            it('Columns ratio', () => {
                expect(getRowStyleCols([1, 2, 1])).toEqual({
                    gridTemplateColumns: '1fr 2fr 1fr',
                });
            });
            it('Columns ratio + fixed in pixels', () => {
                expect(getRowStyleCols(['100px', 2, 1])).toEqual({
                    gridTemplateColumns: '100px 2fr 1fr',
                });
            });
            it('Columns ratio + fixed in percents', () => {
                expect(getRowStyleCols(['20%', 2, 1])).toEqual({
                    gridTemplateColumns: '20% 2fr 1fr',
                });
            });
        });
        describe('responsive', () => {
            it('screenSize=mobile', () => {
                expect(
                    getRowStyleCols(
                        {
                            mobile: [1],
                            tab: [1, 1],
                            desktop: [1, 1, 1],
                            largeDesktop: [1, 1, 1, 1, 1, 1],
                        },
                        0,
                        SCREEN_SIZES.MOBILE,
                    ),
                ).toEqual({ gridTemplateColumns: '1fr' });
            });
            it('screenSize=tab', () => {
                expect(
                    getRowStyleCols(
                        {
                            mobile: [1],
                            tab: [1, 1],
                            desktop: [1, 1, 1],
                            largeDesktop: [1, 1, 1, 1, 1, 1],
                        },
                        0,
                        SCREEN_SIZES.TAB,
                    ),
                ).toEqual({ gridTemplateColumns: '1fr 1fr' });
            });
            it('screenSize=desktop', () => {
                expect(
                    getRowStyleCols(
                        {
                            mobile: [1],
                            tab: [1, 1],
                            desktop: [1, 1, 1],
                            largeDesktop: [1, 1, 1, 1, 1, 1],
                        },
                        0,
                        SCREEN_SIZES.DESKTOP,
                    ),
                ).toEqual({ gridTemplateColumns: '1fr 1fr 1fr' });
            });
            it('screenSize=largeDesktop', () => {
                expect(
                    getRowStyleCols(
                        {
                            mobile: [1],
                            tab: [1, 1],
                            desktop: [1, 1, 1],
                            largeDesktop: [1, 1, 1, 1, 1, 1],
                        },
                        0,
                        SCREEN_SIZES.LARGE_DESKTOP,
                    ),
                ).toEqual({ gridTemplateColumns: '1fr 1fr 1fr 1fr 1fr 1fr' });
            });
        });
    });
    describe('getRowStyleGap', () => {
        it('NO gap', () => {
            expect(getRowStyleGap()).toEqual({});
        });
        it('gap is string', () => {
            expect(getRowStyleGap('10px')).toEqual({ columnGap: '10px' });
        });
        it('gap is array', () => {
            expect(getRowStyleGap(['10px', '20px'])).toEqual({ columnGap: '20px', rowGap: '10px' });
        });

        describe('responsive', () => {
            it('screenSize=mobile', () => {
                expect(
                    getRowStyleGap(
                        {
                            mobile: '15px',
                            tab: ['20px', '10px'],
                            desktop: '25px',
                            largeDesktop: ['30px', '20px'],
                        },
                        SCREEN_SIZES.MOBILE,
                    ),
                ).toEqual({ columnGap: '15px' });
            });
            it('screenSize=tab', () => {
                expect(
                    getRowStyleGap(
                        {
                            mobile: '15px',
                            tab: ['20px', '10px'],
                            desktop: '25px',
                            largeDesktop: ['30px', '20px'],
                        },
                        SCREEN_SIZES.TAB,
                    ),
                ).toEqual({ columnGap: '10px', rowGap: '20px' });
            });
            it('screenSize=desktop', () => {
                expect(
                    getRowStyleGap(
                        {
                            mobile: '15px',
                            tab: ['20px', '10px'],
                            desktop: '25px',
                            largeDesktop: ['30px', '20px'],
                        },
                        SCREEN_SIZES.DESKTOP,
                    ),
                ).toEqual({ columnGap: '25px' });
            });
            it('screenSize=largeDesktop', () => {
                expect(
                    getRowStyleGap(
                        {
                            mobile: '15px',
                            tab: ['20px', '10px'],
                            desktop: '25px',
                            largeDesktop: ['30px', '20px'],
                        },
                        SCREEN_SIZES.LARGE_DESKTOP,
                    ),
                ).toEqual({ columnGap: '20px', rowGap: '30px' });
            });
        });
    });
});
