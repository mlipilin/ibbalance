import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import { withTheme } from '../../theme-provider';

function Col(props) {
    const { className, children, applyTheme, ...otherProps } = props;

    const componentClass = applyTheme(cn('Col'), className);

    return (
        <div className={componentClass} {...otherProps}>
            {children}
        </div>
    );
}

Col.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    className: PropTypes.string,
    applyTheme: PropTypes.func,
};

Col.defaultProps = {
    children: null,
    className: null,
    applyTheme: _ => _,
};

export default withTheme(Col);
