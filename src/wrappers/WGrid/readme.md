## Usage

```jsx
<Row alignItems="center" cols={[1, 2, 3]} gap={20}>
    <Col>Column with 1/6 size</Col>
    <Col>Column with 2/6 size</Col>
    <Col>Column with 3/6 size</Col>
</Row>
```

## Row Props

| Prop name    | Prop type                                             | Default value |
| ------------ | ------------------------------------------------------| ------------- |
| _alignItems_ | oneOf(['start', 'center', 'end']) \| responsiveObject | `null`        |
| _cols_       | arrayOf(number \| string) \| responsiveObject         | `null`        |
| _gap_        | string \| arrayOf(string) \| responsiveObject         | `null`        |

## Col Props

—

## CSS Selectors

```css
.Row {
}
.Col {
}
```
