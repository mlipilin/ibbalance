// Helpers
import { isArray, isObject } from '../../helpers/is';

export const getRowStyleAlignItems = (alignItems = null, screenSize = null) => {
    let result = null;

    if (typeof alignItems === 'string') {
        result = alignItems;
    } else if (isObject(alignItems)) {
        return getRowStyleAlignItems(alignItems[screenSize]);
    }

    return result
        ? {
              alignItems: result,
          }
        : {};
};

export const getRowStyleCols = (cols = null, childrenLength = 0, screenSize = null) => {
    let result = null;

    if (Array.isArray(cols)) {
        result = cols.map(getColValue).join(' ');
    } else if (isObject(cols)) {
        return getRowStyleCols(cols[screenSize], childrenLength);
    } else {
        result = '1fr '.repeat(childrenLength).trim();
    }

    return result
        ? {
              gridTemplateColumns: result,
          }
        : {};
};

export const getRowStyleGap = (gap = null, screenSize = null) => {
    let columnGap = null;
    let rowGap = null;

    if (isArray(gap)) {
        rowGap = gap[0] || null;
        columnGap = gap[1] || null;
    } else if (isObject(gap)) {
        return getRowStyleGap(gap[screenSize]);
    } else if (gap) {
        columnGap = gap;
    }
    return {
        ...(columnGap ? { columnGap } : {}),
        ...(rowGap ? { rowGap } : {}),
    };
};

function getColValue(col) {
    if (typeof col === 'number') {
        return `${col}fr`;
    }
    if (typeof col === 'string') {
        return col;
    }
    return null;
}
