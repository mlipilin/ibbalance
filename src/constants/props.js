export const IMAGE_SIZES = {
    CONTAIN: 'contain',
    COVER: 'cover',
    DEFAULT: 'default',
};

export const PLACEMENTS = {
    TOP: 'top',
    RIGHT: 'right',
    BOTTOM: 'bottom',
    LEFT: 'left',
};

export const SCREEN_SIZES = {
    MOBILE: 'mobile',
    TAB: 'tab',
    DESKTOP: 'desktop',
    LARGE_DESKTOP: 'largeDesktop',
};

export const SIZES = {
    XS: 'xs',
    S: 's',
    M: 'm',
    L: 'l',
    XL: 'xl',
    XXL: 'xxl',
    XXXL: 'xxxl',
};

export const TYPES = {
    CIRCLE: 'circle',
    DANGER: 'danger',
    DEFAULT: 'default',
    INFO: 'info',
    PRIMARY: 'primary',
    SECONDARY: 'secondary',
    SUCCESS: 'success',
    WARNING: 'warning',
};
