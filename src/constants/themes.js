export const THEMES = {
    ABSOLUT: 'absolut',
    ALFA: 'alfa',
    KYC: 'kyc',
    KYC_LANDING: 'kyc_landing',
    DEFAULT: 'default',
    DEMO: 'demo',
    OZON: 'ozon',
    SBERMARKET: 'sbermarket',
    SPB: 'spb',
    TKB: 'tkb',
    TKB_LKZ: 'tkb_lkz',
    VKUSVILL: 'vkusvill',
};

export const THEMES_NAMES = {
    [THEMES.ABSOLUT]: 'Абсолют',
    [THEMES.ALFA]: 'Альфа',
    [THEMES.KYC]: 'KYC',
    [THEMES.KYC_LANDING]: 'kyc_landing',
    [THEMES.DEFAULT]: 'По умолчанию',
    [THEMES.DEMO]: 'Демо',
    [THEMES.OZON]: 'Озон',
    [THEMES.SBERMARKET]: 'Сбермаркет',
    [THEMES.SPB]: 'СПБ',
    [THEMES.TKB]: 'ТКБ',
    [THEMES.TKB_LKZ]: 'ТКБ-ЛКЗ',
    [THEMES.VKUSVILL]: 'ВкусВилл',
};
