import React from 'react';

// Utils
import applyTheme from './utils/apply-theme';

const MyContext = React.createContext(null);
const ThemeProvider = MyContext.Provider;

// HOC
export const withTheme = Component =>
    React.forwardRef((props, ref) => (
        <MyContext.Consumer>
            {(theme = {}) => {
                const componentProps = {
                    ...props,
                    applyTheme: applyTheme(theme),
                    ref,
                };

                return <Component {...componentProps} />;
            }}
        </MyContext.Consumer>
    ));

/* eslint react/prop-types: [2, { ignore: ['children', 'theme'] }] */
// This wrapper needs for passing a theme using "theme" prop instead of "value" prop (as a default prop for Provider)
export default ({ children, theme }) => <ThemeProvider value={theme}>{children}</ThemeProvider>;
