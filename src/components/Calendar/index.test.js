import React from 'react';
import userEvent from '@testing-library/user-event';

import FeatherIcon from '../FeatherIcon';
import Calendar from './index';

import { render } from '../../hoc/with-theme';

describe('Тестирование компонента Calendar', () => {
    const REAL_DATE = Date;
    beforeAll(() => {
        Date.now = jest.fn(() => 1609489800000);
    });
    afterAll(() => {
        Date.now = REAL_DATE.now;
    });
    it('Должен отобразиться Calendar', () => {
        const { container } = render(<Calendar />);
        const component = container.firstChild;
        expect(component).toBeInTheDocument();
        expect(component).toMatchSnapshot();
    });

    it('передали className -> отобразится классом "Calendar__Style"', () => {
        const CLASS_NAME = 'Calendar__Style';
        const { container } = render(<Calendar className={CLASS_NAME} />);
        const component = container.querySelector('.Calendar');
        expect(component).toHaveClass(CLASS_NAME);
        expect(component).toMatchSnapshot();
    });

    it('передали hideYearSwitch -> отобразится без навигации по году вправо', () => {
        const { container } = render(<Calendar hideYearSwitch />);
        const iconYearRight = container.querySelector('.feather-chevrons-right');
        expect(iconYearRight).not.toBeInTheDocument();
        expect(container).toMatchSnapshot();
    });
    it('передали hideYearSwitch -> отобразится без навигации по году влево', () => {
        const { container } = render(<Calendar hideYearSwitch />);
        const iconYearLeft = container.querySelector('.feather-chevrons-left');
        expect(iconYearLeft).not.toBeInTheDocument();
        expect(container).toMatchSnapshot();
    });

    it('передали iconNextMonth -> отобразится кастомная иконка след. месяца', () => {
        const { container } = render(
            <Calendar iconNextMonth={<FeatherIcon name="arrow-right" />} />,
        );
        const iconNextMonth = container.querySelectorAll('.Calendar__NavigateButton')[2];
        const iconMonthRight = iconNextMonth.querySelector('.feather-arrow-right');
        expect(iconMonthRight).toBeInTheDocument();
        expect(iconMonthRight).toMatchSnapshot();
    });

    it('передали iconPrevMonth -> отобразится кастомная иконка пред. месяца', () => {
        const { container } = render(
            <Calendar iconPrevMonth={<FeatherIcon name="arrow-left" />} />,
        );
        const iconPrevMonth = container.querySelectorAll('.Calendar__NavigateButton')[1];
        const iconMonthLeft = iconPrevMonth.querySelector('.feather-arrow-left');
        expect(iconMonthLeft).toBeInTheDocument();
        expect(iconMonthLeft).toMatchSnapshot();
    });

    it('передали iconNextYear -> отобразится кастомная иконкой след. года', () => {
        const { container } = render(
            <Calendar iconNextYear={<FeatherIcon name="arrow-right" />} />,
        );
        const iconNextYear = container.querySelectorAll('.Calendar__NavigateButton')[3];
        const iconYearRight = iconNextYear.querySelector('.feather-arrow-right');
        expect(iconYearRight).toBeInTheDocument();
        expect(iconYearRight).toMatchSnapshot();
    });

    it('передали iconPrevYear -> отобразится кастомная иконка пред. года', () => {
        const { container } = render(<Calendar iconPrevYear={<FeatherIcon name="arrow-left" />} />);
        const iconPrevYear = container.querySelectorAll('.Calendar__NavigateButton')[0];
        const iconYearLeft = iconPrevYear.querySelector('.feather-arrow-left');
        expect(iconYearLeft).toBeInTheDocument();
        expect(iconYearLeft).toMatchSnapshot();
    });

    it('передали max -> отобразятся даты больше, чем max с классом Calendar__DayButton_disabled', () => {
        const MAX = new Date(2021, 0, 5);
        const { container } = render(<Calendar max={MAX} />);
        const day = container.querySelectorAll('.Calendar__DayWrapper .Calendar__DayButton')[7];
        expect(day).toHaveClass('Calendar__DayButton_disabled');
        expect(day).toMatchSnapshot();
    });
    it('передали max, onChange, выбираем дату, больше max -> не вызван обработчик onChange', () => {
        const MAX = new Date(2021, 0, 5);
        const handleChange = jest.fn();
        const { container } = render(<Calendar max={MAX} onChange={handleChange} />);
        const day = container.querySelectorAll('.Calendar__DayWrapper .Calendar__DayButton')[7];
        userEvent.click(day);
        expect(handleChange).toHaveBeenCalledTimes(0);
    });

    it('передали min -> отобразятся даты меньше, чем min с классом Calendar__DayButton_disabled', () => {
        const MIN = new Date(2021, 0, 5);
        const { container } = render(<Calendar min={MIN} />);
        const day = container.querySelectorAll('.Calendar__DayWrapper .Calendar__DayButton')[3];
        expect(day).toHaveClass('Calendar__DayButton_disabled');
        expect(day).toMatchSnapshot();
    });
    it('передали min, onChange, выбираем дату, меньше min -> не вызван обработчик onChange', () => {
        const MIN = new Date(2021, 0, 5);
        const handleChange = jest.fn();
        const { container } = render(<Calendar min={MIN} onChange={handleChange} />);
        const day = container.querySelectorAll('.Calendar__DayWrapper .Calendar__DayButton')[3];
        userEvent.click(day);
        expect(handleChange).toHaveBeenCalledTimes(0);
    });

    it('передали value -> отобразится переданное значение', () => {
        const DATE = '01.01.2021';
        const { container } = render(<Calendar value={new Date(DATE)} />);
        const day = container.querySelector('.Calendar__DayButton_selected');
        expect(day.textContent).toBe('1');
        const info = container.querySelector('.Calendar__Info');
        expect(info.textContent).toBe('Январь 2021 г.');
    });

    it('передали value, onChange, выбрали дату -> вызван обработчик onChange с выбранной датой', async () => {
        const value = new Date('2021-01-01');
        const mockedDate = new Date(2021, 0, 2);
        const spy = jest.spyOn(global, 'Date').mockImplementation(() => mockedDate);
        Date.now = jest.fn(() => 1609489800000);
        const handleChange = jest.fn();
        const { container } = render(<Calendar value={value} onChange={handleChange} />);
        const newValueDay = container.querySelectorAll('.Calendar__DayButton')[1];
        await userEvent.click(newValueDay);
        expect(handleChange).toHaveBeenCalledTimes(1);
        expect(handleChange).toBeCalledWith(
            new Date('2021-01-02T00:00:00.000Z'),
            expect.any(Object),
        );
        spy.mockRestore();
    });

    it('передали value -> отобразится сегодня 01.01.2021', () => {
        const value = new Date('2021-01-01');
        const { container } = render(<Calendar value={value} />);
        const today = container.querySelector('.Calendar__DayButton_today');
        expect(today.textContent).toBe('1');
    });

    it('передали value -> отобразится 02.01.2021 - выходной день', () => {
        const value = new Date('2021-01-01');
        const { container } = render(<Calendar value={value} />);
        const day = container.querySelector('.Calendar__DayButton_weekday');
        expect(day.textContent).toBe('2');
    });

    it('передали value -> отобразится 10 выходныx дней', () => {
        const value = new Date('2021-01-01');
        const { container } = render(<Calendar value={value} />);
        const weekends = container.querySelectorAll('.Calendar__DayButton_weekday');
        expect(weekends.length).toBe(10);
    });

    it('передали value, клик на след. год -> отобразится январь 2022 ', async () => {
        const value = new Date('2021-01-01');
        const { container } = render(<Calendar value={value} />);
        const iconNextYear = container.querySelector('.feather-chevrons-right');
        await userEvent.click(iconNextYear);
        const info = container.querySelector('.Calendar__Info');
        expect(info.textContent).toBe('Январь 2022 г.');
    });

    it('передали value, клик на пред. год -> отобразится январь 2020 ', async () => {
        const value = new Date('2021-01-01');
        const { container } = render(<Calendar value={value} />);
        const iconPrevYear = container.querySelector('.feather-chevrons-left');
        await userEvent.click(iconPrevYear);
        const info = container.querySelector('.Calendar__Info');
        expect(info.textContent).toBe('Январь 2020 г.', expect.any(Object));
    });

    it('передали value, клик на след. мес -> отобразится февраль 2021 ', async () => {
        const value = new Date('2021-01-01');
        const { container } = render(<Calendar value={value} />);
        const iconNextMonth = container.querySelector('.feather-chevron-right');
        await userEvent.click(iconNextMonth);
        const info = container.querySelector('.Calendar__Info');
        expect(info.textContent).toBe('Февраль 2021 г.');
    });

    it('передали value, клик на пред. мес -> отобразится декабрь 2020 ', async () => {
        const value = new Date('2021-01-01');
        const { container } = render(<Calendar value={value} />);
        const iconPrevMonth = container.querySelector('.feather-chevron-left');
        await userEvent.click(iconPrevMonth);
        const info = container.querySelector('.Calendar__Info');
        expect(info.textContent).toBe('Декабрь 2020 г.', expect.any(Object));
    });

    it('передали value -> отобразится 01.01.2021 в пт ', () => {
        const value = new Date('2021-01-01');
        const { container } = render(<Calendar value={value} />);
        const days = container.querySelector('.Calendar__Days');
        const firstFridayWrapper = days.querySelectorAll('.Calendar__DayWrapper')[4];
        expect(firstFridayWrapper).toBeInTheDocument();
        const friday = firstFridayWrapper.querySelector('button');
        expect(friday.textContent).toBe('1');
    });
});
