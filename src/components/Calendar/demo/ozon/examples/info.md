#  Код

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Calendar, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/ozon/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Calendar />
  </ThemeProvider>,
  document.getElementById('root')
);
```
