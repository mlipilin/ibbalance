# Код

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Calendar, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/tkb/theme';

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <Calendar
            hideYearSwitch
            iconNextMonth={<FeatherIcon name={'arrow-right'} />}
            iconPrevMonth={<FeatherIcon name={'arrow-left'} />}
        />
    </ThemeProvider>,
    document.getElementById('root'),
);
```
