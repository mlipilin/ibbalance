import React from 'react';

import info from './examples/info.md';

// Components
import Calendar from '../../index';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';
import Example from '../../../../../demo/components/Example';
import { withState } from '../../../../hoc';
import dateLimitation from '../default/examples/date-limitation.md';

const CalendarWithState = withState(Calendar);

const DemoCalendar = () => {
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>

            <div id="example">
                <Example readme={info}>
                    <CalendarWithState />
                </Example>
                <Example readme={dateLimitation}>
                    <CalendarWithState
                        value={new Date(2020, 1, 23)}
                        min={new Date(2020, 1, 21)}
                        max={new Date(2020, 1, 25)}
                    />
                </Example>
            </div>
        </DemoComponent>
    );
};

export default DemoCalendar;
