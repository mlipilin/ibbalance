#  Код

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Calendar, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/absolut/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Calendar value={new Date(2020, 1, 23)} />
  </ThemeProvider>,
  document.getElementById('root')
);
```
