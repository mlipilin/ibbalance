# Calendar

Календарь

## Когда использовать

Всякий раз, когда нам нужно выбрать дату

## Примеры использования

## Свойства

Название | Описание | Тип | Значение по умолчанию
--- | --- | --- | ---
**hideYearSwitch**|Доступлно ли переключение года|*bool*|`false`
**iconNextMonth**|Иконка (компонент), которая переключает следующий месяц|*object*|`<FeatherIcon name={'chevron-right'} />`
**iconNextYear**|Иконка (компонент), которая переключает следующий год|*object*|`<FeatherIcon name={'chevrons-right'} />`
**iconPrevMonth**|Иконка (компонент), которая переключает предыдущий месяц|*object*|`<FeatherIcon name={'chevron-left'} />`
**iconPrevYear**|Иконка (компонент), которая переключает предыдущий год|*object*|`<FeatherIcon name={'chevrons-left'} />`
**value**|Начальное значение |*date*|`null`
**onChange**|Обработчик события "click"|*func*|`(date, props) => {}`