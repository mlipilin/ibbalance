import React from 'react';
import { storiesOf } from '@storybook/react';
import { text, withKnobs } from '@storybook/addon-knobs';

import Calendar from '../index';

import notes from '../readme.md';
import { withState } from '../../../hoc';

const CalendarWithState = withState(Calendar);
const stories = storiesOf('Calendar');

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories.add('Simple calendar', () => <CalendarWithState value="2020-01-29T11:51:14.470Z" />, {
    notes,
});
stories.add(
    'Limit calendar',
    () => (
        <CalendarWithState
            value="2020-01-29T11:51:14.470Z"
            min={text('min', '05.01.2020')}
            max={text('max', '30.01.2020')}
        />
    ),
    {
        notes,
    },
);
