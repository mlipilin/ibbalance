import React, { useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import moment from 'moment';

// Components
import FeatherIcon from '../FeatherIcon';
import { Text } from '../../index'; // eslint-disable-line

// Constants
import { DAYS, MONTH } from './constants';
import { withTheme } from '../../theme-provider';

// Helpers
import { isDateBetweenMinAndMax } from '../../helpers/date';

const Calendar = props => {
    const {
        className,
        iconNextMonth = <FeatherIcon name="chevron-right" />,
        iconNextYear = <FeatherIcon name="chevrons-right" />,
        iconPrevMonth = <FeatherIcon name="chevron-left" />,
        iconPrevYear = <FeatherIcon name="chevrons-left" />,
        hideYearSwitch = false,
        max = null,
        min = null,
        value = null,
        applyTheme = () => {},
        onChange = () => {},
    } = props;
    const [state, setState] = useState({
        daysInMonth: 0,
        firstMonthDay: 1,
        month: null,
        year: null,
    });

    const getDate = m => {
        return {
            daysInMonth: m.daysInMonth(),
            firstMonthDay: m.set('date', 1).isoWeekday(),
            month: m.get('month'),
            year: m.get('year'),
        };
    };

    const handleClickSelectDate = day => () => {
        const date = moment(new Date(state.year, state.month, day));
        if (!isDateBetweenMinAndMax(date, min, max)) {
            return;
        }
        onChange(new Date(state.year, state.month, day), props);
    };

    const handleClickPrevMonth = () => {
        changeMonth('prev');
    };

    const handleClickPrevYear = () => {
        changeYear('prev');
    };

    const handleClickNextMonth = () => {
        changeMonth('next');
    };

    const handleClickNextYear = () => {
        changeYear('next');
    };

    const changeMonth = (param = 'next') => {
        setState(
            getDate(
                moment()
                    .set('year', state.year)
                    .set('month', param === 'next' ? state.month + 1 : state.month - 1),
            ),
        );
    };

    const changeYear = (param = 'next') => {
        setState(
            getDate(
                moment()
                    .set('year', param === 'next' ? state.year + 1 : state.year - 1)
                    .set('month', state.month),
            ),
        );
    };

    const componentClass = applyTheme(cn('Calendar'), className);
    const contentClass = applyTheme(cn('Calendar__Content'));
    const dayWrapperClass = applyTheme(cn('Calendar__DayWrapper'));
    const dayNameClass = applyTheme(cn('Calendar__DayName'));
    const daysClass = applyTheme(cn('Calendar__Days'));
    const headerClass = applyTheme(cn('Calendar__ContentHeader'));
    const infoClass = applyTheme(cn('Calendar__Info'));
    const navigateButtonClass = applyTheme(cn('Calendar__NavigateButton'));
    const navigationClass = applyTheme(cn('Calendar__Navigation'));

    useEffect(() => {
        setState(value ? getDate(moment(new Date(value))) : getDate(moment()));
    }, [value]);

    const days = useMemo(() => {
        const array = new Array(state.daysInMonth).fill(1);
        const additionalStart = new Array(state.firstMonthDay - 1).fill(0);

        return [...additionalStart, ...array];
    }, [state.firstMonthDay, state.daysInMonth]);

    const selected = useMemo(() => {
        if (value) {
            const m = moment(value);
            return { year: m.get('year'), month: m.get('month'), day: m.get('date') };
        }
        return { year: null, month: null, day: null };
    }, [value]);

    const today = useMemo(() => {
        const m = moment();
        return { year: m.get('year'), month: m.get('month'), day: m.get('date') };
    }, []);

    return (
        <>
            <div className={componentClass}>
                <div className={navigationClass}>
                    {!hideYearSwitch && (
                        <button
                            className={navigateButtonClass}
                            onClick={handleClickPrevYear}
                            type="button"
                        >
                            {iconPrevYear}
                        </button>
                    )}
                    <button
                        className={navigateButtonClass}
                        onClick={handleClickPrevMonth}
                        type="button"
                    >
                        {iconPrevMonth}
                    </button>
                    <Text className={infoClass}>{`${MONTH[state.month]} ${state.year} г.`}</Text>
                    <button
                        className={navigateButtonClass}
                        onClick={handleClickNextMonth}
                        type="button"
                    >
                        {iconNextMonth}
                    </button>
                    {!hideYearSwitch && (
                        <button
                            className={navigateButtonClass}
                            onClick={handleClickNextYear}
                            type="button"
                        >
                            {iconNextYear}
                        </button>
                    )}
                </div>
                <div className={contentClass}>
                    <div className={headerClass}>
                        {DAYS.map(item => (
                            <div className={dayNameClass} key={item}>
                                {item}
                            </div>
                        ))}
                    </div>
                    <div className={daysClass}>
                        {days.map((item, key) => {
                            const day = key - state.firstMonthDay + 2;
                            const isSelected =
                                selected.year === state.year &&
                                selected.month === state.month &&
                                selected.day === day;
                            const isToday =
                                today.year === state.year &&
                                today.month === state.month &&
                                today.day === day;
                            const isDisabled = !isDateBetweenMinAndMax(
                                new Date(state.year, state.month, day),
                                min,
                                max,
                            );

                            return (
                                <div className={dayWrapperClass} key={day}>
                                    {item > 0 && (
                                        <button
                                            type="button"
                                            className={applyTheme(
                                                cn('Calendar__DayButton', {
                                                    Calendar__DayButton_selected: isSelected,
                                                    Calendar__DayButton_weekday: [5, 6].includes(
                                                        key % 7,
                                                    ),
                                                    Calendar__DayButton_today: isToday,
                                                    Calendar__DayButton_disabled: isDisabled,
                                                }),
                                            )}
                                            onClick={handleClickSelectDate(day)}
                                        >
                                            {day}
                                        </button>
                                    )}
                                </div>
                            );
                        })}
                    </div>
                </div>
            </div>
        </>
    );
};

Calendar.propTypes = {
    className: PropTypes.string,
    hideYearSwitch: PropTypes.bool,
    iconNextMonth: PropTypes.element,
    iconNextYear: PropTypes.element,
    iconPrevMonth: PropTypes.element,
    iconPrevYear: PropTypes.element,
    max: PropTypes.instanceOf(Date),
    min: PropTypes.instanceOf(Date),
    value: PropTypes.instanceOf(Date),

    applyTheme: PropTypes.func,
    onChange: PropTypes.func,
};

Calendar.defaultProps = {
    className: '',
    hideYearSwitch: false,
    iconNextMonth: <FeatherIcon name="chevron-right" />,
    iconNextYear: <FeatherIcon name="chevrons-right" />,
    iconPrevMonth: <FeatherIcon name="chevron-left" />,
    iconPrevYear: <FeatherIcon name="chevrons-left" />,
    max: null,
    min: null,
    value: null,

    applyTheme: _ => _,
    onChange: (date, props) => {}, // eslint-disable-line no-unused-vars
};

export default withTheme(Calendar);
