import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

// Constants
import { SIZES } from '../../constants/props';

import { withTheme } from '../../theme-provider';

@withTheme
class Radio extends Component {
    handleChange = e => {
        e.preventDefault();
        const { disabled, onChange } = this.props;
        if (!disabled) {
            onChange(e.target.value, this.props);
        }
    };

    render() {
        const {
            className,
            checked,
            children,
            disabled,
            name,
            size,
            value,
            applyTheme,
            onChange,
            ...otherProps
        } = this.props;

        const componentClass = applyTheme(
            cn('Radio__Label', `Radio__Label_size_${size}`, {
                Radio__Label_checked: checked,
                Radio__Label_disabled: disabled,
            }),
            className,
        );

        return (
            <>
                <label className={componentClass}>
                    <input
                        {...otherProps}
                        checked={checked}
                        disabled={disabled}
                        name={name}
                        type="radio"
                        value={value}
                        onChange={this.handleChange}
                    />
                    {children}
                </label>
            </>
        );
    }
}

Radio.propTypes = {
    checked: PropTypes.bool,
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    disabled: PropTypes.bool,
    field: PropTypes.string,
    name: PropTypes.string,
    className: PropTypes.string,
    size: PropTypes.oneOf([SIZES.S, SIZES.M, SIZES.L]),
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    applyTheme: PropTypes.func,
    onChange: PropTypes.func,
};

Radio.defaultProps = {
    checked: false,
    children: null,
    disabled: false,
    field: null,
    name: '',
    className: '',
    size: SIZES.M,
    value: '',
    applyTheme: _ => _,
    onChange: (value, props) => {}, // eslint-disable-line no-unused-vars
};

export default Radio;
