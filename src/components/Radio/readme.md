## Usage

```jsx
<Radio size="s"/>
```

With content:

```jsx
<Radio size="m">
    Check me
</Radio>
```

## Props

Prop name | Prop type | Default value
--- | --- | ---
*checked*|bool|`false`
*disabled*|bool|`false`
*name*|string|
*size*|`s`, `m`|`m`
*value*|string, number|`m`
*onChange*|func|`_ => _`

## CSS Selectors

```css
.Radio {}

/* Label */
.Radio__Label {}
.Radio__Label:after {}
.Radio__Label:before {}

/* Label: By size */
.Radio__Label_size_s {}
.Radio__Label_size_m {}

/* Label: Booleans */
.Radio__Label_checked {}
.Radio__Label_disabled {}
```
