import React, { useState } from 'react';

// Components
import Radio from '../../index';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';

// Examples
import disabled from './examples/disabled.md';
import size from './examples/size.md';

// Wrappers
import { Col, Row } from '../../../../wrappers/WGrid';
import { SIZES } from '../../../../constants/props';

const RadioButtonsSize = () => {
    const [currentValue, setCurrentValue] = useState('size-s');
    const onChange = value => setCurrentValue(value);

    return (
        <>
            <Radio
                checked={currentValue === 'size-s'}
                name="sizes"
                disabled={false}
                size={SIZES.S}
                value="size-s"
                onChange={onChange}
            >
                Radio S
            </Radio>
            <br />
            <Radio
                checked={currentValue === 'size-m'}
                name="sizes"
                disabled={false}
                size={SIZES.M}
                value="size-m"
                onChange={onChange}
            >
                Radio M
            </Radio>
        </>
    );
};

const RadioButtonsDisable = () => {
    const [currentValue, setCurrentValue] = useState('alfa');
    const onChange = value => setCurrentValue(value);

    return (
        <>
            <Radio
                checked={currentValue === 'alfa'}
                name="bank"
                disabled
                value="alfa"
                onChange={onChange}
            >
                Альфа-Банк
            </Radio>
            <br />
            <Radio
                checked={currentValue === 'absolut'}
                name="bank"
                disabled
                value="absolut"
                onChange={onChange}
            >
                Абсолют Банк
            </Radio>
        </>
    );
};

const DemoRadio = () => {
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>

            <div id="example">
                <Row
                    cols={{ mobile: [1], tab: [1, 1], desktop: [1], largeDesktop: [1, 1] }}
                    gap="20px"
                >
                    <Col>
                        <Example readme={size}>
                            <RadioButtonsSize />
                        </Example>
                    </Col>
                    <Col>
                        <Example readme={disabled}>
                            <RadioButtonsDisable />
                        </Example>
                    </Col>
                </Row>
            </div>
        </DemoComponent>
    );
};

export default DemoRadio;
