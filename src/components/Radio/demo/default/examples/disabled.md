# "disabled" состояние

`true`: Radio button становится 'disabled' (выключенный)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Radio, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/default/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <div>
      <Radio disabled={true} checked>Альфа-Банк</Radio>
      <Radio disabled={true}>Абсолют Банк</Radio>
    </div>
  </ThemeProvider>,
  document.getElementById('root')
);
```
