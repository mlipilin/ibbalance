# Размер радиобаттона

Свойство size может принимать одно из следующих значений: `s` и `m`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Radio, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/alfa/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <div>
      <Radio size="s">Radio S</Radio>
      <Radio size="m">Radio M</Radio>
      <Radio size="l">Radio L</Radio>
    </div>
  </ThemeProvider>,
  document.getElementById('root')
);
```
