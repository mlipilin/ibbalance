import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean, select } from '@storybook/addon-knobs';

// Constants
import { LOCAL_STORAGE_THEME_KEY } from '../../../constants/local-storage';
import { SIZES } from '../../../constants/props';
import { THEMES } from '../../../constants/themes';

import Radio from '../index';

import notes from '../readme.md';

const theme = localStorage.getItem(LOCAL_STORAGE_THEME_KEY);

let sizesList = [];

switch (theme) {
    case THEMES.DEFAULT:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.ALFA:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.ABSOLUT:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.SPB:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.TKB:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.TKB_LKZ:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.KYC:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.KYC_LANDING:
        sizesList = [SIZES.S, SIZES.M];
        break;
    default:
        break;
}

const stories = storiesOf('Radio', module);

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

const RadioButtons = () => {
    const [currentValue, setCurrentValue] = useState('alfa');
    const onChange = value => setCurrentValue(value);

    return (
        <>
            <Radio
                checked={currentValue === 'alfa'}
                name="bank"
                disabled={boolean('Disabled', false)}
                size={select('Size', sizesList, SIZES.M)}
                value="alfa"
                onChange={onChange}
            >
                Альфа-Банк
            </Radio>
            <Radio
                checked={currentValue === 'absolut'}
                name="bank"
                disabled={boolean('Disabled', false)}
                size={select('Size', sizesList, SIZES.M)}
                value="absolut"
                onChange={onChange}
            >
                Абсолют Банк
            </Radio>
            <Radio
                checked={currentValue === 'tkb'}
                name="bank"
                disabled={boolean('Disabled', false)}
                size={select('Size', sizesList, SIZES.M)}
                value="tkb"
                onChange={onChange}
            >
                ТрансКапиталБанк
            </Radio>
        </>
    );
};

stories.add('Default', () => <RadioButtons />, { notes });
