import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import Radio from './index';

import { render } from '../../hoc/with-theme';

describe('Тестирование компонента Radio', () => {
    it('Должен отобразиться Radio', () => {
        const { container } = render(<Radio />);
        expect(screen.getByRole('radio')).toBeInTheDocument();
        expect(screen.getByLabelText('')).toBeInTheDocument();
        expect(screen.getByRole('radio')).not.toBeChecked();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Radio размере "m" по-умолчанию', () => {
        const { container } = render(<Radio />);
        const label = container.querySelector('label');
        expect(label).toBeInTheDocument();
        expect(label.classList.contains('Radio__Label_size_m')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Radio размере "s"', () => {
        const { container } = render(<Radio size="s" />);
        const label = container.querySelector('label');
        expect(label).toBeInTheDocument();
        expect(label.classList.contains('Radio__Label_size_s')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Radio размере "m"', () => {
        const { container } = render(<Radio size="m" />);
        const label = container.querySelector('label');
        expect(label).toBeInTheDocument();
        expect(label.classList.contains('Radio__Label_size_m')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Radio с классом "radio__style"', () => {
        const { container } = render(<Radio className="radio__style" />);
        const label = container.querySelector('label');
        expect(label).toBeInTheDocument();
        expect(label.classList.contains('radio__style')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Radio в состоянии checked', () => {
        const { container } = render(<Radio checked />);
        const input = container.querySelector('input');
        expect(input).toBeInTheDocument();
        expect(input.hasAttribute('checked')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Radio в состоянии checked', () => {
        const { container } = render(<Radio checked />);
        const input = container.querySelector('input');
        expect(input).toBeInTheDocument();
        expect(input.hasAttribute('checked')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Radio с методом onChange', async () => {
        const onChange = jest.fn();
        const { container } = render(<Radio onChange={onChange} value="isData" />);
        const input = container.querySelector('input');
        expect(input).toBeInTheDocument();
        await userEvent.click(input);
        expect(onChange).toBeCalledTimes(1);
        expect(onChange).toBeCalledWith('isData', expect.any(Object));
    });

    it('Должен отобразиться Radio в состоянии disabled', () => {
        const onChange = jest.fn();
        const { container } = render(<Radio disabled onChange={onChange} />);
        const input = container.querySelector('input');
        expect(container.firstChild).toMatchSnapshot();
        expect(input).toBeInTheDocument();
        expect(input.hasAttribute('disabled')).toBeTruthy();
        userEvent.click(input);
        expect(onChange).toBeCalledTimes(0);
    });

    it('Должен отобразиться 2 Radio с именем name = "size" и значениями 50 и "m"', () => {
        const { container } = render(
            <>
                <Radio name="size" value={50} />
                <Radio name="size" value="m" />
            </>,
        );
        const radioValueIsNumber = container.querySelector('input[value="50"]');
        expect(radioValueIsNumber).toBeInTheDocument();
        expect(radioValueIsNumber.getAttribute('name')).toBe('size');
        const radioValueIsString = container.querySelector('input[value="m"]');
        expect(radioValueIsString).toBeInTheDocument();
        expect(radioValueIsString.getAttribute('name')).toBe('size');
        expect(container).toMatchSnapshot();
    });

    it('Должен отобразиться Radio в состоянии focus', () => {
        const onFocus = jest.fn();
        const { container } = render(<Radio name="size" value="s" onFocus={onFocus} />);
        const input = container.querySelector('input');
        input.focus();
        expect(input).toHaveFocus();
        expect(onFocus).toHaveBeenCalledTimes(1);
    });

    it('Должен отобразиться 2 Radio, выбран value="m"', async () => {
        let checkedValueS = true;
        let checkedValueM = false;
        const onChange = value => {
            checkedValueS = value === 's';
            checkedValueM = value === 'm';
        };
        const { container } = render(
            <>
                <Radio name="size" value="s" checked={checkedValueS} onChange={onChange} />
                <Radio name="size" value="m" checked={checkedValueM} onChange={onChange} />
            </>,
        );

        const inputS = container.querySelector('input[value="s"]');
        const inputM = container.querySelector('input[value="m"]');

        inputS.focus();
        await userEvent.type(inputM, '{arrowdown}{space}');

        expect(checkedValueS).toBeFalsy();
        expect(checkedValueM).toBeTruthy();
    });
});
