import { sliderValueToValue, valueToSliderValue } from './helpers';

describe('src/components/InputSlider/helpers.js', () => {
    describe('sliderValueToValue', () => {
        it('undefined -> null', () => {
            expect(sliderValueToValue()).toBeNull();
        });
        it('null -> null', () => {
            expect(sliderValueToValue(null)).toBeNull();
        });
        it('Number -> String(Number)', () => {
            expect(sliderValueToValue(123)).toBe('123');
        });
    });
    describe('valueToSliderValue', () => {
        it('undefined -> null', () => {
            expect(valueToSliderValue()).toBeNull();
        });
        it('null -> null', () => {
            expect(valueToSliderValue(null)).toBeNull();
        });
        it('"" -> null', () => {
            expect(valueToSliderValue('')).toBeNull();
        });
        it('"not number string" -> null', () => {
            expect(valueToSliderValue('not number string')).toBeNull();
        });
        it('"123" -> 123', () => {
            expect(valueToSliderValue('123')).toBe(123);
        });
        it('"123.1" -> 123.1', () => {
            expect(valueToSliderValue('123.1')).toBe(123.1);
        });
        it('"123." -> 123.', () => {
            expect(valueToSliderValue('123.')).toBe(123);
        });
        it('"123," -> 123.', () => {
            expect(valueToSliderValue('123,')).toBe(123);
        });
        it('"123aaa.1aaa" -> 123.1', () => {
            expect(valueToSliderValue('123aaa.1aaa')).toBe(123.1);
        });
        it('"123aaa.a1aaa" -> 123.1', () => {
            expect(valueToSliderValue('123aaa.a1aaa')).toBe(123.1);
        });
        it('"123.123" -> 123.12', () => {
            expect(valueToSliderValue('123.123')).toBe(123.12);
        });
        it('"123.127" -> 123.12', () => {
            expect(valueToSliderValue('123.127')).toBe(123.12);
        });
        it('"-123.127" -> -123.12', () => {
            expect(valueToSliderValue('-123.127')).toBe(-123.12);
        });
        it('"-" -> null', () => {
            expect(valueToSliderValue('-')).toBeNull();
        });
        it('"---" -> null', () => {
            expect(valueToSliderValue('---')).toBeNull();
        });
    });
});
