import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean, number, select, text } from '@storybook/addon-knobs';

// Constants
import { LOCAL_STORAGE_THEME_KEY } from '../../../constants/local-storage';
import { SIZES, TYPES } from '../../../constants/props';
import { THEMES } from '../../../constants/themes';

// HOC
import { withState } from '../../../hoc';

// Utils
import { formatPriceNumber, parsePriceNumber } from '../../../utils/text';

import InputSlider from '../index';

import notes from '../readme.md';

const InputSliderWithState = withState(InputSlider);
const theme = localStorage.getItem(LOCAL_STORAGE_THEME_KEY);

let sizesList = [];
let typesList = [];

switch (theme) {
    case THEMES.DEFAULT:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        typesList = [TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    case THEMES.ALFA:
        sizesList = [SIZES.S, SIZES.M];
        typesList = [TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    case THEMES.ABSOLUT:
        sizesList = [SIZES.S, SIZES.M];
        typesList = [TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    case THEMES.SPB:
        sizesList = [SIZES.S, SIZES.M];
        typesList = [TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    case THEMES.TKB:
        sizesList = [SIZES.S, SIZES.M];
        typesList = [TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    case THEMES.TKB_LKZ:
        sizesList = [SIZES.S, SIZES.M];
        typesList = [TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    case THEMES.KYC:
        sizesList = [SIZES.S, SIZES.M];
        typesList = [TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    case THEMES.KYC_LANDING:
        sizesList = [SIZES.S, SIZES.M];
        typesList = [TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    default:
        break;
}

const stories = storiesOf('InputSlider', module);

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '30px' }}>{store()}</div>);

stories.add(
    'Default',
    () => (
        <>
            <InputSliderWithState
                disabled={boolean('Disabled', false)}
                error={text('Error', '')}
                label={text('Label', 'Сумма кредита, руб')}
                max={number('Max', 20000000)}
                min={number('Min', 1000000)}
                readOnly={boolean('Read only', false)}
                size={select('Size', sizesList, SIZES.M)}
                step={number('Step', 100000)}
                success={boolean('Success', false)}
                type={select('Type', typesList, TYPES.PRIMARY)}
                value={text('Value', '8000000')}
                formatValue={formatPriceNumber}
                parseValue={parsePriceNumber}
            />
            <br />
            <InputSliderWithState
                disabled={boolean('Disabled', false)}
                error={text('Error', '')}
                label={text('Label', 'Сумма кредита, руб')}
                max={number('Max', 20000000)}
                min={number('Min', 1000000)}
                readOnly={boolean('Read only', false)}
                size={select('Size', sizesList, SIZES.L)}
                step={number('Step', 100000)}
                success={boolean('Success', false)}
                type={select('Type', typesList, TYPES.PRIMARY)}
                value={text('Value', '8000000')}
                formatValue={formatPriceNumber}
                parseValue={parsePriceNumber}
            />
        </>
    ),
    { notes },
);
