import React from 'react';
import { screen, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import FeatherIcon from '../FeatherIcon';

import InputSlider from './index';

import { render } from '../../hoc/with-theme';

describe('Тестирование компонента InputSlider', () => {
    it('Должен отобразиться InputSlider без заголовка и placeholder', () => {
        const { container } = render(<InputSlider />);
        expect(screen.getByPlaceholderText('')).toBeInTheDocument();
        expect(screen.getByLabelText('')).toBeInTheDocument();
        expect(screen.getByDisplayValue('')).toBeInTheDocument();
        const slider = container.querySelector('.Slider');
        expect(slider).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться InputSlider с label = "Стоимость" ', () => {
        const { container } = render(<InputSlider label="Стоимость" />);
        expect(screen.getByPlaceholderText('')).toBeInTheDocument();
        expect(screen.getByLabelText('Стоимость')).toBeInTheDocument();
        expect(screen.getByDisplayValue('')).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться InputSlider с label = "<span>Стоимость</span>" ', () => {
        const { container } = render(<InputSlider label={<span>Стоимость</span>} />);
        expect(screen.getByLabelText('Стоимость')).toBeInTheDocument();
        const textElement = container.querySelector('.Input__LabelText > span');
        expect(textElement.textContent).toBe('Стоимость');
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться InputSlider с label = "[<span>Стоимость</span>,<strong>недвижимости</strong>]" ', () => {
        const { container } = render(
            <InputSlider
                label={[
                    <span key="price_part1">Стоимость</span>,
                    <strong key="price_part2">недвижимости</strong>,
                ]}
            />,
        );
        expect(screen.getByText('Стоимость')).toBeInTheDocument();
        expect(screen.getByText('недвижимости')).toBeInTheDocument();
        const textElement = container.querySelector('.Input__LabelText');
        const textSurname = textElement.querySelector('span');
        expect(textSurname.textContent).toBe('Стоимость');
        const textName = textElement.querySelector('strong');
        expect(textName.textContent).toBe('недвижимости');
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться InputSlider с placeholder = "Введите сумму кредита" ', () => {
        const { container } = render(<InputSlider placeholder="Введите сумму кредита" />);
        const input = container.querySelector('input');
        expect(input.placeholder).toBe('Введите сумму кредита');
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться InputSlider с классом "InputSlider__Style" ', () => {
        const { container } = render(<InputSlider className="InputSlider__Style" />);
        const elemWithInputClass = container.querySelector('.InputSlider__Style');
        expect(elemWithInputClass).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться InputSlider с size = "m" по-умолчанию ', () => {
        const { container } = render(<InputSlider />);
        const elemWithInputClass = container.querySelector('.Input');
        expect(elemWithInputClass).toMatchSnapshot();
        expect(elemWithInputClass.classList.contains('Input_size_m')).toBeTruthy();
        const slider = container.querySelector('.Slider');
        expect(slider.classList.contains('Slider_size_m')).toBeTruthy();
    });

    it('Должен отобразиться InputSlider с size = "s" ', () => {
        const { container } = render(<InputSlider size="s" />);
        const elemWithInputClass = container.querySelector('.Input');
        expect(elemWithInputClass).toMatchSnapshot();
        expect(elemWithInputClass.classList.contains('Input_size_s')).toBeTruthy();
        const slider = container.querySelector('.Slider');
        expect(slider.classList.contains('Slider_size_s')).toBeTruthy();
    });

    it('Должен отобразиться InputSlider с size = "m" ', () => {
        const { container } = render(<InputSlider size="m" />);
        const elemWithInputClass = container.querySelector('.Input');
        expect(elemWithInputClass).toMatchSnapshot();
        expect(elemWithInputClass.classList.contains('Input_size_m')).toBeTruthy();
        const slider = container.querySelector('.Slider');
        expect(slider.classList.contains('Slider_size_m')).toBeTruthy();
    });

    it('Должен отобразиться InputSlider с size = "l" ', () => {
        const { container } = render(<InputSlider size="l" />);
        const elemWithInputClass = container.querySelector('.Input');
        expect(elemWithInputClass).toMatchSnapshot();
        expect(elemWithInputClass.classList.contains('Input_size_l')).toBeTruthy();
        const slider = container.querySelector('.Slider');
        expect(slider.classList.contains('Slider_size_l')).toBeTruthy();
    });

    it('Должен отобразиться InputSlider с форматированным значением согласно маске 999-999', () => {
        const { container } = render(
            <InputSlider label="Код подразделения" mask="999-999" value="1234567890" />,
        );
        const input = container.querySelector('input');
        expect(input.value).toBe('123-456');
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Проверка работы formatValue и parseValue', async () => {
        const handleChange = jest.fn();
        const { container } = render(
            <InputSlider
                label="Стоимость"
                value="1000000"
                formatValue={value => String(value).replace(/(.)(?=(\d{3})+$)/g, '$1 ')}
                parseValue={value => value.replace(/\s/g, '')}
                onChange={handleChange}
            />,
        );
        const input = container.querySelector('input');
        await userEvent.type(
            input,
            '{arrowright}{arrowright}{arrowright}{arrowright}{arrowright}{arrowright}{arrowright}{arrowright}{arrowright}1',
        );
        expect(handleChange).toBeCalledTimes(1);
        expect(handleChange).toBeCalledWith('10000001', expect.any(Object));
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться InputSlider в состоянии disabled', () => {
        const onChange = jest.fn();
        const { container } = render(
            <InputSlider label="Disabled поле" onChange={onChange} disabled />,
        );
        const inputContainer = container.querySelector('.Input_disabled');
        expect(inputContainer).toBeInTheDocument();
        const input = container.querySelector('input');
        expect(input.disabled).toBeTruthy();
        const iconContainer = container.querySelector('.Input__DisabledIcon');
        expect(iconContainer).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
        userEvent.type(input, 'text for disabled');
        const slider = container.querySelector('.Slider');
        expect(slider.classList.contains('Slider_disabled')).toBeTruthy();
        expect(onChange).toBeCalledTimes(0);
    });

    it('Должен отобразиться InputSlider в состоянии read only', () => {
        const onChange = jest.fn();
        const { container } = render(
            <InputSlider label="Readonly поле" onChange={onChange} readOnly />,
        );
        const input = container.querySelector('input');
        expect(input.readOnly).toBeTruthy();
        expect(input.classList.contains('Input__Input_readonly')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
        userEvent.type(input, 'text for readonly');
        const slider = container.querySelector('.Slider');
        expect(slider.classList.contains('Slider_readonly')).toBeTruthy();
        expect(onChange).toBeCalledTimes(0);
    });

    it('Должен отобразиться InputSlider с ошибкой "Данный e-mail не существует"', () => {
        const { container } = render(
            <InputSlider
                label="Невалидное поле"
                value="admin@example.com"
                error="Данный e-mail не существует"
            />,
        );
        const input = container.querySelector('input');
        expect(input.classList.contains('Input__Input_error')).toBeTruthy();
        const error = container.querySelector('.Input__Error');
        expect(error).toBeInTheDocument();
        expect(screen.getByText('Данный e-mail не существует')).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться InputSlider с ошибкой', () => {
        const { container } = render(
            <InputSlider label="Невалидное поле" value="admin@example.com" error />,
        );
        const input = container.querySelector('input');
        expect(input.classList.contains('Input__Input_error')).toBeTruthy();
        const error = container.querySelector('.Input__Error');
        expect(error).toBeInTheDocument();
        expect(error.textContent).toBe('');
    });

    it('Должен отобразиться InputSlider с ошибкой - node', () => {
        const { container } = render(
            <InputSlider
                label="Невалидное поле"
                value="admin@example.com"
                error={
                    <>
                        <FeatherIcon name="alert-triangle" />
                        <span>Данный e-mail не существует</span>
                    </>
                }
            />,
        );
        const input = container.querySelector('input');
        expect(input.classList.contains('Input__Input_error')).toBeTruthy();
        const error = container.querySelector('.Input__Error');
        expect(error).toBeInTheDocument();
        expect(error.textContent).toBe('Данный e-mail не существует');
        const errorIcon = container.querySelector('.feather-alert-triangle');
        expect(errorIcon).toBeInTheDocument();
    });

    it('Должен отобразиться InputSlider в состоянии success', () => {
        const { container } = render(
            <InputSlider label="Валидное поле" value="admin@example.com" success />,
        );
        const input = container.querySelector('input');
        expect(input.classList.contains('Input__Input_success')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться InputSlider со значением "10000"', () => {
        const onChange = jest.fn();
        const { container, rerender } = render(
            <InputSlider label="Стоимость" onChange={onChange} />,
        );
        const input = screen.getByLabelText(/Стоимость/i);
        const newValue = '10000';
        userEvent.type(input, newValue);
        expect(onChange).toHaveBeenCalledTimes(newValue.length);
        rerender(<InputSlider label="Стоимость" value={newValue} onChange={onChange} />);
        expect(input).toHaveValue(newValue);
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться InputSlider со значением "100"', async () => {
        let inputValue = '';
        const onChange = value => {
            inputValue += value;
        };
        const { container, rerender } = render(
            <InputSlider label="Стоимость" onChange={onChange} value={inputValue} />,
        );
        const input = container.querySelector('input');
        const newValue = '100';
        await userEvent.type(input, newValue);
        expect(inputValue).toBe(newValue);
        rerender(<InputSlider label="Стоимость" onChange={onChange} value={inputValue} />);
        expect(input.value).toBe(newValue);
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться InputSlider со числовым значением 123 ', () => {
        const numberValue = 123;
        const { container } = render(<InputSlider label="Номер" value={numberValue} />);
        const input = container.querySelector('input');
        expect(input).toHaveValue(numberValue.toString());
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться InputSlider в состоянии focus', () => {
        const handleFocus = jest.fn();
        const { container } = render(<InputSlider label="Стоимость" onFocus={handleFocus} />);
        const input = screen.getByLabelText(/Стоимость/i);
        userEvent.click(input);
        expect(input).toHaveFocus();
        expect(handleFocus).toHaveBeenCalledTimes(1);
        expect(handleFocus).toHaveBeenCalledWith(expect.any(Object));
        const elem = container.querySelector('.Input__LabelText_focus');
        expect(elem).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться InputSlider в состоянии blur', () => {
        const onBlur = jest.fn();
        const { container } = render(<InputSlider label="Стоимость" onBlur={onBlur} />);
        const input = screen.getByLabelText(/Стоимость/i);
        input.focus();
        userEvent.tab();
        const elem = container.querySelector('.Input__LabelText_focus');
        expect(elem).not.toBeInTheDocument();

        expect(onBlur).toHaveBeenCalledTimes(1);
    });

    it('Должен отобразиться InputSlider с типом контрола "primary" по-умолчанию', () => {
        const { container } = render(<InputSlider />);
        const slider = container.querySelector('.Slider');
        expect(slider).toBeInTheDocument();
        expect(slider.classList.contains('Slider_type_primary')).toBeTruthy();
    });
    it('Должен отобразиться InputSlider с типом контрола "primary"', () => {
        const { container } = render(<InputSlider type="primary" />);
        const slider = container.querySelector('.Slider');
        expect(slider).toBeInTheDocument();
        expect(slider.classList.contains('Slider_type_primary')).toBeTruthy();
    });
    it('Должен отобразиться InputSlider с типом контрола "secondary"', () => {
        const { container } = render(<InputSlider type="secondary" />);
        const slider = container.querySelector('.Slider');
        expect(slider).toBeInTheDocument();
        expect(slider.classList.contains('Slider_type_secondary')).toBeTruthy();
    });

    it('Должен отобразиться InputSlider с минимальным значением 49 и максимальным 51', () => {
        const { container } = render(<InputSlider min={49} max={51} value={50} />);
        const track = container.querySelector('.Slider__Track');
        expect(track).toHaveStyle('width: 50%');
        expect(container.firstChild).toMatchSnapshot();
    });
    it('Должен отобразиться InputSlider со значением 50', () => {
        const { container } = render(<InputSlider min={49} max={51} value={50} />);
        const button = container.querySelector('button');
        expect(button).toHaveStyle('left: 50%');
    });
    it('Должен отобразиться InputSlider со значением по-умолчанию 0', () => {
        const { container } = render(<InputSlider />);
        const track = container.querySelector('.Slider__Track');
        const button = container.querySelector('button');
        expect(track).toHaveStyle('width: 0%');
        expect(button).toHaveStyle('left: 0%');
    });
    it('Должен отобразиться InputSlider с шагом 50', () => {
        const { container } = render(<InputSlider step={50} value={60} />);
        const track = container.querySelector('.Slider__Track');
        const button = container.querySelector('button');
        expect(track).toHaveStyle('width: 50%');
        expect(button).toHaveStyle('left: 50%');
    });
    it('Должен отобразиться InputSlider onBlur через изменение slider', async () => {
        let value = 0;
        const handleBlur = jest.fn();
        const handleChange = jest.fn(newValue => {
            value = newValue;
        });
        const { container } = render(
            <InputSlider value={value} onBlur={handleBlur} onChange={handleChange} />,
        );
        const button = container.querySelector('button');
        const rail = container.querySelector('.Slider__Rail');
        expect(rail).toBeInTheDocument();
        rail.getBoundingClientRect = jest.fn(() => {
            return { width: 400, height: 10, top: 0, left: 0, bottom: 0, right: 0 };
        });

        await fireEvent.mouseDown(button);
        await fireEvent.mouseMove(button, {
            clientX: 200,
        });

        await fireEvent.mouseUp(button);

        await userEvent.tab();
        expect(handleBlur).toBeCalledTimes(1);
        expect(handleBlur).toBeCalledWith(expect.any(Object));
    });

    it('Должен отобразиться InputSlider onChange через изменение slider', async () => {
        let value = 0;
        const onChange = jest.fn(newValue => {
            value = newValue;
        });
        const { container, rerender } = render(<InputSlider onChange={onChange} value={value} />);
        const button = container.querySelector('button');
        const rail = container.querySelector('.Slider__Rail');
        expect(rail).toBeInTheDocument();
        rail.getBoundingClientRect = jest.fn(() => {
            return { width: 400, height: 10, top: 0, left: 0, bottom: 0, right: 0 };
        });

        await fireEvent.mouseDown(button);
        await fireEvent.mouseMove(button, {
            clientX: 200,
        });

        await fireEvent.mouseUp(button);

        rerender(<InputSlider onChange={onChange} value={value} />);
        const track = container.querySelector('.Slider__Track');
        const input = container.querySelector('input');

        expect(value).toBe('50');
        expect(input.value).toBe('50');
        expect(track).toHaveStyle('width: 50%');
        expect(button).toHaveStyle('left: 50%');
        expect(onChange).toBeCalledTimes(1);
        expect(onChange).toBeCalledWith('50', expect.any(Object));
    });
});
