import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

// Components
import Input from '../Input';
import Slider from '../Slider';

// Constants
import { SIZES, TYPES } from '../../constants/props';

// Helpers
import { sliderValueToValue, valueToSliderValue } from './helpers';

import { withTheme } from '../../theme-provider';

@withTheme
class InputSlider extends Component {
    inputRef = React.createRef();

    // Public methods
    blur = () => {
        this.input.blur();
    };

    focus = () => {
        this.input.focus();
    };

    // Handlers
    handleSliderChange = (value, props) => {
        const { onChange } = this.props;
        onChange(sliderValueToValue(value), props);
    };

    handleSliderBlur = () => {
        this.inputRef.current.focus();
    };

    render() {
        const {
            className,
            disabled,
            error,
            field,
            label,
            mask,
            max,
            min,
            placeholder,
            readOnly,
            size,
            step,
            type,
            success,
            value,
            applyTheme,
            formatValue,
            onBlur,
            onChange,
            onFocus,
            parseValue,
        } = this.props;

        const componentClass = applyTheme(
            cn('InputSlider', `InputSlider_size_${size}`, `InputSlider_type_${type}`),
            className,
        );

        const inputClass = applyTheme(
            cn(
                'InputSlider__Input',
                `InputSlider__Input_size_${size}`,
                `InputSlider__Input_type_${type}`,
            ),
        );
        const sliderClass = applyTheme(
            cn(
                'InputSlider__Slider',
                `InputSlider__Slider_size_${size}`,
                `Input__Slider_type_${type}`,
            ),
        );

        const inputProps = {
            disabled,
            error,
            field,
            label,
            mask,
            placeholder,
            readOnly,
            size,
            success,
            value,
            formatValue,
            onBlur,
            onChange,
            onFocus,
            parseValue,
        };

        const sliderProps = {
            disabled,
            field,
            max,
            min,
            readOnly,
            size,
            step,
            type,
            value: valueToSliderValue(value),
            onBlur: this.handleSliderBlur,
            onChange: this.handleSliderChange,
        };

        return (
            <div className={componentClass}>
                <div className={inputClass}>
                    <Input {...inputProps} ref={this.inputRef} />
                </div>
                <div className={sliderClass}>
                    <Slider {...sliderProps} />
                </div>
            </div>
        );
    }
}

InputSlider.propTypes = {
    className: PropTypes.string,
    disabled: PropTypes.bool,
    error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool, PropTypes.object]),
    field: PropTypes.string,
    label: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
        PropTypes.string,
    ]),
    mask: PropTypes.string,
    max: PropTypes.number,
    min: PropTypes.number,
    placeholder: PropTypes.string,
    readOnly: PropTypes.bool,
    size: PropTypes.oneOf([SIZES.S, SIZES.M, SIZES.L]),
    step: PropTypes.number,
    success: PropTypes.bool,
    type: PropTypes.oneOf([TYPES.PRIMARY, TYPES.SECONDARY]),
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    applyTheme: PropTypes.func,
    formatValue: PropTypes.func,
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    parseValue: PropTypes.func,
};

InputSlider.defaultProps = {
    className: '',
    disabled: false,
    error: null,
    field: null,
    label: null,
    mask: null,
    max: 100,
    min: 0,
    placeholder: '',
    readOnly: false,
    size: SIZES.M,
    step: 1,
    success: false,
    type: TYPES.PRIMARY,
    value: null,
    applyTheme: _ => _,
    formatValue: value => value,
    onBlur: props => {}, // eslint-disable-line no-unused-vars
    onChange: (value, props) => {}, // eslint-disable-line no-unused-vars
    onFocus: props => {}, // eslint-disable-line no-unused-vars
    parseValue: value => value,
};

export default InputSlider;
