import React from 'react';

// Components
import InputSlider from '../../index';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';

// Examples
import calculator from './examples/calculator.md';
import disabled from './examples/disabled.md';
import error from './examples/error.md';
import readOnly from './examples/readonly.md';
import size from './examples/size.md';
import type from './examples/type.md';

// HOC
import { withState } from '../../../../hoc';

// Utils
import { formatPriceNumber, parsePriceNumber } from '../../../../utils/text';

// Wrappers
import { Col, Row } from '../../../../wrappers/WGrid';

const InputSliderWithState = withState(InputSlider);

const DemoButton = () => {
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>

            <div id="example">
                <Row
                    cols={{ mobile: [1], tab: [1, 1], desktop: [1], largeDesktop: [1, 1] }}
                    gap="20px"
                >
                    <Col>
                        <Example readme={calculator}>
                            <InputSliderWithState
                                label="Стоимость недвижимости, руб"
                                max={20000000}
                                min={1000000}
                                step={100000}
                                value="10000000"
                                formatValue={formatPriceNumber}
                                parseValue={parsePriceNumber}
                            />
                            <br />
                            <InputSliderWithState
                                label="Сумма кредита, руб"
                                max={20000000}
                                min={1000000}
                                step={100000}
                                value="8000000"
                                formatValue={formatPriceNumber}
                                parseValue={parsePriceNumber}
                            />
                            <br />
                            <InputSliderWithState
                                label="Первоначальный взнос, руб"
                                max={10000000}
                                min={1000000}
                                step={100000}
                                value="2000000"
                                formatValue={formatPriceNumber}
                                parseValue={parsePriceNumber}
                            />
                        </Example>
                        <Example readme={disabled}>
                            <InputSliderWithState
                                disabled
                                label="Шаг"
                                max={10}
                                min={1}
                                step={1}
                                type="primary"
                                value="4"
                                formatValue={formatPriceNumber}
                                parseValue={parsePriceNumber}
                            />
                        </Example>
                        <Example readme={readOnly}>
                            <InputSliderWithState
                                disabled
                                label="Шаг"
                                max={10}
                                min={1}
                                step={1}
                                type="primary"
                                value="4"
                                formatValue={formatPriceNumber}
                                parseValue={parsePriceNumber}
                            />
                        </Example>
                    </Col>
                    <Col>
                        <Example readme={type}>
                            <InputSliderWithState
                                label="Шаг"
                                max={10}
                                min={1}
                                step={1}
                                type="primary"
                                value="4"
                                formatValue={formatPriceNumber}
                                parseValue={parsePriceNumber}
                            />
                            <br />
                            <InputSliderWithState
                                label="Шаг"
                                max={10}
                                min={1}
                                step={1}
                                type="secondary"
                                value="7"
                                formatValue={formatPriceNumber}
                                parseValue={parsePriceNumber}
                            />
                        </Example>
                        <Example readme={size}>
                            <InputSliderWithState
                                label="Шаг"
                                max={10}
                                min={1}
                                size="s"
                                step={1}
                                type="primary"
                                value="4"
                                formatValue={formatPriceNumber}
                                parseValue={parsePriceNumber}
                            />
                            <br />
                            <InputSliderWithState
                                label="Шаг"
                                max={10}
                                min={1}
                                size="m"
                                step={1}
                                type="primary"
                                value="7"
                                formatValue={formatPriceNumber}
                                parseValue={parsePriceNumber}
                            />
                        </Example>
                        <Example readme={error}>
                            <InputSliderWithState
                                error="text errors"
                                label="Шаг"
                                max={10}
                                min={1}
                                step={1}
                                type="primary"
                                value="4"
                                formatValue={formatPriceNumber}
                                parseValue={parsePriceNumber}
                            />
                        </Example>
                    </Col>
                </Row>
            </div>
        </DemoComponent>
    );
};

export default DemoButton;
