# Размер контрола

Свойство size может принимать одно из следующих значений: `s` и `m`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { InputSlider, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/ozon/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <InputSlider
      label="Шаг"
      max={10}
      min={1}
      size="s"
      step={1}
      type="primary"
      value="4"
      formatValue={formatPriceNumber}
      parseValue={parsePriceNumber}
    />
    <br />
    <InputSlider
      label="Шаг"
      max={10}
      min={1}
      size="m"
      step={1}
      type="primary"
      value="7"
      formatValue={formatPriceNumber}
      parseValue={parsePriceNumber}
    />
  </ThemeProvider>,
  document.getElementById('root')
);
```
