# Тип контрола

Свойство type может принимать одно из следующих значений: `primary`, и `secondary`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { InputSlider, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/absolut/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <InputSlider
      label="Шаг"
      max={10}
      min={1}
      step={1}
      type="primary"
      value="4"
      formatValue={formatPriceNumber}
      parseValue={parsePriceNumber}
    />
    <br />
    <InputSlider
      label="Шаг"
      max={10}
      min={1}
      step={1}
      type="secondary"
      value="7"
      formatValue={formatPriceNumber}
      parseValue={parsePriceNumber}
    />
  </ThemeProvider>,
  document.getElementById('root')
);
```
