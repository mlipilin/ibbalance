# Поля для калькулятора

Калькулятор присутствует в каждом проекте

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { InputSlider, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/alfa/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <InputSlider
      label="Стоимость недвижимости, руб"
      max={20000000}
      min={1000000}
      step={100000}
      value="10000000"
      formatValue={formatPriceNumber}
      parseValue={parsePriceNumber}
    />
    <br />
    <InputSlider
      label="Сумма кредита, руб"
      max={20000000}
      min={1000000}
      step={100000}
      value="8000000"
      formatValue={formatPriceNumber}
      parseValue={parsePriceNumber}
    />
    <br />
    <InputSlider
      label="Первоначальный взнос, руб"
      max={10000000}
      min={1000000}
      step={100000}
      value="2000000"
      formatValue={formatPriceNumber}
      parseValue={parsePriceNumber}
    />
  </ThemeProvider>,
  document.getElementById('root')
);
```
