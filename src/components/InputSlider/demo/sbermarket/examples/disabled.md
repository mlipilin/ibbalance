# "disabled" состояние

`true`: контрол становится 'disabled' (недоступен для редактирования)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { InputSlider, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/sbermarket/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <InputSlider
      disabled
      label="Шаг"
      max={10}
      min={1}
      step={1}
      type="primary"
      value="4"
      formatValue={formatPriceNumber}
      parseValue={parsePriceNumber}
    />
  </ThemeProvider>,
  document.getElementById('root')
);
```
