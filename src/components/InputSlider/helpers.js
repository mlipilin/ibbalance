// Utils
import { parseFloatValue } from '../../utils/text';

export const sliderValueToValue = (sliderValue = null) => {
    return sliderValue !== null ? String(sliderValue) : null;
};

export const valueToSliderValue = (value = '') => {
    if (value === null) return null;
    if (value === '') return null;

    const [integer, fraction] = parseFloatValue(value);

    if (integer === null) {
        return null;
    }
    if (integer && fraction === null) {
        return Number(integer);
    }
    if (integer && fraction) {
        return Number(`${integer}.${fraction}`);
    }
    return null;
};
