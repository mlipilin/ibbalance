## Usage

Input with placeholder (NO label):

```jsx
<InputSlider
    size="s"
    placeholder="Сумма кредита"
    value={creditAmount}
    onBlur={_ => _}
    onChange={_ => _}
    onFocus={_ => _}
/>
```

Input with label (NO placeholder):

```jsx
<InputSlider
    label="Сумма кредита"
    size="s"
    value={creditAmount}
    onBlur={_ => _}
    onChange={_ => _}
    onFocus={_ => _}
/>
```

## Props

Prop name | Prop type | Default value
--- | --- | ---
*disabled*|bool|`false`
*error*|string, object|`null`
*isReadOnly*|bool|`false`
*label*|string|`null`
*mask*|string|`null`
*max*|number|`100`
*min*|number|`0`
*readOnly*|bool|`false`
*size*|`s`, `m`, `l`|`m`
*step*|number|`1`
*success*|bool|`false`
*type*|`primary`, `secondary`|`primary`
*value*|string|`null`
*formatValue*|func|`_ => _`
*onBlur*|func|`_ => _`
*onChange*|func|`_ => _`
*onFocus*|func|`_ => _`
*parseValue*|func|`_ => _`

## CSS Selectors

```css
.InputSlider {}
.InputSlider_size_s {}
.InputSlider_size_m {}
.InputSlider_size_l {}
.InputSlider_type_primary {}
.InputSlider_type_secondary {}

/* Input */
.InputSlider__Input {}
.InputSlider__Input_size_s {}
.InputSlider__Input_size_m {}
.InputSlider__Input_size_l {}
.InputSlider__Input_type_primary {}
.InputSlider__Input_type_secondary {}

/* Slider */
.InputSlider__Slider {}
.InputSlider__Slider_size_s {}
.InputSlider__Slider_size_m {}
.InputSlider__Slider_size_l {}
.InputSlider__Slider_type_primary {}
.InputSlider__Slider_type_secondary {}
