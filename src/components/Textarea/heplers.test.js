import { textForHiddenBlock } from './helpers';

describe('src/components/Textarea/helpers.js', () => {
    describe('textForHiddenBlock', () => {
        it('undefined -> "text"', () => {
            expect(textForHiddenBlock(undefined)).toBe('text');
        });
        it('null -> "text"', () => {
            expect(textForHiddenBlock(null)).toBe('text');
        });
        it('empry string -> "text"', () => {
            expect(textForHiddenBlock('')).toBe('text');
        });
        it('example -> "example"', () => {
            expect(textForHiddenBlock('example')).toBe('example');
        });
        it('example\n -> "example\n1"', () => {
            expect(textForHiddenBlock('example\n')).toBe('example\n1');
        });
    });
});
