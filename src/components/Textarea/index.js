import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

// Components
import FeatherIcon from '../FeatherIcon';
// Constants
import { SIZES } from '../../constants/props';

import { withTheme } from '../../theme-provider';
import { hasNodeInTree } from '../../helpers/dom';
import { textForHiddenBlock } from './helpers';

@withTheme
class Textarea extends Component {
    constructor(props) {
        super(props);
        this.state = { hasFocus: false, isErrorDetailOpen: false };
    }

    componentDidMount() {
        if (this.target) {
            this.init();
        }
    }

    componentWillUnmount() {
        if (this.target) {
            this.target.removeEventListener('click', this.handleTargetClickEvent);
        }
        window.removeEventListener('click', this.handleWindowClickEvent);
    }

    handleTargetClickEvent = e => {
        e.preventDefault();

        this.setState(prevState => ({ isErrorDetailOpen: !prevState.isErrorDetailOpen }));
    };

    handleWindowClickEvent = e => {
        const isClickOnTarget = hasNodeInTree(e.target, this.target);
        const isClickOnTooltip = hasNodeInTree(e.target, this.tooltip);

        if (!isClickOnTooltip && !isClickOnTarget) {
            this.setState({ isErrorDetailOpen: false });
        }
    };

    init = () => {
        this.target.addEventListener('click', this.handleTargetClickEvent);
        window.addEventListener('click', this.handleWindowClickEvent);
    };

    // Public methods
    blur = () => {
        this.textarea.blur();
    };

    focus = () => {
        this.textarea.focus();
    };

    // Event handlers
    handleBlur = () => {
        const { onBlur } = this.props;
        this.setState({ hasFocus: false }, () => {
            onBlur(this.props);
        });
    };

    handleChange = e => {
        const { onChange } = this.props;
        onChange(e.target.value, this.props);
    };

    handleFocus = () => {
        const { onFocus } = this.props;
        this.setState({ hasFocus: true }, () => {
            onFocus(this.props);
        });
    };

    render() {
        const {
            className,
            disabled,
            error,
            errorDetail,
            errorDetailIcon,
            field,
            label,
            placeholder,
            readOnly,
            resize,
            size,
            success,
            value,
            applyTheme,
            ...otherProps
        } = this.props;

        const { hasFocus, isErrorDetailOpen } = this.state;

        const componentClass = applyTheme(cn('Textarea', `Textarea_size_${size}`), className);
        const containerClass = applyTheme(cn('Textarea__Container'));

        const labelClass = applyTheme(cn('Textarea__Label'));

        const labelTextClass = applyTheme(
            cn('Textarea__LabelText', `Textarea__LabelText_size_${size}`, {
                'Textarea__LabelText_place-top':
                    (!readOnly && hasFocus) || !!value || !!placeholder,
                Textarea__LabelText_focus: hasFocus,
            }),
        );

        const textareaClass = applyTheme(
            cn('Textarea__Textarea', `Textarea__Textarea_size_${size}`, {
                Textarea__Textarea_error: !!error || !!errorDetail,
                Textarea__Textarea_readonly: readOnly,
                Textarea__Textarea_success: !error && success,
                'Textarea__Textarea_with-label': !!label,
                Textarea__Textarea_resize: resize,
            }),
        );

        const errorClass = applyTheme(cn('Textarea__Error', `Textarea__Error_size_${size}`));

        const errorDetailClass = applyTheme(
            cn('Textarea__ErrorDetail', `Textarea__ErrorDetail_size_${size}`, {
                Textarea__ErrorDetail_visible: !!errorDetail,
            }),
        );
        const errorIconClass = applyTheme(
            cn('Textarea__ErrorIcon', `Textarea__ErrorIcon_size_${size}`),
        );
        const tooltipClass = applyTheme(
            cn('Textarea__Tooltip', `Textarea__Tooltip_size_${size}`, {
                Textarea__Tooltip_visible: isErrorDetailOpen,
            }),
        );

        const hiddenBlockClass = applyTheme(
            cn('Textarea__HiddenBlock', `Textarea__HiddenBlock_size_${size}`, {
                'Textarea__HiddenBlock_with-label': !!label,
            }),
        );

        const textareaProps = {
            ...otherProps,
            disabled,
            className: textareaClass,
            placeholder,
            readOnly,
            ref: el => {
                this.textarea = el;
                return this.textarea;
            },
            value: value === null ? '' : value,
            onBlur: this.handleBlur,
            onChange: this.handleChange,
            onFocus: this.handleFocus,
        };

        return (
            <div className={componentClass}>
                <div className={containerClass}>
                    <div
                        className={hiddenBlockClass}
                        ref={el => {
                            this.hiddenBlock = el;
                            return this.hiddenBlock;
                        }}
                    >
                        {textForHiddenBlock(value)}
                    </div>
                    <label className={labelClass}>
                        {/* Label */}
                        {!!label && <span className={labelTextClass}>{label}</span>}
                        {/* Textarea */}
                        <textarea
                            {...textareaProps}
                            ref={el => {
                                this.textarea = el;
                                return this.textarea;
                            }}
                        />

                        {disabled && <FeatherIcon name="lock" />}
                    </label>
                </div>

                {/* Error */}
                {!!error && <div className={errorClass}>{error}</div>}

                <div className={errorDetailClass}>
                    <div
                        className={errorIconClass}
                        ref={el => {
                            this.target = el;
                            return this.target;
                        }}
                    >
                        {errorDetailIcon}
                    </div>
                    <div
                        className={tooltipClass}
                        ref={el => {
                            this.tooltip = el;
                            return this.tooltip;
                        }}
                    >
                        {errorDetail}
                    </div>
                </div>
            </div>
        );
    }
}

Textarea.propTypes = {
    className: PropTypes.string,
    disabled: PropTypes.bool,
    error: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    errorDetail: PropTypes.string,
    errorDetailIcon: PropTypes.node,
    field: PropTypes.string,
    label: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
        PropTypes.string,
    ]),
    placeholder: PropTypes.string,
    readOnly: PropTypes.bool,
    resize: PropTypes.bool,
    size: PropTypes.oneOf([SIZES.S, SIZES.M, SIZES.L]),
    success: PropTypes.bool,
    value: PropTypes.string,
    applyTheme: PropTypes.func,
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
};

Textarea.defaultProps = {
    className: '',
    disabled: false,
    error: null,
    errorDetail: null,
    errorDetailIcon: <FeatherIcon name="alert-circle" />,
    field: null,
    label: null,
    placeholder: '',
    readOnly: false,
    resize: false,
    size: SIZES.M,
    success: false,
    value: null,
    applyTheme: _ => _,
    onBlur: props => {}, // eslint-disable-line no-unused-vars
    onChange: (value, props) => {}, // eslint-disable-line no-unused-vars
    onFocus: props => {}, // eslint-disable-line no-unused-vars
};

export default Textarea;
