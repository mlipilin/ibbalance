export const textForHiddenBlock = text => {
    if (text === '' || text === undefined || text === null) return 'text';
    const lastChar = text[text.length - 1];
    return lastChar === '\n' ? `${text}1` : text;
};
