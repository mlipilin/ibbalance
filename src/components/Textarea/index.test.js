import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import FeatherIcon from '../FeatherIcon';

import Textarea from './index';

import { render } from '../../hoc/with-theme';

describe('Тестирование компонента Textarea', () => {
    it('Должен отобразиться Textarea без заголовка и placeholder', () => {
        const { container } = render(<Textarea />);
        expect(screen.getByPlaceholderText('')).toBeInTheDocument();
        expect(screen.getByLabelText('')).toBeInTheDocument();
        expect(screen.getByDisplayValue('')).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Textarea c классом "Textarea__Styled"', () => {
        const { container } = render(<Textarea className="Textarea__Styled" />);
        const wrapper = container.querySelector('.Textarea__Styled');
        expect(wrapper).toBeInTheDocument();
    });

    it('Должен отобразиться Textarea в состоянии disabled', () => {
        const { container } = render(<Textarea disabled />);
        const textarea = container.querySelector('textarea');
        const icon = container.querySelector('.feather-lock');
        expect(textarea).toBeDisabled();
        expect(icon).toBeInTheDocument();
    });

    it('Должен отобразиться Textarea в состоянии disabled не изменяется', async () => {
        const handleFocus = jest.fn();
        const { container } = render(<Textarea disabled onFocus={handleFocus} />);
        const textarea = container.querySelector('textarea');
        textarea.focus();
        expect(handleFocus).toBeCalledTimes(0);
    });

    it('Должен отобразиться Textarea в состоянии error с текстом "Обязательное поле"', () => {
        const { container } = render(<Textarea error="Обязательное поле" />);
        const error = container.querySelector('.Textarea__Error');
        expect(error).toBeInTheDocument();
        expect(error.textContent).toBe('Обязательное поле');
    });

    it('Должен отобразиться Textarea в состоянии error с текстом "<strong>Обязательное</strong> поле"', () => {
        const { container } = render(
            <Textarea
                error={
                    <div>
                        <strong>Обязательное</strong>поле
                    </div>
                }
            />,
        );
        const error = container.querySelector('.Textarea__Error');
        expect(error).toBeInTheDocument();
        const strongPart = error.querySelector('strong');
        expect(strongPart.textContent).toBe('Обязательное');
    });
    it('Должен отобразиться Textarea в состоянии errorDetail с текстом "Обязательное поле"', () => {
        const { container } = render(<Textarea errorDetail="Обязательное поле" />);
        const errorDetail = container.querySelector('.Textarea__ErrorDetail');
        expect(errorDetail.classList.contains('Textarea__ErrorDetail_visible')).toBeTruthy();
        const iconDetail = errorDetail.querySelector('.feather-alert-circle');
        expect(iconDetail).toBeInTheDocument();
        const tooltip = errorDetail.querySelector('.Textarea__Tooltip');
        expect(tooltip.textContent).toBe('Обязательное поле');
    });

    it('Должен отобразиться Textarea в состоянии errorDetail с иконкой "<FeatherIcon name="alert-triangle" />"', () => {
        const { container } = render(
            <Textarea
                errorDetail="Обязательное поле"
                errorDetailIcon={<FeatherIcon name="alert-triangle" />}
            />,
        );
        const detailIcon = container.querySelector('.feather-alert-triangle');
        expect(detailIcon).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });
    it('Должен отобразиться Textarea в состоянии errorDetail с тултипом', async () => {
        const { container } = render(<Textarea errorDetail="Обязательное поле" />);

        const detailIcon = container.querySelector('.Textarea__ErrorIcon');
        await userEvent.click(detailIcon);
        const tooltip = container.querySelector('.Textarea__Tooltip');
        expect(tooltip).toHaveClass('Textarea__Tooltip_visible');
    });
    it('Должен отобразиться Textarea с label = "Описание" ', () => {
        const { container } = render(<Textarea label="Описание" />);
        expect(screen.getByPlaceholderText('')).toBeInTheDocument();
        expect(screen.getByLabelText('Описание')).toBeInTheDocument();
        expect(screen.getByDisplayValue('')).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Textarea с label = "<span>Описание</span>" ', () => {
        const { container } = render(<Textarea label={<span>Описание</span>} />);
        expect(screen.getByLabelText('Описание')).toBeInTheDocument();
        const textElement = container.querySelector('.Textarea__LabelText > span');
        expect(textElement.textContent).toBe('Описание');
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Textarea с label = "[<span>Поле для</span>,<strong>описания</strong>]" ', () => {
        const { container } = render(
            <Textarea
                label={[
                    <span key="for">Поле для</span>,
                    <strong key="description">описания</strong>,
                ]}
            />,
        );
        expect(screen.getByText('Поле для')).toBeInTheDocument();
        expect(screen.getByText('описания')).toBeInTheDocument();
        const textElement = container.querySelector('.Textarea__LabelText');
        const textSurname = textElement.querySelector('span');
        expect(textSurname.textContent).toBe('Поле для');
        const textName = textElement.querySelector('strong');
        expect(textName.textContent).toBe('описания');
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Textarea с placeholder = "Введите фамилию" ', () => {
        const { container } = render(<Textarea placeholder="Введите фамилию" />);
        const textarea = container.querySelector('textarea');
        expect(textarea.placeholder).toBe('Введите фамилию');
        expect(textarea).toMatchSnapshot();
    });

    it('Должен отобразиться Textarea с name="description" ', () => {
        const { container } = render(<Textarea name="description" />);
        const textarea = container.querySelector('textarea');
        expect(textarea.name).toBe('description');
        expect(textarea).toMatchSnapshot();
    });

    it('Должен отобразиться Textarea в состоянии read only', () => {
        const onChange = jest.fn();
        const { container } = render(<Textarea readOnly onChange={onChange} />);
        const textarea = container.querySelector('textarea');
        expect(textarea).toHaveClass('Textarea__Textarea_readonly');
        expect(textarea.readOnly).toBeTruthy();
        userEvent.type(textarea, 'text for readonly');
        expect(onChange).toBeCalledTimes(0);
    });
    it('Должен отобразиться Textarea в состоянии readonly не изменяется', async () => {
        const handleChange = jest.fn();
        const { container } = render(<Textarea readOnly onChange={handleChange} />);
        const textarea = container.querySelector('textarea');
        await userEvent.type(textarea, '{space}');
        expect(handleChange).toBeCalledTimes(0);
    });

    it('Должен отобразиться Textarea в состоянии resize', () => {
        const { container } = render(<Textarea resize />);
        const textarea = container.querySelector('textarea');
        expect(textarea.classList.contains('Textarea__Textarea_resize')).toBeTruthy();
    });

    it('Должен отобразиться Textarea в состоянии size=m по-умолчанию', () => {
        const { container } = render(
            <Textarea
                label="Подробное описание"
                placeholder="Введите текст проблемы"
                error="Обязательное поле"
            />,
        );
        const wrapper = container.querySelector('.Textarea');
        expect(wrapper.classList.contains('Textarea_size_m')).toBeTruthy();
        const hiddenBlock = wrapper.querySelector('.Textarea__HiddenBlock');
        expect(hiddenBlock).toHaveClass('Textarea__HiddenBlock_size_m');
        const labelText = wrapper.querySelector('.Textarea__Textarea_size_m');
        expect(labelText).toBeInTheDocument();
        const error = wrapper.querySelector('.Textarea__Error_size_m');
        expect(error).toBeInTheDocument();
        const errorDetail = wrapper.querySelector('.Textarea__ErrorDetail_size_m');
        expect(errorDetail).toBeInTheDocument();
        const errorIcon = wrapper.querySelector('.Textarea__ErrorIcon_size_m');
        expect(errorIcon).toBeInTheDocument();
        const tooltip = wrapper.querySelector('.Textarea__Tooltip_size_m');
        expect(tooltip).toBeInTheDocument();
    });
    it('Должен отобразиться Textarea в состоянии size=s', () => {
        const { container } = render(
            <Textarea
                size="s"
                label="Подробное описание"
                placeholder="Введите текст проблемы"
                error="Обязательное поле"
            />,
        );
        const wrapper = container.querySelector('.Textarea');
        expect(wrapper.classList.contains('Textarea_size_s')).toBeTruthy();
        const hiddenBlock = wrapper.querySelector('.Textarea__HiddenBlock');
        expect(hiddenBlock.classList.contains('Textarea__HiddenBlock_size_s'));
        const labelText = wrapper.querySelector('.Textarea__Textarea_size_s');
        expect(labelText).toBeInTheDocument();
        const error = wrapper.querySelector('.Textarea__Error_size_s');
        expect(error).toBeInTheDocument();
        const errorDetail = wrapper.querySelector('.Textarea__ErrorDetail_size_s');
        expect(errorDetail).toBeInTheDocument();
        const errorIcon = wrapper.querySelector('.Textarea__ErrorIcon_size_s');
        expect(errorIcon).toBeInTheDocument();
        const tooltip = wrapper.querySelector('.Textarea__Tooltip_size_s');
        expect(tooltip).toBeInTheDocument();
    });
    it('Должен отобразиться Textarea в состоянии size=l', () => {
        const { container } = render(
            <Textarea
                size="l"
                label="Подробное описание"
                placeholder="Введите текст проблемы"
                error="Обязательное поле"
            />,
        );
        const wrapper = container.querySelector('.Textarea');
        expect(wrapper.classList.contains('Textarea_size_l')).toBeTruthy();
        const hiddenBlock = wrapper.querySelector('.Textarea__HiddenBlock');
        expect(hiddenBlock.classList.contains('Textarea__HiddenBlock_size_l'));
        const labelText = wrapper.querySelector('.Textarea__Textarea_size_l');
        expect(labelText).toBeInTheDocument();
        const error = wrapper.querySelector('.Textarea__Error_size_l');
        expect(error).toBeInTheDocument();
        const errorDetail = wrapper.querySelector('.Textarea__ErrorDetail_size_l');
        expect(errorDetail).toBeInTheDocument();
        const errorIcon = wrapper.querySelector('.Textarea__ErrorIcon_size_l');
        expect(errorIcon).toBeInTheDocument();
        const tooltip = wrapper.querySelector('.Textarea__Tooltip_size_l');
        expect(tooltip).toBeInTheDocument();
    });

    it('Должен отобразиться Textarea в состоянии success', () => {
        const { container } = render(<Textarea success />);
        const textarea = container.querySelector('textarea');
        expect(textarea.classList.contains('Textarea__Textarea_success'));
    });

    it('Должен отобразиться Textarea со значением "Подробное описание"', () => {
        const { container } = render(<Textarea value="Подробное описание" />);
        const textarea = container.querySelector('textarea');
        expect(textarea.textContent).toBe('Подробное описание');
        const hiddenBlock = container.querySelector('.Textarea__HiddenBlock');
        expect(hiddenBlock.textContent).toBe('Подробное описание');
    });

    it('Должен отобразиться Textarea в состоянии blur', () => {
        const onBlur = jest.fn();
        render(<Textarea label="Дополнительная информация" onBlur={onBlur} />);
        const textarea = screen.getByLabelText(/Дополнительная информация/i);
        textarea.focus();
        userEvent.tab();
        expect(onBlur).toHaveBeenCalledTimes(1);
        expect(onBlur).toHaveBeenCalledWith(expect.any(Object));
    });

    it('Должен отобразиться Textarea со значением "iBalance - это дизайн-система"', () => {
        let valueState = '';
        const onChange = jest.fn(valueProps => {
            valueState += valueProps;
        });
        const { container, rerender } = render(
            <Textarea label="Дополнительная информация" onChange={onChange} value={valueState} />,
        );
        const textarea = screen.getByLabelText(/Дополнительная информация/i);
        const newValue = 'iBalance - это дизайн-система';
        userEvent.type(textarea, newValue);
        expect(onChange).toHaveBeenCalledTimes(newValue.length);
        expect(onChange).toHaveBeenCalledWith(newValue[0], expect.any(Object));
        rerender(
            <Textarea label="Дополнительная информация" value={valueState} onChange={onChange} />,
        );
        expect(textarea).toHaveValue('iBalance - это дизайн-система');
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Textarea в состоянии focus', () => {
        const handleFocus = jest.fn();
        const { container } = render(
            <Textarea label="Дополнительная информация" onFocus={handleFocus} />,
        );
        const textarea = screen.getByLabelText(/Дополнительная информация/i);
        userEvent.click(textarea);
        expect(textarea).toHaveFocus();
        expect(handleFocus).toHaveBeenCalledTimes(1);
        expect(handleFocus).toHaveBeenCalledWith(expect.any(Object));
        expect(container.firstChild).toMatchSnapshot();
    });
    it('Должен отобразиться Textarea c использованием ref focus', () => {
        const handleFocus = jest.fn();
        const refFocus = React.createRef();
        render(<Textarea ref={refFocus} onFocus={handleFocus} />);
        refFocus.current.focus();
        expect(handleFocus).toHaveBeenCalledTimes(1);
        expect(handleFocus).toHaveBeenCalledWith(expect.any(Object));
    });

    it('Должен отобразиться Textarea c использованием ref', () => {
        const refBlur = React.createRef();
        const { container } = render(<Textarea ref={refBlur} />);
        const textarea = container.querySelector('textarea');
        textarea.focus();
        refBlur.current.blur();
        expect(textarea).not.toHaveFocus();
    });
});
