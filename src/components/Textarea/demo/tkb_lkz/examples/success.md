# "success" состояние

`true`: поле становится 'success' (значение в поле валидно)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Textarea, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/tkb_lkz/theme';

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <Textarea label="Валидное поле" value="iBalance — это дизайн-система." success />
    </ThemeProvider>,
    document.getElementById('root'),
);
```
