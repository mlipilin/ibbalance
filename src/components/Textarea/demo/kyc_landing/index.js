import React from 'react';
import WFormRow from '../../../../wrappers/WFormRow';

// Components
import Textarea from '../../index';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';

// Examples
import disabled from './examples/disabled.md';
import error from './examples/error.md';
import errorDetail from './examples/errorDetail.md';
import label from './examples/label.md';
import readOnly from './examples/readonly.md';
import size from './examples/size.md';
import success from './examples/success.md';

// HOC
import { withState } from '../../../../hoc';

// Wrappers
import { Col, Row } from '../../../../wrappers/WGrid';

const TextareaWithState = withState(Textarea);

const DemoTextarea = () => {
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>

            <div id="example">
                <Row
                    cols={{ mobile: [1], tab: [1, 1], desktop: [1], largeDesktop: [1, 1] }}
                    gap="20px"
                >
                    <Col>
                        <Example readme={label}>
                            <TextareaWithState label="Дополнительная информация" />
                        </Example>
                        <Example readme={size}>
                            <div>
                                <WFormRow>
                                    <TextareaWithState label="Size M" size="m" />
                                </WFormRow>
                            </div>
                            <div>
                                <WFormRow>
                                    <TextareaWithState label="Size S" size="s" />
                                </WFormRow>
                            </div>
                        </Example>
                        <Example readme={error}>
                            <TextareaWithState
                                label="Невалидное поле"
                                value="iBalance — это дизайн-система."
                                error="Слишком короткий текст"
                            />
                        </Example>
                        <Example readme={success}>
                            <TextareaWithState
                                label="Валидное поле"
                                value="iBalance — это дизайн-система."
                                success
                            />
                        </Example>
                    </Col>
                    <Col>
                        <Example readme={disabled}>
                            <TextareaWithState
                                label="Disabled поле"
                                value="iBalance — это дизайн-система."
                                disabled
                            />
                        </Example>
                        <Example readme={readOnly}>
                            <TextareaWithState
                                label="Readonly поле"
                                value="iBalance — это дизайн-система."
                                readOnly
                            />
                        </Example>
                        <Example readme={errorDetail}>
                            <TextareaWithState
                                label="Невалидное поле"
                                value="iBalance — это дизайн-система."
                                errorDetail="Слишком короткий текст"
                            />
                        </Example>
                    </Col>
                </Row>
            </div>
        </DemoComponent>
    );
};

export default DemoTextarea;
