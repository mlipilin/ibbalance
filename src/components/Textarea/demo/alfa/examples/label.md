# Поле с заголовком

Заголовок передается в свойстве `label`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Textarea, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/alfa/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Textarea label="Дополнительная информация"  />
  </ThemeProvider>,
  document.getElementById('root')
);
```
