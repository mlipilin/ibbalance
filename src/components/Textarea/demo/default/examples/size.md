# Размер поля

Свойство size может принимать одно из следующих значений: `s`, `m` и `l`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Textarea, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/default/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Textarea label="Size L" size="l" />
    <Textarea label="Size M" size="m" />
    <Textarea label="Size S" size="s" />
  </ThemeProvider>,
  document.getElementById('root')
);
```
