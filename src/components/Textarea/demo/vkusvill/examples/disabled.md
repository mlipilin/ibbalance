# "disabled" состояние

`true`: поле становится 'disabled' (недоступен для редактирования)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Textarea, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/vkusvill/theme';

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <Textarea label="Disabled поле" value="iBalance — это дизайн-система." disabled />
    </ThemeProvider>,
    document.getElementById('root'),
);
```
