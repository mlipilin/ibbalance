# "read only" состояние

`true`: поле становится 'read only' (только для чтения)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Textarea, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/vkusvill/theme';

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <Textarea label="Readonly поле" value="iBalance — это дизайн-система." readOnly />
    </ThemeProvider>,
    document.getElementById('root'),
);
```
