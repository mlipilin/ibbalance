# Наличие длиной ошибки

В поле `errorDetail` можно передать текст ошибки

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Textarea, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/absolut/theme';

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <Textarea
            label="Невалидное поле"
            value="iBalance — это дизайн-система."
            errorDetail="Слишком короткий текст"
        />
    </ThemeProvider>,
    document.getElementById('root'),
);
```
