import React from 'react';
import userEvent from '@testing-library/user-event';

import ConfirmModal from './index';

import { render } from '../../hoc/with-theme';

jest.mock('react', () => {
    const originReact = jest.requireActual('react');
    const mUseRef = jest.fn();
    return {
        ...originReact,
        useRef: mUseRef,
    };
});

describe('Тестирование компонента ConfirmModal', () => {
    it('Должен отобразиться ConfirmModal', () => {
        render(<ConfirmModal />);
        const confirmModal = document.querySelector('.ConfirmModal');
        expect(confirmModal).toBeInTheDocument();
        expect(confirmModal).toMatchSnapshot();
    });
    it('Должен отобразиться ConfirmModal с классом "ConfirmModal__Styled"', () => {
        render(<ConfirmModal className="ConfirmModal__Styled" />);
        const modal = document.querySelector('.Modal');
        expect(modal.classList.contains('ConfirmModal__Styled')).toBeTruthy();
        expect(modal).toMatchSnapshot();
    });
    it('Должен отобразиться ConfirmModal с children', () => {
        render(
            <ConfirmModal>
                <div className="Modal__Children">Содержимое ConfirmModal</div>
            </ConfirmModal>,
        );
        const modalChildren = document.querySelector('.Modal__Children');
        expect(modalChildren.textContent).toBe('Содержимое ConfirmModal');
        expect(modalChildren).toMatchSnapshot();
    });
    it('Должен отобразиться ConfirmModal с заголовком "Содержимое ConfirmModal"', () => {
        const modal = React.createRef();

        const { rerender } = render(
            <>
                <ConfirmModal ref={modal}>Содержимое ConfirmModal</ConfirmModal>
            </>,
        );
        modal.current.open({
            title: 'Вы уверены?',
        });
        rerender(<ConfirmModal ref={modal}>Содержимое ConfirmModal</ConfirmModal>);
        const modalElement = document.querySelector('.Modal_open');
        expect(modalElement).toBeInTheDocument();
        const title = modalElement.querySelector('.Modal__Title');
        expect(title.textContent).toBe('Вы уверены?');
        expect(modalElement).toMatchSnapshot();
    });
    it('Должен отобразиться ConfirmModal с кнопкой закрыть', async () => {
        const handleClose = jest.fn();
        const modal = React.createRef();

        const { rerender } = render(
            <>
                <ConfirmModal ref={modal} />
            </>,
        );
        modal.current.open({
            cancelBtnText: 'Закрыть',
            onCancel: handleClose,
        });
        rerender(<ConfirmModal ref={modal} />);
        const buttons = document.querySelectorAll('.Button__Content');
        expect(buttons[0]).toBeInTheDocument();
        expect(buttons[0].textContent).toBe('Закрыть');

        await userEvent.click(buttons[0]);

        expect(handleClose).toBeCalledTimes(1);
        expect(handleClose).toBeCalledWith(expect.any(Object));
    });
    it('Должен отобразиться ConfirmModal закрытие по ESC', async () => {
        const handleClose = jest.fn();
        const modal = React.createRef();

        const { rerender } = render(
            <>
                <ConfirmModal ref={modal} />
            </>,
        );
        modal.current.open({
            cancelBtnText: 'Закрыть',
            onCancel: handleClose,
        });
        rerender(<ConfirmModal ref={modal} />);
        const buttons = document.querySelectorAll('.Button__Content');
        expect(buttons[0]).toBeInTheDocument();
        expect(buttons[0].textContent).toBe('Закрыть');

        await userEvent.type(buttons[0], '{esc}');

        expect(handleClose).toBeCalledTimes(1);
        expect(handleClose).toBeCalledWith(expect.any(Object));
    });
    it('Должен отобразиться ConfirmModal с кнопкой подтвердить', async () => {
        const handleConfirm = jest.fn();
        const modal = React.createRef();

        const { rerender } = render(
            <>
                <ConfirmModal ref={modal} />
            </>,
        );
        modal.current.open({
            confirmBtnText: 'Подтвердить',
            onConfirm: handleConfirm,
        });
        rerender(<ConfirmModal ref={modal} />);
        const buttons = document.querySelectorAll('.Button__Content');
        expect(buttons[1]).toBeInTheDocument();
        expect(buttons[1].textContent).toBe('Подтвердить');

        await userEvent.click(buttons[1]);

        expect(handleConfirm).toBeCalledTimes(1);
        expect(handleConfirm).toBeCalledWith(expect.any(Object));
    });
    it('Должен отобразиться ConfirmModal с подтверждением по ENTER', async () => {
        const handleConfirm = jest.fn();
        const modal = React.createRef();

        const { rerender } = render(
            <>
                <ConfirmModal ref={modal} />
            </>,
        );
        modal.current.open({
            confirmBtnText: 'Подтвердить',
            onConfirm: handleConfirm,
        });
        rerender(<ConfirmModal ref={modal} />);
        const modalWrapper = document.querySelector('.Modal__Wrapper');
        await userEvent.type(modalWrapper, '{enter}');

        expect(handleConfirm).toBeCalledTimes(1);
        expect(handleConfirm).toBeCalledWith(expect.any(Object));
    });

    it('Должен отобразиться ConfirmModal без кнопки Отмены', async () => {
        const handleConfirm = jest.fn();
        const modal = React.createRef();

        const { rerender } = render(
            <>
                <ConfirmModal ref={modal} />
            </>,
        );
        modal.current.open({
            cancelBtnText: null,
            confirmBtnText: 'Подтвердить',
            onConfirm: handleConfirm,
        });
        rerender(<ConfirmModal ref={modal} />);
        const buttons = document.querySelectorAll('.Button__Content');
        expect(buttons[0]).toBeInTheDocument();
        expect(buttons[0].textContent).toBe('Подтвердить');

        await userEvent.click(buttons[0]);

        expect(handleConfirm).toBeCalledTimes(1);
        expect(handleConfirm).toBeCalledWith(expect.any(Object));
    });
});
