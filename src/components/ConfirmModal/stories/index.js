import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean, text } from '@storybook/addon-knobs';
import PropTypes from 'prop-types';

import ConfirmModal from '../index';
import Button from '../../Button';

import notes from '../readme.md';

const stories = storiesOf('ConfirmModal', module);

class ComponentWithConfirmModal extends React.Component {
    onOpenConfirmModal = () => {
        const { closeBtnText, confirmBtnText, title } = this.props;
        const params = {
            closeBtnText,
            confirmBtnText,
            title,
            onConfirm: () => console.log('Confirm'),
            onClose: () => console.log('Close'),
        };
        this.confirmModal.open(params);
    };

    onCloseConfirmModal = () => {
        this.confirmModal.close();
    };

    render() {
        const { content, hideCloseBtn } = this.props;
        return (
            <>
                <Button onClick={this.onOpenConfirmModal}>Открыть модалку</Button>
                <ConfirmModal
                    hideCloseBtn={hideCloseBtn}
                    ref={el => {
                        this.confirmModal = el;
                        return this.confirmModal;
                    }}
                >
                    {content}
                </ConfirmModal>
            </>
        );
    }
}
ComponentWithConfirmModal.propTypes = {
    closeBtnText: PropTypes.string,
    confirmBtnText: PropTypes.string,
    content: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    hideCloseBtn: PropTypes.bool,
    title: PropTypes.string,
};

ComponentWithConfirmModal.defaultProps = {
    closeBtnText: '',
    confirmBtnText: '',
    content: null,
    hideCloseBtn: false,
    title: '',
};

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories.add(
    'Default',
    () => (
        <ComponentWithConfirmModal
            closeBtnText={text('closeBtnText', 'Нет')}
            confirmBtnText={text('confirmBtnText', 'Да')}
            content={text(
                'content',
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
            )}
            hideCloseBtn={boolean('hideCloseBtn', false)}
            title={text('title', 'Вы уверены, что хотите удалить файл?')}
        />
    ),
    { notes },
);
