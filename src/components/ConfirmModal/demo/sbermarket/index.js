import React, { Component } from 'react';

// Components
import Button from '../../../Button';
import ConfirmModal from '../../index';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

// Examples
import example from './examples/example.md';

import readme from './readme.md';

class DemoModal extends Component {
    render() {
        return (
            <DemoComponent>
                <ReadmeView>{readme}</ReadmeView>

                <div id="example">
                    <Example readme={example}>
                        <Button
                            onClick={() =>
                                this.modal.open({
                                    cancelBtnText: 'Нет',
                                    confirmBtnText: 'Да',
                                    title: 'Вы уверены?',
                                    onCancel: () => console.log('Cancel callback'),
                                    onConfirm: () => console.log('Confirm callback'),
                                })
                            }
                        >
                            Обычный Confirm
                        </Button>
                        <Button
                            onClick={() =>
                                this.modal.open({
                                    cancelBtnText: null,
                                    confirmBtnText: 'Ок',
                                    title: 'Действие успешно выполнилось',
                                    onConfirm: () => console.log('Confirm callback'),
                                })
                            }
                        >
                            Confirm без кнопки отмены
                        </Button>

                        <ConfirmModal
                            ref={el => {
                                this.modal = el;
                                return this.modal;
                            }}
                        >
                            Содержимое ConfirmModal
                        </ConfirmModal>
                    </Example>
                </div>
            </DemoComponent>
        );
    }
}

export default DemoModal;
