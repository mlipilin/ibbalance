# Код

У данного компонента есть методы, которые можно вызвать через `ref`.

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { ConfirmModal, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/default/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Button
      onClick={() =>
        this.modal.open({
          cancelBtnText: 'Нет',
          confirmBtnText: 'Да',
          title: 'Вы уверены?',
          onCancel: () => console.log('Cancel callback'),
          onConfirm: () => console.log('Confirm callback'),
        })
      }
    >
      Обычный Confirm
    </Button>
    <Button
      onClick={() =>
        this.modal.open({
          cancelBtnText: null,
          confirmBtnText: 'Ок',
          title: 'Действие успешно выполнилось',
          onConfirm: () => console.log('Confirm callback'),
        })
      }
    >
      Confirm без кнопки отмены
    </Button>

    <ConfirmModal ref={el => (this.modal = el)}>
      Содержимое ConfirmModal
    </ConfirmModal>
  </ThemeProvider>,
  document.getElementById('root')
);
```
