import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

// Components
import Modal from '../Modal';
import Button from '../Button';

// Constants
import { KEY_ENTER, KEY_ESC } from '../../constants/keyboard';

import { withTheme } from '../../theme-provider';

@withTheme
class ConfirmModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false,
            params: {},
        };
    }

    componentWillUnmount() {
        this.removeEvents();
    }

    addEvents = () => {
        window.addEventListener('keyup', this.handleWindowKeyup);
    };

    removeEvents = () => {
        window.removeEventListener('keyup', this.handleWindowKeyup);
    };

    handleWindowKeyup = e => {
        switch (e.keyCode) {
            case KEY_ENTER:
                this.handleConfirmBtnClick();
                break;
            case KEY_ESC:
                this.handleCancelBtnClick();
                break;
            default:
                break;
        }
    };

    open = (sourceParams = {}) => {
        const {
            cancelBtnText = 'Нет',
            confirmBtnText = 'Да',
            title = 'Вы уверены?',
            onCancel = _ => _,
            onConfirm = _ => _,
        } = sourceParams || {};

        const params = { cancelBtnText, confirmBtnText, title, onCancel, onConfirm };
        this.setState({ isModalOpen: true, params }, () => {
            this.addEvents();
        });
    };

    close = () => {
        this.handleCancelBtnClick();
    };

    handleConfirmBtnClick = () => {
        const {
            params: { onConfirm },
        } = this.state;
        if (typeof onConfirm === 'function') {
            this.setState({ isModalOpen: false }, () => {
                onConfirm(this.props);
            });
        }
        this.removeEvents();
    };

    handleCancelBtnClick = () => {
        const {
            params: { onCancel },
        } = this.state;
        if (typeof onCancel === 'function') {
            this.setState({ isModalOpen: false }, () => {
                onCancel(this.props);
            });
        }
        this.removeEvents();
    };

    render() {
        const { children, className, applyTheme } = this.props;
        const { isModalOpen, params } = this.state;
        const { title, cancelBtnText, confirmBtnText } = params;
        const componentClass = applyTheme(cn('ConfirmModal'), className);
        const childrenClass = applyTheme(cn('ConfirmModal__Content'));
        const buttonsClass = applyTheme(cn('ConfirmModal__Buttons'));
        const buttonClass = applyTheme(cn('ConfirmModal__Button'));

        return (
            <Modal
                className={componentClass}
                disableOverlayClick
                isOpen={isModalOpen}
                title={title}
                hideCloseBtn
            >
                {!!children && <div className={childrenClass}>{children}</div>}

                {/* Buttons */}
                <div className={buttonsClass}>
                    {!!cancelBtnText && (
                        <div className={buttonClass}>
                            <Button block onClick={this.handleCancelBtnClick}>
                                {cancelBtnText}
                            </Button>
                        </div>
                    )}
                    <div className={buttonClass}>
                        <Button block type="primary" onClick={this.handleConfirmBtnClick}>
                            {confirmBtnText}
                        </Button>
                    </div>
                </div>
            </Modal>
        );
    }
}

ConfirmModal.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    applyTheme: PropTypes.func,
};

ConfirmModal.defaultProps = {
    children: null,
    className: '',
    applyTheme: _ => _,
};

export default ConfirmModal;
