## Usage

```jsx
<ConfirmModal ref={el => (this.modal = el)}>
    Some content
</ConfirmModal>
```

## Props

| Prop name      | Prop type   | Default value |
| -------------- | ----------- | ------------- |
| _children_     | **node**    | null          |
| _className_    | **string**  | ''            |

## Methods

| Method name | Method params | Description              |
| ----------- | ------------- | ------------------------ |
| _open_      | `params = {}` | Открывает модальное окно |
| _close_     |               | Закрывает модальное окно |

## CSS Selectors

```css
.ConfirmModal {
}
.ConfirmModal__Content {
}
.ConfirmModal__Buttons {
}
.ConfirmModal__Button {
}
```
