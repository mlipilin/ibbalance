import React from 'react';
import PropTypes from 'prop-types';

// Components
import ImageContain from './image-contain';
import ImageCover from './image-cover';
import ImageDefault from './image-default';

// Constants
import { IMAGE_SIZES } from '../../constants/props';

import { withTheme } from '../../theme-provider';

function Image(props) {
    const { size, withLoader, applyTheme, ...imgProps } = props;

    if (size === IMAGE_SIZES.CONTAIN) {
        return <ImageContain withLoader={withLoader} applyTheme={applyTheme} {...imgProps} />;
    }
    if (size === IMAGE_SIZES.COVER) {
        return <ImageCover withLoader={withLoader} applyTheme={applyTheme} {...imgProps} />;
    }
    if (size === IMAGE_SIZES.DEFAULT) {
        return <ImageDefault withLoader={withLoader} applyTheme={applyTheme} {...imgProps} />;
    }

    return null;
}

Image.propTypes = {
    size: PropTypes.oneOf(Object.values(IMAGE_SIZES)),
    src: PropTypes.string,
    withLoader: PropTypes.bool,
    applyTheme: PropTypes.func,
};

Image.defaultProps = {
    size: IMAGE_SIZES.DEFAULT,
    src: null,
    withLoader: false,
    applyTheme: _ => _,
};

export default withTheme(Image);
