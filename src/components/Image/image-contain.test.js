import React from 'react';
import { fireEvent } from '@testing-library/react';
import { act } from 'react-dom/test-utils';

import ImageContain from './index';

import { render } from '../../hoc/with-theme';

describe('Тестирование компонента ImageContain', () => {
    it('Должен отобразиться ImageContain ', () => {
        const { container } = render(<ImageContain />);
        const wrapper = container.querySelector('.Image');
        expect(wrapper).toBeInTheDocument();
    });
    it('Должен отобразиться ImageContain с классом ImageContain__Styled', () => {
        const { container } = render(<ImageContain className="ImageContain__Styled" />);
        const wrapper = container.querySelector('.Image');
        expect(wrapper).toHaveClass('ImageContain__Styled');
    });

    it('Должен отобразиться ImageContain с изображением', () => {
        const { container } = render(<ImageContain src="image_300x200.png" />);
        const img = container.querySelector('img');
        expect((img.src = 'image_300x200.png')).toBeTruthy();
    });
    it('Должен отобразиться ImageContain с loader пока загужается изображение', () => {
        const { container } = render(<ImageContain src="image_300x200.png" withLoader />);
        const wrapper = container.querySelector('.Image');
        expect(wrapper).toHaveClass('Image_show-loader');
    });

    it('Должен отобразиться ImageContain с изображением alt', () => {
        const { container } = render(
            <ImageContain src="image_300x200.png" alt="image for example" />,
        );
        const img = container.querySelector('img');
        expect((img.alt = 'image for example')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться ImageContain с загруженным изображением', async () => {
        jest.useFakeTimers();
        const { container } = render(<ImageContain src="image_3000x2000.png" withLoader />);
        await act(async () => {
            const wrapper = container.querySelector('.Image');
            expect(wrapper).toHaveClass('Image_show-loader');
            const img = container.querySelector('img');
            await fireEvent.load(img);
            await jest.advanceTimersByTime(600);
            expect(wrapper).not.toHaveClass('Image_show-loader');
        });
    });
});
