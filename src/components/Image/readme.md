## Usage

```jsx
<Image src="path_to_image" />
```

Size:

```jsx
<Image src="path_to_image" size="contain" />
<Image src="path_to_image" size="cover" />
```

With loader:

```jsx
<Image src="path_to_image" withLoader />
```

## Props

Prop name | Prop type | Default value
--- | --- | ---
*size*|`cover`, `contain`, `default`|`default`
*src*|string|`null`
*withLoader*|bool|`false`

## CSS Selectors

```css
.Image {}
.Image_show-loader {}

/* Size */
.Image_size_contain {}
.Image_size_cover {}

.Image__Loader {}
```
