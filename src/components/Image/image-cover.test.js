import React from 'react';
import { fireEvent } from '@testing-library/react';
import { act } from 'react-dom/test-utils';

import ImageCover from './index';

import { render } from '../../hoc/with-theme';

describe('Тестирование компонента ImageCover', () => {
    it('Должен отобразиться ImageCover ', () => {
        const { container } = render(<ImageCover />);
        const wrapper = container.querySelector('.Image');
        expect(wrapper).toBeInTheDocument();
    });
    it('Должен отобразиться ImageCover с классом ImageCover__Styled', () => {
        const { container } = render(<ImageCover className="ImageCover__Styled" />);
        const wrapper = container.querySelector('.Image');
        expect(wrapper).toHaveClass('ImageCover__Styled');
    });

    it('Должен отобразиться ImageCover с изображением', () => {
        const { container } = render(<ImageCover src="image_300x200.png" />);
        const img = container.querySelector('img');
        expect((img.src = 'image_300x200.png')).toBeTruthy();
    });
    it('Должен отобразиться ImageCover с loader пока загужается изображение', () => {
        const { container } = render(<ImageCover src="image_300x200.png" withLoader />);
        const wrapper = container.querySelector('.Image');
        expect(wrapper).toHaveClass('Image_show-loader');
    });

    it('Должен отобразиться ImageCover с изображением alt', () => {
        const { container } = render(
            <ImageCover src="image_300x200.png" alt="image for example" />,
        );
        const img = container.querySelector('img');
        expect((img.alt = 'image for example')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться ImageCover с загруженным изображением', async () => {
        jest.useFakeTimers();
        const { container } = render(<ImageCover src="image_3000x2000.png" withLoader />);
        await act(async () => {
            const wrapper = container.querySelector('.Image');
            expect(wrapper).toHaveClass('Image_show-loader');
            const img = container.querySelector('img');
            await fireEvent.load(img);
            await jest.advanceTimersByTime(600);
            expect(wrapper).not.toHaveClass('Image_show-loader');
        });
    });
});
