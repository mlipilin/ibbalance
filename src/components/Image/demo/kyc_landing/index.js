import React from 'react';

// Components
import Image from '../../index';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';

// Examples
import size from './examples/size.md';
import withLoader from './examples/withLoader.md';

// Wrappers
import { Col, Row } from '../../../../wrappers/WGrid';

const DemoImage = () => {
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>

            <div id="example">
                <Row
                    cols={{ mobile: [1], tab: [1, 1], desktop: [1], largeDesktop: [1, 1] }}
                    gap="20px"
                >
                    <Col>
                        <Example readme={size}>
                            <div style={{ display: 'flex', justifyContent: 'space-around' }}>
                                <div>
                                    <p>Cover:</p>
                                    <div
                                        style={{
                                            boxShadow: '0 0 3px black',
                                            height: '100px',
                                            width: '100px',
                                        }}
                                    >
                                        <Image
                                            size="cover"
                                            src={require('../../../../../public/demo/image_300x200.png')}
                                        />
                                    </div>
                                </div>
                                <div>
                                    <p>Contain (1):</p>
                                    <div
                                        style={{
                                            boxShadow: '0 0 3px black',
                                            height: '100px',
                                            width: '100px',
                                        }}
                                    >
                                        <Image
                                            size="contain"
                                            src={require('../../../../../public/demo/image_300x200.png')}
                                        />
                                    </div>
                                </div>
                                <div>
                                    <p>Contain (2):</p>
                                    <div
                                        style={{
                                            boxShadow: '0 0 3px black',
                                            height: '100px',
                                            width: '100px',
                                        }}
                                    >
                                        <Image
                                            size="contain"
                                            src={require('../../../../../public/demo/image_200x300.png')}
                                        />
                                    </div>
                                </div>
                            </div>
                        </Example>
                    </Col>
                    <Col>
                        <Example readme={withLoader}>
                            <div style={{ display: 'flex', justifyContent: 'space-around' }}>
                                <div>
                                    <p>Cover:</p>
                                    <div
                                        style={{
                                            boxShadow: '0 0 3px black',
                                            height: '100px',
                                            width: '100px',
                                        }}
                                    >
                                        <Image
                                            size="cover"
                                            src={require('../../../../../public/demo/image_3000x2000.png')}
                                            withLoader
                                        />
                                    </div>
                                </div>
                                <div>
                                    <p>Contain:</p>
                                    <div
                                        style={{
                                            boxShadow: '0 0 3px black',
                                            height: '100px',
                                            width: '100px',
                                        }}
                                    >
                                        <Image
                                            size="contain"
                                            src={require('../../../../../public/demo/image_3000x2000.png')}
                                            withLoader
                                        />
                                    </div>
                                </div>
                            </div>
                        </Example>
                    </Col>
                </Row>
            </div>
        </DemoComponent>
    );
};

export default DemoImage;
