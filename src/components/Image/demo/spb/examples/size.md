# Варианты расположения в контейнере

Работает по аналогии с CSS-свойством `background-size`.

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Image, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/spb/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <div style={{ display: 'flex', justifyContent: 'space-around' }}>
        <div>
            <p>Cover:</p>
            <div
                style={{
                    boxShadow: '0 0 3px black',
                    height: '100px',
                    width: '100px',
                }}
            >
                <Image
                    size="cover"
                    src={require('../../../../../public/demo/image_300x200.png')}
                />
            </div>
        </div>
        <div>
            <p>Contain (1):</p>
            <div
                style={{
                    boxShadow: '0 0 3px black',
                    height: '100px',
                    width: '100px',
                }}
            >
                <Image
                    size="contain"
                    src={require('../../../../../public/demo/image_300x200.png')}
                />
            </div>
        </div>
        <div>
            <p>Contain (2):</p>
            <div
                style={{
                    boxShadow: '0 0 3px black',
                    height: '100px',
                    width: '100px',
                }}
            >
                <Image
                    size="contain"
                    src={require('../../../../../public/demo/image_200x300.png')}
                />
            </div>
        </div>
    </div>
  </ThemeProvider>,
  document.getElementById('root')
);
```
