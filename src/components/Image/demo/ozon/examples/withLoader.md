# Индикация загрузки изображения

Подходит для ситуация, когда размер изображения слишком большой и хочется показать индикатор загрузки.

**Внимание!**

Ввиду того, что большой размер изображение подразумевает его большое разрешение, рекомендуется также определять свойство `size`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Image, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/ozon/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <div style={{ display: 'flex', justifyContent: 'space-around' }}>
        <div>
            <p>Cover:</p>
            <div
                style={{
                    boxShadow: '0 0 3px black',
                    height: '100px',
                    width: '100px',
                }}
            >
                <Image
                    size="cover"
                    src={require('../../../../../public/demo/image_3000x2000.png')}
                    withLoader
                />
            </div>
        </div>
        <div>
            <p>Contain:</p>
            <div
                style={{
                    boxShadow: '0 0 3px black',
                    height: '100px',
                    width: '100px',
                }}
            >
                <Image
                    size="contain"
                    src={require('../../../../../public/demo/image_3000x2000.png')}
                    withLoader
                />
            </div>
        </div>
    </div>
  </ThemeProvider>,
  document.getElementById('root')
);
```
