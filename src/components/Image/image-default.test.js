import React from 'react';
import { fireEvent } from '@testing-library/react';
import { act } from 'react-dom/test-utils';

import ImageDefault from './index';

import { render } from '../../hoc/with-theme';

describe('Тестирование компонента ImageDefault', () => {
    it('Должен отобразиться ImageDefault ', () => {
        const { container } = render(<ImageDefault />);
        const wrapper = container.querySelector('.Image');
        expect(wrapper).toBeInTheDocument();
    });
    it('Должен отобразиться ImageDefault с классом ImageDefault__Styled', () => {
        const { container } = render(<ImageDefault className="ImageDefault__Styled" />);
        const wrapper = container.querySelector('.Image');
        expect(wrapper).toHaveClass('ImageDefault__Styled');
    });

    it('Должен отобразиться ImageDefault с изображением', () => {
        const { container } = render(<ImageDefault src="image_300x200.png" />);
        const img = container.querySelector('img');
        expect((img.src = 'image_300x200.png')).toBeTruthy();
    });
    it('Должен отобразиться ImageDefault с loader пока загужается изображение', () => {
        const { container } = render(<ImageDefault src="image_300x200.png" withLoader />);
        const wrapper = container.querySelector('.Image');
        expect(wrapper).toHaveClass('Image_show-loader');
    });

    it('Должен отобразиться ImageDefault с изображением alt', () => {
        const { container } = render(
            <ImageDefault src="image_300x200.png" alt="image for example" />,
        );
        const img = container.querySelector('img');
        expect((img.alt = 'image for example')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });
    it('Должен отобразиться ImageDefault с загруженным изображением', async () => {
        jest.useFakeTimers();
        const { container } = render(<ImageDefault src="image_3000x2000.png" withLoader />);
        await act(async () => {
            const wrapper = container.querySelector('.Image');
            expect(wrapper).toHaveClass('Image_show-loader');
            const img = container.querySelector('img');
            await fireEvent.load(img);
            await jest.advanceTimersByTime(600);
            expect(wrapper).not.toHaveClass('Image_show-loader');
        });
    });
});
