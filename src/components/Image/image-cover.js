import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

// Components
import Spin from '../Spin';

// Helpers
import { getCoverSize } from '../../helpers/size';

import { withTheme } from '../../theme-provider';

function ImageCover(props) {
    const { className, src, withLoader, applyTheme, ...otherProps } = props;

    const container = useRef(null);
    const img = useRef(null);

    // Hooks
    const [isLoading, setIsLoading] = useState(withLoader);
    const [style, setStyle] = useState({});

    // Handlers
    function handleImageLoad() {
        const {
            height: containerHeight,
            width: containerWidth,
        } = container.current.getBoundingClientRect();
        const { height: elementHeight, width: elementWidth } = img.current.getBoundingClientRect();
        const { height, width } = getCoverSize({
            containerWidth,
            containerHeight,
            elementHeight,
            elementWidth,
        });
        setStyle({ height: `${height}px`, width: `${width}px` });

        if (withLoader) {
            setTimeout(() => setIsLoading(false), 500);
        } else {
            setIsLoading(false);
        }
    }

    // Render props
    const componentClass = applyTheme(
        cn('Image', 'Image_size_cover', {
            'Image_show-loader': withLoader && isLoading,
        }),
        className,
    );
    const loaderClass = applyTheme(cn('Image__Loader'));

    return (
        <div className={componentClass} ref={container}>
            <img
                ref={img}
                src={src}
                {...otherProps}
                style={style}
                onLoad={handleImageLoad}
                alt=""
            />
            <div className={loaderClass}>
                <Spin />
            </div>
        </div>
    );
}

ImageCover.propTypes = {
    className: PropTypes.string,
    src: PropTypes.string,
    withLoader: PropTypes.bool,
    applyTheme: PropTypes.func,
};

ImageCover.defaultProps = {
    className: null,
    src: null,
    withLoader: false,
    applyTheme: _ => _,
};

export default withTheme(ImageCover);
