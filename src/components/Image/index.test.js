import React from 'react';
import { fireEvent } from '@testing-library/react';
import { act } from 'react-dom/test-utils';

import Image from './index';

import { render } from '../../hoc/with-theme';

describe('Тестирование компонента Image', () => {
    it('Должен отобразиться Image ', () => {
        const { container } = render(<Image />);
        const wrapper = container.querySelector('.Image');
        expect(wrapper).toBeInTheDocument();
    });
    it('Должен отобразиться Image с классом Image__Styled', () => {
        const { container } = render(<Image className="Image__Styled" />);
        const wrapper = container.querySelector('.Image');
        expect(wrapper).toHaveClass('Image__Styled');
    });
    it('Должен отобразиться Image с size = contain', () => {
        const { container } = render(<Image src="image_300x200.png" size="contain" />);
        const wrapper = container.querySelector('.Image');
        expect(wrapper).toHaveClass('Image_size_contain');
    });
    it('Должен отобразиться Image с size = cover', () => {
        const { container } = render(<Image src="image_300x200.png" size="cover" />);
        const wrapper = container.querySelector('.Image');
        expect(wrapper).toHaveClass('Image_size_cover');
    });

    it('Должен отобразиться Image с изображением', () => {
        const { container } = render(<Image src="image_300x200.png" />);
        const img = container.querySelector('img');
        expect((img.src = 'image_300x200.png')).toBeTruthy();
    });
    it('Должен отобразиться Image с loader пока загужается изображение', () => {
        const { container } = render(<Image src="image_300x200.png" withLoader />);
        const wrapper = container.querySelector('.Image');
        expect(wrapper).toHaveClass('Image_show-loader');
    });

    it('Должен отобразиться Image с загруженным изображением', async () => {
        jest.useFakeTimers();
        const { container } = render(<Image src="image_3000x2000.png" withLoader />);
        await act(async () => {
            const wrapper = container.querySelector('.Image');
            expect(wrapper).toHaveClass('Image_show-loader');
            const img = container.querySelector('img');
            await fireEvent.load(img);
            await jest.advanceTimersByTime(600);
            expect(wrapper).not.toHaveClass('Image_show-loader');
        });
    });

    it('Должен отобразиться Image с изображением alt', () => {
        const { container } = render(<Image src="image_300x200.png" alt="image for example" />);
        const img = container.querySelector('img');
        expect((img.alt = 'image for example')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });
});
