import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, select } from '@storybook/addon-knobs';

// Constants
import { IMAGE_SIZES } from '../../../constants/props';

import Image from '../index';

import notes from '../readme.md';

const stories = storiesOf('Image', module);

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories
    .add(
        'Size',
        () => (
            <>
                <div style={{ border: '3px solid black', height: '100px', width: '100px' }}>
                    <Image
                        alt="300x200"
                        size={select('Size', Object.values(IMAGE_SIZES), IMAGE_SIZES.DEFAULT)}
                        src={require('../../../../public/demo/image_300x200.png')}
                        title="Ширина изображения больше контейнера"
                    />
                </div>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <div style={{ border: '3px solid black', height: '100px', width: '100px' }}>
                    <Image
                        alt="200x300"
                        size={select('Size', Object.values(IMAGE_SIZES), IMAGE_SIZES.DEFAULT)}
                        src={require('../../../../public/demo/image_200x300.png')}
                        title="Высота изображения больше контейнера"
                    />
                </div>
            </>
        ),
        { notes },
    )
    .add(
        'With loader',
        () => (
            <>
                <div style={{ border: '3px solid black', height: '100px', width: '100px' }}>
                    <Image
                        alt="3000x2000"
                        size={IMAGE_SIZES.COVER}
                        src={require('../../../../public/demo/image_3000x2000.png')}
                        title="Огромная картинка, нужен лоадер"
                        withLoader
                    />
                </div>
                <br />
                <div style={{ border: '3px solid black', height: '100px', width: '100px' }}>
                    <Image
                        alt="3000x2000"
                        size={IMAGE_SIZES.CONTAIN}
                        src={require('../../../../public/demo/image_3000x2000.png')}
                        title="Огромная картинка, нужен лоадер"
                        withLoader
                    />
                </div>
                <br />
                <div style={{ border: '3px solid black', height: '100px', width: '100px' }}>
                    <Image
                        alt="3000x2000"
                        src={require('../../../../public/demo/image_3000x2000.png')}
                        title="Огромная картинка, нужен лоадер"
                        withLoader
                    />
                </div>
            </>
        ),
        { notes },
    );
