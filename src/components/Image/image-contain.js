import React, { useState } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

// Components
import Spin from '../Spin';

import { withTheme } from '../../theme-provider';

function ImageContain(props) {
    const { className, src, withLoader, applyTheme, ...otherProps } = props;

    // Hooks
    const [isLoading, setIsLoading] = useState(withLoader);

    // Handlers
    function handleImageLoad() {
        if (withLoader) {
            setTimeout(() => setIsLoading(false), 500);
        } else {
            setIsLoading(false);
        }
    }

    // Render props
    const componentClass = applyTheme(
        cn('Image', 'Image_size_contain', {
            'Image_show-loader': withLoader && isLoading,
        }),
        className,
    );
    const loaderClass = applyTheme(cn('Image__Loader'));

    return (
        <div className={componentClass}>
            <img src={src} {...otherProps} onLoad={handleImageLoad} alt="" />
            <div className={loaderClass}>
                <Spin />
            </div>
        </div>
    );
}

ImageContain.propTypes = {
    className: PropTypes.string,
    src: PropTypes.string,
    withLoader: PropTypes.bool,
    applyTheme: PropTypes.func,
};

ImageContain.defaultProps = {
    className: null,
    src: null,
    withLoader: false,
    applyTheme: _ => _,
};

export default withTheme(ImageContain);
