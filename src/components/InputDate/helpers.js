import moment from 'moment';

import { dateLessThanMax, dateMoreThanMin } from '../../helpers/date';

export const getDateFromString = value => {
    // DD.MM.YYYY
    if (
        value &&
        typeof value === 'string' &&
        new RegExp(/(^0[1-9]|[12][0-9]|3[01]).(0[1-9]|1[0-2]).(\d{4}$)/).test(value)
    ) {
        const arr = value.split('.').reverse();
        return new Date(arr[0], arr[1] - 1, arr[2]);
    }
    return null;
};

export const getErrorTextDate = (
    dateValue = null,
    error = null,
    hasFocus = false,
    max = null,
    min = null,
    maxText = null,
    minText = null,
) => {
    if (hasFocus) {
        return null;
    }
    const date = dateValue ? moment(dateValue, 'DD.MM.YYYY') : null;
    if (error) {
        return error;
    }
    if (!dateMoreThanMin(date, min)) {
        return minText || `Дата не может быть меньше ${min}`;
    }
    if (!dateLessThanMax(date, max)) {
        return maxText || `Дата не может быть больше ${max}`;
    }
    return null;
};

export default { getDateFromString };
