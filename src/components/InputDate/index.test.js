import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import FeatherIcon from '../FeatherIcon';
import InputDate from './index';

import { render } from '../../hoc/with-theme';

const intersectionObserverOrigin = window.IntersectionObserver;

describe('InputDate', () => {
    const REAL_DATE = Date;
    beforeAll(() => {
        Date.now = jest.fn(() => 1609489800000);
        window.IntersectionObserver = jest.fn(() => ({
            disconnect: jest.fn(),
            observe: jest.fn(),
            unobserve: jest.fn(),
        }));
    });
    afterAll(() => {
        Date.now = REAL_DATE.now;
        window.IntersectionObserver = intersectionObserverOrigin;
    });
    const initOpenCalendar = async props => {
        const { container } = render(<InputDate {...props} />);
        const iconCalendar = container.querySelector('.feather-calendar');
        await userEvent.click(iconCalendar);
    };
    it('ничего не передали -> отобразиться InputDate', () => {
        const { container } = render(<InputDate />);
        expect(screen.getByLabelText('')).toBeInTheDocument();
        const icon = container.querySelector('.InputDate__Icon');
        expect(icon).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });
    it('передали disabled -> есть класс "Input_disabled"', () => {
        const { container } = render(<InputDate disabled />);
        const input = container.querySelector('.Input_disabled');
        expect(input).toBeInTheDocument();
        expect(input).toMatchSnapshot();
    });
    it('передали disabled -> есть класс "Input__DisabledIcon"', () => {
        const { container } = render(<InputDate disabled />);
        const iconDisabled = container.querySelector('.Input__DisabledIcon');
        expect(iconDisabled).toBeInTheDocument();
        expect(iconDisabled).toMatchSnapshot();
    });
    describe('передали disableUserInput', () => {
        let options;
        beforeEach(() => {
            options = render(<InputDate disableUserInput />);
        });
        it('-> есть readonly свойство у input', () => {
            const input = options.container.querySelector('input');
            expect(input.readOnly).toBeTruthy();
            expect(input).toMatchSnapshot();
        });
        it('клик по иконке календаря -> есть класс "Calendar"', async () => {
            const iconCalendar = options.container.querySelector('.feather-calendar');
            await userEvent.click(iconCalendar);
            const calendar = document.querySelector('.Calendar');
            expect(calendar).toBeInTheDocument();
        });
        it('onChange, ввод -> не вызывается обработчик onChange', async () => {
            const handleChange = jest.fn();
            options.rerender(<InputDate disableUserInput onChange={handleChange} />);
            const input = options.container.querySelector('input');
            userEvent.type(input, '01.02.2021');
            expect(handleChange).toBeCalledTimes(0);
        });
    });
    describe('передали error', () => {
        const ERROR_TEXT = 'Некорректная дата';
        it('передали пустой error -> отобразится класс "Input__Error" c пустым текстом', () => {
            const { container } = render(<InputDate error />);
            const error = container.querySelector('.Input__Error');
            expect(error).toBeInTheDocument();
            expect(error).toHaveTextContent('');
            expect(error).toMatchSnapshot();
        });
        it('передали error as string -> отобразиться текст ошибки', () => {
            const { container } = render(<InputDate error={ERROR_TEXT} />);
            const error = container.querySelector('.Input__Error');
            expect(error).toHaveTextContent(ERROR_TEXT);
            expect(error).toMatchSnapshot();
        });
        it('передали error as node -> отобразиться переданный node', () => {
            render(<InputDate error={<span data-testid={ERROR_TEXT}>{ERROR_TEXT}</span>} />);
            const error = screen.getByTestId(ERROR_TEXT);
            expect(error).toHaveTextContent(ERROR_TEXT);
            expect(error).toMatchSnapshot();
        });
    });

    describe('передали errorDetail', () => {
        const ERROR_TEXT = 'Некорректная дата';
        it('-> отобразиться заданный текст', () => {
            const { container } = render(<InputDate errorDetail={ERROR_TEXT} />);
            const error = container.querySelector('.Input__ErrorDetail');
            expect(error).toBeInTheDocument();
            expect(error).toHaveTextContent(ERROR_TEXT);
            expect(error).toMatchSnapshot();
        });
        it('-> иконка по-умолчанию имеет класс feather-alert-circle', () => {
            const { container } = render(<InputDate errorDetail={ERROR_TEXT} />);
            const detailIcon = container.querySelector('.feather-alert-circle');
            expect(detailIcon).toBeInTheDocument();
            expect(detailIcon).toMatchSnapshot();
        });
        it('клик на иконку ошибки -> откроется тултип', () => {
            const { container } = render(<InputDate errorDetail={ERROR_TEXT} />);
            const detailIcon = container.querySelector('.feather-alert-circle');
            const tooltip = container.querySelector('.Input__Tooltip');
            userEvent.click(detailIcon);
            expect(tooltip.classList.contains('Input__Tooltip_visible')).toBeTruthy();
            expect(tooltip).toMatchSnapshot();
        });

        it('передали errorDetailIcon -> отобразиться "feather-alert-triangle"', () => {
            const { container } = render(
                <InputDate
                    errorDetail={ERROR_TEXT}
                    errorDetailIcon={<FeatherIcon name="alert-triangle" />}
                />,
            );
            const detailIcon = container.querySelector('.feather-alert-triangle');
            expect(detailIcon).toBeInTheDocument();
            expect(container.firstChild).toMatchSnapshot();
        });
    });
    it('передали errorTextMaxDate, max, value больше макс -> отобразиться текст ошибки', async () => {
        const MAX = '05.01.2021';
        const VALUE = '06.01.2021';
        const TEXT = `Ввели дату, больше, чем ${MAX}`;
        const { container } = render(<InputDate errorTextMaxDate={TEXT} max={MAX} value={VALUE} />);
        const input = container.querySelector('input');
        const error = container.querySelector('.Input__Error');
        expect(error).toHaveTextContent(TEXT);
        expect(input).toMatchSnapshot();
    });
    it('не передали errorTextMaxDate, передали max, value больше макс -> отобразиться текст ошибки по-умолчанию', async () => {
        const MAX = '05.01.2021';
        const VALUE = '06.01.2021';
        const TEXT = `Дата не может быть больше ${MAX}`;
        const { container } = render(<InputDate max={MAX} value={VALUE} />);
        const input = container.querySelector('input');
        const error = container.querySelector('.Input__Error');
        expect(error).toHaveTextContent(TEXT);
        expect(input).toMatchSnapshot();
    });
    it('передали errorTextMinDate, min, value меньше мин -> отобразиться текст ошибки', async () => {
        const MIN = '05.01.2021';
        const VALUE = '04.01.2021';
        const TEXT = `Ввели дату, больше, чем ${MIN}`;
        const { container } = render(<InputDate errorTextMinDate={TEXT} min={MIN} value={VALUE} />);
        const input = container.querySelector('input');
        input.focus();
        userEvent.tab();
        const error = container.querySelector('.Input__Error');
        expect(error).toHaveTextContent(TEXT);
        expect(input).toMatchSnapshot();
    });
    it('не передали errorTextMinDate, передали min, value меньше мин -> отобразиться текст ошибки', async () => {
        const MIN = '05.01.2021';
        const VALUE = '04.01.2021';
        const TEXT = `Дата не может быть меньше ${MIN}`;
        const { container } = render(<InputDate min={MIN} value={VALUE} />);
        const input = container.querySelector('input');
        const error = container.querySelector('.Input__Error');
        expect(error).toHaveTextContent(TEXT);
        expect(input).toMatchSnapshot();
    });

    it('передали name -> отобразиться input.name с заданным значением', () => {
        const NAME = 'date';
        const { container } = render(<InputDate name={NAME} />);
        const input = container.querySelector('input');
        expect(input).toBeInTheDocument();
        expect(input.name).toBe(NAME);
        expect(input).toMatchSnapshot();
    });

    describe('свойство hideYearSwitch', () => {
        it('передали hideYearSwitch=true -> нет стрелки для навигации по году в будущее', async () => {
            await initOpenCalendar({ hideYearSwitch: true });
            const iconYearRight = document.querySelector('.feather-chevrons-right');
            expect(iconYearRight).not.toBeInTheDocument();
            expect(iconYearRight).toMatchSnapshot();
        });

        it('передали hideYearSwitch=true -> нет стрелки для навигации по году в прошлое', async () => {
            await initOpenCalendar({ hideYearSwitch: true });
            const iconYearLeft = document.querySelector('.feather-chevrons-left');
            expect(iconYearLeft).not.toBeInTheDocument();
            expect(iconYearLeft).toMatchSnapshot();
        });

        it('передали hideYearSwitch=false -> есть стрелка для навигации по году в будущее', async () => {
            await initOpenCalendar({ hideYearSwitch: false });
            const iconYearRight = document.querySelector('.feather-chevrons-right');
            expect(iconYearRight).toBeInTheDocument();
            expect(iconYearRight).toMatchSnapshot();
        });

        it('передали hideYearSwitch=false -> есть стрелка для навигации по году в прошлое', async () => {
            await initOpenCalendar({ hideYearSwitch: false });
            const iconYearLeft = document.querySelector('.feather-chevrons-left');
            expect(iconYearLeft).toBeInTheDocument();
            expect(iconYearLeft).toMatchSnapshot();
        });
        it('не передали hideYearSwitch -> есть стрелка для навигации по году в будущее', async () => {
            await initOpenCalendar();
            const iconYearRight = document.querySelector('.feather-chevrons-right');
            expect(iconYearRight).toBeInTheDocument();
            expect(iconYearRight).toMatchSnapshot();
        });

        it('не передали hideYearSwitch -> есть стрелка для навигации по году в прошлое', async () => {
            await initOpenCalendar();
            const iconYearLeft = document.querySelector('.feather-chevrons-left');
            expect(iconYearLeft).toBeInTheDocument();
            expect(iconYearLeft).toMatchSnapshot();
        });
    });

    it('передали iconCalendar -> отобразится переданная иконка "credit-card"', () => {
        const { container } = render(
            <InputDate iconCalendar={<FeatherIcon name="credit-card" />} />,
        );
        const iconCalendar = container.querySelector('.InputDate__Icon .feather');
        expect(iconCalendar).toHaveClass('feather-credit-card');
    });
    it('не передали iconCalendar -> отобразится иконка по-умолчанию', () => {
        const { container } = render(<InputDate />);
        const iconCalendar = container.querySelector('.InputDate__Icon .feather');
        expect(iconCalendar).toHaveClass('feather-calendar');
    });

    it('передали iconNextMonth -> отобразится переданная иконка "arrow-right"', async () => {
        await initOpenCalendar({ iconNextMonth: <FeatherIcon name="arrow-right" /> });

        const button = screen.getAllByRole('button')[2];
        const iconNextMonth = button.querySelector('.feather');
        expect(iconNextMonth).toHaveClass('feather-arrow-right');
        expect(iconNextMonth).toBeInTheDocument();
    });
    it('не передали iconNextMonth -> отобразится иконка по-умолчанию', async () => {
        await initOpenCalendar();

        const button = screen.getAllByRole('button')[2];
        const iconNextMonth = button.querySelector('.feather');
        expect(iconNextMonth).toHaveClass('feather-chevron-right');
    });

    it('передали iconNextYear -> отобразится переданная иконка "arrow-right"', async () => {
        await initOpenCalendar({ iconNextYear: <FeatherIcon name="arrow-right" /> });

        const button = screen.getAllByRole('button')[3];
        const iconNextYear = button.querySelector('.feather');
        expect(iconNextYear).toHaveClass('feather-arrow-right');
    });
    it('не передали iconNextYear -> отобразится иконка по-умолчанию', async () => {
        await initOpenCalendar();

        const button = screen.getAllByRole('button')[3];
        const iconNextYear = button.querySelector('.feather');
        expect(iconNextYear).toHaveClass('feather-chevrons-right');
    });

    it('передали iconPrevMonth -> отобразится переданная иконка "arrow-left"', async () => {
        await initOpenCalendar({ iconPrevMonth: <FeatherIcon name="arrow-left" /> });

        const button = screen.getAllByRole('button')[1];
        const iconPrevMonth = button.querySelector('.feather-arrow-left');
        expect(iconPrevMonth).toBeInTheDocument();
    });
    it('не передали iconPrevMonth -> отобразится иконка по-умолчанию', async () => {
        await initOpenCalendar();

        const button = screen.getAllByRole('button')[1];
        const iconPrevMonth = button.querySelector('.feather');
        expect(iconPrevMonth).toHaveClass('feather-chevron-left');
    });

    it('передали iconPrevYear -> отобразится переданная иконка "arrow-left"', async () => {
        await initOpenCalendar({ iconPrevYear: <FeatherIcon name="arrow-left" /> });

        const button = screen.getAllByRole('button')[0];
        const iconPrevYear = button.querySelector('.feather');
        expect(iconPrevYear).toHaveClass('feather-arrow-left');
    });
    it('не передали iconPrevYear -> отобразится иконка по-умолчанию', async () => {
        await initOpenCalendar();

        const button = screen.getAllByRole('button')[0];
        const iconPrevYear = button.querySelector('.feather');
        expect(iconPrevYear).toHaveClass('feather-chevrons-left');
    });

    describe('свойство label', () => {
        const TEXT_LABEL = 'Дата';
        it('передали label as text -> отобразится переданный текст', () => {
            render(<InputDate label={TEXT_LABEL} />);
            const label = screen.getByLabelText(TEXT_LABEL);
            expect(label).toBeInTheDocument();
            expect(label).toMatchSnapshot();
        });

        it('передали label as node -> отобразится переданная node', () => {
            render(<InputDate label={<span data-testid={TEXT_LABEL}>{TEXT_LABEL}</span>} />);
            const textElement = screen.getByTestId(TEXT_LABEL);
            expect(textElement).toHaveTextContent(TEXT_LABEL);
            expect(textElement).toMatchSnapshot();
        });

        it('передали label as array -> отобразятся все элементы переданного массива', () => {
            const FIRST = 'first';
            const SECOND = 'second';
            render(
                <InputDate
                    label={[
                        <span key={FIRST} data-testid={FIRST}>
                            {FIRST}
                        </span>,
                        <span key={SECOND} data-testid={SECOND}>
                            {SECOND}
                        </span>,
                    ]}
                />,
            );

            const first = screen.getByTestId(FIRST);
            expect(first).toHaveTextContent(FIRST);

            const second = screen.getByTestId(SECOND);
            expect(second).toHaveTextContent(SECOND);
        });
    });

    it('не передали max -> не имеет класс Calendar__DayButton_disabled ', async () => {
        await initOpenCalendar();
        const day = document.querySelectorAll('.Calendar__DayWrapper .Calendar__DayButton')[28];
        expect(day).not.toHaveClass('Calendar__DayButton_disabled');
        expect(day).toMatchSnapshot();
    });
    it('передали max -> 29.01.2021 имеет класс Calendar__DayButton_disabled ', async () => {
        const MAX = '28.01.2021';
        await initOpenCalendar({ max: MAX });
        const day = document.querySelectorAll('.Calendar__DayWrapper .Calendar__DayButton')[28];
        expect(day).toHaveClass('Calendar__DayButton_disabled');
        expect(day).toMatchSnapshot();
    });

    it('передали max, onChange выбираем дату больше max -> дата не выбрана ', async () => {
        const MAX = '28.01.2021';
        const handleChange = jest.fn();
        await initOpenCalendar({ max: MAX, onChange: handleChange });
        const day = document.querySelectorAll('.Calendar__DayWrapper .Calendar__DayButton')[28];
        await userEvent.click(day);
        expect(handleChange).toBeCalledTimes(0);
    });
    it('передали max, onChange выбираем дату меньше max -> дата выбрана ', async () => {
        const MAX = '28.01.2021';
        const handleChange = jest.fn();
        await initOpenCalendar({ max: MAX, onChange: handleChange });
        const day = document.querySelectorAll('.Calendar__DayWrapper .Calendar__DayButton')[25];
        await userEvent.click(day);
        expect(handleChange).toBeCalledTimes(1);
        expect(handleChange).toHaveBeenCalledWith('26.01.2021', expect.any(Object), true);
    });

    it('передали min -> 02.01.2021 имеет класс Calendar__DayButton_disabled ', async () => {
        const MIN = '03.01.2021';
        await initOpenCalendar({ min: MIN });
        const day = document.querySelectorAll('.Calendar__DayWrapper .Calendar__DayButton')[1];
        expect(day).toHaveClass('Calendar__DayButton_disabled');
        expect(day).toMatchSnapshot();
    });
    it('не передали min -> не имеет класс Calendar__DayButton_disabled ', async () => {
        await initOpenCalendar();
        const day = document.querySelectorAll('.Calendar__DayWrapper .Calendar__DayButton')[1];
        expect(day).not.toHaveClass('Calendar__DayButton_disabled');
        expect(day).toMatchSnapshot();
    });
    it('передали min, onChange выбираем дату меньше min -> дата не выбрана ', async () => {
        const MIN = '25.01.2021';
        const handleChange = jest.fn();
        await initOpenCalendar({ min: MIN, onChange: handleChange });
        const day = document.querySelectorAll('.Calendar__DayWrapper .Calendar__DayButton')[23];
        await userEvent.click(day);
        expect(handleChange).toBeCalledTimes(0);
    });
    it('передали min, onChange выбираем дату больше min -> дата выбрана ', async () => {
        const MIN = '25.01.2021';
        const handleChange = jest.fn();
        await initOpenCalendar({ min: MIN, onChange: handleChange });
        const day = document.querySelectorAll('.Calendar__DayWrapper .Calendar__DayButton')[27];
        await userEvent.click(day);
        expect(handleChange).toBeCalledTimes(1);
        expect(handleChange).toHaveBeenCalledWith('28.01.2021', expect.any(Object), true);
    });

    it('передали placeholder -> отобразится переданный текст', () => {
        const PLACEHOLDER = 'Введите дату';
        const { container } = render(<InputDate placeholder={PLACEHOLDER} />);
        const input = container.querySelector('input');
        expect(input.placeholder).toBe(PLACEHOLDER);
        expect(input).toMatchSnapshot();
    });
    it('не передали placeholder -> отобразится ""', () => {
        const { container } = render(<InputDate />);
        const input = container.querySelector('input');
        expect(input.placeholder).toBeFalsy();
        expect(input).toMatchSnapshot();
    });

    it('не передали placement -> отобразится по-умолчанию bottom', async () => {
        await initOpenCalendar();
        const tooltip = document.querySelector('.Tooltip');
        expect(tooltip).toHaveClass('Tooltip_placement_bottom');
        expect(tooltip).toMatchSnapshot();
    });
    it('передали placement="top" -> отобразится с классом Tooltip_placement_top', async () => {
        document.elementFromPoint = jest.fn();
        const { container } = render(<InputDate placement="top" />);
        const iconCalendar = container.querySelectorAll('.feather-calendar')[1];
        await userEvent.click(iconCalendar);
        const tooltip = document.querySelector('.Tooltip');
        expect(tooltip).toHaveClass('Tooltip_placement_top');
        expect(tooltip).toMatchSnapshot();
    });

    it('передали placement="left" -> отобразится с классом Tooltip_placement_left', async () => {
        document.elementFromPoint = jest.fn();
        const { container } = render(<InputDate placement="left" />);
        const iconCalendar = container.querySelectorAll('.feather-calendar')[1];
        await userEvent.click(iconCalendar);
        const tooltip = document.querySelector('.Tooltip');
        expect(tooltip).toHaveClass('Tooltip_placement_left');
        expect(tooltip).toMatchSnapshot();
    });

    it('передали placement="right" -> отобразится с классом Tooltip_placement_right', async () => {
        document.elementFromPoint = jest.fn();
        const { container } = render(<InputDate placement="right" />);
        const iconCalendar = container.querySelectorAll('.feather-calendar')[1];
        await userEvent.click(iconCalendar);
        const tooltip = document.querySelector('.Tooltip');
        expect(tooltip).toHaveClass('Tooltip_placement_right');
        expect(tooltip).toMatchSnapshot();
    });
    it('передали placement="bottom" -> отобразится с классом Tooltip_placement_bottom', async () => {
        document.elementFromPoint = jest.fn();
        const { container } = render(<InputDate placement="bottom" />);
        const iconCalendar = container.querySelectorAll('.feather-calendar')[1];
        await userEvent.click(iconCalendar);
        const tooltip = document.querySelector('.Tooltip');
        expect(tooltip).toHaveClass('Tooltip_placement_bottom');
        expect(tooltip).toMatchSnapshot();
    });

    it('не передали position -> отобразится c position: absolute ', async () => {
        await initOpenCalendar();
        const tooltip = document.body.querySelector('.Tooltip');
        expect(tooltip).toHaveStyle({ position: 'absolute' });
        expect(tooltip).toMatchSnapshot();
    });
    it('передали position="fixed" -> отобразится c position: fixed ', async () => {
        await initOpenCalendar({ position: 'fixed' });
        const tooltip = document.body.querySelector('.Tooltip');
        expect(tooltip).toHaveStyle({ position: 'fixed' });
        expect(tooltip).toMatchSnapshot();
    });
    it('передали position="absolute" -> отобразится c position: absolute ', async () => {
        await initOpenCalendar({ position: 'absolute' });
        const tooltip = document.body.querySelector('.Tooltip');
        expect(tooltip).toHaveStyle({ position: 'absolute' });
        expect(tooltip).toMatchSnapshot();
    });

    it('не передали readonly -> отобразится input в состоянии readonly', async () => {
        const { container } = render(<InputDate />);
        const input = container.querySelector('input');
        expect(input.readOnly).toBeFalsy();
        expect(input).toMatchSnapshot();
    });
    it('передали readonly -> отобразится input в состоянии readonly', async () => {
        const { container } = render(<InputDate readOnly />);
        const input = container.querySelector('input');
        expect(input.readOnly).toBeTruthy();
        expect(input).toMatchSnapshot();
    });
    it('передали readonly -> при изменении onChange не вызыван', async () => {
        const onChange = jest.fn();
        const { container } = render(<InputDate readOnly onChange={onchange} />);
        const input = container.querySelector('input');
        await userEvent.type(input, '123');
        expect(onChange).toHaveBeenCalledTimes(0);
    });

    it('не передали size -> отобразится класс "InputDate_size_m"', () => {
        const { container } = render(<InputDate />);
        const wrapper = container.querySelector('.InputDate');
        expect(wrapper).toHaveClass('InputDate_size_m');
    });
    it('передали size="s" -> отобразится класс "InputDate_size_s"', () => {
        const { container } = render(<InputDate size="s" />);
        const wrapper = container.querySelector('.InputDate');
        expect(wrapper).toHaveClass('InputDate_size_s');
    });
    it('передали size="m" -> отобразится класс "InputDate_size_m"', () => {
        const { container } = render(<InputDate size="m" />);
        const wrapper = container.querySelector('.InputDate');
        expect(wrapper).toHaveClass('InputDate_size_m');
    });
    it('передали size="l" -> отобразится класс "InputDate_size_l"', () => {
        const { container } = render(<InputDate size="l" />);
        const wrapper = container.querySelector('.InputDate');
        expect(wrapper).toHaveClass('InputDate_size_l');
    });

    it('не передали success -> нет класса "Input__Input_success"', () => {
        const { container } = render(<InputDate />);
        const wrapper = container.querySelector('.Input__Input');
        expect(wrapper).not.toHaveClass('Input__Input_success');
    });
    it('передали success -> есть класса "Input__Input_success"', () => {
        const { container } = render(<InputDate success />);
        const wrapper = container.querySelector('.Input__Input');
        expect(wrapper).toHaveClass('Input__Input_success');
    });

    it('не передали date -> input.value пустая строка', () => {
        const { container } = render(<InputDate />);
        const input = container.querySelector('input');
        expect(input.value).toBe('');
        expect(input).toHaveTextContent('');
    });
    it('не передали date -> отображается пустая строка', () => {
        const { container } = render(<InputDate />);
        const input = container.querySelector('input');
        expect(input).toHaveTextContent('');
    });
    it('передали date -> input.value содержит date', () => {
        const DATE = '01.01.2021';
        const { container } = render(<InputDate value={DATE} />);
        const input = container.querySelector('input');
        expect(input.value).toBe(DATE);
    });

    it('передали parseValue -> в обработчике данные в заданном формате "01/01/2021"', async () => {
        const DATE = '01.01.2021';
        const handleChange = jest.fn();
        const { container } = render(
            <InputDate
                value={DATE}
                onChange={handleChange}
                parseValue={value => value.replace(/\./g, '/')}
            />,
        );
        const input = container.querySelector('input');
        await userEvent.type(input, '1');
        expect(handleChange).toHaveBeenCalledTimes(1);
        expect(handleChange).toBeCalledWith('01/01/2021', expect.any(Object), false);
        expect(input).toMatchSnapshot();
    });

    it('передали label, элемент в фокусе -> есть класс Input__LabelText_place-top', () => {
        const LABEL_TEXT = 'Дата';
        const { container } = render(<InputDate label={LABEL_TEXT} />);
        const input = container.querySelector('input');
        input.focus();
        const elem = container.querySelector('.Input__LabelText');
        expect(elem).toHaveClass('Input__LabelText_place-top');
    });

    it('элемент в фокусе -> есть фокус', () => {
        const { container } = render(<InputDate />);
        const input = container.querySelector('input');
        input.focus();
        expect(input).toHaveFocus();
    });

    it('передали обработчик onBlur, потеря фокуса -> вызовется обработчик', () => {
        const onBlur = jest.fn();
        const { container } = render(<InputDate onBlur={onBlur} />);
        const input = container.querySelector('input');
        input.focus();
        userEvent.tab();
        const elem = container.querySelector('.Input__LabelText_focus');
        expect(elem).not.toBeInTheDocument();
        expect(onBlur).toHaveBeenCalledTimes(1);
        expect(onBlur).toHaveBeenCalledWith(expect.any(Object));
    });
    it('передали label, потеря фокуса -> нет класса Input__LabelText_place-top', () => {
        const LABEL_TEXT = 'Дата';
        const { container } = render(<InputDate label={LABEL_TEXT} />);
        const input = container.querySelector('input');
        input.focus();
        userEvent.tab();
        const elem = container.querySelector('.Input__LabelText');
        expect(elem).not.toHaveClass('Input__LabelText_place-top');
    });
    it('передали обработчик onChange -> вызывается с корректными параметрами', async () => {
        const value = '01.01';
        const handleChange = jest.fn();
        const { container } = render(<InputDate value={value} onChange={handleChange} />);
        const input = container.querySelector('input');
        await userEvent.type(input, '2021');
        expect(handleChange).toHaveBeenNthCalledWith(1, '01.01.2', expect.any(Object), false);
    });
    it('передали обработчик onChange -> вызывается на каждое изменение', async () => {
        const handleChange = jest.fn();
        const { container } = render(<InputDate value="01.01" onChange={handleChange} />);
        const input = container.querySelector('input');
        await userEvent.type(input, '2021');
        expect(handleChange).toBeCalledTimes(4);
    });
});
