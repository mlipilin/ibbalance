# "read only" состояние

`true`: инпут становится 'read only' (только для чтения)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { InputDate, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/vkusvill/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <InputDate label="Поле (readonly)" readOnly />
  </ThemeProvider>,
  document.getElementById('root')
);
```
