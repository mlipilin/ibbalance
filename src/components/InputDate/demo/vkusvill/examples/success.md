# "success" состояние

`true`: инпут становится 'success' (значение в поле валидно)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { InputDate, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/vkusvill/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <InputDate label="Валидное поле" value="01.03.2000" success />
  </ThemeProvider>,
  document.getElementById('root')
);
```
