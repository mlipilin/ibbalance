import React from 'react';

// Components
import InputDate from '../../index';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';

// Examples
import basic from './examples/basic.md';
import dateRange from './examples/date-range.md';
import disabled from './examples/disabled.md';
import disableUserInput from '../default/examples/disabledUserInput.md';
import error from './examples/error.md';
import readOnly from './examples/readonly.md';
import size from './examples/size.md';
import success from './examples/success.md';

// HOC
import { withState } from '../../../../hoc';
// Wrappers
import { Col, Row } from '../../../../wrappers/WGrid';

const InputDateWithState = withState(InputDate);

const DemoInputDate = () => {
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>

            <div id="example">
                <Row
                    cols={{ mobile: [1], tab: [1, 1], desktop: [1], largeDesktop: [1, 1] }}
                    gap="20px"
                >
                    <Col>
                        <Example readme={basic}>
                            <InputDateWithState label="Дата" />
                        </Example>
                        <Example readme={dateRange}>
                            <InputDateWithState
                                label="поле с минимальным значением для даты"
                                min="03.04.2021"
                            />
                            <br />
                            <InputDateWithState
                                label="поле с максимальным значением для даты"
                                max="20.04.2021"
                            />
                            <br />
                            <InputDateWithState
                                label="поле с минимальным и максимальным значениями дат"
                                min="03.04.2021"
                                max="20.04.2021"
                            />
                        </Example>
                        <Example readme={size}>
                            <InputDateWithState label="Поле размера M" size="m" />
                            <br />
                            <InputDateWithState label="Поле размера S" size="s" />
                        </Example>
                        <Example readme={disabled}>
                            <InputDateWithState label="Поле (disabled)" disabled />
                        </Example>
                        <Example readme={disableUserInput}>
                            <InputDateWithState label="Поле (disableUserInput)" disableUserInput />
                        </Example>
                        <Example readme={readOnly}>
                            <InputDateWithState label="Поле (readonly)" readOnly />
                        </Example>
                    </Col>
                    <Col>
                        <Example readme={error}>
                            <InputDateWithState
                                label="Невалидное поле"
                                value="11.22.3333"
                                error="Некорректная дата"
                            />
                        </Example>
                        <Example readme={success}>
                            <InputDateWithState label="Валидное поле" value="01.03.2000" success />
                        </Example>
                    </Col>
                </Row>
            </div>
        </DemoComponent>
    );
};

export default DemoInputDate;
