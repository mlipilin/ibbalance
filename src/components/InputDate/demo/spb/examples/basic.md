# Базовое использование

Подходит для большинства случаев

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { InputDate, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/spb/theme';

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <InputDate />
    </ThemeProvider>,
    document.getElementById('root'),
);
```
