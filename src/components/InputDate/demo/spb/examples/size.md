# Размер поля

Свойство size может принимать значение `s` или `m` 

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { InputDate, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/spb/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <InputDate label="Поле размера M" size="m" />
    <br />
    <InputDate label="Поле размера S" size="s" />
  </ThemeProvider>,
  document.getElementById('root'),
);
```
