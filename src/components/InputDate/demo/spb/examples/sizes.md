# Размер поля

Свойство size может принимать значение `s` или `m` 

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Input, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/spb/theme';

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <InputDate size="m" />
        <InputDate size="s" />
    </ThemeProvider>,
    document.getElementById('root'),
);
```
