# Наличие ошибки

В поле `error` можно передать текст ошибки

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { InputDate, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/ozon/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <InputDate
      label="Невалидное поле"
      value="11.22.3333"
      error="Некорректная дата"
    />
  </ThemeProvider>,
  document.getElementById('root')
);
```
