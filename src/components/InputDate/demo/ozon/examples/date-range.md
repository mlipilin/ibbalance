# "min" значение, меньше минимального нельзя выбрать

`string`: строка в формате "DD.MM.YYYY"

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { InputDate, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/ozon/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <InputDate label="поле с минимальным значением для даты" min={'03.04.2021'} />
  </ThemeProvider>,
  document.getElementById('root')
);
```
