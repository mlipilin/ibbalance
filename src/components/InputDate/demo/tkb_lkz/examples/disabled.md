# "disabled" состояние

`true`: инпут становится 'disabled' (недоступен для редактирования)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { InputDate, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/tkb_lkz/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <InputDate label="Disabled поле" disabled />
  </ThemeProvider>,
  document.getElementById('root')
);
```
