# Размер поля

Свойство size может принимать одно из следующих значений: `s`, `m` и `l`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Input, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/default/theme';

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <InputDate size="l" />
        <InputDate size="m" />
        <InputDate size="s" />
    </ThemeProvider>,
    document.getElementById('root'),
);
```
