# Размер поля

Свойство size может принимать значение `s`, `m` или `l` 

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { InputDate, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/default/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <InputDate label="Поле размера L" size="l" />
    <br />
    <InputDate label="Поле размера M" size="m" />
    <br />
    <InputDate label="Поле размера S" size="s" />
  </ThemeProvider>,
  document.getElementById('root'),
);
```
