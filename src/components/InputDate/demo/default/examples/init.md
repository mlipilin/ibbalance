# Поле с начальным значением 

В поле `value` можно передать начальное значение

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { InputDate, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/default/theme';

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <InputDate value={"22.02.2020"}/>
    </ThemeProvider>,
    document.getElementById('root'),
);
```
