# "disableUserInput" состояние

`true`: инпут становится 'readonly' (недоступен для редактирования с клавиатуры), можно редактировать через календарь

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { InputDate, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/default/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <InputDate label="DisableUserInput поле" disableUserInput />
  </ThemeProvider>,
  document.getElementById('root')
);
```
