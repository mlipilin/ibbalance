# InputDate

Поле для ввода даты

## Когда использовать

Всякий раз, когда нам нужно ввести дату

## Примеры использования

## Свойства

Название | Описание | Тип | Значение по умолчанию
--- | --- | --- | ---
**disabled**|`Disabled` состояние поля|*bool*|`false`
**disableUserInput**|Доступен для редактирования только через календарь|*bool*|`false`
**error**|Текст ошибки для поля|*string*|`null`
**errorDetail**|Текст длинной ошибки, которую можно посмотреть в открывающимся тултипе |*string*|`null`
**errorDetailIcon**|Иконка (компонент), которая используется для открытия тултипа с ошибкой|*object*|`<FeatherIcon name={'alert-circle'} />`
**formatValue**|Функция, преобразующая значение поля перед выводом|*func*|`_ => _`
**hideYearSwitch**|Доступлно ли переключение года|*bool*|`true`
**iconCalendar**|Иконка (компонент), которая используется для открытия календаря|*object*|`<FeatherIcon name={'calendar'} /`
**iconNextMont**|Иконка (компонент), которая переключает следующий месяц|*object*|`<FeatherIcon name={'chevron-right'} />`
**iconNextYear**|Иконка (компонент), которая переключает следующий год|*object*|`<FeatherIcon name={'chevrons-right'} />`
**iconPrevMont**|Иконка (компонент), которая переключает предыдущий месяц|*object*|`<FeatherIcon name={'chevron-left'} />`
**iconPrevYear**|Иконка (компонент), которая переключает предыдущий год|*object*|`<FeatherIcon name={'chevrons-left'} />`
**label**|Заголовок поля|*string,node,node[]*|`null`
**placeholder**|Плейсхолдер поля|*string*|`null`
**placement** | Расположение тултипа | Один из: `top`, `right`, `left`, `bottom` | `bottom`|
**position**|CSS position для тултипа|Один из: `absolute`, `fixed` | `absolute`|
**readOnly**|`ReadOnly` состояние поля|*bool*|`false`
**size**|Размер инпута|Один из: `s`, `m`|`m`
**success**|`Success` состояние поля (как правило чтобы показать, что в поле валидное значение)|*bool*|`false`
**value**|Начальное значение |*string*|`null`
**onBlur**|Обработчик события "blur"|*func*|`props => {}`
**onChange**|Обработчик события "change"|*func*|`(value, props, isCalendarDate) => {}`
**parseValue**|Функция, преобразующая значение поля перед тем, как передать это значение в обработчик `onChange`|*func*|`value => value`
