import { getDateFromString, getErrorTextDate } from './helpers';

describe('src/components/InputDate/helpers.js', () => {
    describe('getDateFromString', () => {
        it('не передали date -> null', () => {
            expect(getDateFromString()).toBeNull();
        });
        it('передали коррекную дату в виде строки -> дата в виде Date', () => {
            expect(getDateFromString('31.12.2019')).toEqual(new Date('2019', '11', '31'));
        });
        it('передали не коррекную дату в виде строки -> null', () => {
            expect(getDateFromString('33.15.23456')).toBeNull();
        });
    });
    describe('getErrorTextDate', () => {
        it('не передали ничего -> null', () => {
            expect(getErrorTextDate()).toBeNull();
        });
        it('передали dateValue -> null', () => {
            expect(getErrorTextDate('01.02.2020')).toBeNull();
        });
        it('передали dateValue, error -> error', () => {
            const ERROR_TEXT = 'Ошибка';
            expect(getErrorTextDate('01.02.2020', ERROR_TEXT)).toBe(ERROR_TEXT);
        });
        it('передали dateValue, error=null, hasFocus, max=null -> null', () => {
            expect(getErrorTextDate('01.02.2020', null, false, null)).toBeNull();
        });
        it('передали dateValue, error=null, hasFocus, max < dateValue -> defaultTextMinError', () => {
            const VALUE = '01.02.2020';
            const MAX_DATE = '02.01.2020';
            const DEFAULT_TEXT_MAX = `Дата не может быть больше ${MAX_DATE}`;
            expect(getErrorTextDate(VALUE, null, false, MAX_DATE)).toBe(DEFAULT_TEXT_MAX);
        });
        it('передали dateValue, error=null, hasFocus, max > dateValue, min=null, maxText -> maxText', () => {
            const VALUE = '01.02.2020';
            const MAX_DATE = '02.01.2020';
            const TEXT_MAX = `Ошибка`;
            expect(getErrorTextDate(VALUE, null, false, MAX_DATE, null, TEXT_MAX)).toBe(TEXT_MAX);
        });
        it('передали dateValue, error, hasFocus max < dateValue -> error', () => {
            const VALUE = '01.02.2020';
            const ERROR_TEXT = 'Ошибка';
            const MAX_DATE = '02.02.2020';
            expect(getErrorTextDate(VALUE, ERROR_TEXT, false, MAX_DATE)).toBe(ERROR_TEXT);
        });
        it('передали dateValue, error=null, hasFocus, max < dateValue, min > dateValue -> defaultTextMinError', () => {
            const VALUE = '01.02.2020';
            const MIN_DATE = '02.02.2020';
            const MAX_DATE = '31.01.2020';
            const DEFAULT_TEXT_MIN = `Дата не может быть меньше ${MIN_DATE}`;
            expect(getErrorTextDate(VALUE, null, false, MAX_DATE, MIN_DATE)).toBe(DEFAULT_TEXT_MIN);
        });
        it('передали dateValue, error=null, hasFocus, max=null, min > dateValue -> defaultTextMinError', () => {
            const VALUE = '01.02.2020';
            const MIN_DATE = '02.02.2020';
            const DEFAULT_TEXT_MIN = `Дата не может быть меньше ${MIN_DATE}`;
            expect(getErrorTextDate(VALUE, null, false, null, MIN_DATE)).toBe(DEFAULT_TEXT_MIN);
        });
        it('передали dateValue, error, hasFocus, max=null, min > dateValue -> error', () => {
            const VALUE = '01.02.2020';
            const MIN_DATE = '02.02.2020';
            const ERROR_TEXT = `Ошибка`;
            expect(getErrorTextDate(VALUE, ERROR_TEXT, false, null, MIN_DATE)).toBe(ERROR_TEXT);
        });
        it('передали dateValue, error=null, hasFocus, max=null, min > dateValue, maxText=null, minText -> minText', () => {
            const VALUE = '01.02.2020';
            const MIN_DATE = '02.02.2020';
            const TEXT_MIN = `Ошибка`;
            expect(getErrorTextDate(VALUE, null, false, null, MIN_DATE, null, TEXT_MIN)).toBe(
                TEXT_MIN,
            );
        });
        it('передали dateValue, error=null, hasFocus=true, max=null, min > dateValue -> null', () => {
            const VALUE = '01.02.2020';
            const MIN_DATE = '02.02.2020';
            expect(getErrorTextDate(VALUE, null, true, null, MIN_DATE)).toBeNull();
        });
        it('передали dateValue, error=null, hasFocus=true, max < dateValue -> null', () => {
            const VALUE = '01.02.2020';
            const MAX_DATE = '02.01.2020';
            expect(getErrorTextDate(VALUE, null, true, MAX_DATE)).toBeNull();
        });
    });
});
