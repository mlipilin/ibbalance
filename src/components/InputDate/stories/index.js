import React from 'react';
import { storiesOf } from '@storybook/react';
import { boolean, select, text, withKnobs } from '@storybook/addon-knobs';

// Components
import InputDate from '../index';

// Constants
import { LOCAL_STORAGE_THEME_KEY } from '../../../constants/local-storage';

import notes from '../readme.md';
import { withState } from '../../../hoc';
import { THEMES } from '../../../constants/themes';
import { PLACEMENTS, SIZES } from '../../../constants/props';

const InputDateWithState = withState(InputDate);
const theme = localStorage.getItem(LOCAL_STORAGE_THEME_KEY);

let sizesList = [];

switch (theme) {
    case THEMES.DEFAULT:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        break;
    case THEMES.ALFA:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.ABSOLUT:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.SPB:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.TKB:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.TKB_LKZ:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.KYC:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.KYC_LANDING:
        sizesList = [SIZES.S, SIZES.M];
        break;
    default:
        break;
}

const stories = storiesOf('InputDate');

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories.add('Default', () => <InputDateWithState size={select('Size', sizesList, SIZES.M)} />, {
    notes,
});
stories.add(
    'Limit date',
    () => (
        <InputDateWithState
            disabled={boolean('disabled', false)}
            disableUserInput={boolean('disableUserInput', false)}
            error={text('error', null)}
            errorDetail={text('errorDetail', null)}
            errorTextMaxDate={text('errorTextMaxDate', 'max')}
            errorTextMinDate={text('errorTextMinDate', 'min')}
            hideYearSwitch={boolean('hideYearSwitch', false)}
            label={text('label', 'Введите дату')}
            max={text('max', '20.04.2021')}
            min={text('min', '04.04.2021')}
            placeholder={text('placeholder', 'Введите дату')}
            placement={select('placement', PLACEMENTS, PLACEMENTS.BOTTOM)}
            readOnly={boolean('readOnly', false)}
            size={select('Size', sizesList, SIZES.M)}
            success={boolean('success', false)}
            value={text('value', '01.04.2021')}
        />
    ),
    {
        notes,
    },
);
