import React, { useMemo, useRef, useState } from 'react';
import cn from 'classnames';
import moment from 'moment';
import PropTypes from 'prop-types';

/* eslint-disable */
import Calendar from '../Calendar';
import Input from '../Input';
import Tooltip from '../Tooltip';
import { FeatherIcon } from '../../index';
/* eslint-enable */

import { PLACEMENTS, SIZES } from '../../constants/props';

import { withTheme } from '../../theme-provider';
import { getDateFromString, getErrorTextDate } from './helpers';

const InputDate = props => {
    const { max = null, min = null } = props;
    const {
        disabled = false,
        disableUserInput = false,
        error = null,
        errorDetail = null,
        errorDetailIcon = <FeatherIcon name="alert-circle" />,
        errorTextMaxDate = `Дата не может быть больше ${max || 'dd.mm.yyyy'}`,
        errorTextMinDate = `Дата не может быть меньше ${min || 'dd.mm.yyyy'}`,
        field = null,
        hideYearSwitch = false,
        iconCalendar = <FeatherIcon name="calendar" />,
        iconNextMonth = <FeatherIcon name="chevron-right" />,
        iconNextYear = <FeatherIcon name="chevrons-right" />,
        iconPrevMonth = <FeatherIcon name="chevron-left" />,
        iconPrevYear = <FeatherIcon name="chevrons-left" />,
        label = null,
        placeholder = null,
        placement = PLACEMENTS.BOTTOM,
        position = 'absolute',
        readOnly = false,
        size = SIZES.M,
        success = false,
        value = '',
        applyTheme = _ => _,
        onBlur = _ => _,
        onChange = _ => _,
        parseValue = _ => _,
        ...otherProps
    } = props;
    const tooltip = useRef(null);
    const [hasFocus, setHasFocus] = useState(false);
    const inputRef = useRef(null);

    const inputError = useMemo(
        () =>
            getErrorTextDate(value, error, hasFocus, max, min, errorTextMaxDate, errorTextMinDate),
        [error, errorTextMaxDate, errorTextMinDate, hasFocus, max, min, value],
    );

    // Handlers
    function handleInputFocus() {
        setHasFocus(!!inputRef.current);
        tooltip.current.closeTooltip();
    }

    const handleInputClick = e => {
        e.stopPropagation();
    };

    const handleCalendarWrapperClick = e => {
        e.stopPropagation();
    };

    const handleCalendarChange = calendarValue => {
        onChange(moment(calendarValue).format('DD.MM.YYYY'), { ...props }, true);
        tooltip.current.closeTooltip();
    };

    const handleInputChange = (valueChange, propsChange) => {
        onChange(valueChange, propsChange, false);
    };

    const handleInputBlur = () => {
        setHasFocus(false);
        onBlur(props);
    };

    const calendarClass = applyTheme(
        cn('InputDate__Calendar', `InputDate__Calendar_size_${size}`, {
            InputDate__Calendar_disabled: disabled,
            InputDate__Calendar_readonly: readOnly,
        }),
    );
    const componentClass = applyTheme(cn('InputDate', `InputDate_size_${size}`));
    const iconClass = applyTheme(
        cn('InputDate__Icon', `InputDate__Icon_size_${size}`, {
            InputDate__Icon_disabled: disabled,
            InputDate__Icon_readonly: readOnly,
        }),
    );
    const inputClass = applyTheme(cn('InputDate__Input', `InputDate__Input_size_${size}`));

    const calendarValue = getDateFromString(value);
    const calendarMax = getDateFromString(max);
    const calendarMin = getDateFromString(min);

    const calendar = (
        <div onClick={handleCalendarWrapperClick}>
            <Calendar
                {...{ iconNextMonth, iconNextYear, iconPrevMonth, iconPrevYear, hideYearSwitch }}
                max={calendarMax}
                min={calendarMin}
                value={calendarValue}
                onChange={handleCalendarChange}
            />
        </div>
    );

    const inputProps = {
        disabled,
        errorDetail,
        errorDetailIcon,
        field,
        label,
        placeholder,
        readOnly: readOnly || disableUserInput,
        success,
        parseValue,
        ...otherProps,
    };

    return (
        <div className={componentClass}>
            <div className={inputClass} onClick={handleInputClick}>
                <Input
                    {...inputProps}
                    error={inputError}
                    ref={inputRef}
                    mask="99.99.9999"
                    size={size}
                    value={value}
                    onBlur={handleInputBlur}
                    onChange={handleInputChange}
                    onFocus={handleInputFocus}
                />
            </div>
            <div className={iconClass}>{iconCalendar}</div>
            <Tooltip
                content={calendar}
                className={calendarClass}
                placement={placement}
                position={position}
                ref={tooltip}
            >
                <div className={iconClass}>{iconCalendar}</div>
            </Tooltip>
        </div>
    );
};

InputDate.propTypes = {
    disabled: PropTypes.bool,
    disableUserInput: PropTypes.bool,
    error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool, PropTypes.object]),
    errorDetail: PropTypes.string,
    errorDetailIcon: PropTypes.element,
    errorTextMaxDate: PropTypes.string,
    errorTextMinDate: PropTypes.string,
    field: PropTypes.string,
    hideYearSwitch: PropTypes.bool,
    iconCalendar: PropTypes.element,
    iconNextMonth: PropTypes.element,
    iconNextYear: PropTypes.element,
    iconPrevMonth: PropTypes.element,
    iconPrevYear: PropTypes.element,
    label: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
        PropTypes.string,
    ]),
    max: PropTypes.string,
    min: PropTypes.string,
    placeholder: PropTypes.string,
    placement: PropTypes.oneOf([
        PLACEMENTS.TOP,
        PLACEMENTS.RIGHT,
        PLACEMENTS.BOTTOM,
        PLACEMENTS.LEFT,
    ]),
    position: PropTypes.oneOf(['absolute', 'fixed']),
    readOnly: PropTypes.bool,
    size: PropTypes.oneOf([SIZES.S, SIZES.M, SIZES.L]),
    success: PropTypes.bool,
    value: PropTypes.string,

    applyTheme: PropTypes.func,
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    parseValue: PropTypes.func,
};

InputDate.defaultProps = {
    disabled: false,
    disableUserInput: false,
    error: null,
    errorDetail: null,
    errorDetailIcon: <FeatherIcon name="alert-circle" />,
    errorTextMaxDate: undefined,
    errorTextMinDate: undefined,
    field: null,
    hideYearSwitch: false,
    iconCalendar: <FeatherIcon name="calendar" />,
    iconNextMonth: <FeatherIcon name="chevron-right" />,
    iconNextYear: <FeatherIcon name="chevrons-right" />,
    iconPrevMonth: <FeatherIcon name="chevron-left" />,
    iconPrevYear: <FeatherIcon name="chevrons-left" />,
    label: null,
    max: null,
    min: null,
    placeholder: null,
    placement: PLACEMENTS.BOTTOM,
    position: 'absolute',
    readOnly: false,
    size: SIZES.M,
    success: false,
    value: '',

    applyTheme: _ => _,
    onBlur: props => {}, // eslint-disable-line no-unused-vars
    onChange: (value, props, isCalendarDate) => {}, // eslint-disable-line no-unused-vars
    parseValue: value => value,
};

export default withTheme(InputDate);
