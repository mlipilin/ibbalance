import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

// Constants
import { SIZES, TYPES } from '../../constants/props';

import { withTheme } from '../../theme-provider';

const CLOSING_DELAY = 300;

@withTheme
class Notification extends Component {
    constructor(props) {
        super(props);
        this.closeAfterTimeout = null;
        this.closingTimeout = null;
        this.state = { closing: false };
    }

    componentDidMount() {
        const { closeAfter } = this.props;

        if (closeAfter > 0) {
            this.closeAfterTimeout = setTimeout(() => {
                this.startClosing();
            }, closeAfter * 1000);
        }
    }

    componentWillUnmount() {
        if (this.closeAfterTimeout) {
            clearTimeout(this.closeAfterTimeout);
        }
        if (this.closingTimeout) {
            clearTimeout(this.closingTimeout);
        }
    }

    handleCloseClick = e => {
        e.preventDefault();
        const { closing } = this.state;

        if (!closing) {
            if (this.closeAfterTimeout) {
                clearTimeout(this.closeAfterTimeout);
            }

            this.startClosing();
        }
    };

    startClosing = () => {
        const blockHeight = this.block.offsetHeight;
        this.block.style.marginBottom = `-${blockHeight}px`;
        const { onClose, id } = this.props;
        this.setState({ closing: true }, () => {
            this.closingTimeout = setTimeout(() => {
                onClose(id, this.props);
            }, CLOSING_DELAY);
        });
    };

    render() {
        const {
            className,
            closeAfter,
            description,
            noCloseButton,
            size,
            title,
            type,
            applyTheme,
            ...otherProps
        } = this.props;

        const { closing } = this.state;

        const componentClass = applyTheme(
            cn('Notification', `Notification_size_${size}`, `Notification_type_${type}`, {
                Notification_closing: closing,
            }),
            className,
        );

        const closeClass = applyTheme(
            cn(
                'Notification__Close',
                `Notification__Close_size_${size}`,
                `Notification__Close_type_${type}`,
            ),
        );

        const descriptionClass = applyTheme(
            cn(
                'Notification__Description',
                `Notification__Description_size_${size}`,
                `Notification__Description_type_${type}`,
            ),
        );

        const titleClass = applyTheme(
            cn(
                'Notification__Title',
                `Notification__Title_size_${size}`,
                `Notification__Title_type_${type}`,
            ),
        );

        const descriptionMessageClass = applyTheme(cn('Notification__Message'));

        const descriptionMessage = messages => {
            if (Array.isArray(messages)) {
                return (
                    <>
                        {messages.map(message => (
                            <p key={message} className={descriptionMessageClass}>
                                {message}
                            </p>
                        ))}
                    </>
                );
            }
            return messages;
        };

        return (
            <div
                ref={el => {
                    this.block = el;
                    return this.block;
                }}
                className={componentClass}
                {...otherProps}
            >
                {/* Close */}
                {!noCloseButton && (
                    <button type="button" className={closeClass} onClick={this.handleCloseClick} />
                )}

                {/* Title */}
                {!!title && <div className={titleClass}>{title}</div>}

                {/* Description */}
                {!!description && (
                    <div className={descriptionClass}>{descriptionMessage(description)}</div>
                )}
            </div>
        );
    }
}

Notification.propTypes = {
    className: PropTypes.string,
    closeAfter: PropTypes.number, // seconds (0 - close manual, number - close after number seconds)
    description: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.node,
        PropTypes.arrayOf(PropTypes.string),
    ]),
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    noCloseButton: PropTypes.bool,
    size: PropTypes.oneOf([SIZES.S, SIZES.M, SIZES.L]),
    title: PropTypes.string,
    type: PropTypes.oneOf([TYPES.DANGER, TYPES.INFO, TYPES.SUCCESS, TYPES.WARNING]),
    applyTheme: PropTypes.func,
    onClose: PropTypes.func,
};

Notification.defaultProps = {
    className: '',
    closeAfter: 5,
    description: null,
    id: null,
    noCloseButton: false,
    size: SIZES.M,
    title: null,
    type: TYPES.INFO,
    applyTheme: _ => _,
    onClose: (id, props) => {}, // eslint-disable-line no-unused-vars
};

export default Notification;
