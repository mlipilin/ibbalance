import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import cn from 'classnames';

// Constants
import { POSITION } from './constants';

import { withTheme } from '../../theme-provider';

@withTheme
class NotificationWrapper extends Component {
    render() {
        const { className, position, applyTheme, children, ...otherProps } = this.props;

        const componentClass = applyTheme(
            cn('NotificationWrapper', `NotificationWrapper_position_${position}`),
            className,
        );

        return ReactDOM.createPortal(
            <div className={componentClass} {...otherProps}>
                {children}
            </div>,
            document.body,
        );
    }
}

NotificationWrapper.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    className: PropTypes.string,
    position: PropTypes.oneOf([
        POSITION.BOTTOM_CENTER,
        POSITION.BOTTOM_LEFT,
        POSITION.BOTTOM_RIGHT,
        POSITION.TOP_CENTER,
        POSITION.TOP_LEFT,
        POSITION.TOP_RIGHT,
    ]),
    applyTheme: PropTypes.func,
};

NotificationWrapper.defaultProps = {
    children: null,
    className: '',
    position: POSITION.TOP_RIGHT,
    applyTheme: _ => _,
};

export default NotificationWrapper;
