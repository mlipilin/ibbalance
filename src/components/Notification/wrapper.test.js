import React from 'react';

import { NotificationWrapper } from './index';

import { render } from '../../hoc/with-theme';

describe('Тестирование компонента NotificationWrapper', () => {
    it('Должен отобразиться NotificationWrapper c параметрами по-умолчанию', () => {
        render(<NotificationWrapper />);
        const notificationWrapper = document.querySelector('.NotificationWrapper');
        expect(notificationWrapper).toBeInTheDocument();
        expect(
            notificationWrapper.classList.contains('NotificationWrapper_position_top-right'),
        ).toBeTruthy();
        expect(notificationWrapper).toMatchSnapshot();
    });
    it('Должен отобразиться NotificationWrapper c классом "NotificationWrapper__Styles"', () => {
        render(<NotificationWrapper className="NotificationWrapper__Styles" />);
        const notificationWrapper = document.querySelector('.NotificationWrapper');
        expect(notificationWrapper.classList.contains('NotificationWrapper__Styles')).toBeTruthy();
    });
    it('Должен отобразиться NotificationWrapper в позиции bottom-center', () => {
        render(<NotificationWrapper position="bottom-center" />);
        const notificationWrapper = document.querySelector('.NotificationWrapper');
        expect(
            notificationWrapper.classList.contains('NotificationWrapper_position_bottom-center'),
        ).toBeTruthy();
    });
    it('Должен отобразиться NotificationWrapper в позиции bottom-left', () => {
        render(<NotificationWrapper position="bottom-left" />);
        const notificationWrapper = document.querySelector('.NotificationWrapper');
        expect(
            notificationWrapper.classList.contains('NotificationWrapper_position_bottom-left'),
        ).toBeTruthy();
    });
    it('Должен отобразиться NotificationWrapper в позиции bottom-right', () => {
        render(<NotificationWrapper position="bottom-right" />);
        const notificationWrapper = document.querySelector('.NotificationWrapper');
        expect(
            notificationWrapper.classList.contains('NotificationWrapper_position_bottom-right'),
        ).toBeTruthy();
    });
    it('Должен отобразиться NotificationWrapper в позиции top-center', () => {
        render(<NotificationWrapper position="top-center" />);
        const notificationWrapper = document.querySelector('.NotificationWrapper');
        expect(
            notificationWrapper.classList.contains('NotificationWrapper_position_top-center'),
        ).toBeTruthy();
    });
    it('Должен отобразиться NotificationWrapper в позиции top-left', () => {
        render(<NotificationWrapper position="top-left" />);
        const notificationWrapper = document.querySelector('.NotificationWrapper');
        expect(
            notificationWrapper.classList.contains('NotificationWrapper_position_top-left'),
        ).toBeTruthy();
    });
    it('Должен отобразиться NotificationWrapper в позиции top-right', () => {
        render(<NotificationWrapper position="top-right" />);
        const notificationWrapper = document.querySelector('.NotificationWrapper');
        expect(
            notificationWrapper.classList.contains('NotificationWrapper_position_top-right'),
        ).toBeTruthy();
    });
    it('Должен отобразиться NotificationWrapper проверка children', () => {
        render(
            <NotificationWrapper>
                <div className="Children__Styled">Children</div>
            </NotificationWrapper>,
        );
        const children = document.querySelector('.Children__Styled');
        expect(children).toBeInTheDocument();
        expect(children.textContent).toBe('Children');
    });
    it('Должен отобразиться NotificationWrapper проверка children - nodes', () => {
        render(
            <NotificationWrapper>
                <div className="Children__Styled">Children1</div>
                <div className="Children__Styled">Children2</div>
            </NotificationWrapper>,
        );
        const childrens = document.querySelectorAll('.Children__Styled');
        expect(childrens[0].textContent).toBe('Children1');
        expect(childrens[1].textContent).toBe('Children2');
    });
});
