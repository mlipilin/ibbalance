import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, select, text, array } from '@storybook/addon-knobs';

// Constants
import { LOCAL_STORAGE_THEME_KEY } from '../../../constants/local-storage';
import { SIZES, TYPES } from '../../../constants/props';
import { THEMES } from '../../../constants/themes';

import { Notification } from '../index';

import notes from '../readme.md';

const theme = localStorage.getItem(LOCAL_STORAGE_THEME_KEY);

let sizesList = [];
let typesList = [];

switch (theme) {
    case THEMES.DEFAULT:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        typesList = [TYPES.DANGER, TYPES.INFO, TYPES.SUCCESS, TYPES.WARNING];
        break;
    case THEMES.ALFA:
        sizesList = [SIZES.M];
        typesList = [TYPES.DANGER, TYPES.INFO, TYPES.SUCCESS, TYPES.WARNING];
        break;
    case THEMES.SPB:
        sizesList = [SIZES.M];
        typesList = [TYPES.DANGER, TYPES.INFO, TYPES.SUCCESS, TYPES.WARNING];
        break;
    case THEMES.TKB:
        sizesList = [SIZES.M];
        typesList = [TYPES.DANGER, TYPES.INFO, TYPES.SUCCESS, TYPES.WARNING];
        break;
    case THEMES.TKB_LKZ:
        sizesList = [SIZES.M];
        typesList = [TYPES.DANGER, TYPES.INFO, TYPES.SUCCESS, TYPES.WARNING];
        break;
    case THEMES.KYC:
        sizesList = [SIZES.M];
        typesList = [TYPES.DANGER, TYPES.INFO, TYPES.SUCCESS, TYPES.WARNING];
        break;
    case THEMES.KYC_LANDING:
        sizesList = [SIZES.M];
        typesList = [TYPES.DANGER, TYPES.INFO, TYPES.SUCCESS, TYPES.WARNING];
        break;
    default:
        break;
}

const stories = storiesOf('Notification', module);

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories
    .add(
        'Auto closing',
        () => (
            <Notification
                description={text(
                    'Description',
                    'Для работы с системой вы должны быть авторизованы',
                )}
                size={select('Size', sizesList, SIZES.M)}
                title={text('Title', 'Ошибка')}
                type={select('Type', typesList, TYPES.INFO)}
            />
        ),
        { notes },
    )
    .add(
        'Manual closing',
        () => (
            <Notification
                closeAfter={0}
                description={text(
                    'Description',
                    'Для работы с системой вы должны быть авторизованы',
                )}
                size={select('Size', sizesList, SIZES.M)}
                title={text('Title', 'Ошибка')}
                type={select('Type', typesList, TYPES.INFO)}
            />
        ),
        { notes },
    )
    .add(
        'Array description',
        () => (
            <Notification
                closeAfter={0}
                description={array(
                    'DescriptionArray',
                    ['Ошибка авторизации авторизованы', 'Не верный пароль'],
                    ',',
                )}
                size={select('Size', sizesList, SIZES.M)}
                title={text('Title', 'Ошибка')}
                type={select('Type', typesList, TYPES.INFO)}
            />
        ),
        { notes },
    );
