# Ручное / автоматическое закрытие уведомления

По умолчанию уведомление нужно закрывать "вручную". Для автоматического закрытия предусмотрено свойство `closeAfter` (0 - закрываем вручную, N - закроется само через N секунд)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import {
  Button,
  H,
  Notification,
  NotificationWrapper,
  ThemeProvider
} from 'ibalance';
import theme from 'ibalance/src/themes/tkb/theme';

const DemoComponent extends React.Component {
  state = { notifications: [] };

  addNotification = props => {
    const notifications = [
      ...this.state.notifications,
      { ...props },
    ];
    this.setState({ notifications });
  }

  addManualNotification = () => {
    this.addNotification({
      closeAfter: 0,
      title: 'Ручное уведомление',
      description: 'Закроется только нажатием на крестик',
    });
  };

  addAutoNotification = () => {
    this.addNotification({
      title: 'Автоматическое уведомление',
      description: 'Закроется через 3 секунды',
    });
  };

  render () {
    return (
      <>
        <H size="3">Показать уведомление, закрываемое:</H>
        <Button onClick={this.addAutoNotification}>
          ⏰ Автоматически
        </Button>
        <Button onClick={this.addManualNotification}>
          👆Вручную
        </Button>

        /** Список уведомлений */ 
        <NotificationWrapper>
          {this.state.notifications.map((notification, index) => (
            <Notification key={index} {...notification} />
          ))}
        </NotificationWrapper>
      </>
    )
  }
}

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <DemoComponent/>
  </ThemeProvider>,
  document.getElementById('root')
);
```
