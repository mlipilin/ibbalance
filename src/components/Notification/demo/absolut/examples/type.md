# Тип уведомления

Свойство type может принимать одно из следующих значений: `danger`, `info`, `success` и `warning`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import {
  Button,
  H,
  Notification,
  NotificationWrapper,
  ThemeProvider
} from 'ibalance';
import theme from 'ibalance/src/themes/absolut/theme';

const DemoComponent extends React.Component {
  state = { notifications: [] };

  addNotification = type => e => {
    const notifications = [
      ...this.state.notifications,
      {
        title: 'Уведомление',
        description: `Тип: ${type}`,
        type,
      },
    ];
    this.setState({ notifications });
  };

  render () {
    return (
      <>
        <H size="3">Показать уведомление типа:</H>
        <Button
          type="danger"
          onClick={this.addNotification('danger')}
        >
          Danger
        </Button>
        <Button
          type="info"
          onClick={this.addNotification('info')}
        >
          Info
        </Button>
        <Button 
          type="success"
          onClick={this.addNotification('success')}
        >
          Success
        </Button>
        <Button
          type="warning"
          onClick={this.addNotification('warning')}
        >
          Warning
        </Button>

        /** Список уведомлений */ 
        <NotificationWrapper>
          {this.state.notifications.map((notification, index) => (
            <Notification key={index} {...notification} />
          ))}
        </NotificationWrapper>
      </>
    )
  }
}

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <DemoComponent/>
  </ThemeProvider>,
  document.getElementById('root')
);
```
