# Размер блока с уведомлением

Свойство size может принимать одно из следующих значений: `s`, `m` и `l`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import {
  Button,
  H,
  Notification,
  NotificationWrapper,
  ThemeProvider
} from 'ibalance';
import theme from 'ibalance/src/themes/default/theme';

const DemoComponent extends React.Component {
  state = { notifications: [] };

  addNotification = size => e => {
    const notifications = [
      ...this.state.notifications,
      {
        title: 'Уведомление',
        description: `Размер: ${size}`,
        size,
      },
    ];
    this.setState({ notifications });
  };

  render () {
    return (
      <>
        <H size="3">Показать уведомление размера:</H>
        <Button size="l" onClick={this.addNotification('l')}>
          L
        </Button>
        <Button size="m" onClick={this.addNotification('m')}>
          M
        </Button>
        <Button size="s" onClick={this.addNotification('s')}>
          S
        </Button>

        /** Список уведомлений */ 
        <NotificationWrapper>
          {this.state.notifications.map((notification, index) => (
            <Notification key={index} {...notification} />
          ))}
        </NotificationWrapper>
      </>
    )
  }
}

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <DemoComponent/>
  </ThemeProvider>,
  document.getElementById('root')
);
```
