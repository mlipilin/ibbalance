# Массив уведомлений

Свойство description может содержать массив уведомлений: `['1', '2']`, `<p>1</p>`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import {
  Button,
  H,
  Notification,
  NotificationWrapper,
  ThemeProvider
} from 'ibalance';
import theme from 'ibalance/src/themes/default/theme';

const DemoComponent extends React.Component {
  state = { notifications: [] };

  addNotification = description => e => {
      this.setState(prevState => {
          const notifications = [
              ...prevState.notifications,
              {
                  id: uuidv1(),
                  title: 'Несколько уведомлений в одном сообщении',
                  description: description,
              },
          ];
          return { notifications };
      });
  };

  render () {
    return (
      <>
        <H size="3">Показать уведомление из нескольких:</H>
        <Button onClick={this.addArrayNotification(['1', '2'])}>
            Array of text
        </Button>
        <Button onClick={this.addArrayNotification([<p>1</p>, <p>2</p>])}>
            Array of node
        </Button>
        
        /** Список уведомлений */ 
        <NotificationWrapper>
          {this.state.notifications.map((notification, index) => (
            <Notification key={index} {...notification} />
          ))}
        </NotificationWrapper>
      </>
    )
  }
}

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <DemoComponent/>
  </ThemeProvider>,
  document.getElementById('root')
);
```
