# Позиция уведомления

Расположение уведомления на экране (в одном из 4х углов или по центру)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import {
  Button,
  FeatherIcon,
  H,
  Notification,
  NotificationWrapper,
  ThemeProvider
} from 'ibalance';
import theme from 'ibalance/src/themes/default/theme';

const DemoComponent extends React.Component {
  state = {
    bottomCenter: [],
    bottomLeft: [],
    bottomRight: [],
    topCenter: [],
    topLeft: [],
    topRight: [],
  };

  addNotification = position => e => {
    const notifications = [
      ...this.state[position],
      {
        title: 'Уведомление',
        description: `Позиция: ${position}`,
        type,
      },
    ];
    this.setState({ [position]: notifications });
  };

  render () {
    return (
      <>
        <H size="3">Показать уведомление:</H>
        <Button
          onClick={this.addNotification('topLeft')}
        >
            <FeatherIcon name="arrow-up-left" />
        </Button>
        <Button
          onClick={this.addNotification('topCenter')}
        >
            <FeatherIcon name="arrow-up" />
        </Button>
        <Button
          onClick={this.addNotification('topRight')}
        >
            <FeatherIcon name="arrow-up-right" />
        </Button>
        <br />
        <Button
          onClick={this.addNotification('bottomLeft')}
        >
            <FeatherIcon name="arrow-down-left" />
        </Button>
        <Button
          onClick={this.addNotification('bottomCenter')}
        >
            <FeatherIcon name="arrow-down" />
        </Button>
        <Button
          onClick={this.addNotification('bottomRight')}
        >
            <FeatherIcon name="arrow-down-right" />
        </Button>

        {/* Bottom Center */}
        <NotificationWrapper position="bottom-center">
          {this.state.bottomCenter.map((notification, index) => (
            <Notification key={index} {...notification} />
          ))}
        </NotificationWrapper>

        {/* Bottom Left */}
        <NotificationWrapper position="bottom-left">
          {this.state.bottomLeft.map((notification, index) => (
            <Notification key={index} {...notification} />
          ))}
        </NotificationWrapper>

        {/* Bottom Right */}
        <NotificationWrapper position="bottom-right">
          {this.state.bottomRight.map((notification, index) => (
            <Notification key={index} {...notification} />
          ))}
        </NotificationWrapper>

        {/* Top Center */}
        <NotificationWrapper position="top-center">
          {this.state.topCenter.map((notification, index) => (
            <Notification key={index} {...notification} />
          ))}
        </NotificationWrapper>

        {/* Top Left */}
        <NotificationWrapper position="top-left">
          {this.state.topLeft.map((notification, index) => (
            <Notification key={index} {...notification} />
          ))}
        </NotificationWrapper>

        {/* Top Right */}
        <NotificationWrapper position="top-right">
          {this.state.topRight.map((notification, index) => (
            <Notification key={index} {...notification} />
          ))}
        </NotificationWrapper>
      </>
    )
  }
}

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <DemoComponent/>
  </ThemeProvider>,
  document.getElementById('root')
);
```
