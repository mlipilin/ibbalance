import React, { Component } from 'react';
import uuidv1 from 'uuid/v1';

// Components
import { NotificationWrapper, Notification } from '../../index';
import Button from '../../../Button';
import FeatherIcon from '../../../FeatherIcon';
import H from '../../../../typography/H';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';

// Examples
import automateManual from './examples/automate-manual.md';
import position from './examples/position.md';
import type from './examples/type.md';
import arrayMessage from './examples/array-message.md';

// Wrappers
import { Col, Row } from '../../../../wrappers/WGrid';

class DemoNotification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            notifications: [],
            bottomCenter: [],
            bottomLeft: [],
            bottomRight: [],
            topCenter: [],
            topLeft: [],
            topRight: [],
        };
    }

    addManualNotification = () => {
        this.setState(prevState => {
            const notifications = [
                ...prevState.notifications,
                {
                    id: uuidv1(),
                    closeAfter: 0,
                    title: 'Ручное уведомление',
                    description: 'Закроется только нажатием на крестик',
                },
            ];
            return { notifications };
        });
    };

    addAutoNotification = () => {
        this.setState(prevState => {
            const notifications = [
                ...prevState.notifications,
                {
                    id: uuidv1(),
                    title: 'Автоматическое уведомление',
                    description: 'Закроется через 5 секунд',
                },
            ];
            return { notifications };
        });
    };

    addNotificationWithType = typeValue => () => {
        this.setState(prevState => {
            const notifications = [
                ...prevState.notifications,
                {
                    id: uuidv1(),
                    title: 'Уведомление',
                    description: `Тип: ${typeValue}`,
                    type: typeValue,
                },
            ];
            return { notifications };
        });
    };

    addNotificationWithPosition = positionValue => () => {
        this.setState(prevState => {
            const notifications = [
                ...prevState[positionValue],
                {
                    id: uuidv1(),
                    title: 'Уведомление',
                    description: `Позиция: ${positionValue}`,
                },
            ];
            return { [positionValue]: notifications };
        });
    };

    removeNotification = (id, typeNotification = 'notifications') => () => {
        this.setState(prevState => {
            const notifications = prevState[typeNotification].filter(n => n.id !== id);
            return { [typeNotification]: notifications };
        });
    };

    addArrayNotification = description => () => {
        this.setState(prevState => {
            const notifications = [
                ...prevState.notifications,
                {
                    id: uuidv1(),
                    title: 'Несколько уведомлений в одном сообщении или node',
                    description,
                },
            ];
            return { notifications };
        });
    };

    render() {
        const {
            notifications,
            bottomCenter,
            bottomLeft,
            bottomRight,
            topCenter,
            topLeft,
            topRight,
        } = this.state;

        return (
            <DemoComponent>
                <ReadmeView>{readme}</ReadmeView>

                <div id="example">
                    <Row
                        cols={{ mobile: [1], tab: [1, 1], desktop: [1], largeDesktop: [1, 1] }}
                        gap="20px"
                    >
                        <Col>
                            <Example readme={automateManual}>
                                <H size="3">Показать уведомление, закрываемое:</H>
                                <Button onClick={this.addAutoNotification}>
                                    <span role="img" aria-label="clock">
                                        ⏰
                                    </span>{' '}
                                    Автоматически
                                </Button>
                                <Button onClick={this.addManualNotification}>
                                    <span role="img" aria-label="finger-up">
                                        👆
                                    </span>{' '}
                                    Вручную
                                </Button>
                                <NotificationWrapper>
                                    {notifications.map(({ id, ...notification }) => (
                                        <Notification
                                            key={id}
                                            {...notification}
                                            onClose={this.removeNotification(id)}
                                        />
                                    ))}
                                </NotificationWrapper>
                            </Example>
                        </Col>
                        <Col>
                            <Example readme={position}>
                                <H size="3">Показать уведомление:</H>
                                <Button onClick={this.addNotificationWithPosition('topLeft')}>
                                    <FeatherIcon name="arrow-up-left" />
                                </Button>
                                <Button onClick={this.addNotificationWithPosition('topCenter')}>
                                    <FeatherIcon name="arrow-up" />
                                </Button>
                                <Button onClick={this.addNotificationWithPosition('topRight')}>
                                    <FeatherIcon name="arrow-up-right" />
                                </Button>
                                <br />
                                <Button onClick={this.addNotificationWithPosition('bottomLeft')}>
                                    <FeatherIcon name="arrow-down-left" />
                                </Button>
                                <Button onClick={this.addNotificationWithPosition('bottomCenter')}>
                                    <FeatherIcon name="arrow-down" />
                                </Button>
                                <Button onClick={this.addNotificationWithPosition('bottomRight')}>
                                    <FeatherIcon name="arrow-down-right" />
                                </Button>
                                <NotificationWrapper position="bottom-center">
                                    {bottomCenter.map(({ id, ...notification }) => (
                                        <Notification
                                            key={id}
                                            {...notification}
                                            onClose={this.removeNotification(id, 'bottomCenter')}
                                        />
                                    ))}
                                </NotificationWrapper>
                                <NotificationWrapper position="bottom-left">
                                    {bottomLeft.map(({ id, ...notification }) => (
                                        <Notification
                                            key={id}
                                            {...notification}
                                            onClose={this.removeNotification(id, 'bottomLeft')}
                                        />
                                    ))}
                                </NotificationWrapper>
                                <NotificationWrapper position="bottom-right">
                                    {bottomRight.map(({ id, ...notification }) => (
                                        <Notification
                                            key={id}
                                            {...notification}
                                            onClose={this.removeNotification(id, 'bottomRight')}
                                        />
                                    ))}
                                </NotificationWrapper>
                                <NotificationWrapper position="top-center">
                                    {topCenter.map(({ id, ...notification }) => (
                                        <Notification
                                            key={id}
                                            {...notification}
                                            onClose={this.removeNotification(id, 'topCenter')}
                                        />
                                    ))}
                                </NotificationWrapper>
                                <NotificationWrapper position="top-left">
                                    {topLeft.map(({ id, ...notification }) => (
                                        <Notification
                                            key={id}
                                            {...notification}
                                            onClose={this.removeNotification(id, 'topLeft')}
                                        />
                                    ))}
                                </NotificationWrapper>
                                <NotificationWrapper position="top-right">
                                    {topRight.map(({ id, ...notification }) => (
                                        <Notification
                                            key={id}
                                            {...notification}
                                            onClose={this.removeNotification(id, 'topRight')}
                                        />
                                    ))}
                                </NotificationWrapper>
                            </Example>
                        </Col>
                        <Col>
                            <Example readme={type}>
                                <H size="3">Показать уведомление типа:</H>
                                <Button onClick={this.addNotificationWithType('danger')}>
                                    Danger
                                </Button>
                                <Button onClick={this.addNotificationWithType('info')}>Info</Button>
                                <Button onClick={this.addNotificationWithType('success')}>
                                    Success
                                </Button>
                                <Button onClick={this.addNotificationWithType('warning')}>
                                    Warning
                                </Button>
                            </Example>
                        </Col>
                        <Col>
                            <Example readme={arrayMessage}>
                                <H size="3">Показать уведомление из нескольких сообщений:</H>
                                <Button onClick={this.addArrayNotification(['1', '2'])}>
                                    Array of text
                                </Button>
                                <Button onClick={this.addArrayNotification(<p>1</p>)}>Node</Button>
                            </Example>
                        </Col>
                    </Row>
                </div>
            </DemoComponent>
        );
    }
}

export default DemoNotification;
