## Usage

Basic notification (will be close in 5 seconds)

```jsx
<NotificationWrapper position="top-right">
  <Notification
    description="Some long or short notification description"
    size="m"
    title="Warning!"
    type="danger"
  />
</NotificationWrapper>
```

Notification withOUT description

```jsx
<NotificationWrapper>
  <Notification title="Some title" type="info" />
</NotificationWrapper>
```

Notification withOUT closing button

```jsx
<NotificationWrapper>
  <Notification
    noCloseButton
    description="Some long or short notification description"
    size="s"
    title="Data is successfully saved"
    type="success"
  />
</NotificationWrapper>
```

Notification with manual closing

```jsx
<NotificationWrapper>
  <Notification
    closeAfter={0}
    description="Some long or short notification description"
    size="m"
    title="Warning!"
    type="danger"
  />
</NotificationWrapper>
```

## Props (NotificationWrapper)

Prop name | Prop type | Default value
--- | --- | ---
*position*|`bottom-center`, `bottom-left`, `bottom-right`, `top-center`, `top-left`, `top-right`|`top-right`
*error*|string, object|`null`

## Props (Notification)

Prop name | Prop type | Default value
--- | --- | ---
*closeAfter*|number|`0`
*description*|string|`null`
*noCloseButton*|bool|`true`
*size*|`s`, `m`, `l`|`m`
*title*|string|`null`
*type*|`danger`, `info`, `success`, `warning`|`info`
*success*|bool|`false`
*value*|string|`null`
*onClose*|func|`_ => _`

## CSS Selectors (NotificationWrapper)

```css
.NotificationWrapper {}

/* Position */
.NotificationWrapper_position_bottom-center {}
.NotificationWrapper_position_bottom-left {}
.NotificationWrapper_position_bottom-right {}
.NotificationWrapper_position_top-center {}
.NotificationWrapper_position_top-left {}
.NotificationWrapper_position_top-right {}
```

## CSS Selectors (Notification)

```css
.Notification {}

/* Size */
.Notification_size_s {}
.Notification_size_m {}
.Notification_size_l {}

/* Type */
.Notification_type_danger {}
.Notification_type_info {}
.Notification_type_success {}
.Notification_type_warning {}

/* Title */
.Notification__Title {}

/* Title: Size */
.Notification__Title_size_s {}
.Notification__Title_size_m {}
.Notification__Title_size_l {}

/* Title: Type */
.Notification__Title_type_danger {}
.Notification__Title_type_info {}
.Notification__Title_type_success {}
.Notification__Title_type_warning {}

/* Description */
.Notification__Description {}

/* Description: Size */
.Notification__Description_size_s {}
.Notification__Description_size_m {}
.Notification__Description_size_l {}

/* Description: Type */
.Notification__Description_type_danger {}
.Notification__Description_type_info {}
.Notification__Description_type_success {}
.Notification__Description_type_warning {}

/* Close */
.Notification__Close {}

/* Close: Size */
.Notification__Close_size_s {}
.Notification__Close_size_m {}
.Notification__Close_size_l {}

/* Close: Type */
.Notification__Close_type_danger {}
.Notification__Close_type_info {}
.Notification__Close_type_success {}
.Notification__Close_type_warning {}
```
