import Notification from './notification';
import NotificationWrapper from './wrapper';

export { Notification, NotificationWrapper };
export default { Notification, NotificationWrapper };
