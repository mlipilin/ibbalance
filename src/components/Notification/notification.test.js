import React from 'react';
import userEvent from '@testing-library/user-event';

import { Notification } from './index';

import { render } from '../../hoc/with-theme';

describe('Тестирование компонента Notification', () => {
    it('Должен отобразиться Notification c параметрами по-умолчанию', () => {
        const { container } = render(<Notification />);
        const notification = container.querySelector('.Notification');
        expect(notification).toBeInTheDocument();
        expect(notification.classList.contains('Notification_size_m')).toBeTruthy();
        expect(notification.classList.contains('Notification_type_info')).toBeTruthy();
        expect(notification).toMatchSnapshot();
    });
    it('Должен отобразиться Notification с классом "Notification__Styled"', () => {
        const { container } = render(<Notification className="Notification__Styled" />);
        const notification = container.querySelector('.Notification');
        expect(notification.classList.contains('Notification__Styled')).toBeTruthy();
    });
    it('Должен отобразиться Notification с заголовком "Уведомление"', () => {
        const { container } = render(<Notification title="Уведомление" />);
        const title = container.querySelector('.Notification__Title');
        expect(title.textContent).toBe('Уведомление');
    });
    it('Должен отобразиться Notification с описанием "Подробное описание"', () => {
        const { container } = render(<Notification description="Подробное описание" />);
        const description = container.querySelector('.Notification__Description');
        expect(description.textContent).toBe('Подробное описание');
    });
    it('Должен отобразиться Notification с описанием "<span>Подробное описание</span>"', () => {
        const { container } = render(
            <Notification description={<span>Подробное описание</span>} />,
        );
        const description = container.querySelector('.Notification__Description span');
        expect(description.textContent).toBe('Подробное описание');
    });
    it('Должен отобразиться Notification с описанием "["Подробное", "описание"]"', () => {
        const { container } = render(<Notification description={['Подробное', 'описание']} />);
        const descriptionMessages = container.querySelectorAll('.Notification__Message');
        expect(descriptionMessages[0].textContent).toBe('Подробное');
        expect(descriptionMessages[1].textContent).toBe('описание');
    });
    it('Должен отобразиться Notification со свойством closeAfter (закрытие через 1 секунду)', async () => {
        jest.useFakeTimers();
        const { container } = render(<Notification closeAfter={2} title="Автозакрытие" />);
        jest.runAllTimers();

        const notification = container.querySelector('.Notification');
        expect(notification.classList.contains('Notification_closing')).toBeTruthy();
        expect(notification).toHaveStyle({ marginBottom: '-0px' });
    });

    it('Должен отобразиться Notification закрываем уведомление', async () => {
        const { container } = render(<Notification title="Закрытие вручную" />);
        const notificationX = container.querySelector('.Notification__Close');
        await userEvent.click(notificationX);
        expect(notificationX).toMatchSnapshot();
        const notification = container.querySelector('.Notification');
        expect(notification.classList.contains('Notification_closing')).toBeTruthy();
        expect(notification).toHaveStyle({ marginBottom: '-0px' });
    });

    it('Должен отобразиться Notification без Х', async () => {
        const { container } = render(<Notification noCloseButton />);
        const notificationX = container.querySelector('.Notification__Close');
        expect(notificationX).not.toBeInTheDocument();
    });

    it('Должен отобразиться Notification в размере s', async () => {
        const { container } = render(
            <Notification size="s" title="Заголовок" description="Описание" />,
        );
        const notification = container.querySelector('.Notification');
        expect(notification.classList.contains('Notification_size_s')).toBeTruthy();
        const buttonX = notification.querySelector('.Notification__Close');
        expect(buttonX.classList.contains('Notification__Close_size_s')).toBeTruthy();
        const title = notification.querySelector('.Notification__Title');
        expect(title.classList.contains('Notification__Title_size_s')).toBeTruthy();
        const description = notification.querySelector('.Notification__Description');
        expect(description.classList.contains('Notification__Description_size_s')).toBeTruthy();
    });
    it('Должен отобразиться Notification в размере m', async () => {
        const { container } = render(
            <Notification size="m" title="Заголовок" description="Описание" />,
        );
        const notification = container.querySelector('.Notification');
        expect(notification.classList.contains('Notification_size_m')).toBeTruthy();
        const buttonX = notification.querySelector('.Notification__Close');
        expect(buttonX.classList.contains('Notification__Close_size_m')).toBeTruthy();
        const title = notification.querySelector('.Notification__Title');
        expect(title.classList.contains('Notification__Title_size_m')).toBeTruthy();
        const description = notification.querySelector('.Notification__Description');
        expect(description.classList.contains('Notification__Description_size_m')).toBeTruthy();
    });
    it('Должен отобразиться Notification в размере l', async () => {
        const { container } = render(
            <Notification size="l" title="Заголовок" description="Описание" />,
        );
        const notification = container.querySelector('.Notification');
        expect(notification.classList.contains('Notification_size_l')).toBeTruthy();
        const buttonX = notification.querySelector('.Notification__Close');
        expect(buttonX.classList.contains('Notification__Close_size_l')).toBeTruthy();
        const title = notification.querySelector('.Notification__Title');
        expect(title.classList.contains('Notification__Title_size_l')).toBeTruthy();
        const description = notification.querySelector('.Notification__Description');
        expect(description.classList.contains('Notification__Description_size_l')).toBeTruthy();
    });
    it('Должен отобразиться Notification c типом danger', async () => {
        const { container } = render(
            <Notification type="danger" title="Заголовок" description="Описание" />,
        );
        const notification = container.querySelector('.Notification');
        expect(notification.classList.contains('Notification_type_danger')).toBeTruthy();
        const buttonX = notification.querySelector('.Notification__Close');
        expect(buttonX.classList.contains('Notification__Close_type_danger')).toBeTruthy();
        const title = notification.querySelector('.Notification__Title');
        expect(title.classList.contains('Notification__Title_type_danger')).toBeTruthy();
        const description = notification.querySelector('.Notification__Description');
        expect(
            description.classList.contains('Notification__Description_type_danger'),
        ).toBeTruthy();
    });
    it('Должен отобразиться Notification c типом info', async () => {
        const { container } = render(
            <Notification type="info" title="Заголовок" description="Описание" />,
        );
        const notification = container.querySelector('.Notification');
        expect(notification.classList.contains('Notification_type_info')).toBeTruthy();
        const buttonX = notification.querySelector('.Notification__Close');
        expect(buttonX.classList.contains('Notification__Close_type_info')).toBeTruthy();
        const title = notification.querySelector('.Notification__Title');
        expect(title.classList.contains('Notification__Title_type_info')).toBeTruthy();
        const description = notification.querySelector('.Notification__Description');
        expect(description.classList.contains('Notification__Description_type_info')).toBeTruthy();
    });
    it('Должен отобразиться Notification c типом success', async () => {
        const { container } = render(
            <Notification type="success" title="Заголовок" description="Описание" />,
        );
        const notification = container.querySelector('.Notification');
        expect(notification.classList.contains('Notification_type_success')).toBeTruthy();
        const buttonX = notification.querySelector('.Notification__Close');
        expect(buttonX.classList.contains('Notification__Close_type_success')).toBeTruthy();
        const title = notification.querySelector('.Notification__Title');
        expect(title.classList.contains('Notification__Title_type_success')).toBeTruthy();
        const description = notification.querySelector('.Notification__Description');
        expect(
            description.classList.contains('Notification__Description_type_success'),
        ).toBeTruthy();
    });
    it('Должен отобразиться Notification c типом warning', async () => {
        const { container } = render(
            <Notification type="warning" title="Заголовок" description="Описание" />,
        );
        const notification = container.querySelector('.Notification');
        expect(notification.classList.contains('Notification_type_warning')).toBeTruthy();
        const buttonX = notification.querySelector('.Notification__Close');
        expect(buttonX.classList.contains('Notification__Close_type_warning')).toBeTruthy();
        const title = notification.querySelector('.Notification__Title');
        expect(title.classList.contains('Notification__Title_type_warning')).toBeTruthy();
        const description = notification.querySelector('.Notification__Description');
        expect(
            description.classList.contains('Notification__Description_type_warning'),
        ).toBeTruthy();
    });
    it('Должен отобразиться Notification закрываем уведомление onClose', async () => {
        const handleClose = jest.fn();
        jest.useFakeTimers();
        const { container } = render(
            <Notification title="Закрытие вручную" onClose={handleClose} id={123} />,
        );
        const notificationX = container.querySelector('.Notification__Close');
        await userEvent.click(notificationX);
        expect(notificationX).toMatchSnapshot();
        const notification = container.querySelector('.Notification');
        expect(notification.classList.contains('Notification_closing')).toBeTruthy();
        expect(notification).toHaveStyle({ marginBottom: '-0px' });
        jest.runAllTimers();
        expect(handleClose).toBeCalledTimes(1);
        expect(handleClose).toBeCalledWith(123, expect.any(Object));
    });
});
