import React from 'react';

// Components
import Slider from '../../index';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';

// Examples
import disabled from './examples/disabled.md';
import steps from './examples/steps.md';
import size from './examples/size.md';
import type from './examples/type.md';

// HOC
import { withState } from '../../../../hoc';

// Wrappers
import { Col, Row } from '../../../../wrappers/WGrid';

const SliderWithState = withState(Slider);

const DemoButton = () => {
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>

            <div id="example">
                <Row
                    cols={{ mobile: [1], tab: [1, 1], desktop: [1], largeDesktop: [1, 1] }}
                    gap="20px"
                >
                    <Col>
                        <Example readme={type}>
                            <SliderWithState type="primary" value={30} />
                            <br />
                            <SliderWithState type="secondary" value={70} />
                        </Example>
                        <Example readme={disabled}>
                            <SliderWithState disabled value={30} />
                        </Example>
                    </Col>
                    <Col>
                        <Example readme={size}>
                            <SliderWithState size="s" value={30} />
                            <br />
                            <SliderWithState size="m" value={50} />
                        </Example>
                        <Example readme={steps}>
                            Шаг: 10
                            <SliderWithState min={0} max={100} step={10} value={30} />
                            <br />
                            Шаг: 20
                            <SliderWithState min={0} max={100} step={20} value={40} />
                            <br />
                            Шаг: 50
                            <SliderWithState min={0} max={100} step={50} value={50} />
                        </Example>
                    </Col>
                </Row>
            </div>
        </DemoComponent>
    );
};

export default DemoButton;
