# Тип контрола

Свойство type может принимать одно из следующих значений: `primary`, и `secondary`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Slider, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/alfa/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <SliderWithState type="primary" value={30} />
    <br />
    <SliderWithState type="secondary" value={70} />
  </ThemeProvider>,
  document.getElementById('root')
);
```
