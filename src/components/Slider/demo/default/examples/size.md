# Размер контрола

Свойство size может принимать одно из следующих значений: `s`, `m` и `l`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Slider, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/default/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Slider size="s" value={30} />
    <br />
    <Slider size="m" value={50} />
    <br />
    <Slider size="l" value={70} />
  </ThemeProvider>,
  document.getElementById('root')
);
```
