# Шаг ползунка

Можно устанавливать любой шаг ползунка, по умолчанию он равен `1`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Slider, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/default/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    Шаг: 10
    <Slider min={0} max={100} step={10} value={30} />
    <br />
    Шаг: 20
    <Slider min={0} max={100} step={20} value={40} />
    <br />
    Шаг: 50
    <Slider min={0} max={100} step={50} value={50} />
  </ThemeProvider>,
  document.getElementById('root')
);
```
