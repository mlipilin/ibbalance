# "disabled" состояние

`true`: контрол становится 'disabled' (недоступен для редактирования)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Slider, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/sbermarket/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Slider disabled value={30} />
  </ThemeProvider>,
  document.getElementById('root')
);
```
