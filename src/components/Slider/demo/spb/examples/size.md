# Размер контрола

Свойство size может принимать одно из следующих значений: `s` и `m`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Slider, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/spb/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Slider size="s" value={30} />
    <br />
    <Slider size="m" value={50} />
  </ThemeProvider>,
  document.getElementById('root')
);
```
