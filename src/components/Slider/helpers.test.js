import { normalizeValueByStep, percentToValue, valueToPercent } from './helpers';

describe('src/components/Slider/helpers.js', () => {
    describe('normalizeValueByStep', () => {
        it('0...0...0 (step = 1) -> 0', () => {
            expect(normalizeValueByStep({ min: 0, value: 0, max: 0, step: 1 })).toBe(0);
        });
        it('0...50...100 (step = 1) -> 50', () => {
            expect(normalizeValueByStep({ min: 0, value: 50, max: 100, step: 1 })).toBe(50);
        });
        it('0...50...100 (step = 20) -> 60', () => {
            expect(normalizeValueByStep({ min: 0, value: 50, max: 100, step: 20 })).toBe(60);
        });
        it('0...20...100 (step = 50) -> 0', () => {
            expect(normalizeValueByStep({ min: 0, value: 20, max: 100, step: 50 })).toBe(0);
        });
        it('0...30...100 (step = 50) -> 50', () => {
            expect(normalizeValueByStep({ min: 0, value: 30, max: 100, step: 50 })).toBe(50);
        });
        it('-50...0...50 (step = 1) -> 0', () => {
            expect(normalizeValueByStep({ min: -50, value: 0, max: 50, step: 1 })).toBe(0);
        });
        it('-50...0...50 (step = 20) -> 10', () => {
            expect(normalizeValueByStep({ min: -50, value: 0, max: 50, step: 20 })).toBe(10);
        });
        it('-50...20...50 (step = 50) -> 0', () => {
            expect(normalizeValueByStep({ min: -50, value: 20, max: 50, step: 50 })).toBe(0);
        });
        it('-50...30...50 (step = 50) -> 50', () => {
            expect(normalizeValueByStep({ min: -50, value: 30, max: 50, step: 50 })).toBe(50);
        });
        it('0...-10...100 (step = 1) -> 0', () => {
            expect(normalizeValueByStep({ min: 0, value: -10, max: 100, step: 1 })).toBe(0);
        });
        it('0...110...100 (step = 1) -> 100', () => {
            expect(normalizeValueByStep({ min: 0, value: 110, max: 100, step: 1 })).toBe(100);
        });
        it('0...95...100 (step = 60) -> 100', () => {
            expect(normalizeValueByStep({ min: 0, value: 95, max: 100, step: 60 })).toBe(100);
        });
        it('0...94...100 (step = 30) -> 100', () => {
            expect(normalizeValueByStep({ min: 0, value: 94, max: 100, step: 30 })).toBe(90);
        });
        it('0...96...100 (step = 30) -> 100', () => {
            expect(normalizeValueByStep({ min: 0, value: 96, max: 100, step: 30 })).toBe(100);
        });
    });
    describe('percentToValue', () => {
        it('0...0%...0 (step = 1) -> 0', () => {
            expect(percentToValue({ min: 0, percent: 0, max: 0, step: 1 })).toBe(0);
        });
        it('0...-2%...10 (step = 1) -> 0', () => {
            expect(percentToValue({ min: 0, percent: -2, max: 10, step: 1 })).toBe(0);
        });
        it('0...120%...10 (step = 1) -> 0', () => {
            expect(percentToValue({ min: 0, percent: 120, max: 10, step: 1 })).toBe(10);
        });
        it('0...77%...100 (step = 1) -> 77', () => {
            expect(percentToValue({ min: 0, percent: 77, max: 100, step: 1 })).toBe(77);
        });
    });
    describe('valueToPercent', () => {
        it('0...0...0 (step = 1) -> 0%', () => {
            expect(valueToPercent({ min: 0, value: 0, max: 0, step: 1 })).toBe(0);
        });
        it('0...-2...10 (step = 1) -> 0%', () => {
            expect(valueToPercent({ min: 0, value: -2, max: 10, step: 1 })).toBe(0);
        });
        it('0...12...10 (step = 1) -> 100%', () => {
            expect(valueToPercent({ min: 0, value: 12, max: 10, step: 1 })).toBe(100);
        });
        it('0...77...100 (step = 1) -> 77%', () => {
            expect(valueToPercent({ min: 0, value: 77, max: 100, step: 1 })).toBe(77);
        });
        it('-30...-21...20 (step = 1) -> 18%', () => {
            expect(valueToPercent({ min: -30, value: -21, max: 20, step: 1 })).toBe(18);
        });
    });
});
