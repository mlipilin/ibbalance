export const normalizeValueByStep = ({ min = 0, value = 0, max = 0, step = 1 }) => {
    let stepValue = value;

    const remainder = (value - min) % step;
    if (remainder > 0) {
        stepValue = value - remainder;

        let nextStepValue = stepValue + step;
        if (nextStepValue > max) {
            nextStepValue = max;
        }

        if (remainder >= (nextStepValue - stepValue) / 2) {
            stepValue += step;
        }
    }

    if (stepValue < min) stepValue = min;
    if (stepValue > max) stepValue = max;

    return stepValue;
};

export const percentToValue = ({ min, percent, max, step }) => {
    if (min >= max) return 0;
    if (percent <= 0) return min;
    if (percent >= 100) return max;

    const value = min + ((max - min) * percent) / 100;

    return normalizeValueByStep({ min, value, max, step });
};

export const valueToPercent = ({ min = 0, value = 0, max = 0, step = 1 }) => {
    if (min >= max) return 0;
    if (value <= min) return 0;
    if (value >= max) return 100;

    const valueFormat = normalizeValueByStep({ min, value, max, step });

    return ((valueFormat - min) / (max - min)) * 100;
};
