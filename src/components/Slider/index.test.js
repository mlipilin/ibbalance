import React from 'react';
import { fireEvent } from '@testing-library/react';

import Slider from './index';

import { render } from '../../hoc/with-theme';

describe('Тестирование компонента Slider', () => {
    it('Должен отобразиться Slider', () => {
        const { container } = render(<Slider />);
        const slider = container.querySelector('.Slider');
        expect(slider).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });
    it('Должен отобразиться Slider с классом "Slider__Styles"', () => {
        const { container } = render(<Slider className="Slider__Styles" />);
        const slider = container.querySelector('.Slider');
        expect(slider).toBeInTheDocument();
        expect(slider.classList.contains('Slider__Styles')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });
    it('Должен отобразиться Slider с типом контрола "primary" по-умолчанию', () => {
        const { container } = render(<Slider />);
        const slider = container.querySelector('.Slider');
        expect(slider).toBeInTheDocument();
        expect(slider.classList.contains('Slider_type_primary')).toBeTruthy();
    });
    it('Должен отобразиться Slider с типом контрола "primary"', () => {
        const { container } = render(<Slider type="primary" />);
        const slider = container.querySelector('.Slider');
        expect(slider).toBeInTheDocument();
        expect(slider.classList.contains('Slider_type_primary')).toBeTruthy();
    });
    it('Должен отобразиться Slider с типом контрола "secondary"', () => {
        const { container } = render(<Slider type="secondary" />);
        const slider = container.querySelector('.Slider');
        expect(slider).toBeInTheDocument();
        expect(slider.classList.contains('Slider_type_secondary')).toBeTruthy();
    });

    it('Должен отобразиться Slider с кастомным свойством "name"="slider"', () => {
        const { container } = render(<Slider name="slider" />);
        const slider = container.querySelector('.Slider');
        expect(slider.getAttribute('name')).toBe('slider');
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Slider в состоянии disabled', () => {
        const { container } = render(<Slider disabled />);
        const slider = container.querySelector('.Slider');
        expect(slider.classList.contains('Slider_disabled')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Slider в состоянии readonly', () => {
        const { container } = render(<Slider readOnly />);
        const slider = container.querySelector('.Slider');
        expect(slider.classList.contains('Slider_readonly')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Slider с размером m по-умолчанию', () => {
        const { container } = render(<Slider />);
        const slider = container.querySelector('.Slider');
        expect(slider.classList.contains('Slider_size_m')).toBeTruthy();
    });
    it('Должен отобразиться Slider с размером s', () => {
        const { container } = render(<Slider size="s" />);
        const slider = container.querySelector('.Slider');
        expect(slider.classList.contains('Slider_size_s')).toBeTruthy();
    });
    it('Должен отобразиться Slider с размером m', () => {
        const { container } = render(<Slider size="m" />);
        const slider = container.querySelector('.Slider');
        expect(slider.classList.contains('Slider_size_m')).toBeTruthy();
    });
    it('Должен отобразиться Slider с размером l', () => {
        const { container } = render(<Slider size="l" />);
        const slider = container.querySelector('.Slider');
        expect(slider.classList.contains('Slider_size_l')).toBeTruthy();
    });
    it('Должен отобразиться Slider с минимальным значением 49 и максимальным 51', () => {
        const { container } = render(<Slider min={49} max={51} value={50} />);
        const track = container.querySelector('.Slider__Track');
        expect(track).toHaveStyle('width: 50%');
        expect(container.firstChild).toMatchSnapshot();
    });
    it('Должен отобразиться Slider со значением 50', () => {
        const { container } = render(<Slider min={49} max={51} value={50} />);
        const button = container.querySelector('button');
        expect(button).toHaveStyle('left: 50%');
    });
    it('Должен отобразиться Slider со значением по-умолчанию 0', () => {
        const { container } = render(<Slider />);
        const track = container.querySelector('.Slider__Track');
        const button = container.querySelector('button');
        expect(track).toHaveStyle('width: 0%');
        expect(button).toHaveStyle('left: 0%');
    });
    it('Должен отобразиться Slider с шагом 50', () => {
        const { container } = render(<Slider step={50} value={60} />);
        const track = container.querySelector('.Slider__Track');
        const button = container.querySelector('button');
        expect(track).toHaveStyle('width: 50%');
        expect(button).toHaveStyle('left: 50%');
    });
    it('Должен отобразиться Slider onBlur', async () => {
        const handleBlur = jest.fn();
        const { container } = render(<Slider onBlur={handleBlur} />);
        const button = container.querySelector('button');
        const rail = container.querySelector('.Slider__Rail');
        expect(rail).toBeInTheDocument();
        rail.getBoundingClientRect = jest.fn(() => {
            return { width: 100, height: 10, top: 0, left: 0, bottom: 0, right: 0 };
        });

        await fireEvent.mouseDown(button);
        await fireEvent.mouseMove(button, {
            clientX: 200,
        });

        await fireEvent.mouseUp(button);

        expect(handleBlur).toBeCalledTimes(1);
        expect(handleBlur).toBeCalledWith(expect.any(Object));
    });
    it('Должен отобразиться Slider onChange', async () => {
        let value = 0;
        const handleChange = jest.fn(newValue => {
            value = newValue;
        });
        const { container, rerender } = render(<Slider onChange={handleChange} value={value} />);
        const button = container.querySelector('button');
        const rail = container.querySelector('.Slider__Rail');
        expect(rail).toBeInTheDocument();
        rail.getBoundingClientRect = jest.fn(() => {
            return { width: 100, height: 10, top: 0, left: 0, bottom: 0, right: 0 };
        });

        await fireEvent.mouseDown(button);
        await fireEvent.mouseMove(button, {
            clientX: 200,
        });

        await fireEvent.mouseUp(button);

        rerender(<Slider onChange={handleChange} value={value} />);
        const track = container.querySelector('.Slider__Track');

        expect(value).toBe(100);
        expect(track).toHaveStyle('width: 100%');
        expect(button).toHaveStyle('left: 100%');
        expect(handleChange).toBeCalledTimes(1);
        expect(handleChange).toBeCalledWith(100, expect.any(Object));
    });
});
