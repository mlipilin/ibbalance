import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

// Constants
import { SIZES, TYPES } from '../../constants/props';

// Helpers
import { valueToPercent, percentToValue } from './helpers';

import { withTheme } from '../../theme-provider';

@withTheme
class Slider extends Component {
    _isHandleMoving = false;

    componentDidMount() {
        document.addEventListener('mousedown', this.handleDocumentMouseDown);
        document.addEventListener('mousemove', this.handleDocumentMouseMove);
        document.addEventListener('mouseup', this.handleDocumentMouseUp);
        document.addEventListener('mouseleave', this.handleDocumentMouseLeave);

        document.addEventListener('touchstart', this.handleDocumentTouchStart);
        document.addEventListener('touchmove', this.handleDocumentTouchMove);
        document.addEventListener('touchend', this.handleDocumentTouchEnd);
        document.addEventListener('touchcancel', this.handleDocumentTouchCancel);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleDocumentMouseDown);
        document.removeEventListener('mousemove', this.handleDocumentMouseMove);
        document.removeEventListener('mouseup', this.handleDocumentMouseUp);
        document.removeEventListener('mouseleave', this.handleDocumentMouseLeave);

        document.removeEventListener('touchstart', this.handleDocumentTouchStart);
        document.removeEventListener('touchmove', this.handleDocumentTouchMove);
        document.removeEventListener('touchend', this.handleDocumentTouchEnd);
        document.removeEventListener('touchcancel', this.handleDocumentTouchCancel);
    }

    // Methods
    calculateNewValue = clientX => {
        const { max, min, step } = this.props;
        const clientRect = this.rail.getBoundingClientRect();
        const percent = ((clientX - clientRect.left) / clientRect.width) * 100;
        return percentToValue({ max, min, percent, step });
    };

    changeValueByClientX = clientX => {
        const newValue = this.calculateNewValue(clientX);
        const { value, onChange } = this.props;
        if (newValue !== value) {
            onChange(newValue, this.props);
        }
    };

    // Handlers: Mouse events
    handleDocumentMouseLeave = () => {
        const { onBlur } = this.props;
        if (this._isHandleMoving) {
            onBlur(this.props);
        }
        /* eslint no-underscore-dangle: 0 */
        this._isHandleMoving = false;
    };

    handleDocumentMouseDown = (e, clientX) => {
        const { disabled, readOnly } = this.props;
        if (disabled || readOnly) {
            return false;
        }

        if (e.target === this.slider || e.target === this.rail || e.target === this.track) {
            const newValue = this.calculateNewValue(e.clientX || clientX);
            const { value, onChange } = this.props;
            if (newValue !== value) {
                onChange(newValue, this.props);
            }
        } else if (e.target === this.handleWrapper || e.target === this.handle) {
            e.preventDefault();
            e.stopPropagation();
            /* eslint no-underscore-dangle: 0 */
            this._isHandleMoving = true;
        }
        return undefined;
    };

    handleDocumentMouseMove = e => {
        /* eslint no-underscore-dangle: 0 */
        if (this._isHandleMoving) {
            this.changeValueByClientX(e.clientX);
        }
    };

    handleDocumentMouseUp = () => {
        const { onBlur } = this.props;
        if (this._isHandleMoving) {
            onBlur(this.props);
        }
        /* eslint no-underscore-dangle: 0 */
        this._isHandleMoving = false;
    };

    // Handlers: Touch events
    handleDocumentTouchStart = e => {
        if (e.touches.length > 0) {
            this.handleDocumentMouseDown(e, e.touches[0].clientX);
        }
    };

    handleDocumentTouchMove = e => {
        /* eslint no-underscore-dangle: 0 */
        if (this._isHandleMoving && e.touches.length > 0) {
            this.changeValueByClientX(e.touches[0].clientX);
        }
    };

    handleDocumentTouchEnd = () => {
        this.handleDocumentMouseUp();
    };

    handleDocumentTouchCancel = () => {
        this.handleDocumentMouseUp();
    };

    render() {
        const {
            className,
            disabled,
            max,
            min,
            readOnly,
            size,
            step,
            type,
            value,
            applyTheme,
            ...otherProps
        } = this.props;

        const componentClass = applyTheme(
            cn('Slider', `Slider_size_${size}`, `Slider_type_${type}`, {
                Slider_disabled: disabled,
                Slider_readonly: readOnly,
            }),
            className,
        );

        const railClass = applyTheme(
            cn('Slider__Rail', `Slider__Rail_size_${size}`, `Slider__Rail_type_${type}`, {
                Slider__Rail_disabled: disabled,
                Slider__Rail_readonly: readOnly,
            }),
        );
        const trackClass = applyTheme(
            cn('Slider__Track', `Slider__Track_size_${size}`, `Slider__Track_type_${type}`, {
                Slider__Track_disabled: disabled,
                Slider__Track_readonly: readOnly,
            }),
        );
        const handleWrapperClass = applyTheme(
            cn(
                'Slider__HandleWrapper',
                `Slider__HandleWrapper_size_${size}`,
                `Slider__HandleWrapper_type_${type}`,
                {
                    Slider__HandleWrapper_disabled: disabled,
                    Slider__HandleWrapper_readonly: readOnly,
                },
            ),
        );
        const handleClass = applyTheme(
            cn('Slider__Handle', `Slider__Handle_size_${size}`, `Slider__Handle_type_${type}`, {
                Slider__Handle_disabled: disabled,
                Slider__Handle_readonly: readOnly,
            }),
        );

        const valuePercent = valueToPercent({ value, min, max, step });

        const handleWrapperStyle = {
            left: `${valuePercent}%`,
        };
        const trackStyle = {
            width: `${valuePercent}%`,
        };

        return (
            <div
                ref={el => {
                    this.slider = el;
                    return this.slider;
                }}
                className={componentClass}
                {...otherProps}
            >
                <div
                    ref={el => {
                        this.rail = el;
                        return this.rail;
                    }}
                    className={railClass}
                >
                    <div
                        ref={el => {
                            this.track = el;
                            return this.track;
                        }}
                        className={trackClass}
                        style={trackStyle}
                    />
                </div>

                <button
                    type="button"
                    ref={el => {
                        this.handleWrapper = el;
                        return this.handleWrapper;
                    }}
                    className={handleWrapperClass}
                    style={handleWrapperStyle}
                >
                    <span
                        ref={el => {
                            this.handle = el;
                            return this.handle;
                        }}
                        className={handleClass}
                    />
                </button>
            </div>
        );
    }
}

Slider.propTypes = {
    className: PropTypes.string,
    disabled: PropTypes.bool,
    field: PropTypes.string,
    max: PropTypes.number,
    min: PropTypes.number,
    readOnly: PropTypes.bool,
    size: PropTypes.oneOf([SIZES.S, SIZES.M, SIZES.L]),
    step: PropTypes.number,
    type: PropTypes.oneOf([TYPES.PRIMARY, TYPES.SECONDARY]),
    value: PropTypes.number,
    applyTheme: PropTypes.func,
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
};

Slider.defaultProps = {
    className: '',
    disabled: false,
    field: null,
    max: 100,
    min: 0,
    readOnly: false,
    size: SIZES.M,
    step: 1,
    type: TYPES.PRIMARY,
    value: 0,
    applyTheme: _ => _,
    onBlur: props => {}, // eslint-disable-line no-unused-vars
    onChange: (value, props) => {}, // eslint-disable-line no-unused-vars
};

export default Slider;
