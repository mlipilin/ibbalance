## Usage

```jsx
<Slider
    max={100}
    min={0}
    size="s"
    step={1}
    type="secondary"
    value={50}
    onBlur={_ => _}
    onChange={_ => _}
    onFocus={_ => _}
/>
```

## Props

Prop name | Prop type | Default value
--- | --- | ---
*disabled*|bool|`false`
*max*|number|`100`
*min*|number|`0`
*readOnly*|bool|`false`
*size*|`s`, `m`, `l`|`m`
*step*|number|`1`
*type*|`primary`, `secondary`|`primary`
*value*|number|`null`
*onChange*|func|`_ => _`

## CSS Selectors

```css
.Slider {}
.Slider_disabled {}
.Slider_readonly {}
.Slider_side_s {}
.Slider_side_m {}
.Slider_side_l {}
.Slider_type_primary {}
.Slider_type_secondary {}

/* Rail */
.Slider__Rail {}
.Slider__Rail_disabled {}
.Slider__Rail_readonly {}
.Slider__Rail_side_s {}
.Slider__Rail_side_m {}
.Slider__Rail_side_l {}
.Slider__Rail_type_primary {}
.Slider__Rail_type_secondary {}

/* Track */
.Slider__Track {}
.Slider__Track_disabled {}
.Slider__Track_readonly {}
.Slider__Track_side_s {}
.Slider__Track_side_m {}
.Slider__Track_side_l {}
.Slider__Track_type_primary {}
.Slider__Track_type_secondary {}

/* HandleWrapper */
.Slider__HandleWrapper {}
.Slider__HandleWrapper_disabled {}
.Slider__HandleWrapper_readonly {}
.Slider__HandleWrapper_side_s {}
.Slider__HandleWrapper_side_m {}
.Slider__HandleWrapper_side_l {}
.Slider__HandleWrapper_type_primary {}
.Slider__HandleWrapper_type_secondary {}

/* Handle */
.Slider__Handle {}
.Slider__Handle_disabled {}
.Slider__Handle_readonly {}
.Slider__Handle_side_s {}
.Slider__Handle_side_m {}
.Slider__Handle_side_l {}
.Slider__Handle_type_primary {}
.Slider__Handle_type_secondary {}
```
