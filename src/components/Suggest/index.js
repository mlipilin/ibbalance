import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

// Components
import SuggestOption from './option';

// Constants
import { SIZES } from '../../constants/props';

import { withTheme } from '../../theme-provider';

@withTheme
class Suggest extends Component {
    // Handlers
    handleOptionClick = id => {
        const { onChange, options } = this.props;
        onChange(options.find(o => o.id === id));
    };

    handleMouseOver = id => {
        const { onItemHover, options } = this.props;
        onItemHover(options.find(o => o.id === id));
    };

    render() {
        const {
            className,
            activeOptionId,
            optionSearchMask,
            options,
            size,
            value,
            applyTheme,
            formatOption,
        } = this.props;

        const componentClass = applyTheme(cn('Suggest'), className);

        const optionsClass = applyTheme(cn('Suggest__Options'));

        const optionSearchMaskPrepared = optionSearchMask
            ? optionSearchMask
                  .trim()
                  .replace(/(\*|\.|\+|\{|\}|\(|\)|\$|\?|\^|\[|\]|\;|\:|\,|\/|\-|\_|\=)/g, '\\$1')
            : null;

        return (
            <div className={componentClass}>
                <ul className={optionsClass}>
                    {options.map(option => {
                        const { id, name = '' } = option;
                        const optionClass = applyTheme(cn('Suggest__Option'));

                        let nameWithHighlights = [name];
                        if (optionSearchMaskPrepared) {
                            const regexp = new RegExp(optionSearchMaskPrepared, 'ig');
                            const match = name.match(regexp);

                            if (Array.isArray(match)) {
                                const nameParts = name.split(regexp);
                                nameWithHighlights = [];

                                let matchIndex = 0;
                                nameParts.forEach(part => {
                                    nameWithHighlights.push(part);
                                    if (matchIndex < match.length) {
                                        nameWithHighlights.push(
                                            <i key={matchIndex}>{match[matchIndex]}</i>,
                                        );
                                        matchIndex += 1;
                                    }
                                });
                            }
                        }

                        return (
                            <li key={id} className={optionClass}>
                                <SuggestOption
                                    id={id}
                                    isActive={id === activeOptionId}
                                    isDisabled={option.disabled}
                                    isValue={id === value}
                                    size={size}
                                    onClick={this.handleOptionClick}
                                    onMouseOver={this.handleMouseOver}
                                >
                                    {formatOption(nameWithHighlights, option)}
                                </SuggestOption>
                            </li>
                        );
                    })}
                </ul>
            </div>
        );
    }
}

Suggest.propTypes = {
    activeOptionId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    optionSearchMask: PropTypes.string,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            data: PropTypes.shape({}),
            disabled: PropTypes.bool,
            id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
            name: PropTypes.string,
        }),
    ),
    size: PropTypes.oneOf([SIZES.S, SIZES.M, SIZES.L]),
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    applyTheme: PropTypes.func,
    formatOption: PropTypes.func,
    onChange: PropTypes.func,
    onItemHover: PropTypes.func,
    className: PropTypes.string,
};

Suggest.defaultProps = {
    activeOptionId: null,
    className: '',
    optionSearchMask: '',
    options: [],
    size: SIZES.M,
    value: null,
    applyTheme: _ => _,
    formatOption: (name, option) => name, // eslint-disable-line
    onChange: option => {}, // eslint-disable-line no-unused-vars
    onItemHover: option => {}, // eslint-disable-line no-unused-vars
};

export default Suggest;
