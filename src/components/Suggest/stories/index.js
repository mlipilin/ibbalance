import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, select, text } from '@storybook/addon-knobs';

// Constants
import { LOCAL_STORAGE_THEME_KEY } from '../../../constants/local-storage';
import { SIZES } from '../../../constants/props';
import { THEMES } from '../../../constants/themes';

// HOC
import { withState } from '../../../hoc';

import Suggest from '../index';

import countries from '../../SelectBox/stories/countries.json';

const SuggestWithState = withState(Suggest);

const theme = localStorage.getItem(LOCAL_STORAGE_THEME_KEY);

const countriesObject = countries.reduce(
    (res, c) => ({
        ...res,
        [c.name]: c.id,
    }),
    {},
);
const countriesWithDisabledOptions = countries.map(country =>
    country.id > 5 && country.id <= 10 ? { ...country, disabled: true } : country,
);

let sizesList = [];

switch (theme) {
    case THEMES.DEFAULT:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        break;
    case THEMES.ALFA:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.ABSOLUT:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.SPB:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.TKB:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.TKB_LKZ:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.KYC:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.KYC_LANDING:
        sizesList = [SIZES.S, SIZES.M];
        break;
    default:
        break;
}

const stories = storiesOf('Suggest', module);

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories
    .add('Default', () => (
        <SuggestWithState
            activeOptionId={select('activeOptionId', countriesObject, countries[0].id)}
            options={countries}
            optionSearchMask={text('optionSearchMask', '')}
            size={select('size', sizesList, SIZES.M)}
            value={select('value', countriesObject, countries[2].id)}
        />
    ))
    .add('Disabled options', () => (
        <SuggestWithState
            activeOptionId={select('activeOptionId', countriesObject, countries[0].id)}
            options={countriesWithDisabledOptions}
            optionSearchMask={text('optionSearchMask', '')}
            size={select('size', sizesList, SIZES.M)}
            value={select('value', countriesObject, countries[2].id)}
        />
    ));
