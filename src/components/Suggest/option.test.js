import React from 'react';
import userEvent from '@testing-library/user-event';

import SuggestOption from './option';

import { render } from '../../hoc/with-theme';

describe('Тестирование компонента SuggestOption', () => {
    it('Должен отобразиться SuggestOption', () => {
        const { container } = render(<SuggestOption />);
        const component = container.firstChild;
        expect(component).toBeInTheDocument();
        expect(component).toMatchSnapshot();
    });

    it('Должен отобразиться SuggestOption c className = "SuggestOption__Style"', () => {
        const { container } = render(<SuggestOption className="SuggestOption__Style" />);
        const containerWithClassName = container.querySelector('.SuggestOption__Style');
        expect(containerWithClassName).toBeInTheDocument();
        expect(containerWithClassName).toMatchSnapshot();
    });

    it('Должен отобразиться SuggestOption cо свойством isActive', () => {
        const { container } = render(<SuggestOption isActive id="isActive" />);
        const containerWithClassName = container.querySelector('.SuggestOption_active');
        expect(containerWithClassName).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться SuggestOption cо свойством isDisabled', () => {
        const { container } = render(<SuggestOption isDisabled id="isDisabled" />);
        const containerWithClassName = container.querySelector('.SuggestOption_disabled');
        expect(containerWithClassName).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться SuggestOption cо свойством isValue', () => {
        const { container } = render(<SuggestOption isValue id="isValid" />);
        const containerWithClassName = container.querySelector('.SuggestOption_value');
        expect(containerWithClassName).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться SuggestOption cо свойством id', () => {
        const { container } = render(<SuggestOption id="123" />);
        const containerWithClassName = container.querySelector('.SuggestOption');
        expect(containerWithClassName.id).toBe('select_box_option_123');
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться SuggestOption cо свойством size="m" по-умолчанию', () => {
        const { container } = render(<SuggestOption />);
        const containerWithSizeClass = container.querySelector('.SuggestOption_size_m');
        expect(containerWithSizeClass).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться SuggestOption cо свойством size="s"', () => {
        const { container } = render(<SuggestOption size="s" />);
        const containerWithSizeClass = container.querySelector('.SuggestOption_size_s');
        expect(containerWithSizeClass).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться SuggestOption cо свойством size="m"', () => {
        const { container } = render(<SuggestOption size="m" />);
        const containerWithSizeClass = container.querySelector('.SuggestOption_size_m');
        expect(containerWithSizeClass).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться SuggestOption cо свойством size="l"', () => {
        const { container } = render(<SuggestOption size="l" />);
        const containerWithSizeClass = container.querySelector('.SuggestOption_size_l');
        expect(containerWithSizeClass).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться SuggestOption проверка onClick', () => {
        const handleClick = jest.fn();
        const { container } = render(<SuggestOption id="123" onClick={handleClick} />);
        const containerWithClassName = container.querySelector('.SuggestOption');
        expect(containerWithClassName.id).toBe('select_box_option_123');
        userEvent.click(containerWithClassName);
        expect(handleClick).toBeCalledWith('123');
        expect(handleClick).toHaveBeenCalledTimes(1);
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться SuggestOption проверка onMouseOver', () => {
        const handleMouseOver = jest.fn();
        const { container } = render(<SuggestOption id="123" onMouseOver={handleMouseOver} />);
        const containerWithClassName = container.querySelector('.SuggestOption');
        expect(containerWithClassName.id).toBe('select_box_option_123');
        userEvent.click(containerWithClassName);
        expect(handleMouseOver).toBeCalledWith('123');
        expect(handleMouseOver).toHaveBeenCalledTimes(1);
        expect(container.firstChild).toMatchSnapshot();
    });
});
