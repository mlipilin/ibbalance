import React from 'react';
import userEvent from '@testing-library/user-event';

import Suggest from './index';

import countries from './stories/countries.json';

import { render } from '../../hoc/with-theme';

describe('Тестирование компонента Suggest', () => {
    it('Должен отобразиться Suggest', () => {
        const { container } = render(<Suggest />);
        const component = container.firstChild;
        expect(component).toBeInTheDocument();
        expect(component).toMatchSnapshot();
    });

    it('Должен отобразиться Suggest со списком стран', () => {
        const { container } = render(<Suggest options={countries} />);
        const component = container.firstChild;
        const countriesElements = container.querySelectorAll('.Suggest__Option');
        expect(countriesElements.length).toBe(9);
        expect(component).toBeInTheDocument();
        expect(component).toMatchSnapshot();
    });

    it('Должен отобразиться Suggest с classname = "Suggest__Style"', () => {
        const { container } = render(<Suggest className="Suggest__Style" />);
        const component = container.querySelector('.Suggest__Style');
        expect(component).toBeInTheDocument();
        expect(component).toMatchSnapshot();
    });

    it('Должен отобразиться Suggest с размером "m" по-умолчанию', () => {
        const { container } = render(<Suggest options={countries} />);
        const component = container.querySelector('.SuggestOption_size_m');
        expect(component).toBeInTheDocument();
    });

    it('Должен отобразиться Suggest с размером "s"', () => {
        const { container } = render(<Suggest size="s" options={countries} />);
        const component = container.querySelector('.SuggestOption_size_s');
        expect(component).toBeInTheDocument();
    });

    it('Должен отобразиться Suggest с размером "m"', () => {
        const { container } = render(<Suggest size="m" options={countries} />);
        const component = container.querySelector('.SuggestOption_size_m');
        expect(component).toBeInTheDocument();
    });

    it('Должен отобразиться Suggest с размером "l"', () => {
        const { container } = render(<Suggest size="l" options={countries} />);
        const component = container.querySelector('.SuggestOption_size_l');
        expect(component).toBeInTheDocument();
    });

    it('Должен отобразиться Suggest проверка свойства onItemHover', () => {
        const handleItemHover = jest.fn();
        const { container } = render(<Suggest options={countries} onItemHover={handleItemHover} />);
        const firstElem = container.querySelector('.SuggestOption');
        userEvent.hover(firstElem);
        expect(handleItemHover).toHaveBeenCalledTimes(1);
        expect(handleItemHover).toBeCalledWith({ id: 0, name: 'Австралия' });
    });

    it('Должен отобразиться Suggest со списком стран и выбранным Россия', () => {
        const { container } = render(<Suggest options={countries} value={130} />);
        const component = container.querySelector('.SuggestOption_value');
        expect(component).toBeInTheDocument();
        expect(component.textContent).toBe('Россия');
        expect(component).toMatchSnapshot();
    });

    it('Должен отобразиться Suggest со списком стран и активным Россия', () => {
        const { container } = render(<Suggest options={countries} activeOptionId={130} />);
        const component = container.querySelector('.SuggestOption_active');
        expect(component).toBeInTheDocument();
        expect(component.textContent).toBe('Россия');
        expect(component).toMatchSnapshot();
    });

    it('Должен отобразиться Suggest со списком стран и поиском optionSearchMask', () => {
        const { container } = render(<Suggest options={countries} optionSearchMask="Р" />);
        const component = container.querySelector('i');
        expect(component).toBeInTheDocument();
        expect(component.textContent).toBe('р');
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Suggest проверка свойства onChange', () => {
        const handleChange = jest.fn();
        const { container } = render(<Suggest options={countries} onChange={handleChange} />);
        const firstElem = container.querySelector('.SuggestOption');
        expect(firstElem).toBeInTheDocument();
        userEvent.click(firstElem);
        expect(handleChange).toBeCalledWith({ id: 0, name: 'Австралия' });
        expect(handleChange).toHaveBeenCalledTimes(1);
    });

    it('Должен отобразиться Suggest со странами в lowerCase', () => {
        const handleFormatOption = jest.fn(name => name[0].toLowerCase());
        const { container } = render(
            <Suggest options={countries} formatOption={handleFormatOption} />,
        );
        const firstElem = container.querySelector('.SuggestOption');
        expect(firstElem).toBeInTheDocument();
        expect(firstElem).toMatchSnapshot();
        expect(firstElem.textContent).toBe('австралия');
        expect(handleFormatOption).toHaveBeenCalledTimes(countries.length);
        expect(handleFormatOption).toHaveBeenCalledWith([countries[0].name], countries[0]);
    });
});
