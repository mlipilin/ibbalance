import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

// Constants
import { SIZES } from '../../constants/props';

import { withTheme } from '../../theme-provider';

@withTheme
class SuggestOption extends Component {
    handleClick = e => {
        e.preventDefault();
        const { onClick, id } = this.props;
        onClick(id);
    };

    handleMouseOver = () => {
        const { onMouseOver, id } = this.props;
        onMouseOver(id);
    };

    render() {
        const {
            className,
            id,
            isActive,
            isDisabled,
            isValue,
            size,
            applyTheme,
            children,
        } = this.props;

        const componentClass = applyTheme(
            cn('SuggestOption', `SuggestOption_size_${size}`, {
                SuggestOption_active: isActive,
                SuggestOption_disabled: isDisabled,
                SuggestOption_value: isValue,
            }),
            className,
        );

        /* eslint-disable */
        return (
            <div
                className={componentClass}
                id={`select_box_option_${id}`}
                ref={el => {
                    this.div = el;
                    return this.div;
                }}
                onClick={this.handleClick}
                onMouseOver={this.handleMouseOver}
            >
                {children}
            </div>
        );
    }
}

SuggestOption.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    className: PropTypes.string,
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    isActive: PropTypes.bool,
    isDisabled: PropTypes.bool,
    isValue: PropTypes.bool,
    size: PropTypes.oneOf([SIZES.S, SIZES.M, SIZES.L]),
    applyTheme: PropTypes.func,
    onClick: PropTypes.func,
    onMouseOver: PropTypes.func,
};

SuggestOption.defaultProps = {
    children: null,
    className: '',
    id: null,
    isActive: false,
    isDisabled: false,
    isValue: false,
    size: SIZES.M,
    applyTheme: _ => _,
    onClick: id => {},
    onMouseOver: id => {},
};

export default SuggestOption;
