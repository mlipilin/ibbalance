# "disabled" состояние

`true`: инпут становится 'disabled' (недоступен для редактирования)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Input, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/alfa/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Input label="Disabled поле" disabled />
  </ThemeProvider>,
  document.getElementById('root')
);
```
