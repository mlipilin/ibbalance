# Наличие длиной ошибки

В поле `errorDetail` можно передать текст ошибки

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Input, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/tkb_lkz/theme';

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <Input
            label="Невалидное поле c длиной ошибкой"
            value="admin@example.com"
            errorDetail="Данный e-mail не существует"
        />
    </ThemeProvider>,
    document.getElementById('root'),
);
```
