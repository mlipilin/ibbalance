# Наличие ошибки

В поле `error` можно передать текст ошибки

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Input, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/tkb_lkz/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Input label="Невалидное поле" value="admin@example.com" error="Данный e-mail не существует" />
  </ThemeProvider>,
  document.getElementById('root')
);
```
