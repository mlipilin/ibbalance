# Маска ввода

Для реализации ввода по маске используется библиотека [react-input-mask](https://github.com/sanniassin/react-input-mask)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Input, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/sbermarket/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Input label="Мобильный телефон" mask="+7 (999) 999-99-99" />
  </ThemeProvider>,
  document.getElementById('root')
);
```
