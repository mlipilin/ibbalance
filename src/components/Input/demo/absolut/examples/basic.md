# Поле без заголовка

Но можно передать placeholder

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Input, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/absolut/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Input placeholder="Фамилия" />
  </ThemeProvider>,
  document.getElementById('root')
);
```
