# Инпут для ввода пароля

В поле `type` необходимо передать тип поля 'password'

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Input, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/absolut/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Input label="Пароль" type="password" />
  </ThemeProvider>,
  document.getElementById('root')
);
```
