# Размер инпута

Свойство size может принимать одно из следующих значений: `s` и `m`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Input, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/tkb/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Input label="Size M" size="m" />
    <Input label="Size S" size="s" />
  </ThemeProvider>,
  document.getElementById('root')
);
```
