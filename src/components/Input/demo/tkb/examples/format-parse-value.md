# Обработчики formatValue / parseValue

Вывод форматированного значения

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Input, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/tkb/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Input
      label="Стоимость"
      value="1000000"
      formatValue={value => String(value).replace(/(.)(?=(\d{3})+$)/g, '$1 ')}
      parseValue={value => value.replace(' ', '')}
    />
  </ThemeProvider>,
  document.getElementById('root')
);
```
