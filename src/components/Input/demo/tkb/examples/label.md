# Поле с заголовком

Заголовок передается в свойстве `label`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Input, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/tkb/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Input label="Фамилия" />
  </ThemeProvider>,
  document.getElementById('root')
);
```
