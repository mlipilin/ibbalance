import React from 'react';

// Components
import Input from '../../index';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';

// Examples
import basic from './examples/basic.md';
import disabled from './examples/disabled.md';
import error from './examples/error.md';
import errorDetail from './examples/errorDetail.md';
import formatParseValue from './examples/format-parse-value.md';
import label from './examples/label.md';
import mask from './examples/mask.md';
import password from './examples/password.md';
import readOnly from './examples/readonly.md';
import size from './examples/size.md';
import success from './examples/success.md';

// HOC
import { withState } from '../../../../hoc';

// Wrappers
import { Col, Row } from '../../../../wrappers/WGrid';

const InputWithState = withState(Input);

const DemoInput = () => {
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>

            <div id="example">
                <Row
                    cols={{ mobile: [1], tab: [1, 1], desktop: [1], largeDesktop: [1, 1] }}
                    gap="20px"
                >
                    <Col>
                        <Example readme={basic}>
                            <InputWithState placeholder="Фамилия" />
                        </Example>
                        <Example readme={label}>
                            <InputWithState label="Фамилия" />
                        </Example>
                        <Example readme={size}>
                            <InputWithState label="Size M" size="m" />
                            <InputWithState label="Size S" size="s" />
                        </Example>
                        <Example readme={mask}>
                            <InputWithState label="Мобильный телефон" mask="+7 (999) 999-99-99" />
                        </Example>
                        <Example readme={formatParseValue}>
                            <InputWithState
                                label="Стоимость, руб."
                                value="1000000"
                                formatValue={value =>
                                    String(value).replace(/(.)(?=(\d{3})+$)/g, '$1 ')
                                }
                                parseValue={value => value.replace(' ', '')}
                            />
                        </Example>
                        <Example readme={password}>
                            <InputWithState label="Пароль" type="password" />
                        </Example>
                    </Col>
                    <Col>
                        <Example readme={disabled}>
                            <InputWithState label="Disabled поле" disabled />
                        </Example>
                        <Example readme={readOnly}>
                            <InputWithState label="Readonly поле" readOnly />
                        </Example>
                        <Example readme={error}>
                            <InputWithState
                                label="Невалидное поле"
                                value="admin@example.com"
                                error="Данный e-mail не существует"
                            />
                        </Example>
                        <Example readme={errorDetail}>
                            <InputWithState
                                label="Невалидное поле"
                                value="admin@example.com"
                                errorDetail="Данный e-mail не существует"
                            />
                        </Example>
                        <Example readme={success}>
                            <InputWithState
                                label="Валидное поле"
                                value="ivan.petrov@balance-pl.ru"
                                success
                            />
                        </Example>
                    </Col>
                </Row>
            </div>
        </DemoComponent>
    );
};

export default DemoInput;
