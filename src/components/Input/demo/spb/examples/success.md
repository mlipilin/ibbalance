# "success" состояние

`true`: инпут становится 'success' (значение в поле валидно)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Input, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/spb/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Input label="Валидное поле" value="ivan.petrov@balance-pl.ru" success />
  </ThemeProvider>,
  document.getElementById('root')
);
```
