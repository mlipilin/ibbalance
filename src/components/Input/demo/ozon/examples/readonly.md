# "read only" состояние

`true`: инпут становится 'read only' (только для чтения)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Input, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/ozon/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Input label="Readonly поле" readOnly />
  </ThemeProvider>,
  document.getElementById('root')
);
```
