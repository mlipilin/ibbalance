## Usage

Input with placeholder (NO label):

```jsx
<Input
    size="s"
    placeholder="First Name"
    value={firstName}
    onBlur={_ => _}
    onChange={_ => _}
    onFocus={_ => _}
/>
```

Input with label (NO placeholder):

```jsx
<Input
    label="First Name"
    size="s"
    value={firstName}
    onBlur={_ => _}
    onChange={_ => _}
    onFocus={_ => _}
/>
```

## Props

Prop name | Prop type | Default value
--- | --- | ---
*disabled*|bool|`false`
*error*|string, object|`null`
*errorDetail*|string|`null`
*isReadOnly*|bool|`false`
*label*|string, node, node[]|`null`
*mask*|string|`null`
*size*|`s`, `m`, `l`|`m`
*success*|bool|`false`
*value*|string|`null`
*formatValue*|func|`value => {}`
*onBlur*|func|`props => {}`
*onChange*|func|`(value, props) => {}`
*onFocus*|func|`props => {}`
*parseValue*|func|`value => {}`

## CSS Selectors

```css
.Input {}
.Input_disabled {}

/* Label */
.Input__Label {}

/* LabelText */
.Input__LabelText {}

/* LabelText: Booleans */
.Input__LabelText_place-top {}

/* LabelText: Size */
.Input__LabelText_size_s {}
.Input__LabelText_size_m {}
.Input__LabelText_size_l {}

/* Input */
.Input__Input {}

/* Input: Booleans */
.Input__Input_error {}
.Input__Input_readonly {}
.Input__Input_success {}
.Input__Input_with-label {}

/* Input: Size */
.Input__Input_size_s {}
.Input__Input_size_m {}
.Input__Input_size_l {}

/* Error */
.Input__Error {}

/* Error: Size */
.Input__Error_size_s {}
.Input__Error_size_m {}
.Input__Error_size_l {}
```
