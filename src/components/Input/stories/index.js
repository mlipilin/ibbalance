import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean, select, text } from '@storybook/addon-knobs';

// Constants
import { LOCAL_STORAGE_THEME_KEY } from '../../../constants/local-storage';
import { SIZES } from '../../../constants/props';
import { THEMES } from '../../../constants/themes';

// HOC
import { withState } from '../../../hoc';

import Input from '../index';

import notes from '../readme.md';

const InputWithState = withState(Input);
const theme = localStorage.getItem(LOCAL_STORAGE_THEME_KEY);

let sizesList = [];

switch (theme) {
    case THEMES.DEFAULT:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        break;
    case THEMES.ALFA:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.ABSOLUT:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.SPB:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.TKB:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.TKB_LKZ:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.KYC:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.KYC_LANDING:
        sizesList = [SIZES.S, SIZES.M];
        break;
    default:
        break;
}

const stories = storiesOf('Input', module);

stories.addDecorator(withKnobs);

stories
    .add(
        'With label',
        () => (
            <InputWithState
                disabled={boolean('Disabled', false)}
                error={text('Error', '')}
                errorDetail={text('Error detail', '')}
                label={text('Label', 'First name')}
                readOnly={boolean('ReadOnly', false)}
                size={select('Size', sizesList, SIZES.M)}
                success={boolean('Success', false)}
            />
        ),
        { notes },
    )
    .add(
        'Without label',
        () => (
            <InputWithState
                disabled={boolean('Disabled', false)}
                error={text('Error', '')}
                errorDetail={text('Error detail', '')}
                placeholder={text('placeholder', 'First name')}
                readOnly={boolean('ReadOnly', false)}
                size={select('Size', sizesList, SIZES.M)}
                success={boolean('Success', false)}
            />
        ),
        { notes },
    )
    .add(
        'With mask',
        () => (
            <InputWithState
                disabled={boolean('Disabled', false)}
                error={text('Error', '')}
                errorDetail={text('Error detail', '')}
                label={text('Label', 'Phone')}
                mask="+7\ (999) 999-99-99"
                readOnly={boolean('ReadOnly', false)}
                size={select('Size', sizesList, SIZES.M)}
                success={boolean('Success', false)}
            />
        ),
        { notes },
    );
