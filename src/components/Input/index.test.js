import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import FeatherIcon from '../FeatherIcon';

import Input from './index';

import { render } from '../../hoc/with-theme';

describe('Тестирование компонента Input', () => {
    it('Должен отобразиться Input без заголовка и placeholder', () => {
        const { container } = render(<Input />);
        expect(screen.getByPlaceholderText('')).toBeInTheDocument();
        expect(screen.getByLabelText('')).toBeInTheDocument();
        expect(screen.getByDisplayValue('')).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Input с label = "Фамилия" ', () => {
        const { container } = render(<Input label="Фамилия" />);
        expect(screen.getByPlaceholderText('')).toBeInTheDocument();
        expect(screen.getByLabelText('Фамилия')).toBeInTheDocument();
        expect(screen.getByDisplayValue('')).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Input с label = "<span>Фамилия</span>" ', () => {
        const { container } = render(<Input label={<span>Фамилия</span>} />);
        expect(screen.getByLabelText('Фамилия')).toBeInTheDocument();
        const textElement = container.querySelector('.Input__LabelText > span');
        expect(textElement.textContent).toBe('Фамилия');
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Input с label = "[<span>Фамилия</span>,<strong>Имя</strong>]" ', () => {
        const { container } = render(
            <Input label={[<span key="surname">Фамилия</span>, <strong key="name">Имя</strong>]} />,
        );
        expect(screen.getByText('Фамилия')).toBeInTheDocument();
        expect(screen.getByText('Имя')).toBeInTheDocument();
        const textElement = container.querySelector('.Input__LabelText');
        const textSurname = textElement.querySelector('span');
        expect(textSurname.textContent).toBe('Фамилия');
        const textName = textElement.querySelector('strong');
        expect(textName.textContent).toBe('Имя');
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Input с placeholder = "Введите фамилию" ', () => {
        const { container } = render(<Input placeholder="Введите фамилию" />);
        const input = container.querySelector('input');
        expect(input.placeholder).toBe('Введите фамилию');
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Input с классом "Input__Style" ', () => {
        const { container } = render(<Input className="Input__Style" />);
        const elemWithInputClass = container.querySelector('.Input__Style');
        expect(elemWithInputClass).toBeInTheDocument();
        expect(elemWithInputClass).toMatchSnapshot();
    });

    it('Должен отобразиться Input с size = "m" по-умолчанию ', () => {
        const { container } = render(<Input />);
        const elemWithInputClass = container.querySelector('.Input');
        expect(elemWithInputClass).toMatchSnapshot();
        expect(elemWithInputClass.classList.contains('Input_size_m')).toBeTruthy();
    });

    it('Должен отобразиться Input с size = "s" ', () => {
        const { container } = render(<Input size="s" />);
        const elemWithInputClass = container.querySelector('.Input');
        expect(elemWithInputClass).toMatchSnapshot();
        expect(elemWithInputClass.classList.contains('Input_size_s')).toBeTruthy();
    });

    it('Должен отобразиться Input с size = "m" ', () => {
        const { container } = render(<Input size="m" />);
        const elemWithInputClass = container.querySelector('.Input');
        expect(elemWithInputClass).toMatchSnapshot();
        expect(elemWithInputClass.classList.contains('Input_size_m')).toBeTruthy();
    });

    it('Должен отобразиться Input с size = "l" ', () => {
        const { container } = render(<Input size="l" />);
        const elemWithInputClass = container.querySelector('.Input');
        expect(elemWithInputClass).toMatchSnapshot();
        expect(elemWithInputClass.classList.contains('Input_size_l')).toBeTruthy();
    });

    it('Должен отобразиться Input с форматированным значением номера телефона согласно маске +7 (999) 999-99-99', () => {
        const { container } = render(
            <Input label="Мобильный телефон" mask="+7 (999) 999-99-99" value="1234567890" />,
        );
        const input = container.querySelector('input');
        expect(input.value).toBe('+7 (123) 456-78-90');
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Проверка работы formatValue и parseValue', async () => {
        const onChange = jest.fn();
        const { container } = render(
            <Input
                label="Стоимость"
                value="1000000"
                formatValue={value => String(value).replace(/(.)(?=(\d{3})+$)/g, '$1 ')}
                parseValue={value => value.replace(/\s/g, '')}
                onChange={onChange}
            />,
        );
        const input = container.querySelector('input');
        expect(input.value).toBe('1 000 000');
        await userEvent.type(
            input,
            '{arrowright}{arrowright}{arrowright}{arrowright}{arrowright}{arrowright}{arrowright}{arrowright}{arrowright}1',
        );
        expect(onChange).toBeCalledWith('10000001', expect.any(Object));
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Input для ввода пароля', async () => {
        const { container } = render(<Input label="Пароль" type="password" />);
        const input = container.querySelector('input');
        expect(input.type).toBe('password');
        expect(input.classList.contains('Input__Input_password')).toBeTruthy();
        const iconPassword = container.querySelector('svg');
        expect(iconPassword).toBeInTheDocument();
        expect(iconPassword.classList.contains('feather-eye-off')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
        await userEvent.click(iconPassword);
        expect(input.type).toBe('text');
        const iconText = container.querySelector('svg');
        expect(iconText).toBeInTheDocument();
        expect(iconText.classList.contains('feather-eye')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Input в состоянии disabled', () => {
        const onChange = jest.fn();
        const { container } = render(<Input label="Disabled поле" onChange={onChange} disabled />);
        expect(container.firstChild.classList.contains('Input_disabled')).toBeTruthy();
        const input = container.querySelector('input');
        expect(input.disabled).toBeTruthy();
        const iconContainer = container.querySelector('.Input__DisabledIcon');
        expect(iconContainer).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
        userEvent.type(input, 'text for disabled');
        expect(onChange).toBeCalledTimes(0);
    });

    it('Должен отобразиться Input в состоянии read only', () => {
        const onChange = jest.fn();
        const { container } = render(<Input label="Readonly поле" onChange={onChange} readOnly />);
        const input = container.querySelector('input');
        expect(input.readOnly).toBeTruthy();
        expect(input.classList.contains('Input__Input_readonly')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
        userEvent.type(input, 'text for readonly');
        expect(onChange).toBeCalledTimes(0);
    });

    it('Должен отобразиться Input с ошибкой "Данный e-mail не существует"', () => {
        const { container } = render(
            <Input
                label="Невалидное поле"
                value="admin@example.com"
                error="Данный e-mail не существует"
            />,
        );
        const input = container.querySelector('input');
        expect(input.classList.contains('Input__Input_error')).toBeTruthy();
        const error = container.querySelector('.Input__Error');
        expect(error).toBeInTheDocument();
        expect(screen.getByText('Данный e-mail не существует')).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Input с ошибкой', () => {
        const { container } = render(
            <Input label="Невалидное поле" value="admin@example.com" error />,
        );
        const input = container.querySelector('input');
        expect(input.classList.contains('Input__Input_error')).toBeTruthy();
        const error = container.querySelector('.Input__Error');
        expect(error).toBeInTheDocument();
        expect(error.textContent).toBe('');
    });

    it('Должен отобразиться Input с ошибкой - node', () => {
        const { container } = render(
            <Input
                label="Невалидное поле"
                value="admin@example.com"
                error={
                    <>
                        <FeatherIcon name="alert-triangle" />
                        <span>Данный e-mail не существует</span>
                    </>
                }
            />,
        );
        const input = container.querySelector('input');
        expect(input.classList.contains('Input__Input_error')).toBeTruthy();
        const error = container.querySelector('.Input__Error');
        expect(error).toBeInTheDocument();
        expect(error.textContent).toBe('Данный e-mail не существует');
        const errorIcon = container.querySelector('.feather-alert-triangle');
        expect(errorIcon).toBeInTheDocument();
    });

    it('Должен отобразиться Input с ошибкой "Данный e-mail не существует" скрытой под иконкой', () => {
        const { container } = render(
            <Input
                label="Невалидное поле"
                value="admin@example.com"
                errorDetail="Данный e-mail не существует"
            />,
        );
        const input = container.querySelector('input');
        expect(input.classList.contains('Input__Input_error')).toBeTruthy();
        const error = container.querySelector('.Input__ErrorDetail');
        expect(error).toBeInTheDocument();
        expect(screen.getByText('Данный e-mail не существует')).toBeInTheDocument();
        const detailIcon = container.querySelector('.feather-alert-circle');
        expect(detailIcon).toBeInTheDocument();
        const tooltip = container.querySelector('.Input__Tooltip');
        expect(tooltip.classList.contains('Input__Tooltip_visible')).toBeFalsy();
        userEvent.click(detailIcon);
        expect(tooltip.classList.contains('Input__Tooltip_visible')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
        userEvent.click(input);
        expect(tooltip.classList.contains('Input__Tooltip_visible')).toBeFalsy();
    });

    it('Должен отобразиться Input с ошибкой и иконкой "<FeatherIcon name="alert-triangle" />"', () => {
        const { container } = render(
            <Input
                label="Невалидное поле"
                value="admin@example.com"
                errorDetail="Данный e-mail не существует"
                errorDetailIcon={<FeatherIcon name="alert-triangle" />}
            />,
        );

        const detailIcon = container.querySelector('.feather-alert-triangle');
        expect(detailIcon).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Input в состоянии success', () => {
        const { container } = render(
            <Input label="Валидное поле" value="admin@example.com" success />,
        );
        const input = container.querySelector('input');
        expect(input.classList.contains('Input__Input_success')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Input со значением "Иванов"', () => {
        const onChange = jest.fn();
        const { container, rerender } = render(<Input label="Фамилия" onChange={onChange} />);
        const input = screen.getByLabelText(/Фамилия/i);
        const newValue = 'Иванов';
        userEvent.type(input, newValue);
        expect(onChange).toHaveBeenCalledTimes(newValue.length);
        expect(onChange).toHaveBeenNthCalledWith(1, newValue[0], expect.any(Object));
        rerender(<Input label="Фамилия" value={newValue} onChange={onChange} />);
        expect(input).toHaveValue(newValue);
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Input со значением "Петров"', async () => {
        let inputValue = '';
        const handleChange = value => {
            inputValue += value;
        };
        const { container, rerender } = render(
            <Input label="Фамилия" onChange={handleChange} value={inputValue} />,
        );
        const input = container.querySelector('input');
        const newValue = 'Петров';
        await userEvent.type(input, newValue);
        expect(inputValue).toBe(newValue);
        rerender(<Input label="Фамилия" onChange={handleChange} value={inputValue} />);
        expect(input.value).toBe(newValue);
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Input со числовым значением 123 ', () => {
        const numberValue = 123;
        const { container } = render(<Input label="Номер" value={numberValue} />);
        const input = container.querySelector('input');
        expect(input).toHaveValue(numberValue.toString());
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Input в состоянии focus', () => {
        const onFocus = jest.fn();
        const { container } = render(<Input label="Фамилия" onFocus={onFocus} />);
        const input = screen.getByLabelText(/Фамилия/i);
        userEvent.click(input);
        expect(input).toHaveFocus();
        expect(onFocus).toHaveBeenCalledTimes(1);
        expect(onFocus).toHaveBeenCalledWith(expect.any(Object));
        const elem = container.querySelector('.Input__LabelText_focus');
        expect(elem).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Input в состоянии blur', () => {
        const onBlur = jest.fn();
        const { container } = render(<Input label="Фамилия" onBlur={onBlur} />);
        const input = screen.getByLabelText(/Фамилия/i);
        input.focus();
        userEvent.tab();
        const elem = container.querySelector('.Input__LabelText_focus');
        expect(elem).not.toBeInTheDocument();

        expect(onBlur).toHaveBeenCalledTimes(1);
        expect(onBlur).toHaveBeenCalledWith(expect.any(Object));
    });

    it('Должен отобразиться Input c использованием ref', () => {
        const onFocus = jest.fn();
        const refFocus = React.createRef();
        render(<Input ref={refFocus} onFocus={onFocus} />);
        refFocus.current.focus();
        expect(onFocus).toHaveBeenCalledTimes(1);
        expect(onFocus).toHaveBeenCalledWith(expect.any(Object));
    });

    it('передали mask, ref, onFocus, сделали фокус -> вызван метод onFocus', () => {
        const onFocus = jest.fn();
        const inputRefFocus = React.createRef();
        render(<Input mask="+7 (999) 999-99-99" ref={inputRefFocus} onFocus={onFocus} />);
        inputRefFocus.current.focus();
        expect(onFocus).toHaveBeenCalledTimes(1);
        expect(onFocus).toHaveBeenCalledWith(expect.any(Object));
    });

    it('Должен отобразиться Input с типом email', () => {
        const { container } = render(<Input type="email" />);
        const input = container.querySelector('input');
        expect(input.type).toBe('email');
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Input с name="Surname" ', () => {
        const { container } = render(<Input name="Surname" />);
        const input = container.querySelector('input');
        expect(input.name).toBe('Surname');
        expect(input).toMatchSnapshot();
    });
});
