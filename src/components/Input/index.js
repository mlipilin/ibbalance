import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import InputMask from 'react-input-mask';
// Components
import FeatherIcon from '../FeatherIcon';
// Constants
import { SIZES } from '../../constants/props';

import { withTheme } from '../../theme-provider';
import { hasNodeInTree } from '../../helpers/dom';

@withTheme
class Input extends Component {
    constructor(props) {
        super(props);
        const { type } = this.props;
        this.state = { hasFocus: false, isErrorDetailOpen: false, type };
    }

    componentDidMount() {
        if (this.target) {
            this.init();
        }
    }
    /* eslint-disable */
    componentDidUpdate(prevProps, prevState, snapshot) {
        const { type } = this.props;
        if (prevProps.type !== type) {
            this.setState({ type });
        }
    }

    /* eslint-enable */
    componentWillUnmount() {
        if (this.target) {
            this.target.removeEventListener('click', this.handleTargetClickEvent);
        }

        window.removeEventListener('click', this.handleWindowClickEvent);
    }

    handleTargetClickEvent = e => {
        e.preventDefault();

        this.setState(prevState => ({ isErrorDetailOpen: !prevState.isErrorDetailOpen }));
    };

    handleWindowClickEvent = e => {
        const isClickOnTarget = hasNodeInTree(e.target, this.target);
        const isClickOnTooltip = hasNodeInTree(e.target, this.tooltip);

        if (!isClickOnTooltip && !isClickOnTarget) {
            this.setState({ isErrorDetailOpen: false });
        }
    };

    handleClickPassword = () => {
        this.setState(prevState => {
            const { type } = prevState;
            const newType = type === 'password' ? 'text' : 'password';
            return { type: newType };
        });
    };

    init = () => {
        this.target.addEventListener('click', this.handleTargetClickEvent);
        window.addEventListener('click', this.handleWindowClickEvent);
    };

    // Public methods
    blur = () => {
        this.input.blur();
    };

    focus = () => {
        this.input.focus();
    };

    // Event handlers
    handleBlur = () => {
        const { onBlur } = this.props;
        this.setState({ hasFocus: false }, () => {
            onBlur(this.props);
        });
    };

    handleChange = e => {
        const { onChange, parseValue } = this.props;
        onChange(parseValue(e.target.value), this.props);
    };

    handleFocus = () => {
        const { onFocus } = this.props;
        this.setState({ hasFocus: true }, () => {
            onFocus(this.props);
        });
    };

    render() {
        const {
            className,
            disabled,
            error,
            errorDetail,
            errorDetailIcon,
            field,
            label,
            mask,
            placeholder,
            readOnly,
            size,
            success,
            type,
            value,
            applyTheme,
            formatValue,
            parseValue,
            ...otherProps
        } = this.props;

        const { hasFocus } = this.state;

        const componentClass = applyTheme(
            cn('Input', `Input_size_${size}`, {
                Input_disabled: disabled,
            }),
            className,
        );

        const labelClass = applyTheme(cn('Input__Label'));

        const labelTextClass = applyTheme(
            cn('Input__LabelText', `Input__LabelText_size_${size}`, {
                'Input__LabelText_place-top':
                    (!readOnly && hasFocus) || !!value || value === 0 || !!placeholder,
                Input__LabelText_focus: hasFocus,
            }),
        );

        const inputClass = applyTheme(
            cn('Input__Input', `Input__Input_size_${size}`, {
                Input__Input_error: !!error || !!errorDetail,
                Input__Input_readonly: readOnly,
                Input__Input_success: !error && success,
                Input__Input_password: type === 'password',
                'Input__Input_with-label': !!label,
            }),
        );

        const passwordIconClass = applyTheme(
            cn('Input__PasswordIcon', `Input__PasswordIcon_size_${size}`),
        );

        const disabledIconClass = applyTheme(
            cn('Input__DisabledIcon', `Input__DisabledIcon_size_${size}`),
        );

        const errorClass = applyTheme(cn('Input__Error', `Input__Error_size_${size}`));

        const errorDetailClass = applyTheme(
            cn('Input__ErrorDetail', `Input__ErrorDetail_size_${size}`, {
                Input__ErrorDetail_visible: !!errorDetail,
            }),
        );
        const errorIconClass = applyTheme(cn('Input__ErrorIcon', `Input__ErrorIcon_size_${size}`));
        const { type: typeState, isErrorDetailOpen } = this.state;
        const tooltipClass = applyTheme(
            cn('Input__Tooltip', `Input__Tooltip_size_${size}`, {
                Input__Tooltip_visible: isErrorDetailOpen,
            }),
        );

        let inputProps = {
            ...otherProps,
            className: inputClass,
            disabled,
            placeholder,
            readOnly,
            ref: null,
            type: typeState,
            value: formatValue(value === null ? '' : value),
            onBlur: this.handleBlur,
            onChange: this.handleChange,
            onFocus: this.handleFocus,
        };
        if (mask) {
            inputProps = {
                ...inputProps,
                inputRef: el => {
                    this.input = el;
                    return this.input;
                },
                mask,
                maskChar: null,
            };
        } else {
            inputProps = {
                ...inputProps,
                ref: el => {
                    this.input = el;
                    return this.input;
                },
            };
        }

        return (
            <div className={componentClass}>
                {/* eslint-disable */}
                <label className={labelClass}>
                    {/* Label */}
                    {!!label && <span className={labelTextClass}>{label}</span>}

                    {/* Input */}
                    {!mask && <input {...inputProps} />}
                    {!!mask && <InputMask {...inputProps} />}

                    {/* Password icon */}
                    {type === 'password' && (
                        <div className={passwordIconClass} onClick={this.handleClickPassword}>
                            <FeatherIcon name={typeState === 'password' ? 'eye-off' : 'eye'} />
                        </div>
                    )}

                    {/* Disabled icon */}
                    {disabled && (
                        <div className={disabledIconClass}>
                            <FeatherIcon name="lock" />
                        </div>
                    )}
                </label>

                {/* eslint-enable */}
                {/* Error */}
                {!!error && <div className={errorClass}>{error}</div>}

                <div className={errorDetailClass}>
                    <div
                        className={errorIconClass}
                        ref={el => {
                            this.target = el;
                            return this.target;
                        }}
                    >
                        {errorDetailIcon}
                    </div>
                    <div
                        className={tooltipClass}
                        ref={el => {
                            this.tooltip = el;
                            return this.tooltip;
                        }}
                    >
                        {errorDetail}
                    </div>
                </div>
            </div>
        );
    }
}

Input.propTypes = {
    className: PropTypes.string,
    disabled: PropTypes.bool,
    error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool, PropTypes.object]),
    errorDetail: PropTypes.string,
    errorDetailIcon: PropTypes.node,
    field: PropTypes.string,
    label: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
        PropTypes.string,
    ]),
    mask: PropTypes.string,
    placeholder: PropTypes.string,
    readOnly: PropTypes.bool,
    size: PropTypes.oneOf([SIZES.S, SIZES.M, SIZES.L]),
    success: PropTypes.bool,
    type: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    applyTheme: PropTypes.func,
    formatValue: PropTypes.func,
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    parseValue: PropTypes.func,
};

Input.defaultProps = {
    className: '',
    disabled: false,
    error: null,
    errorDetail: null,
    errorDetailIcon: <FeatherIcon name="alert-circle" />,
    field: null,
    label: null,
    mask: null,
    placeholder: '',
    readOnly: false,
    size: SIZES.M,
    success: false,
    type: null,
    value: null,
    applyTheme: _ => _,
    formatValue: value => value,
    onBlur: props => {}, // eslint-disable-line no-unused-vars
    onChange: (value, props) => {}, // eslint-disable-line no-unused-vars
    onFocus: props => {}, // eslint-disable-line no-unused-vars
    parseValue: value => value,
};

export default Input;
