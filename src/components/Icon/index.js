import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withTheme } from '../../theme-provider';
import MaterialIcon from '../MaterialIcon';
import FeatherIcon from '../FeatherIcon';
import NAMES_FEATER_ICON from '../FeatherIcon/constants';
import NAMES_MATERIAL_ICON from '../MaterialIcon/constants';

@withTheme
class Icon extends Component {
    render() {
        const { name, type } = this.props;

        if (!name) return null;

        return type === 'material' ? <MaterialIcon name={name} /> : <FeatherIcon name={name} />;
    }
}

Icon.propTypes = {
    name: PropTypes.oneOf([...NAMES_FEATER_ICON, ...NAMES_MATERIAL_ICON]),
    type: PropTypes.oneOf(['material', 'feather']),
    applyTheme: PropTypes.func, //eslint-disable-line
};

Icon.defaultProps = {
    name: null,
    type: 'feather',
    applyTheme: _ => _, //eslint-disable-line
};

export default Icon;
