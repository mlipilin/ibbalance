## Usage

```jsx
<Icon name="phone" />
```

## Props

Prop name | Prop type | Default value
--- | --- | ---
*name*|one of icon names|`null`
*type*|one of type icon|`feather`

## CSS Selectors

```css
.Icon {}
.Icon > svg {}
```
