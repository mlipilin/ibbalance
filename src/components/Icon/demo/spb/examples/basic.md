# Базовое использование

В свойстве `name` нужно передать название иконки, в свойство `type` тип иконки

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Icon, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/spb/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Icon name="house" type="material"/>
  </ThemeProvider>,
  document.getElementById('root')
);
```
