import React from 'react';
import { storiesOf } from '@storybook/react';

import notes from '../readme.md';
import Icon from '../index';

const stories = storiesOf('Icon', module);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories.add('Default', () => <Icon name="house" />, { notes });
