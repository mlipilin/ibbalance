import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean, select } from '@storybook/addon-knobs';

// Components
import FeatherIcon from '../../FeatherIcon';

// Constants
import { LOCAL_STORAGE_THEME_KEY } from '../../../constants/local-storage';
import { SIZES, TYPES } from '../../../constants/props';
import { THEMES } from '../../../constants/themes';

import Button from '../index';

import notes from '../readme.md';

const theme = localStorage.getItem(LOCAL_STORAGE_THEME_KEY);

let sizesList = [];
let typesList = [];

switch (theme) {
    case THEMES.DEFAULT:
        sizesList = [SIZES.XS, SIZES.S, SIZES.M, SIZES.L, SIZES.XL, SIZES.XXL];
        typesList = Object.values(TYPES);
        break;
    case THEMES.ALFA:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        typesList = [TYPES.DEFAULT, TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    case THEMES.ABSOLUT:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        typesList = [TYPES.DEFAULT, TYPES.PRIMARY, TYPES.SECONDARY, TYPES.INFO];
        break;
    case THEMES.OZON:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        typesList = [TYPES.DEFAULT, TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    case THEMES.SBERMARKET:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        typesList = [TYPES.DEFAULT, TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    case THEMES.SPB:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        typesList = [TYPES.DEFAULT, TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    case THEMES.TKB:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        typesList = [TYPES.DEFAULT, TYPES.PRIMARY, TYPES.SECONDARY, TYPES.CIRCLE];
        break;
    case THEMES.TKB_LKZ:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        typesList = [TYPES.DEFAULT, TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    case THEMES.VKUSVILL:
        sizesList = [SIZES.XS, SIZES.S, SIZES.M, SIZES.L];
        typesList = [TYPES.DEFAULT, TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    case THEMES.KYC:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        typesList = [TYPES.DEFAULT, TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    case THEMES.KYC_LANDING:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        typesList = [TYPES.DEFAULT, TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    default:
        break;
}

const stories = storiesOf('Button');

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories
    .add(
        'Default',
        () => (
            <Button
                block={boolean('Block', false)}
                disabled={boolean('Disabled', false)}
                processing={boolean('Processing', false)}
                size={select('Size', sizesList, SIZES.M)}
                type={select('Type', typesList, TYPES.DEFAULT)}
            >
                Button
            </Button>
        ),
        { notes },
    )
    .add(
        'Left icon',
        () => (
            <Button
                block={boolean('Block', false)}
                disabled={boolean('Disabled', false)}
                iconLeft={<FeatherIcon name="arrow-left" />}
                processing={boolean('Processing', false)}
                size={select('Size', sizesList, SIZES.M)}
                type={select('Type', typesList, TYPES.DEFAULT)}
            >
                Button
            </Button>
        ),
        { notes },
    )
    .add(
        'Right icon',
        () => (
            <Button
                block={boolean('Block', false)}
                disabled={boolean('Disabled', false)}
                iconRight={<FeatherIcon name="arrow-right" />}
                processing={boolean('Processing', false)}
                size={select('Size', sizesList, SIZES.M)}
                type={select('Type', typesList, TYPES.DEFAULT)}
            >
                Button
            </Button>
        ),
        { notes },
    )
    .add(
        'Icon',
        () => (
            <Button
                block={boolean('Block', false)}
                disabled={boolean('Disabled', false)}
                processing={boolean('Processing', false)}
                size={select('Size', sizesList, SIZES.M)}
                type={select('Type', typesList, TYPES.CIRCLE)}
            >
                <FeatherIcon name="check-circle" />
            </Button>
        ),
        { notes },
    );
