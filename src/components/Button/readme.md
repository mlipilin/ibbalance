## Usage

```jsx
<Button block disabled processing size="xl" type="primary">
    Primary Button
</Button>
```

With left icon:

```jsx
<Button iconLeft={<Icon name="arrow-left" />}>Prev</Button>
```

With right icon:

```jsx
<Button iconLeft={<Icon name="arrow-right" />}>Next</Button>
```
## Props

Prop name | Prop type | Default value
--- | --- | ---
*block*|bool|`false`
*htmlType*|string|`null`
*disabled*|bool|`false`
*iconLeft*|object|`null`
*iconRight*|object|`null`
*processing*|bool|`false`
*size*|`xs`, `s`, `m`, `l`, `xl`, `xxl`|`m`
*type*|`default`, `primary`, `secondary`, `danger`, `success`, `info`, `warning`|`default`

## CSS Selectors

```css
.Button {}

/* Size */
.Button_size_xs {}
.Button_size_s {}
.Button_size_m {}
.Button_size_l {}
.Button_size_xl {}
.Button_size_xxl {}

/* Type */
.Button_type_default {}
.Button_type_primary {}
.Button_type_secondary {}
.Button_type_danger {}
.Button_type_success {}
.Button_type_info {}
.Button_type_warning {}

/* Booleans */
.Button_block {}
.Button_disabled {}
.Button_processing {}

/* Container */
.Button__Container {}

/* Content */
.Button__Content {}

/* IconLeft */
.Button__IconLeft {}

/* IconRight */
.Button__IconRight {}

/* Spin */
.Button__Spin {}
```
