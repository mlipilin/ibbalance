# Иконки в кнопке

`iconLeft` - иконка в левой части кнопки

`iconRight` - иконка в правой части кнопки

```jsx
<Button iconLeft={<FeatherIcon name="chevron-left" />}>
    Кнопка с иконкой слева
</Button>
<Button iconRight={<FeatherIcon name="chevron-right" />}>
    Кнопка с иконкой справа
</Button>
```
