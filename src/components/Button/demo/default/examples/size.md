# Размер кнопки

Свойство size может принимать одно из следующих значений: `xs`, `s`, `m`, `l`, `xl` и `xxl`

```jsx
<Button size="xxl">Button XXL</Button>
<Button size="xxl">
    <FeatherIcon name="github" />
</Button>
<br />
<Button size="xl">Button XL</Button>
<Button size="xl">
    <FeatherIcon name="github" />
</Button>
<br />
<Button size="l">Button L</Button>
<Button size="l">
    <FeatherIcon name="github" />
</Button>
<br />
<Button size="m">Button M</Button>
<Button size="m">
    <FeatherIcon name="github" />
</Button>
<br />
<Button size="s">Button S</Button>
<Button size="s">
    <FeatherIcon name="github" />
</Button>
<br />
<Button size="xs">Button XS</Button>
<Button size="xs">
    <FeatherIcon name="github" />
</Button>
```
