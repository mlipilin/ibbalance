# Тип кнопки

Свойство type может принимать одно из следующих значений: `default`, `primary`, `secondary`, `info`, `danger`, `success` и `warning`

```jsx
<Button type="default">Default button</Button>
<Button type="primary">Primary button</Button>
<Button type="secondary">Secondary button</Button>
<Button type="info">Info button</Button>
<Button type="danger">Danger button</Button>
<Button type="success">Success button</Button>
<Button type="warning">Warning button</Button>
```
