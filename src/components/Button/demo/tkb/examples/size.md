# Размер кнопки

Свойство size может принимать одно из следующих значений: `s`, `m`, `l`

```jsx
<Button size="l">Button L</Button>
<Button size="l">
    <FeatherIcon name="github" />
</Button>
<br />
<Button size="m">Button M</Button>
<Button size="m">
    <FeatherIcon name="github" />
</Button>
<br />
<Button size="s">Button S</Button>
<Button size="s">
    <FeatherIcon name="github" />
</Button>
```
