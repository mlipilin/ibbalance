# "disabled" состояние

`true`: кнопка становится 'disabled' (выключенной)

```jsx
<Button disabled>Disabled кнопка</Button>
```
