import React from 'react';

// Components
import Button from '../../index';
import FeatherIcon from '../../../FeatherIcon';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';

// Examples
import block from './examples/block.md';
import disabled from './examples/disabled.md';
import icons from './examples/icons.md';
import processing from './examples/processing.md';
import size from './examples/size.md';
import type from './examples/type.md';

// Wrappers
import { Col, Row } from '../../../../wrappers/WGrid';

const DemoButton = () => {
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>

            <div id="example">
                <Row
                    cols={{ mobile: [1], tab: [1, 1], desktop: [1], largeDesktop: [1, 1] }}
                    gap="20px"
                >
                    <Col>
                        <Example readme={type}>
                            <Button type="default">Default button</Button>
                            <Button type="primary">Primary button</Button>
                            <Button type="secondary">Secondary button</Button>
                        </Example>
                        <Example readme={icons}>
                            <Button iconLeft={<FeatherIcon name="chevron-left" />}>
                                Кнопка с иконкой слева
                            </Button>
                            <Button iconRight={<FeatherIcon name="chevron-right" />}>
                                Кнопка с иконкой справа
                            </Button>
                        </Example>
                        <Example readme={disabled}>
                            <Button disabled>Disabled кнопка</Button>
                        </Example>
                    </Col>
                    <Col>
                        <Example readme={size}>
                            <Button size="l">Button L</Button>
                            <Button size="l">
                                <FeatherIcon name="github" />
                            </Button>
                            <br />
                            <Button size="m">Button M</Button>
                            <Button size="m">
                                <FeatherIcon name="github" />
                            </Button>
                            <br />
                            <Button size="s">Button S</Button>
                            <Button size="s">
                                <FeatherIcon name="github" />
                            </Button>
                        </Example>
                        <Example readme={block}>
                            <Button block>Блочная кнопка</Button>
                        </Example>
                        <Example readme={processing}>
                            <Button processing>Кнопка с индикатором процесса</Button>
                        </Example>
                    </Col>
                </Row>
            </div>
        </DemoComponent>
    );
};

export default DemoButton;
