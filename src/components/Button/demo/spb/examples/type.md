# Тип кнопки

Свойство type может принимать одно из следующих значений: `default`, `primary`, `secondary`, `circle`

```jsx
<Button type="default">Default button</Button>
<Button type="primary">Primary button</Button>
<Button type="secondary">Secondary button</Button>
<Button type="circle">
  <FeatherIcon name="github" />
</Button>
```
