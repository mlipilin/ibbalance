# Тип кнопки

Свойство type может принимать одно из следующих значений: `default`, `primary`, `secondary` и `info`

```jsx
<Button type="default">Default button</Button>
<Button type="primary">Primary button</Button>
<Button type="secondary">Secondary button</Button>
<Button type="info">Info button</Button>
```
