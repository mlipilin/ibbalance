# Button

Простая реализация кнопки

## Когда использовать

Всякий раз, когда на странице нужен "кликабельный" элемент 👆

## Примеры использования

## Свойства

Название | Описание | Тип | Значение по умолчанию
--- | --- | --- | ---
**block**|Является ли кнопка блочным элементом|*bool*|`false`
**disabled**|`Disabled` состояние для кнопки|*bool*|`false`
**htmlType**|свойство `type` обычной HTML кнопки|*string*|`null`
**iconLeft**|Иконка (компонент), которая будет расположена в левой части кнопки|*object*|`null`
**iconRight**|Иконка (компонент), которая будет расположена в правой части кнопки|*object*|`null`
**processing**|Индикатор запущенного процесса после нажатиия кнопки (пока `true`, кнопка не будет нажиматься)|*bool*|`false`
**size**|Размер кнопки|Один из: `s`, `m`, `l`|`m`
**type**|Тип (внешний вид) кнопки|Один из: `default`, `primary`, `secondary`, `info`|`default`
**onClick**|Обработчик события "click"|*func*|`(event, props) => {}`
