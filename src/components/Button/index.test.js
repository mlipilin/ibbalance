import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import Button from './index';
import FeatherIcon from '../FeatherIcon';

import { render } from '../../hoc/with-theme';

describe('Тестирование комопнента Button', () => {
    it('Должена отобразиться кнопка Button без класса, без контекта', () => {
        render(<Button />);
        expect(screen.getByRole('button')).toBeInTheDocument();
        expect(screen.getByRole('button')).toHaveAttribute('type');
        expect(screen.getByRole('button')).toMatchSnapshot();
    });

    it('Должена отобразиться кнопка Button с заданным классом', () => {
        render(<Button className="Container__Button" />);
        expect(screen.getByRole('button')).toBeInTheDocument();
        expect(screen.getByRole('button')).toHaveClass('Container__Button');
        expect(screen.getByRole('button')).toMatchSnapshot(`
            <button class="" type="button"></button>
        `);
    });

    it('Должена отобразиться кнопка Button с htmlType = submit', () => {
        const { container } = render(<Button htmlType="submit" />);
        expect(screen.getByRole('button')).toBeInTheDocument();
        const type = container.firstChild.getAttribute('type');
        expect(type).toBe('submit');
        expect(screen.getByRole('button')).toMatchSnapshot();
    });

    it('Должена отобразиться кнопка Button с текстом "Отправить"', () => {
        render(<Button>Отправить</Button>);
        expect(screen.getByText('Отправить')).toBeInTheDocument();
        expect(screen.getByRole('button')).toMatchSnapshot();
    });

    it('Должена отобразиться кнопка Button с иконкой github', () => {
        const { container } = render(
            <Button>
                <FeatherIcon name="github" />
            </Button>,
        );
        const svgElement = container.getElementsByTagName('svg')[0];
        expect(svgElement).toHaveClass('feather-github');
        expect(screen.getByRole('button')).toMatchSnapshot();
    });

    it('Должена отобразиться кнопка Button с иконкой - компонентом слева и текстом', () => {
        const { container } = render(
            <Button iconLeft={<FeatherIcon name="chevron-left" />}>Кнопка с иконкой слева</Button>,
        );
        expect(screen.getByText('Кнопка с иконкой слева')).toBeInTheDocument();
        const svgElement = container.getElementsByTagName('svg')[0];
        expect(svgElement).toHaveClass('feather-chevron-left');

        const contentElem = container.querySelector('.Button__IconLeft + .Button__Content');
        expect(contentElem).toBeInTheDocument();
        expect(screen.getByRole('button')).toMatchSnapshot();
    });
    it('Должена отобразиться кнопка Button с иконкой - компонентом справа и текстом', () => {
        const { container } = render(
            <Button iconRight={<FeatherIcon name="chevron-right" />}>
                Кнопка с иконкой справа
            </Button>,
        );
        expect(screen.getByText('Кнопка с иконкой справа')).toBeInTheDocument();
        const svgElement = container.getElementsByTagName('svg')[0];
        expect(svgElement).toHaveClass('feather-chevron-right');
        const iconElem = container.querySelector('.Button__Content + .Button__IconRight');
        expect(iconElem).toBeInTheDocument();
        expect(screen.getByRole('button')).toMatchSnapshot();
    });

    it('Должена отобразиться кнопка Button с текстом и иконкой ', () => {
        const { container } = render(
            <Button>
                <FeatherIcon name="github" />
            </Button>,
        );
        const svgElement = container.getElementsByTagName('svg')[0];
        expect(svgElement).toHaveClass('feather-github');
        expect(screen.getByRole('button')).toMatchSnapshot();
    });

    it('Должена отобразиться кнопка Button в состоянии disabled ', () => {
        const { container } = render(<Button disabled>Disabled</Button>);
        expect(container.firstChild).toBeDisabled();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должена отобразиться кнопка Button в состоянии processing', () => {
        const { container } = render(<Button processing>Processing</Button>);
        expect(container.firstChild).toMatchSnapshot();
        const classes = container.getElementsByTagName('button')[0].className;
        expect(classes).toMatch(/Button_processing/);

        const spin = container.querySelector('.Spin');
        expect(spin).toBeInTheDocument();
    });

    it('Должна отобразиться кнопка Button в состоянии фокуса', () => {
        const { container } = render(<Button />);
        container.firstChild.focus();
        expect(container.firstChild).toHaveFocus();
    });

    it('Должна отобразиться кнопка Button в обычном состоянии после того, как убрали фокус', () => {
        const { container } = render(<Button />);
        container.firstChild.focus();
        expect(container.firstChild).toHaveFocus();
        userEvent.tab();
        expect(container.firstChild).not.toHaveFocus();
    });

    it('Должна отобразиться кнопка Button в активном состоянии', () => {
        const handleClick = jest.fn();
        const { container } = render(<Button onClick={handleClick} />);
        userEvent.click(container.firstChild);
        expect(handleClick).toHaveBeenCalledTimes(1);
        expect(handleClick).toHaveBeenCalledWith(expect.any(Object), expect.any(Object));
    });

    it('Должена отобразиться кнопка Button с type = "default" по-умолчанию', () => {
        render(<Button />);
        const button = screen.getByRole('button');
        expect(button).toHaveClass('Button_type_default');
        expect(button).toMatchSnapshot();
    });

    describe('передали type', () => {
        it('type="default" -> есть класс Button_type_default', () => {
            render(<Button type="default" />);
            const button = screen.getByRole('button');
            expect(button).toHaveClass('Button_type_default');
            expect(button).toMatchSnapshot();
        });

        it('type="primary" -> есть класс Button_type_primary', () => {
            render(<Button type="primary" />);
            const button = screen.getByRole('button');
            expect(button).toHaveClass('Button_type_primary');
            expect(button).toMatchSnapshot();
        });

        it('type="secondary" -> есть класс Button_type_secondary', () => {
            render(<Button type="secondary" />);
            const button = screen.getByRole('button');
            expect(button).toHaveClass('Button_type_secondary');
            expect(button).toMatchSnapshot();
        });

        it('type="info" -> есть класс Button_type_info', () => {
            render(<Button type="info" />);
            const button = screen.getByRole('button');
            expect(button).toHaveClass('Button_type_info');
            expect(button).toMatchSnapshot();
        });

        it('type="danger" -> есть класс Button_type_danger', () => {
            render(<Button type="danger" />);
            const button = screen.getByRole('button');
            expect(button).toHaveClass('Button_type_danger');
            expect(button).toMatchSnapshot();
        });

        it('type="success" -> есть класс Button_type_success', () => {
            render(<Button type="success" />);
            const button = screen.getByRole('button');
            expect(button).toHaveClass('Button_type_success');
            expect(button).toMatchSnapshot();
        });

        it('type="warning" -> есть класс Button_type_warning', () => {
            render(<Button type="warning" />);
            const button = screen.getByRole('button');
            expect(button).toHaveClass('Button_type_warning');
            expect(button).toMatchSnapshot();
        });

        it('type=circle -> есть класс Button_type_circle', () => {
            render(<Button type="circle" />);
            const button = screen.getByRole('button');
            expect(button).toHaveClass('Button_type_circle');
        });
    });

    it('Должена отобразиться кнопка Button с size = "m" по-умолчанию', () => {
        const { container } = render(<Button className="Container__Button" />);
        const classes = container.getElementsByClassName('Container__Button')[0].className;
        expect(classes).toMatch(/Button_size_m/);
        expect(screen.getByRole('button')).toMatchSnapshot();
    });

    it('Должена отобразиться кнопка Button с size = "xs"', () => {
        const { container } = render(<Button className="Container__Button" size="xs" />);
        const classes = container.getElementsByClassName('Container__Button')[0].className;
        expect(classes).toMatch(/Button_size_xs/);
        expect(screen.getByRole('button')).toMatchSnapshot();
    });

    it('Должена отобразиться кнопка Button с size = "s"', () => {
        const { container } = render(<Button className="Container__Button" size="s" />);
        const classes = container.getElementsByClassName('Container__Button')[0].className;
        expect(classes).toMatch(/Button_size_s/);
        expect(screen.getByRole('button')).toMatchSnapshot();
    });

    it('Должена отобразиться кнопка Button с size = "m"', () => {
        const { container } = render(<Button className="Container__Button" size="m" />);
        const classes = container.getElementsByClassName('Container__Button')[0].className;
        expect(classes).toMatch(/Button_size_m/);
        expect(screen.getByRole('button')).toMatchSnapshot();
    });

    it('Должена отобразиться кнопка Button с size = "l"', () => {
        const { container } = render(<Button className="Container__Button" size="l" />);
        const classes = container.getElementsByClassName('Container__Button')[0].className;
        expect(classes).toMatch(/Button_size_l/);
        expect(screen.getByRole('button')).toMatchSnapshot();
    });

    it('Должена отобразиться кнопка Button с size = "xl"', () => {
        const { container } = render(<Button className="Container__Button" size="xl" />);
        const classes = container.getElementsByClassName('Container__Button')[0].className;
        expect(classes).toMatch(/Button_size_xl/);
        expect(screen.getByRole('button')).toMatchSnapshot();
    });

    it('Должена отобразиться кнопка Button с size = "xxl"', () => {
        const { container } = render(<Button className="Container__Button" size="xxl" />);
        const classes = container.getElementsByClassName('Container__Button')[0].className;
        expect(classes).toMatch(/Button_size_xxl/);
        expect(screen.getByRole('button')).toMatchSnapshot();
    });

    it('Должена отобразиться кнопка Button с htmlType = submit', () => {
        const { container } = render(<Button htmlType="submit" />);
        expect(screen.getByRole('button')).toBeInTheDocument();
        const type = container.firstChild.getAttribute('type');
        expect(type).toBe('submit');
        expect(screen.getByRole('button')).toMatchSnapshot();
    });

    it('Должна отобразиться кнопка Button со свойством name = "nameForButton"', () => {
        const { container } = render(<Button name="nameForButton" />);
        expect(screen.getByRole('button')).toBeInTheDocument();
        const name = container.firstChild.getAttribute('name');
        expect(name).toBe('nameForButton');
        expect(screen.getByRole('button')).toMatchSnapshot();
    });

    it('Должна отобразиться кнопка Button со свойством autofocus', () => {
        render(<Button name="nameForButton" autoFocus />);
        expect(screen.getByRole('button')).toBeInTheDocument();
        expect(screen.getByRole('button')).toHaveFocus();
        expect(screen.getByRole('button')).toMatchSnapshot();
    });

    it('Должна отобразиться кнопка Button в состоянии disabled после click пользователя', () => {
        const onClick = jest.fn();
        const { container } = render(<Button onClick={onClick} disabled />);
        userEvent.click(container.firstChild);
        expect(onClick).toHaveBeenCalledTimes(0);
    });

    it('Должна отобразиться кнопка Button в состоянии processing после click пользователя', () => {
        const onClick = jest.fn();
        const { container } = render(<Button onClick={onClick} processing />);
        userEvent.click(container.firstChild);
        expect(onClick).toHaveBeenCalledTimes(0);
    });

    it('Должна отобразиться кнопка Button со свойством block', () => {
        const { container } = render(<Button block />);
        const button = container.querySelector('button');
        expect(button.classList.contains('Button_block')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должна отобразиться кнопка Button с content="Название"', () => {
        const { container } = render(
            <Button>
                <div className="Button__ContentName">Название</div>
            </Button>,
        );
        const button = container.querySelector('button');
        const content = button.querySelector('.Button__ContentName');
        expect(content.textContent).toBe('Название');
        expect(container.firstChild).toMatchSnapshot();
    });
});

export default render;
