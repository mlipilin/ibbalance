import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
// Components
import Spin from '../Spin';
// Constants
import { SIZES, TYPES } from '../../constants/props';

import { withTheme } from '../../theme-provider';

@withTheme
class Button extends Component {
    handleClick = e => {
        const { disabled, processing, onClick } = this.props;
        if (disabled || processing) {
            e.preventDefault();
        } else if (typeof onClick === 'function') {
            onClick(e, this.props);
        }
    };

    render() {
        const {
            block,
            className,
            disabled,
            htmlType,
            iconLeft,
            iconRight,
            processing,
            size,
            type,
            applyTheme,
            children,
            ...otherProps
        } = this.props;

        const componentClass = applyTheme(
            cn(
                'Button',
                {
                    Button_block: block,
                    Button_disabled: disabled,
                    Button_processing: processing,
                },
                `Button_size_${size}`,
                `Button_type_${type}`,
            ),
            className,
        );
        const containerClass = applyTheme(
            cn('Button__Container', {
                'Button__Container_no-icons': !iconLeft && !iconRight,
            }),
        );
        const contentClass = applyTheme(cn('Button__Content'));
        const iconLeftClass = applyTheme(cn('Button__IconLeft'));
        const iconRightClass = applyTheme(cn('Button__IconRight'));
        const spinClass = applyTheme(cn('Button__Spin'));

        let buttonProps = { disabled, ...otherProps, onClick: this.handleClick };
        if (htmlType) {
            buttonProps = {
                ...buttonProps,
                type: htmlType,
            };
        }
        /* eslint-disable */
        return (
            <button type={htmlType || 'button'} className={componentClass} {...buttonProps}>
                {/* eslint-enable */}
                <div className={containerClass}>
                    {/* Icon left */}
                    {!!iconLeft && <div className={iconLeftClass}>{iconLeft}</div>}

                    {/* Content */}
                    <div className={contentClass}>{children}</div>

                    {/* Icon right */}
                    {!!iconRight && <div className={iconRightClass}>{iconRight}</div>}

                    {/* Spin */}
                    {processing && (
                        <div className={spinClass}>
                            <Spin size={size} />
                        </div>
                    )}
                </div>
            </button>
        );
    }
}

Button.propTypes = {
    block: PropTypes.bool,
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    className: PropTypes.string,
    disabled: PropTypes.bool,
    htmlType: PropTypes.string,
    iconLeft: PropTypes.node,
    iconRight: PropTypes.node,
    processing: PropTypes.bool,
    size: PropTypes.oneOf(Object.values(SIZES)),
    type: PropTypes.oneOf(Object.values(TYPES)),
    applyTheme: PropTypes.func,
    onClick: PropTypes.func,
};

Button.defaultProps = {
    block: false,
    children: null,
    className: '',
    disabled: false,
    htmlType: null,
    iconLeft: null,
    iconRight: null,
    processing: false,
    size: SIZES.M,
    type: TYPES.DEFAULT,
    applyTheme: _ => _,
    onClick: (event, props) => {}, // eslint-disable-line no-unused-vars
};

export default Button;
