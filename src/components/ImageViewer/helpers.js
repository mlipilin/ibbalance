export const getElementTranslate = el => {
    const translateStyle = el.style.transform
        ? el.style.transform.match(/translate[\s\S]*?\)/)
        : null;

    if (translateStyle && translateStyle.length) {
        const [translateX, translateY] = translateStyle[0]
            .replace('translate(', '')
            .replace(')', '')
            .split(', ')
            .map(parseFloat);
        return { translateX, translateY };
    }

    return { translateX: 0, translateY: 0 };
};

export const getElementRotate = el => {
    const rotateStyle = el.style.transform ? el.style.transform.match(/rotate[\s\S]*?\)/) : null;

    if (rotateStyle && rotateStyle.length) {
        return parseFloat(rotateStyle[0].replace('rotate(', '').replace('deg)', ''));
    }

    return 0;
};

export const getElementScale = el => {
    const scaleStyle = el.style.transform ? el.style.transform.match(/scale[\s\S]*?\)/) : null;

    if (scaleStyle && scaleStyle.length) {
        return parseFloat(scaleStyle[0].replace('scale(', '').replace(')', ''));
    }

    return 1;
};

export const getImagePoint = (image, e) => {
    const boundingClientRect = image.getBoundingClientRect();
    const scale = getElementScale(image);
    return {
        x: (e.clientX - boundingClientRect.left) / scale,
        y: (e.clientY - boundingClientRect.top) / scale,
    };
};
