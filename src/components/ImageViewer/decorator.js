import React from 'react';

// Contexts
import { Consumer } from './context';

export default Component => props => {
    return (
        <Consumer>
            {({ imageViewer }) => <Component {...props} imageViewer={imageViewer} />}
        </Consumer>
    );
};
