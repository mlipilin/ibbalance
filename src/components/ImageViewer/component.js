/* eslint-disable no-param-reassign */
/* eslint-disable react/no-did-update-set-state */
/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable no-return-assign */
/* eslint-disable jsx-a11y/mouse-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';

// iBalance
import FeatherIcon from '../FeatherIcon';
import Spin from '../Spin';

// Constants
import {
    ARROW_LEFT,
    ARROW_RIGHT,
    IMAGE_MARGIN,
    ROTATE_STEP,
    SCALE_MAX,
    SCALE_MIN,
    SCALE_STEP,
} from './constants';

// Helpers
import { getElementRotate, getElementScale, getElementTranslate, getImagePoint } from './helpers';

import { withTheme } from '../../theme-provider';

const DEFAULT_STATE = {
    currentIndex: null,
    images: [],
    isOpen: false,
    isShowImageArrows: true,
    isShowImageCloseButton: true,
    isShowImageRotator: true,
    isShowLoader: false,
};

export const DEFAULT_IMAGE =
    "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 640 480'%3E%3Cdefs/%3E%3Cpath fill='%23fff' d='M0 0h640v480H0z'/%3E%3Cpath fill='%23CFCFCF' d='M131.766 220.98h3.252V240h-3.252v-13.869L122.994 240h-3.252v-19.02h3.252v13.887l8.772-13.887zM150.697 226.184c0-.891-.34-1.594-1.019-2.11-.68-.527-1.612-.791-2.795-.791-1.149 0-2.11.299-2.883.897-.762.597-1.143 1.3-1.143 2.109h-3.234c0-1.652.691-3.006 2.074-4.06 1.383-1.055 3.112-1.583 5.186-1.583 2.226 0 3.961.481 5.203 1.442 1.242.949 1.863 2.308 1.863 4.078a3.94 3.94 0 01-.773 2.355c-.516.715-1.248 1.284-2.197 1.706 2.214.75 3.322 2.226 3.322 4.429 0 1.746-.674 3.129-2.022 4.149-1.347 1.019-3.146 1.529-5.396 1.529-2.192 0-4.002-.533-5.432-1.6-1.418-1.078-2.127-2.531-2.127-4.359h3.235c0 .926.41 1.723 1.23 2.391.832.656 1.863.984 3.094.984 1.242 0 2.244-.287 3.006-.861.761-.575 1.142-1.319 1.142-2.233 0-1.066-.322-1.828-.967-2.285-.632-.469-1.623-.703-2.97-.703h-3.252v-2.742h3.533c2.215-.059 3.322-.973 3.322-2.742zM157.658 230.314c0-1.863.363-3.539 1.09-5.027.738-1.488 1.758-2.637 3.059-3.445 1.312-.809 2.806-1.213 4.482-1.213 2.59 0 4.682.896 6.275 2.689 1.606 1.793 2.409 4.178 2.409 7.155v.228c0 1.852-.358 3.516-1.073 4.992-.703 1.465-1.716 2.608-3.041 3.428-1.312.82-2.824 1.231-4.535 1.231-2.578 0-4.67-.897-6.275-2.69-1.594-1.793-2.391-4.166-2.391-7.119v-.229zm3.27.387c0 2.11.486 3.803 1.459 5.08.984 1.278 2.297 1.916 3.937 1.916 1.653 0 2.965-.644 3.938-1.933.972-1.301 1.459-3.118 1.459-5.45 0-2.085-.498-3.773-1.494-5.062-.985-1.301-2.297-1.951-3.938-1.951-1.605 0-2.9.638-3.885 1.916-.984 1.277-1.476 3.105-1.476 5.484zM187.4 222.07c2.391 0 4.301.803 5.731 2.409 1.441 1.593 2.162 3.697 2.162 6.31v.299c0 1.793-.346 3.398-1.037 4.816-.692 1.407-1.688 2.502-2.988 3.287-1.289.774-2.778 1.161-4.465 1.161-2.555 0-4.612-.85-6.17-2.549-1.559-1.711-2.338-4.002-2.338-6.873v-1.582c0-3.996.738-7.172 2.215-9.528 1.488-2.355 3.679-3.791 6.574-4.306 1.641-.293 2.748-.651 3.322-1.073.574-.421.862-1.002.862-1.74h2.671c0 1.465-.334 2.608-1.001 3.428-.657.82-1.711 1.4-3.165 1.74l-2.425.545c-1.934.457-3.387 1.231-4.36 2.32-.961 1.078-1.588 2.52-1.881 4.325 1.723-1.993 3.821-2.989 6.293-2.989zm-.632 2.672c-1.594 0-2.86.539-3.797 1.617-.938 1.067-1.407 2.549-1.407 4.448v.281c0 2.039.469 3.656 1.407 4.851.949 1.184 2.226 1.776 3.832 1.776 1.617 0 2.894-.598 3.832-1.793.937-1.195 1.406-2.942 1.406-5.238 0-1.793-.475-3.229-1.424-4.307-.937-1.09-2.221-1.635-3.849-1.635zM214.699 230.701c0 2.895-.662 5.227-1.986 6.996-1.324 1.77-3.117 2.655-5.379 2.655-2.309 0-4.125-.733-5.449-2.198v9.158h-3.252V220.98h2.971l.158 2.11c1.324-1.641 3.164-2.461 5.519-2.461 2.285 0 4.09.861 5.414 2.584 1.336 1.723 2.004 4.119 2.004 7.189v.299zm-3.252-.369c0-2.144-.457-3.838-1.371-5.08-.914-1.242-2.168-1.863-3.762-1.863-1.968 0-3.445.873-4.429 2.619v9.088c.972 1.734 2.461 2.601 4.465 2.601 1.558 0 2.795-.615 3.709-1.845.925-1.243 1.388-3.082 1.388-5.52zM230.59 240c-.188-.375-.34-1.043-.457-2.004-1.512 1.57-3.317 2.356-5.414 2.356-1.875 0-3.416-.528-4.623-1.582-1.196-1.067-1.793-2.415-1.793-4.043 0-1.981.75-3.516 2.25-4.606 1.511-1.101 3.633-1.652 6.363-1.652h3.164v-1.494c0-1.137-.34-2.039-1.019-2.707-.68-.68-1.682-1.02-3.006-1.02-1.16 0-2.133.293-2.918.879-.785.586-1.178 1.295-1.178 2.127h-3.27c0-.949.334-1.863 1.002-2.742.68-.891 1.594-1.594 2.743-2.11 1.16-.515 2.431-.773 3.814-.773 2.191 0 3.908.551 5.15 1.652 1.243 1.09 1.887 2.596 1.934 4.518v8.754c0 1.746.223 3.135.668 4.166V240h-3.41zm-5.397-2.479a5.71 5.71 0 002.901-.791c.914-.527 1.576-1.212 1.986-2.056v-3.903h-2.549c-3.984 0-5.976 1.167-5.976 3.499 0 1.019.34 1.816 1.019 2.39.68.574 1.553.861 2.619.861zM253.6 231.738h-2.286V240h-3.252v-8.262h-2.285L240.451 240h-4.113l6.803-9.844-6.153-9.176h3.938l4.904 7.875h2.232v-7.875h3.252v7.875h2.215l4.94-7.875h3.937l-6.17 9.194 6.803 9.826h-4.113l-5.326-8.262zM273.533 240.352c-2.578 0-4.676-.844-6.293-2.532-1.617-1.699-2.426-3.966-2.426-6.802v-.598c0-1.887.358-3.568 1.073-5.045.726-1.488 1.734-2.648 3.023-3.48 1.301-.844 2.707-1.266 4.219-1.266 2.473 0 4.394.814 5.766 2.443 1.371 1.629 2.056 3.961 2.056 6.996v1.354h-12.885c.047 1.875.592 3.392 1.635 4.553 1.055 1.148 2.391 1.722 4.008 1.722 1.148 0 2.121-.234 2.918-.703a7.345 7.345 0 002.092-1.863l1.986 1.547c-1.594 2.449-3.984 3.674-7.172 3.674zm-.404-17.051c-1.313 0-2.414.48-3.305 1.441-.89.949-1.441 2.285-1.652 4.008h9.527v-.246c-.094-1.652-.539-2.93-1.336-3.832-.797-.914-1.875-1.371-3.234-1.371zM300.27 240h-3.252v-8.121h-8.737V240h-3.269v-19.02h3.269v8.245h8.737v-8.245h3.252V240zM317.812 220.98h3.252V240h-3.252v-13.869L309.041 240h-3.252v-19.02h3.252v13.887l8.771-13.887zM334.178 240.352c-2.578 0-4.676-.844-6.293-2.532-1.617-1.699-2.426-3.966-2.426-6.802v-.598c0-1.887.357-3.568 1.072-5.045.727-1.488 1.735-2.648 3.024-3.48 1.3-.844 2.707-1.266 4.218-1.266 2.473 0 4.395.814 5.766 2.443 1.371 1.629 2.057 3.961 2.057 6.996v1.354h-12.885c.047 1.875.592 3.392 1.635 4.553 1.054 1.148 2.39 1.722 4.008 1.722 1.148 0 2.121-.234 2.917-.703a7.331 7.331 0 002.092-1.863l1.987 1.547c-1.594 2.449-3.985 3.674-7.172 3.674zm-.405-17.051c-1.312 0-2.414.48-3.304 1.441-.891.949-1.442 2.285-1.653 4.008h9.528v-.246c-.094-1.652-.539-2.93-1.336-3.832-.797-.914-1.875-1.371-3.235-1.371zM353.443 230.314c0-1.863.364-3.539 1.09-5.027.738-1.488 1.758-2.637 3.059-3.445 1.312-.809 2.806-1.213 4.482-1.213 2.59 0 4.682.896 6.276 2.689 1.605 1.793 2.408 4.178 2.408 7.155v.228c0 1.852-.358 3.516-1.072 4.992-.704 1.465-1.717 2.608-3.041 3.428-1.313.82-2.825 1.231-4.536 1.231-2.578 0-4.67-.897-6.275-2.69-1.594-1.793-2.391-4.166-2.391-7.119v-.229zm3.27.387c0 2.11.486 3.803 1.459 5.08.984 1.278 2.297 1.916 3.937 1.916 1.653 0 2.965-.644 3.938-1.933.973-1.301 1.459-3.118 1.459-5.45 0-2.085-.498-3.773-1.494-5.062-.985-1.301-2.297-1.951-3.938-1.951-1.605 0-2.9.638-3.885 1.916-.984 1.277-1.476 3.105-1.476 5.484zM388.582 223.617h-6.381V240h-3.252v-16.383h-6.258v-2.637h15.891v2.637zM397.969 237.697c1.16 0 2.174-.351 3.041-1.054.867-.704 1.347-1.582 1.441-2.637h3.076c-.058 1.09-.433 2.127-1.125 3.111-.691.985-1.617 1.77-2.777 2.356a7.932 7.932 0 01-3.656.879c-2.59 0-4.653-.862-6.188-2.584-1.523-1.735-2.285-4.102-2.285-7.102v-.545c0-1.851.34-3.498 1.02-4.939.679-1.442 1.652-2.561 2.918-3.358 1.277-.797 2.783-1.195 4.517-1.195 2.133 0 3.903.639 5.309 1.916 1.418 1.277 2.174 2.935 2.267 4.975h-3.076c-.094-1.231-.562-2.239-1.406-3.024-.832-.797-1.863-1.195-3.094-1.195-1.652 0-2.935.597-3.849 1.793-.903 1.183-1.354 2.9-1.354 5.15v.615c0 2.192.451 3.879 1.354 5.063.902 1.183 2.191 1.775 3.867 1.775zM415.406 235.236l4.43-14.256h3.48l-7.646 21.956c-1.184 3.164-3.065 4.746-5.643 4.746l-.615-.053-1.213-.229v-2.636l.879.07c1.102 0 1.957-.223 2.567-.668.621-.445 1.13-1.26 1.529-2.443l.721-1.934-6.786-18.809h3.551l4.746 14.256zM440.367 223.617h-6.381V240h-3.252v-16.383h-6.257v-2.637h15.89v2.637zM449.754 237.697c1.16 0 2.174-.351 3.041-1.054.867-.704 1.348-1.582 1.441-2.637h3.076c-.058 1.09-.433 2.127-1.124 3.111-.692.985-1.618 1.77-2.778 2.356a7.932 7.932 0 01-3.656.879c-2.59 0-4.652-.862-6.188-2.584-1.523-1.735-2.285-4.102-2.285-7.102v-.545c0-1.851.34-3.498 1.02-4.939.679-1.442 1.652-2.561 2.918-3.358 1.277-.797 2.783-1.195 4.517-1.195 2.133 0 3.903.639 5.309 1.916 1.418 1.277 2.174 2.935 2.267 4.975h-3.076c-.093-1.231-.562-2.239-1.406-3.024-.832-.797-1.863-1.195-3.094-1.195-1.652 0-2.935.597-3.849 1.793-.903 1.183-1.354 2.9-1.354 5.15v.615c0 2.192.451 3.879 1.354 5.063.902 1.183 2.191 1.775 3.867 1.775zM475.102 223.617h-6.381V240h-3.252v-16.383h-6.258v-2.637h15.891v2.637zM478.564 240v-19.02h7.418c2.532 0 4.471.446 5.819 1.336 1.359.879 2.039 2.174 2.039 3.885 0 .879-.264 1.676-.791 2.391-.528.703-1.307 1.242-2.338 1.617 1.148.27 2.068.797 2.76 1.582.703.785 1.054 1.723 1.054 2.813 0 1.746-.644 3.082-1.933 4.007-1.278.926-3.088 1.389-5.432 1.389h-8.596zm3.252-8.35v5.731h5.379c1.36 0 2.373-.252 3.041-.756.68-.504 1.02-1.213 1.02-2.127 0-1.898-1.395-2.848-4.184-2.848h-5.256zm0-2.601h4.202c3.046 0 4.57-.891 4.57-2.672 0-1.781-1.442-2.701-4.324-2.76h-4.448v5.432zM504.844 235.236l4.429-14.256h3.481l-7.647 21.956c-1.183 3.164-3.064 4.746-5.642 4.746l-.615-.053-1.213-.229v-2.636l.879.07c1.101 0 1.957-.223 2.566-.668.621-.445 1.131-1.26 1.529-2.443l.721-1.934-6.785-18.809h3.551l4.746 14.256zM523.318 240.352c-2.578 0-4.675-.844-6.293-2.532-1.617-1.699-2.425-3.966-2.425-6.802v-.598c0-1.887.357-3.568 1.072-5.045.726-1.488 1.734-2.648 3.023-3.48 1.301-.844 2.707-1.266 4.219-1.266 2.473 0 4.395.814 5.766 2.443 1.371 1.629 2.056 3.961 2.056 6.996v1.354h-12.884c.046 1.875.591 3.392 1.634 4.553 1.055 1.148 2.391 1.722 4.008 1.722 1.149 0 2.121-.234 2.918-.703a7.345 7.345 0 002.092-1.863l1.986 1.547c-1.594 2.449-3.984 3.674-7.172 3.674zm-.404-17.051c-1.312 0-2.414.48-3.305 1.441-.89.949-1.441 2.285-1.652 4.008h9.527v-.246c-.093-1.652-.539-2.93-1.336-3.832-.796-.914-1.875-1.371-3.234-1.371zM548.438 223.617h-6.381V240h-3.252v-16.383h-6.258v-2.637h15.891v2.637z'/%3E%3C/svg%3E";

const DEFAULT_DOCUMENT = {
    srcDownload: null,
    srcView: DEFAULT_IMAGE,
};

// Drag
let isDragging = false;

let zoomInInterval = null;
let zoomOutInterval = null;

let moveCloseButtonTimeout = null;

@withTheme
class ImageViewer extends React.Component {
    constructor(props) {
        super(props);
        this.state = { ...DEFAULT_STATE };
    }

    componentDidMount() {
        window.addEventListener('resize', this.handleWindowResize);
        window.addEventListener('keyup', this.handleWindowKeyUp);

        const { images } = this.props;
        this.setState({ images });
    }

    componentDidUpdate(prevProps) {
        const { images } = this.props;
        if (prevProps.images.length !== images.length) {
            this.setState({ images });
        }
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleWindowResize);
        window.removeEventListener('keyup', this.handleWindowKeyUp);
    }

    handleWindowResize = () => {
        const { isOpen, isShowImageCloseButton } = this.state;
        if (isOpen && isShowImageCloseButton) {
            const { currentIndex } = this.state;
            this.moveCloseButton(currentIndex);
        }
    };

    handleWindowKeyUp = e => {
        const { isOpen, isShowImageArrows } = this.state;
        if (isOpen && isShowImageArrows) {
            switch (e.keyCode) {
                case ARROW_LEFT:
                    this.handlePrevClick();
                    break;
                case ARROW_RIGHT:
                    this.handleNextClick();
                    break;
                default:
                    break;
            }
        }
    };

    handleCloseClick = e => {
        e.preventDefault();
        this.setDefaultState();
        enableBodyScroll(this.imageWrapper);
    };

    handleNextClick = () => {
        const { currentIndex, images } = this.state;
        if (currentIndex < images.length - 1) {
            this.setState({ currentIndex: currentIndex + 1 }, this.loadImage);
        }
    };

    handlePrevClick = () => {
        const { currentIndex } = this.state;
        if (currentIndex > 0) {
            this.setState({ currentIndex: currentIndex - 1 }, this.loadImage);
        }
    };

    handleImageDragStart = e => {
        e.preventDefault();
    };

    handleImageMouseDown = () => {
        if (Array.from(this.currentImage.classList).indexOf('-move') !== -1) {
            isDragging = true;
        }
    };

    handleImageMouseMove = e => {
        if (isDragging) {
            const boundingClientRect = this.currentImage.getBoundingClientRect();

            let movementX = e.nativeEvent.movementX;
            if (movementX > 0) {
                const imageLeftPosition = boundingClientRect.left;
                if (imageLeftPosition < 0) {
                    if (imageLeftPosition + movementX > 0) {
                        movementX = -imageLeftPosition;
                    }
                } else {
                    movementX = 0;
                }
            } else if (movementX < 0) {
                const imageRightPosition =
                    window.innerWidth - (boundingClientRect.width + boundingClientRect.left);
                if (imageRightPosition < 0) {
                    if (imageRightPosition - movementX > 0) {
                        movementX = imageRightPosition;
                    }
                } else {
                    movementX = 0;
                }
            }

            let movementY = e.nativeEvent.movementY;
            if (movementY > 0) {
                const imageTopPosition = boundingClientRect.top;
                if (imageTopPosition < 0) {
                    if (imageTopPosition + movementY > 0) {
                        movementY = -imageTopPosition;
                    }
                } else {
                    movementY = 0;
                }
            } else if (movementY < 0) {
                const imageBottomPosition =
                    window.innerHeight - (boundingClientRect.height + boundingClientRect.top);
                if (imageBottomPosition < 0) {
                    if (imageBottomPosition - movementY > 0) {
                        movementY = imageBottomPosition;
                    }
                } else {
                    movementY = 0;
                }
            }

            const imagePoint = getImagePoint(this.currentImage, {
                clientX: e.clientX - movementX,
                clientY: e.clientY - movementY,
            });
            const targetPos = { x: e.clientX, y: e.clientY };
            const targetScale = getElementScale(this.currentImage);

            this.transformImage(this.currentImage, imagePoint, targetPos, targetScale);
        }
    };

    handleImageMouseOut = () => {
        isDragging = false;
    };

    handleImageMouseUp = () => {
        isDragging = false;
    };

    handleImageWheel = e => {
        this.zoomImage(e, e.deltaY < 0);
    };

    loadImage = () => {
        const { currentIndex, images } = this.state;
        let currentDocument = images[currentIndex];

        if (!currentDocument) {
            currentDocument = DEFAULT_DOCUMENT;
        }

        return new Promise(resolve => {
            this.setState({
                isShowImageCloseButton: false,
                isShowLoader: true,
            });
            const img = new Image(100, 100);
            img.onload = () => {
                resolve(currentIndex);
            };
            img.src = currentDocument.srcView;
        }).then(index => {
            this.setState(
                {
                    isShowImageCloseButton: true,
                    isShowLoader: false,
                },
                () => {
                    this.moveCloseButton(index);
                },
            );
        });
    };

    rotateImageReverse = e => {
        e.preventDefault();

        const imageCurrentRotate = getElementRotate(this.currentImage);
        const imageRotate = imageCurrentRotate - ROTATE_STEP;

        this.currentImage.classList.add('-transition');
        this.setState({ isShowImageCloseButton: false });
        setTimeout(() => {
            this.currentImage.classList.remove('-transition');
            this.setState({ isShowImageCloseButton: true }, () => {
                const { currentIndex } = this.state;
                this.moveCloseButton(currentIndex);
            });
        }, 100);
        this.currentImage.style.transform = `
            translate(0px, 0px) 
            rotate(${imageRotate}deg) 
            scale(${SCALE_MIN})
        `;

        this.setImageMaxSize(imageRotate);
    };

    rotateImage = e => {
        e.preventDefault();

        const imageCurrentRotate = getElementRotate(this.currentImage);
        const imageRotate = imageCurrentRotate + ROTATE_STEP;

        this.currentImage.classList.add('-transition');
        this.setState({ isShowImageCloseButton: false });
        setTimeout(() => {
            this.currentImage.classList.remove('-transition');
            this.setState({ isShowImageCloseButton: true }, () => {
                const { currentIndex } = this.state;
                this.moveCloseButton(currentIndex);
            });
        }, 100);
        this.currentImage.style.transform = `
            translate(0px, 0px)
            rotate(${imageRotate}deg)
            scale(${SCALE_MIN})
        `;

        this.setImageMaxSize(imageRotate);
    };

    setImageMaxSize = imageRotate => {
        const isImageAngleInverted = (imageRotate / ROTATE_STEP) % 2 === 1;
        const maxWidth = isImageAngleInverted
            ? window.innerHeight - IMAGE_MARGIN
            : window.innerWidth - IMAGE_MARGIN;
        const maxHeight = isImageAngleInverted
            ? window.innerWidth - IMAGE_MARGIN
            : window.innerHeight - IMAGE_MARGIN;

        this.currentImage.style.maxWidth = `${maxWidth}px`;
        this.currentImage.style.maxHeight = `${maxHeight}px`;
    };

    transformImage = (
        image,
        imagePoint = { x: 0, y: 0 },
        targetPos = { x: 0, y: 0 },
        targetScale,
    ) => {
        const clientRect = image.getBoundingClientRect();
        const { height: imageHeight, width: imageWidth } = clientRect;
        let { left: imageLeft, top: imageTop } = clientRect;

        const elScale = getElementScale(image);

        const isNewImageWidthMoreThanWindow =
            (imageWidth / elScale) * targetScale > window.innerWidth;
        const isImageWidthMoreThanWindow =
            elScale <= targetScale ? imageWidth > window.innerWidth : isNewImageWidthMoreThanWindow;

        const isNewImageHeightMoreThanWindow =
            (imageHeight / elScale) * targetScale > window.innerHeight;
        const isImageHeightMoreThanWindow =
            elScale <= targetScale
                ? imageHeight > window.innerHeight
                : isNewImageHeightMoreThanWindow;

        let newTranslateX = 0;
        let newTranslateY = 0;
        // Если размер картинки превысил размер окна (по одной из сторон или по обеим),
        // то высчитываем новый translate
        if (isImageWidthMoreThanWindow || isImageHeightMoreThanWindow) {
            const { x: targetPosX, y: targetPosY } = targetPos;
            const { x: imagePointX, y: imagePointY } = imagePoint;

            // Если элемент меняет свой scale, то в расчетах считаем, что
            // элемент уже сдвинут из-за нового scale (left и top будут чуть другими)
            if (elScale !== targetScale) {
                imageLeft -= (imageWidth / elScale / 2) * (targetScale - elScale);
                imageTop -= (imageHeight / elScale / 2) * (targetScale - elScale);
            }

            const { translateX: oldTranslateX, translateY: oldTranslateY } = getElementTranslate(
                image,
            );

            if (isImageWidthMoreThanWindow) {
                const clientImagePointX = imageLeft + imagePointX * targetScale;
                newTranslateX = oldTranslateX + (targetPosX - clientImagePointX);
            }
            if (isImageHeightMoreThanWindow) {
                const clientImagePointY = imageTop + imagePointY * targetScale;
                newTranslateY = oldTranslateY + (targetPosY - clientImagePointY);
            }
        }

        const imageRotate = getElementRotate(this.currentImage);
        image.style.transform = `
            translate(${newTranslateX}px, ${newTranslateY}px) 
            rotate(${imageRotate}deg) 
            scale(${targetScale})
        `;

        // Cursor: move
        if (isNewImageWidthMoreThanWindow || isNewImageHeightMoreThanWindow) {
            image.classList.add('-move');
        } else {
            image.classList.remove('-move');
        }

        // Rotator & Close
        this.setState(
            {
                isShowImageArrows: targetScale === SCALE_MIN,
                isShowImageCloseButton: targetScale === SCALE_MIN,
                isShowImageRotator: targetScale === SCALE_MIN,
            },
            () => {
                if (targetScale === SCALE_MIN) {
                    const { currentIndex } = this.state;
                    this.moveCloseButton(currentIndex);
                }
            },
        );
    };

    moveCloseButton = index => {
        clearTimeout(moveCloseButtonTimeout);
        const { currentIndex } = this.state;
        if (index !== currentIndex) {
            return;
        }
        const imageBoundingClientRect = this.currentImage.getBoundingClientRect();

        if (imageBoundingClientRect.width === 0 || imageBoundingClientRect.height === 0) {
            this.closeButton.style.visibility = 'hidden';
            moveCloseButtonTimeout = setTimeout(() => {
                this.moveCloseButton(index);
            }, 100);
        } else {
            const closeButtonBoundingClientRect = this.closeButton.getBoundingClientRect();

            const left =
                imageBoundingClientRect.left +
                imageBoundingClientRect.width -
                closeButtonBoundingClientRect.width / 2;
            const top = imageBoundingClientRect.top - closeButtonBoundingClientRect.height / 2;
            this.closeButton.style.visibility = 'visible';
            this.closeButton.style.left = `${left}px`;
            this.closeButton.style.top = `${top}px`;
        }
    };

    handleActionZoomInMouseDown = e => {
        e.preventDefault();
        this.zoomImageIn();
        zoomInInterval = setInterval(this.zoomImageIn, 200);
    };

    handleActionZoomInMouseUp = e => {
        e.preventDefault();
        clearInterval(zoomInInterval);
    };

    handleActionZoomOutMouseDown = e => {
        e.preventDefault();
        this.zoomImageOut();
        zoomOutInterval = setInterval(this.zoomImageOut, 200);
    };

    handleActionZoomOutMouseUp = e => {
        e.preventDefault();
        clearInterval(zoomOutInterval);
    };

    zoomImageIn = () => {
        this.zoomImage({ clientX: window.innerWidth / 2, clientY: window.innerWidth / 2 }, true);
    };

    zoomImageOut = () => {
        this.zoomImage({ clientX: window.innerWidth / 2, clientY: window.innerWidth / 2 }, false);
    };

    zoomImage = (mousePos, isZoomIn = false) => {
        const scale = getElementScale(this.currentImage);
        const imagePoint = getImagePoint(this.currentImage, mousePos);
        const targetPos = { x: mousePos.clientX, y: mousePos.clientY };

        let targetScale = scale;
        if (isZoomIn) {
            // Приближение
            targetScale = scale + SCALE_STEP > SCALE_MAX ? SCALE_MAX : scale + SCALE_STEP;
        } else {
            // Отдаление
            targetScale = scale - SCALE_STEP < SCALE_MIN ? SCALE_MIN : scale - SCALE_STEP;
        }

        this.transformImage(this.currentImage, imagePoint, targetPos, targetScale);
    };

    setDefaultState = () => {
        const { images } = this.state;
        this.setState({ ...DEFAULT_STATE, images });
    };

    setImages = (images = []) => {
        this.setState({ images });
    };

    open = (currentIndex = 0) => {
        this.setState({ currentIndex, isOpen: true }, () => {
            this.loadImage();
            disableBodyScroll(this.imageWrapper);
        });
    };

    render() {
        const { className, applyTheme } = this.props;

        const {
            currentIndex,
            images,
            isOpen,
            isShowImageArrows,
            isShowImageCloseButton,
            isShowImageRotator,
            isShowLoader,
        } = this.state;

        const { srcDownload, srcView } = images[currentIndex] || DEFAULT_DOCUMENT;

        const prevButtonDisabled = currentIndex === 0;
        const nextButtonDisabled = currentIndex === images.length - 1;

        const componentClass = applyTheme(
            cn('ImageViewer', {
                ImageViewer_show: isOpen,
                ImageViewer_black: !isShowImageRotator,
            }),
            className,
        );
        const imageWrapperClass = applyTheme(cn('ImageViewer__ImageWrapper'));
        const backgroundClass = applyTheme(cn('ImageViewer__Background'));
        const rotateClass = applyTheme(cn('ImageViewer__Rotate'));
        const closeClass = applyTheme(cn('ImageViewer__Close'));
        const buttonLeftClass = applyTheme(cn('ImageViewer__Button', 'ImageViewer__Button_left'));
        const buttonRightClass = applyTheme(cn('ImageViewer__Button', 'ImageViewer__Button_right'));
        const actionsClass = applyTheme(cn('ImageViewer__Actions'));
        const actionZoomOutClass = applyTheme(
            cn('ImageViewer__Action', 'ImageViewer__Action_zoom-out'),
        );
        const actionZoomInClass = applyTheme(
            cn('ImageViewer__Action', 'ImageViewer__Action_zoom-in'),
        );
        const actionDownloadClass = applyTheme(
            cn('ImageViewer__Action', 'ImageViewer__Action_download'),
        );
        const actionRotateClass = applyTheme(
            cn('ImageViewer__Action', 'ImageViewer__Action_rotate', {
                ImageViewer__Action_hidden: !isShowImageRotator,
            }),
        );

        return (
            <div className={componentClass}>
                <div className={imageWrapperClass} ref={ref => (this.imageWrapper = ref)}>
                    {isShowLoader && <Spin size="xl" type="default" />}
                    <div className={backgroundClass} onClick={this.handleCloseClick} />
                    {!isShowLoader && (
                        <img
                            alt=""
                            src={srcView}
                            ref={ref => (this.currentImage = ref)}
                            onDragStart={this.handleImageDragStart}
                            onMouseDown={this.handleImageMouseDown}
                            onMouseMove={this.handleImageMouseMove}
                            onMouseOut={this.handleImageMouseOut}
                            onMouseUp={this.handleImageMouseUp}
                            onWheel={this.handleImageWheel}
                        />
                    )}
                    {!isShowLoader && isShowImageRotator && (
                        <span className={rotateClass} onMouseDown={this.rotateImage}>
                            <FeatherIcon name="rotate-cw" />
                        </span>
                    )}
                    {!isShowLoader && isShowImageCloseButton && (
                        <button
                            className={closeClass}
                            onClick={this.handleCloseClick}
                            ref={ref => (this.closeButton = ref)}
                            type="button"
                        >
                            <FeatherIcon name="x" />
                        </button>
                    )}
                    {isShowImageArrows && images.length > 1 && (
                        <button
                            className={buttonLeftClass}
                            onClick={this.handlePrevClick}
                            disabled={prevButtonDisabled}
                            type="button"
                        >
                            <FeatherIcon name="chevron-left" />
                        </button>
                    )}
                    {isShowImageArrows && images.length > 1 && (
                        <button
                            className={buttonRightClass}
                            onClick={this.handleNextClick}
                            disabled={nextButtonDisabled}
                            type="button"
                        >
                            <FeatherIcon name="chevron-right" />
                        </button>
                    )}
                    {!isShowLoader && (
                        <div className={actionsClass}>
                            <button
                                className={actionRotateClass}
                                title="Повернуть на 90° против часовой стрелке"
                                type="button"
                                onMouseDown={this.rotateImageReverse}
                            >
                                <FeatherIcon name="rotate-ccw" />
                            </button>
                            <button
                                className={actionRotateClass}
                                title="Повернуть на 90° по часовой стрелке"
                                type="button"
                                onMouseDown={this.rotateImage}
                            >
                                <FeatherIcon name="rotate-cw" />
                            </button>
                            <button
                                className={actionZoomOutClass}
                                title="Отдалить"
                                type="button"
                                onMouseDown={this.handleActionZoomOutMouseDown}
                                onMouseUp={this.handleActionZoomOutMouseUp}
                            >
                                <FeatherIcon name="zoom-out" />
                            </button>
                            <button
                                className={actionZoomInClass}
                                title="Приблизить"
                                type="button"
                                onMouseDown={this.handleActionZoomInMouseDown}
                                onMouseUp={this.handleActionZoomInMouseUp}
                            >
                                <FeatherIcon name="zoom-in" />
                            </button>
                            <a
                                href={srcDownload}
                                className={actionDownloadClass}
                                rel="noopener noreferrer"
                                target="_blank"
                                title="Скачать"
                            >
                                <FeatherIcon name="download" />
                            </a>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

ImageViewer.propTypes = {
    applyTheme: PropTypes.func,
    className: PropTypes.string,
    images: PropTypes.arrayOf(
        PropTypes.shape({
            srcDownload: PropTypes.string,
            srcView: PropTypes.string,
        }),
    ),
};
ImageViewer.defaultProps = {
    applyTheme: _ => _,
    className: '',
    images: [],
};

export default ImageViewer;
