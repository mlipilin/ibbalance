import React, { useRef } from 'react';
import { storiesOf } from '@storybook/react';

import Button from '../../Button';
import ImageViewer, { withImageViewer } from '../index';

import notes from '../readme.md';

const StoryRef = () => {
    // Refs
    const imageViewer = useRef(null);

    // Handlers
    function handleButtonClick() {
        imageViewer.current.open(0);
    }

    return (
        <>
            <Button onClick={handleButtonClick}>Начать просмотр</Button>
            <ImageViewer.Component
                images={[
                    {
                        srcDownload: require('../../../../public/demo/scans/1.jpg'),
                        srcView: require('../../../../public/demo/scans/1.jpg'),
                    },
                    {
                        srcDownload: require('../../../../public/demo/scans/2.jpg'),
                        srcView: require('../../../../public/demo/scans/2.jpg'),
                    },
                    {
                        srcDownload: require('../../../../public/demo/scans/3.jpeg'),
                        srcView: require('../../../../public/demo/scans/3.jpeg'),
                    },
                ]}
                ref={imageViewer}
            />
        </>
    );
};

const StoryNoImages = () => {
    // Refs
    const imageViewer = useRef(null);

    // Handlers
    function handleButtonClick() {
        imageViewer.current.open();
    }

    return (
        <>
            <Button onClick={handleButtonClick}>Начать просмотр</Button>
            <ImageViewer.Component ref={imageViewer} />
        </>
    );
};

const StoryProvider = withImageViewer(props => {
    const { imageViewer } = props;

    // Handlers
    function handleButtonClick() {
        imageViewer.current.setImages([
            {
                srcDownload: require('../../../../public/demo/scans/1.jpg'),
                srcView: require('../../../../public/demo/scans/1.jpg'),
            },
            {
                srcDownload: require('../../../../public/demo/scans/2.jpg'),
                srcView: require('../../../../public/demo/scans/2.jpg'),
            },
            {
                srcDownload: require('../../../../public/demo/scans/3.jpeg'),
                srcView: require('../../../../public/demo/scans/3.jpeg'),
            },
        ]);
        imageViewer.current.open(2);
    }

    return <Button onClick={handleButtonClick}>Начать просмотр</Button>;
});

const stories = storiesOf('ImageViewer', module);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories
    .add('Use with ref', () => <StoryRef />, { notes })
    .add('No images mock', () => <StoryNoImages />, { notes })
    .add(
        'Use with Profider',
        () => (
            <ImageViewer.Provider>
                <StoryProvider />
            </ImageViewer.Provider>
        ),
        { notes },
    );
