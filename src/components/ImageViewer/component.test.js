import React from 'react';
import userEvent from '@testing-library/user-event';

import ImageViewer from './index';
import { DEFAULT_IMAGE } from './component';

import { render } from '../../hoc/with-theme';

afterEach(() => {
    jest.clearAllTimers();
});

beforeEach(() => {
    window.disableBodyScroll = jest.fn();
    window.enableBodyScroll = jest.fn();
});
beforeAll(() => {
    jest.useFakeTimers();
});

afterAll(() => {
    jest.useRealTimers();
});

describe('Тестирование компонента ImageViewer', () => {
    it('Должен отобразиться ImageViewer без области просмотра', () => {
        const { container } = render(<ImageViewer.Component />);
        const wrapper = container.querySelector('.ImageViewer');
        expect(wrapper).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });
    it('Должен отобразиться ImageViewer c классом ImageViewer__Styled', () => {
        const { container } = render(<ImageViewer.Component className="ImageViewer__Styled" />);
        const wrapper = container.querySelector('.ImageViewer');
        expect(wrapper).toHaveClass('ImageViewer__Styled');
        expect(container.firstChild).toMatchSnapshot();
    });
    it('Должен отобразиться ImageViewer с областью просмотра и spin', () => {
        const imageViewer = React.createRef();
        const { container } = render(<ImageViewer.Component ref={imageViewer} />);
        imageViewer.current.open();
        const wrapper = container.querySelector('.ImageViewer');
        expect(wrapper).toHaveClass('ImageViewer_show');
        const spin = wrapper.querySelector('.Spin');
        expect(spin).toBeInTheDocument();
        const background = container.querySelector('.ImageViewer__Background');
        expect(background.firstChild).toBeNull();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться ImageViewer с областью просмотра и mock документом', async () => {
        const LOAD_SUCCESS_SRC = 'LOAD_SUCCESS_SRC';
        Object.defineProperty(global.Image.prototype, 'src', {
            set(src) {
                if (src === LOAD_SUCCESS_SRC) {
                    setTimeout(() => {
                        this.onload();
                    });
                }
            },
        });
        const imageViewer = React.createRef();
        const { container } = render(
            <ImageViewer.Component
                ref={imageViewer}
                images={[{ srcDownload: LOAD_SUCCESS_SRC, srcView: LOAD_SUCCESS_SRC }]}
            />,
        );
        imageViewer.current.open();
        const wrapper = container.querySelector('.ImageViewer');
        expect(wrapper).toHaveClass('ImageViewer_show');
        const spin = wrapper.querySelector('.Spin');
        expect(spin).toBeInTheDocument();

        await jest.advanceTimersByTime(1000);

        const img = container.querySelector('img');
        expect(img.src).toBe('http://localhost/LOAD_SUCCESS_SRC');

        const buttonRotateCW = container.querySelector('button .feather-rotate-cw');
        expect(buttonRotateCW).toBeInTheDocument();

        const buttonRotateCCW = container.querySelector('button .feather-rotate-ccw');
        expect(buttonRotateCCW).toBeInTheDocument();

        const buttonZoomOut = container.querySelector('button .feather-zoom-out');
        expect(buttonZoomOut).toBeInTheDocument();

        const buttonZoomIn = container.querySelector('button .feather-zoom-in');
        expect(buttonZoomIn).toBeInTheDocument();

        const buttonDownload = container.querySelector('.ImageViewer__Action_download');
        expect(buttonDownload).toBeInTheDocument();
        expect(buttonDownload.href).toBe('http://localhost/LOAD_SUCCESS_SRC');
    });
    it('Должен отобразиться ImageViewer с областью просмотра и default документом', async () => {
        Object.defineProperty(global.Image.prototype, 'src', {
            set() {
                setTimeout(() => {
                    this.onload();
                });
            },
        });
        const imageViewer = React.createRef();
        const { container } = render(<ImageViewer.Component ref={imageViewer} images={[]} />);
        imageViewer.current.open();
        await jest.advanceTimersByTime(1);

        const img = container.querySelector('img');
        expect(img.src).toBe(DEFAULT_IMAGE);

        const buttonDownload = container.querySelector('.ImageViewer__Action_download');
        expect(buttonDownload).toBeInTheDocument();
        expect(buttonDownload.href).toBe('');
    });

    it('Должен отобразиться ImageViewer с mock документами и изменение по arrow left/right', async () => {
        Object.defineProperty(global.Image.prototype, 'src', {
            set() {
                setTimeout(() => {
                    this.onload();
                });
            },
        });
        const imageViewer = React.createRef();
        const { container } = render(
            <ImageViewer.Component
                ref={imageViewer}
                images={[
                    { srcDownload: 'src1', srcView: 'src1' },
                    { srcDownload: 'src2', srcView: 'src2' },
                ]}
            />,
        );

        imageViewer.current.open();

        await jest.advanceTimersByTime(1);
        const img = await container.querySelector('img');
        expect(img.src).toBe('http://localhost/src1');

        await userEvent.type(img, '{arrowleft}');
        await jest.advanceTimersByTime(1);
        const imgAfterArrowLeftClick = await container.querySelector('img');
        expect(imgAfterArrowLeftClick.src).toBe('http://localhost/src1');

        await userEvent.type(img, '{arrowright}');
        await jest.advanceTimersByTime(1);
        const imgAfterArrowRightClick = await container.querySelector('img');
        expect(imgAfterArrowRightClick.src).toBe('http://localhost/src2');

        await userEvent.type(imgAfterArrowRightClick, '{arrowright}');
        await jest.advanceTimersByTime(1);
        const imgAfterArrowRight2Click = await container.querySelector('img');
        expect(imgAfterArrowRight2Click.src).toBe('http://localhost/src2');

        await userEvent.type(imgAfterArrowRight2Click, '{arrowleft}');
        await jest.advanceTimersByTime(1);
        const imgAfterArrowLeft2Click = container.querySelector('img');
        expect(imgAfterArrowLeft2Click.src).toBe('http://localhost/src1');
    });

    it('Должен отобразиться ImageViewer поворотом cw', async () => {
        Object.defineProperty(global.Image.prototype, 'src', {
            set() {
                setTimeout(() => {
                    this.onload();
                });
            },
        });
        const imageViewer = React.createRef();
        const { container } = render(
            <ImageViewer.Component
                ref={imageViewer}
                images={[
                    { srcDownload: 'src1', srcView: 'src1' },
                    { srcDownload: 'src2', srcView: 'src2' },
                ]}
            />,
        );

        imageViewer.current.open();
        await jest.advanceTimersByTime(1);
        const buttonRotateCW = container.querySelector('button .feather-rotate-cw');
        expect(buttonRotateCW).toBeInTheDocument();

        await userEvent.click(buttonRotateCW);
        const img = container.querySelector('img');
        expect(img).toHaveStyle({
            transform: `
            translate(0px, 0px)
            rotate(90deg)
            scale(1)
        `,
        });
    });

    it('Должен отобразиться ImageViewer поворотом ccw', async () => {
        Object.defineProperty(global.Image.prototype, 'src', {
            set() {
                setTimeout(() => {
                    this.onload();
                });
            },
        });
        const imageViewer = React.createRef();
        const { container } = render(
            <ImageViewer.Component
                ref={imageViewer}
                images={[
                    { srcDownload: 'src1', srcView: 'src1' },
                    { srcDownload: 'src2', srcView: 'src2' },
                ]}
            />,
        );

        imageViewer.current.open();

        await jest.advanceTimersByTime(1);
        const buttonRotateCCW = container.querySelector('button .feather-rotate-ccw');
        expect(buttonRotateCCW).toBeInTheDocument();

        await userEvent.click(buttonRotateCCW);
        const img = container.querySelector('img');
        expect(img).toHaveStyle({
            transform: `
            translate(0px, 0px) 
            rotate(-90deg) 
            scale(1)
        `,
        });
    });

    it('Должен отобразиться ImageViewer не уменьшается при начальном zoom out', async () => {
        Object.defineProperty(global.Image.prototype, 'src', {
            set() {
                setTimeout(() => {
                    this.onload();
                });
            },
        });
        const imageViewer = React.createRef();
        const { container } = render(
            <ImageViewer.Component
                ref={imageViewer}
                images={[
                    { srcDownload: 'src1', srcView: 'src1' },
                    { srcDownload: 'src2', srcView: 'src2' },
                ]}
            />,
        );

        imageViewer.current.open();

        await jest.advanceTimersByTime(1);
        const buttonZoomOut = container.querySelector('button .feather-zoom-out');
        expect(buttonZoomOut).toBeInTheDocument();

        await userEvent.click(buttonZoomOut);
        const img = container.querySelector('img');
        expect(img).toHaveStyle({
            transform: `
            translate(0px, 0px) 
            rotate(0deg) 
            scale(1)
        `,
        });
    });
    it('Должен отобразиться ImageViewer увеличение zoom in', async () => {
        Object.defineProperty(global.Image.prototype, 'src', {
            set() {
                setTimeout(() => {
                    this.onload();
                });
            },
        });
        const imageViewer = React.createRef();
        const { container } = render(
            <ImageViewer.Component
                ref={imageViewer}
                images={[
                    { srcDownload: 'src1', srcView: 'src1' },
                    { srcDownload: 'src2', srcView: 'src2' },
                ]}
            />,
        );

        imageViewer.current.open();

        await jest.advanceTimersByTime(1);
        const buttonZoomIn = container.querySelector('button .feather-zoom-in');
        expect(buttonZoomIn).toBeInTheDocument();

        await userEvent.click(buttonZoomIn);
        const img = container.querySelector('img');
        expect(img).toHaveStyle({
            transform: `
            translate(0px, 0px) 
            rotate(0deg) 
            scale(1.2)
        `,
        });
    });
    it('Должен отобразиться ImageViewer закрыться при клике на кнопку x', async () => {
        Object.defineProperty(global.Image.prototype, 'src', {
            set() {
                setTimeout(() => {
                    this.onload();
                });
            },
        });
        const imageViewer = React.createRef();
        const { container } = render(
            <ImageViewer.Component
                ref={imageViewer}
                images={[
                    { srcDownload: 'src1', srcView: 'src1' },
                    { srcDownload: 'src2', srcView: 'src2' },
                ]}
            />,
        );

        imageViewer.current.open();
        await jest.advanceTimersByTime(1);
        await jest.advanceTimersByTime(150);
        expect(container.firstChild).toHaveClass('ImageViewer_show');

        const buttonClose = container.querySelector('button .feather-x');
        expect(buttonClose).toBeInTheDocument();

        await userEvent.click(buttonClose);
        const wrappper = await container.querySelector('.ImageViewer');
        expect(wrappper).not.toHaveClass('ImageViewer_show');
    });
    it('Должен отобразиться ImageViewer поворотом cw при клике по центру', async () => {
        Object.defineProperty(global.Image.prototype, 'src', {
            set() {
                setTimeout(() => {
                    this.onload();
                });
            },
        });
        const imageViewer = React.createRef();
        const { container } = render(
            <ImageViewer.Component
                ref={imageViewer}
                images={[
                    { srcDownload: 'src1', srcView: 'src1' },
                    { srcDownload: 'src2', srcView: 'src2' },
                ]}
            />,
        );

        imageViewer.current.open();

        await jest.advanceTimersByTime(1);
        const buttonRotateCW = container.querySelector('.feather-rotate-cw');
        expect(buttonRotateCW).toBeInTheDocument();

        await userEvent.click(buttonRotateCW);
        const img = container.querySelector('img');
        expect(img).toHaveStyle({
            transform: `
            translate(0px, 0px)
            rotate(90deg)
            scale(1)
        `,
        });
    });

    it('Должен отобразиться ImageViewer при клике на стрелку сбоку right/left', async () => {
        Object.defineProperty(global.Image.prototype, 'src', {
            set() {
                setTimeout(() => {
                    this.onload();
                });
            },
        });
        const imageViewer = React.createRef();
        const { container } = render(
            <ImageViewer.Component
                ref={imageViewer}
                images={[
                    { srcDownload: 'src1', srcView: 'src1' },
                    { srcDownload: 'src2', srcView: 'src2' },
                ]}
            />,
        );

        imageViewer.current.open();
        await jest.advanceTimersByTime(1);
        await jest.advanceTimersByTime(150);
        const buttonLeft = container.querySelector('.ImageViewer__Button_left');
        const buttonRight = container.querySelector('.ImageViewer__Button_right');
        expect(buttonRight).toBeInTheDocument();
        expect(buttonLeft).toBeInTheDocument();
        expect(buttonLeft).toBeDisabled();

        await userEvent.click(buttonLeft);
        await jest.advanceTimersByTime(1);
        const imgAfterLeftClick = await container.querySelector('img');
        expect(imgAfterLeftClick.src).toBe('http://localhost/src1');
        await userEvent.click(buttonRight);
        await jest.advanceTimersByTime(1);
        const imgAfterRightClick = await container.querySelector('img');
        expect(imgAfterRightClick.src).toBe('http://localhost/src2');
        expect(buttonRight).toBeDisabled();
        await userEvent.click(buttonLeft);
        await jest.advanceTimersByTime(1);
        const imgLeftClick = await container.querySelector('img');
        expect(imgLeftClick.src).toBe('http://localhost/src1');
    });
});
