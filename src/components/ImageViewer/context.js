import React, { useRef } from 'react';
import PropTypes from 'prop-types';

// Components
import ImageViewer from './component';

const ImageViewerContext = React.createContext(null);

export const Provider = ({ children }) => {
    const imageViewer = useRef(null);
    return (
        <ImageViewerContext.Provider value={{ imageViewer }}>
            {children}
            <ImageViewer ref={imageViewer} images={[]} />
        </ImageViewerContext.Provider>
    );
};
Provider.propTypes = {
    children: PropTypes.node,
};
Provider.defaultProps = {
    children: null,
};

export const Consumer = ({ children }) => (
    <ImageViewerContext.Consumer>
        {({ imageViewer }) => children({ imageViewer })}
    </ImageViewerContext.Consumer>
);
Consumer.propTypes = {
    children: PropTypes.func,
    imageViewer: PropTypes.shape({}),
};
Consumer.defaultProps = {
    children: null,
    imageViewer: null,
};
