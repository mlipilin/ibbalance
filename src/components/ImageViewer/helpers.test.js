import { getElementTranslate, getElementRotate, getElementScale } from './helpers';

describe('Тестирование компонента ImageViewer/helpers.js', () => {
    describe('Тестирование компонента getElementTranslate', () => {
        it('transform: none -> translateX:0, translateY:0', () => {
            const el = document.createElement('div');
            const outputStyles = {
                translateX: 0,
                translateY: 0,
            };
            expect(getElementTranslate(el)).toEqual(outputStyles);
        });
        it('rotate(2deg) -> translateX:0, translateY:0', () => {
            const el = document.createElement('div');
            el.style.transform = 'rotate(2deg)';
            const outputStyles = {
                translateX: 0,
                translateY: 0,
            };
            expect(getElementTranslate(el)).toEqual(outputStyles);
        });
        it('translate(5px, 10px) -> translateX:0, translateY:0', () => {
            const el = document.createElement('div');
            el.style.transform = 'translate(5px, 10px)';
            const outputStyles = {
                translateX: 5,
                translateY: 10,
            };
            expect(getElementTranslate(el)).toEqual(outputStyles);
        });
    });

    describe('Тестирование компонента getElementRotate', () => {
        it('transform: none -> 0', () => {
            const el = document.createElement('div');
            expect(getElementRotate(el)).toBe(0);
        });
        it('translate(5px, 10px) -> 0', () => {
            const el = document.createElement('div');
            el.style.transform = 'translate(5px, 10px)';
            expect(getElementRotate(el)).toBe(0);
        });
        it('rotate(2deg) -> 2', () => {
            const el = document.createElement('div');
            el.style.transform = 'rotate(2deg)';
            expect(getElementRotate(el)).toBe(2);
        });
    });
    describe('Тестирование компонента getElementScale', () => {
        it('transform: none -> 1', () => {
            const el = document.createElement('div');
            expect(getElementScale(el)).toBe(1);
        });
        it('scale(2) -> 2', () => {
            const el = document.createElement('div');
            el.style.transform = 'scale(2)';
            expect(getElementScale(el)).toBe(2);
        });
    });
});
