export const ARROW_LEFT = 37;
export const ARROW_RIGHT = 39;
export const CMD = 91;
export const CTRL = 17;
export const ESC = 27;

// Rotate
export const ROTATE_MIN = 0;
export const ROTATE_MAX = 360;
export const ROTATE_STEP = 90;

// Scale
export const SCALE_MIN = 1;
export const SCALE_MAX = 10;
export const SCALE_STEP = 0.2;

export const IMAGE_MARGIN = 130;
