# Использование через Provider

Подходит для случаев, когда DOM-ноду с галереей нужно поднять выше по дереву (например при работе с `z-index`). Нода расположится в месте вызова `ImageViever.Provider`.

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Button, ImageViewer, withImageViewer } from 'ibalance';
import theme from 'ibalance/src/themes/spb/theme';

const Demo = withImageViewer((props) => {
    const { imageViewer } = props;

    // Handlers
    function handleButtonClick() {
        imageViewer.current.setImages([
            {
                srcDownload: require('image1.jpg'),
                srcView: require('image1.jpg'),
            },
            {
                srcDownload: require('image2.jpg'),
                srcView: require('image2.jpg'),
            },
            {
                srcDownload: require('image3.jpg'),
                srcView: require('image3.jpg'),
            },
        ]);
        imageViewer.current.open();
    }

    return (
        <Button onClick={handleButtonClick}>Начать просмотр</Button>
    );
});

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <ImageViewer.Provider>
            <Demo />
        </ImageViewer.Provider>
    </ThemeProvider>,
    document.getElementById('root'),
);
```
