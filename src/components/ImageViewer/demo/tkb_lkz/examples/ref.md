# Использование через ref

Подходит для большинства случаев

```jsx
import React, { useRef } from 'react';
import ReactDOM from 'react-dom';

import { Button, ImageViewer } from 'ibalance';
import theme from 'ibalance/src/themes/tkb_lkz/theme';

function Demo() {
    const imageViewer = useRef(null);

    // Handlers
    function handleButtonClick() {
        imageViewer.current.open();
    }

    return (
        <>
            <Button onClick={handleButtonClick}>Начать просмотр</Button>
            <ImageViewer.Component
                ref={imageViewer}
                images={[
                    {
                        srcDownload: require('image1.jpg'),
                        srcView: require('image1.jpg'),
                    },
                    {
                        srcDownload: require('image2.jpg'),
                        srcView: require('image2.jpg'),
                    },
                    {
                        srcDownload: require('image3.jpg'),
                        srcView: require('image3.jpg'),
                    },
                ]}
            />
        </>
    );
}

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <Demo />
    </ThemeProvider>,
    document.getElementById('root'),
);
```
