import React, { useRef } from 'react';

// Components
import Button from '../../../Button';

// Decorators
import ImageViewer, { withImageViewer } from '../../index';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';

// Examples
import provider from './examples/provider.md';
import ref from './examples/ref.md';

// Wrappers
import { Col, Row } from '../../../../wrappers/WGrid';

function DemoRef() {
    const imageViewer = useRef(null);

    // Handlers
    function handleButtonClick() {
        imageViewer.current.open();
    }

    return (
        <>
            <Button onClick={handleButtonClick}>Начать просмотр</Button>
            <ImageViewer.Component
                ref={imageViewer}
                images={[
                    {
                        srcDownload: require('../../../../../public/demo/scans/1.jpg'),
                        srcView: require('../../../../../public/demo/scans/1.jpg'),
                    },
                    {
                        srcDownload: require('../../../../../public/demo/scans/2.jpg'),
                        srcView: require('../../../../../public/demo/scans/2.jpg'),
                    },
                    {
                        srcDownload: require('../../../../../public/demo/scans/3.jpeg'),
                        srcView: require('../../../../../public/demo/scans/3.jpeg'),
                    },
                ]}
            />
        </>
    );
}

const DemoProvider = withImageViewer(props => {
    const { imageViewer } = props;

    // Handlers
    function handleButtonClick() {
        imageViewer.current.setImages([
            {
                srcDownload: require('../../../../../public/demo/scans/1.jpg'),
                srcView: require('../../../../../public/demo/scans/1.jpg'),
            },
            {
                srcDownload: require('../../../../../public/demo/scans/2.jpg'),
                srcView: require('../../../../../public/demo/scans/2.jpg'),
            },
            {
                srcDownload: require('../../../../../public/demo/scans/3.jpeg'),
                srcView: require('../../../../../public/demo/scans/3.jpeg'),
            },
        ]);
        imageViewer.current.open(0);
    }

    return <Button onClick={handleButtonClick}>Начать просмотр</Button>;
});

const DemoImageViewer = () => {
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>

            <div id="example">
                <Row
                    cols={{
                        mobile: [1],
                        tab: [1, 1],
                        desktop: [1],
                        largeDesktop: [1, 1],
                    }}
                    gap="20px"
                >
                    <Col>
                        <Example readme={ref}>
                            <DemoRef />
                        </Example>
                    </Col>
                    <Col>
                        <Example readme={provider}>
                            <DemoProvider />
                        </Example>
                    </Col>
                </Row>
            </div>
        </DemoComponent>
    );
};

export default DemoImageViewer;
