import Component from './component';
import { Consumer, Provider } from './context';
import withImageViewer from './decorator';

export { withImageViewer };
export default { Component, Consumer, Provider };
