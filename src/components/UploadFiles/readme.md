## Usage

```jsx
<UploadFiles >
</UploadFiles>
```

## Props

| Prop name   | Prop type                        | Default value |
| ----------- | -------------------------------- | ------------- |
| _accept_    | node or string                   | `null`        |
| _children_  | node or array of node            | `null`        |
| _compact_   | bool                             | `false`       |
| _disabled_  | `top`, `right`, `bottom`, `left` | `bottom`      |
| _overlayTitle_     | string                    | `''`         |
| _overlayDescription_    | string               |  `''`         |
| _title_     | string                           | `''`         |
| _onDrop_    | func                             |               |

## CSS Selectors

```css
.UploadFiles {
}
.UploadFiles_compact{

}
.UploadFiles_disabled{

}
.UploadFiles__Header{

}
.UploadFiles__Content{

}
.UploadFiles__Footer{

}
.UploadFiles__File{

}
.UploadFiles__Title{

}
.UploadFiles__Icons{

}
.UploadFiles__IconFolder{

}
.UploadFiles__IconFolderWithDocs{

}
.UploadFiles__IconCamera{

}
.UploadFiles__Formats{

}
.UploadFiles__Button{

}
.UploadFiles__Button_compact{

}
.UploadFiles__Overlay{

}
.UploadAttachments__OverlayTitle{

}
.UploadAttachments__OverlayDescription{

}
```
