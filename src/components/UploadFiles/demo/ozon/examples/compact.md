# "compact" состояние

`true`: uploadFiles занимание минимальную высоту

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { UploadFiles, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/ozon/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <UploadFiles
        accept={['pdf', 'png']}
        compact={true}
        title = 'Добавьте файл(ы) с вашего устройства'
        overlayTitle='Прикрепить документы к заявке'
        overlayDescription='Для файлов размером не более 20 МБ'
        onDrop={handleDrop}
    />
  </ThemeProvider>,
  document.getElementById('root')
);
```
