import React from 'react';

// Components
import UploadFiles from '../../index';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';

// Examples
import disabled from './examples/disabled.md';
import compact from './examples/compact.md';
import defaultView from './examples/defaultView.md';

// HOC

// Wrappers
import { Col, Row } from '../../../../wrappers/WGrid';

const DemoUploadFiles = () => {
    const handleDrop = files => {
        console.log('drop', files);
    };
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>

            <div id="example">
                <Row
                    cols={{ mobile: [1], tab: [1], desktop: [1], largeDesktop: [1, 1] }}
                    gap="20px"
                >
                    <Col>
                        <Example readme={defaultView}>
                            <UploadFiles
                                accept={['pdf', 'png']}
                                title="Добавьте файл(ы) с вашего устройства"
                                overlayTitle="Прикрепить документы к заявке"
                                overlayDescription="Для файлов размером не более 20 МБ"
                                onDrop={handleDrop}
                            />
                        </Example>
                    </Col>
                    <Col>
                        <Example readme={disabled}>
                            <UploadFiles
                                accept={['pdf', 'png']}
                                disabled
                                title="Добавьте файл(ы) с вашего устройства"
                                overlayTitle="Прикрепить документы к заявке"
                                overlayDescription="Для файлов размером не более 20 МБ"
                                onDrop={handleDrop}
                            />
                        </Example>
                    </Col>
                    <Col>
                        <Example readme={compact}>
                            <UploadFiles
                                accept={['pdf', 'png']}
                                compact
                                title="Добавьте файл(ы) с вашего устройства"
                                overlayTitle="Прикрепить документы к заявке"
                                overlayDescription="Для файлов размером не более 20 МБ"
                                onDrop={handleDrop}
                            />
                        </Example>
                    </Col>
                </Row>
            </div>
        </DemoComponent>
    );
};

export default DemoUploadFiles;
