# "default" состояние

`false`: uploadFiles активный

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { UploadFiles, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/spb/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <UploadFiles
        accept={['pdf', 'png']}
        compact={false}
        disabled={false}
        title = 'Добавьте файл(ы) с вашего устройства'
        overlayTitle='Прикрепить документы к заявке'
        overlayDescription='Для файлов размером не более 20 МБ'
        onDrop={handleDrop}
    />
  </ThemeProvider>,
  document.getElementById('root')
);
```
