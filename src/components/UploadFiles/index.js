import React, { useEffect, useRef } from 'react';
import { renderToString } from 'react-dom/server';

import PropTypes from 'prop-types';
import cn from 'classnames';

// Components
import Button from '../Button';
import FeatherIcon from '../FeatherIcon';
import H from '../../typography/H';
import { WFromDesktop, WToTab } from '../../wrappers/WResponsive';

// Helpers
import { getFileInfo } from '../../helpers/files';

import { withTheme } from '../../theme-provider';

// Images
import imgFolderWithDocs from './images/folder-with-docs.svg';
import imgFolder from './images/folder.svg';

const OVERLAY_ID = 'uploadFilesOverlay';

let dragTarget = null;

function UploadFiles(props) {
    const {
        accept,
        children,
        className,
        compact,
        disabled,
        disableDragNDrop,
        fileRef,
        isCustomIcons,
        overlayTitle,
        overlayDescription,
        title,
        applyTheme,
        onDrop,
    } = props;

    const file = fileRef || useRef(null);

    const acceptExtensions = accept ? accept.map(extension => `.${extension}`) : '';

    // Effects
    useEffect(() => {
        if (!disableDragNDrop) {
            window.addEventListener('dragenter', handleWindowDragEnter);
            window.addEventListener('dragover', handleWindowDragOver);
            window.addEventListener('dragleave', handleWindowDragLeave);
            window.addEventListener('drop', handleWindowDrop);
        }

        return () => {
            window.removeEventListener('dragenter', handleWindowDragEnter);
            window.removeEventListener('dragover', handleWindowDragOver);
            window.removeEventListener('dragleave', handleWindowDragLeave);
            window.removeEventListener('drop', handleWindowDrop);
        };
    }, [accept, disabled, compact, disableDragNDrop, onDrop]);

    // Handlers
    function handleButtonClick(e) {
        e.preventDefault();
        openFileDialog();
    }

    function openFileDialog() {
        file.current.click();
    }

    function handleFolderClick(e) {
        e.preventDefault();
        file.current.click();
    }
    function handleInputChange(e) {
        e.preventDefault();
        const files = [...e.target.files].filter(isFileAccepted);
        onDrop(files, props);

        file.current.value = '';
    }
    function handleWindowDragEnter(e) {
        if (!disabled) {
            dragTarget = e.target;
            overlayShow();
        }
    }
    function handleWindowDragOver(e) {
        e.preventDefault();
    }
    function handleWindowDragLeave(e) {
        if (!disabled) {
            if (e.target === dragTarget) {
                overlayHide();
            }
        }
    }
    function handleWindowDrop(e) {
        if (!disabled) {
            e.preventDefault();
            overlayHide();

            const files = [];
            if (e.dataTransfer.files) {
                for (let i = 0; i < e.dataTransfer.files.length; i += 1) {
                    const f = e.dataTransfer.files[i];
                    if (isFileAccepted(f)) {
                        files.push(f);
                    }
                }
            } else if (e.dataTransfer.items) {
                for (let i = 0; i < e.dataTransfer.items.length; i += 1) {
                    if (e.dataTransfer.items[i].kind === 'file') {
                        const f = e.dataTransfer.items[i].getAsFile();
                        if (isFileAccepted(f)) {
                            files.push(f);
                        }
                    }
                }
            }
            onDrop(files, props);
        }
    }

    // Methods
    function isFileAccepted(f) {
        const { extension } = getFileInfo(f);
        return Array.isArray(acceptExtensions) ? acceptExtensions.includes(`.${extension}`) : true;
    }
    function overlayShow() {
        if (!document.getElementById(OVERLAY_ID)) {
            const overlayEl = document.createElement('div');
            overlayEl.id = OVERLAY_ID;
            overlayEl.className = applyTheme('UploadFiles__Overlay');

            const titleEl = document.createElement('div');
            titleEl.className = applyTheme('UploadFiles__OverlayTitle');
            titleEl.innerHTML = renderToString(overlayTitle);
            overlayEl.appendChild(titleEl);

            const descriptionEl = document.createElement('div');
            descriptionEl.className = applyTheme('UploadFiles__OverlayDescription');
            descriptionEl.innerHTML = renderToString(overlayDescription);
            overlayEl.appendChild(descriptionEl);

            document.body.appendChild(overlayEl);
        }
    }
    function overlayHide() {
        const overlayEl = document.getElementById(OVERLAY_ID);
        if (overlayEl) {
            overlayEl.remove();
        }
    }

    // Render props
    const componentClass = applyTheme(
        cn('UploadFiles', {
            UploadFiles_compact: compact,
            UploadFiles_disabled: disabled,
        }),
    );

    const contentClass = applyTheme(
        cn('UploadFiles__Content', {
            UploadFiles__Content_compact: compact,
        }),
    );

    const titleClass = applyTheme(
        cn('UploadFiles__Title', {
            UploadFiles__Title_compact: compact,
        }),
    );

    const buttonClass = applyTheme(
        cn('UploadFiles__Button', {
            UploadFiles__Button_compact: compact,
        }),
    );

    return (
        <div className={componentClass}>
            <header className={applyTheme('UploadFiles__Header')}>
                {/* Input */}
                <input
                    accept={acceptExtensions}
                    className={applyTheme(cn('UploadFiles__File', className))}
                    disabled={disabled}
                    multiple
                    ref={file}
                    type="file"
                    onChange={handleInputChange}
                />

                {/* Title */}
                <div className={titleClass}>
                    <WToTab>
                        <H size="3">{title}</H>
                    </WToTab>
                    <WFromDesktop>
                        <H size={compact ? 4 : 3}>{title}</H>
                    </WFromDesktop>
                </div>
            </header>

            <main className={contentClass}>
                <WFromDesktop>
                    <div className={applyTheme('UploadFiles__IconFolderWithDocs')}>
                        <img src={imgFolderWithDocs} alt="Выбрать файлы из галереи" />
                    </div>
                </WFromDesktop>
                <div className={applyTheme('UploadFiles__Formats')}>
                    {accept ? accept.map(a => `.${a}`).join(', ') : ''}
                </div>
            </main>

            {/* Footer */}
            <footer className={applyTheme('UploadFiles__Footer')}>
                {compact && (
                    <div className={buttonClass}>
                        <Button
                            iconLeft={<FeatherIcon name="paperclip" />}
                            size="s"
                            onClick={handleButtonClick}
                        >
                            Добавить файлы
                        </Button>
                    </div>
                )}
                {!compact && (
                    <>
                        <WToTab>
                            <div className={applyTheme('UploadFiles__Icons')}>
                                {!isCustomIcons && (
                                    <div
                                        className={applyTheme('UploadFiles__IconFolder')}
                                        onClick={handleFolderClick}
                                    >
                                        <img src={imgFolder} alt="Выбрать файлы из галереи" />
                                    </div>
                                )}
                            </div>
                            {children && children({ openFileDialog })}
                        </WToTab>
                        <WFromDesktop>
                            <div className={buttonClass}>
                                <Button
                                    iconLeft={<FeatherIcon name="paperclip" />}
                                    size={compact ? 's' : 'm'}
                                    onClick={handleButtonClick}
                                >
                                    Добавить файлы
                                </Button>
                            </div>
                        </WFromDesktop>
                    </>
                )}
            </footer>
        </div>
    );
}

UploadFiles.propTypes = {
    accept: PropTypes.arrayOf(PropTypes.string),
    children: PropTypes.func,
    className: PropTypes.string,
    compact: PropTypes.bool,
    disabled: PropTypes.bool,
    disableDragNDrop: PropTypes.bool,
    fileRef: PropTypes.shape({}),
    isCustomIcons: PropTypes.bool,
    overlayTitle: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    overlayDescription: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    applyTheme: PropTypes.func,
    onDrop: PropTypes.func,
};
UploadFiles.defaultProps = {
    accept: null,
    children: null,
    className: '',
    compact: false,
    disabled: false,
    disableDragNDrop: false,
    fileRef: null,
    isCustomIcons: false,
    overlayTitle: '',
    overlayDescription: '',
    title: '',
    applyTheme: _ => _,
    onDrop: (files, props) => {}, // eslint-disable-line no-unused-vars
};

export default withTheme(UploadFiles);
