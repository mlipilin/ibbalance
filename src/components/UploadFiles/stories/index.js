import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean, optionsKnob } from '@storybook/addon-knobs';

// Constants
import { EXTENSION } from '../../../constants/files';

import UploadFiles from '../index';

const stories = storiesOf('UploadFiles');

stories.addDecorator(withKnobs);

const options = {
    display: 'multi-select',
};

stories.add('Default', () => (
    <UploadFiles
        accept={optionsKnob('accept', EXTENSION, [EXTENSION.PDF], options)}
        compact={boolean('compact', false)}
        disabled={boolean('disabled', false)}
        overlayTitle="Прикрепить документы к заявке"
        overlayDescription="Для файлов размером не более 20 МБ"
        disabledDragNDrop={boolean('disabledDragNDrop', true)}
        title="Добавьте файл(ы) с вашего устройства"
    />
));
