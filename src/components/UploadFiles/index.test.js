import React from 'react';
import { screen, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import FeatherIcon from '../FeatherIcon';

import UploadFiles from './index';

import { render } from '../../hoc/with-theme';

describe('Тестирование компонента UploadFiles', () => {
    it('Должен отобразиться UploadFiles', () => {
        const { container } = render(<UploadFiles />);
        const input = container.querySelector('.UploadFiles__File');
        expect(input).toBeInTheDocument();
        expect(input.type).toBe('file');
        const defaultIcons = container.querySelector('.UploadFiles__IconFolder');
        expect(defaultIcons).toBeInTheDocument();
        const title = document.querySelector('.UploadFiles__Title');
        expect(title).toBeInTheDocument();
        expect(title.textContent).toBe('');
        expect(container.firstChild).toMatchSnapshot();
    });
    it('Должен отобразиться UploadFiles с возможностью загрузить png', async () => {
        const { container } = render(<UploadFiles accept={['png']} />);
        const input = container.querySelector('.UploadFiles__File');
        expect(input.accept).toBe('.png');
        const formats = container.querySelector('.UploadFiles__Formats');
        expect(formats).toBeInTheDocument();
        expect(formats.textContent).toBe('.png');
        const file = new File(['png'], 'file.png', { type: 'image/png' });
        await fireEvent.change(input, { target: { files: [file] } });
        expect(input.files[0]).toStrictEqual(file);
        expect(input.files).toHaveLength(1);
    });
    it('Должен отобразиться UploadFiles с возможностью загрузить только png', async () => {
        const handleDrop = jest.fn();
        const { container } = render(<UploadFiles accept={['png']} onDrop={handleDrop} />);
        const input = container.querySelector('.UploadFiles__File');
        const file = new File(['xml'], 'file.xml', { type: 'application/xml' });
        await fireEvent.change(input, { target: { files: [file] } });
        expect(input.files[0]).toStrictEqual(file);
        expect(input.files).toHaveLength(1);
        expect(handleDrop).toBeCalledTimes(1);
        expect(handleDrop).toBeCalledWith([], expect.any(Object));
    });
    it('Должен отобразиться UploadFiles с возможностью загрузить png - drop', async () => {
        const handleDrop = jest.fn();
        const { container } = render(<UploadFiles accept={['png']} onDrop={handleDrop} />);
        const input = container.querySelector('.UploadFiles__File');
        const file = new File(['png'], 'file.png', { type: 'image/png' });
        await fireEvent.change(input, { target: { files: [file] } });
        expect(input.files[0]).toStrictEqual(file);
        expect(input.files).toHaveLength(1);
        expect(handleDrop).toBeCalledTimes(1);
        expect(handleDrop).toBeCalledWith([expect.any(File)], expect.any(Object));
    });
    it('Должен отобразиться UploadFiles с возможностью загрузить несколько файлов png', async () => {
        const handleDrop = jest.fn();
        const { container } = render(<UploadFiles accept={['png']} onDrop={handleDrop} />);
        const input = container.querySelector('.UploadFiles__File');
        const files = [
            new File(['png'], 'file.png', { type: 'image/png' }),
            new File(['png'], 'file.png', { type: 'image/png' }),
        ];
        await fireEvent.change(input, { target: { files } });
        expect(input.files[0]).toStrictEqual(files[0]);
        expect(input.files[1]).toStrictEqual(files[1]);
        expect(input.files).toHaveLength(2);
        expect(handleDrop).toBeCalledTimes(1);
        expect(handleDrop).toBeCalledWith([expect.any(File), expect.any(File)], expect.any(Object));
    });
    it('Должен отобразиться UploadFiles с возможностью загрузить несколько файлов только png', async () => {
        const handleDrop = jest.fn();
        const { container } = render(<UploadFiles accept={['png']} onDrop={handleDrop} />);
        const input = container.querySelector('.UploadFiles__File');
        const files = [
            new File(['png'], 'file.png', { type: 'image/png' }),
            new File(['xml'], 'file.xml', { type: 'application/xml' }),
        ];
        await fireEvent.change(input, { target: { files } });
        expect(input.files[0]).toStrictEqual(files[0]);
        expect(input.files[1]).toStrictEqual(files[1]);
        expect(input.files).toHaveLength(2);
        expect(handleDrop).toBeCalledTimes(1);
        expect(handleDrop).toBeCalledWith([expect.any(File)], expect.any(Object));
    });
    it('Должен отобразиться UploadFiles compact', () => {
        const { container } = render(<UploadFiles accept={['png']} compact />);
        const compactTitle = container.querySelector('.UploadFiles__Title_compact');
        expect(compactTitle).toBeInTheDocument();
        const compactButton = container.querySelector('.UploadFiles__Button_compact');
        expect(compactButton).toBeInTheDocument();
        const compactContent = container.querySelector('.UploadFiles__Content_compact');
        expect(compactContent).toBeInTheDocument();
    });
    it('Должен отобразиться UploadFiles disabled', () => {
        const { container } = render(<UploadFiles accept={['png']} disabled />);
        const uploadFiles = container.querySelector('.UploadFiles_disabled');
        expect(uploadFiles).toBeInTheDocument();
        const input = container.querySelector('input');
        expect(input).toBeDisabled();
    });
    it('Должен отобразиться UploadFiles disabled, не получится загрузить файлы', async () => {
        const handleDrop = jest.fn();
        const { container } = render(<UploadFiles accept={['png']} disabled onDrop={handleDrop} />);
        const input = container.querySelector('input');
        const file = new File(['png'], 'file.png', { type: 'image/png' });
        await userEvent.upload(input, [file], { target: { files: [file] } });
        expect(handleDrop).toBeCalledTimes(0);
    });
    it('Должен отобразиться UploadFiles c children', () => {
        const { container } = render(
            <UploadFiles accept={['png']}>
                {() => {
                    return (
                        <div className="UploadFiles__CustomIcons">
                            <FeatherIcon name="folder" />
                        </div>
                    );
                }}
            </UploadFiles>,
        );
        const icons = container.querySelector('.UploadFiles__CustomIcons');
        expect(icons).toBeInTheDocument();
        const icon = icons.querySelector('.feather');
        expect(icon).toHaveClass('feather-folder');
    });
    it('Должен отобразиться UploadFiles c fileRef', () => {
        const file = React.createRef();
        const { container } = render(<UploadFiles accept={['png']} fileRef={file} />);
        const input = container.querySelector('input');
        file.current.focus();
        expect(input).toHaveFocus();
        expect(input).toBe(file.current);
    });
    it('Должен отобразиться UploadFiles c isCustomIcons', () => {
        const { container } = render(<UploadFiles accept={['png']} isCustomIcons />);
        const defaultIcons = container.querySelector('.UploadFiles__IconFolder');
        expect(defaultIcons).not.toBeInTheDocument();
    });
    it('Должен отобразиться UploadFiles c overlay title по-умолчанию', async () => {
        const { container } = render(<UploadFiles accept={['png']} />);
        const input = container.querySelector('input');
        await fireEvent.dragEnter(input);
        const overlayTitle = document.querySelector('.UploadFiles__OverlayTitle');
        expect(overlayTitle).toBeInTheDocument();
        expect(overlayTitle.textContent).toBe('');
        await fireEvent.dragLeave(input);
    });

    it('Должен отобразиться UploadFiles c overlay title', async () => {
        const { container } = render(
            <UploadFiles accept={['png']} overlayTitle="Прикрепить документы к заявке" />,
        );
        const input = container.querySelector('input');
        await fireEvent.dragEnter(input);
        const overlayTitle = document.querySelector('.UploadFiles__OverlayTitle');
        expect(overlayTitle).toBeInTheDocument();
        expect(overlayTitle.textContent).toBe('Прикрепить документы к заявке');
        expect(overlayTitle).toMatchSnapshot();
        await fireEvent.dragLeave(input);
    });

    it('Должен отобразиться UploadFiles c overlay title as node', async () => {
        const { container } = render(
            <UploadFiles
                accept={['png']}
                overlayTitle={<h2 data-testid="title_as_node">Прикрепить документы к заявке</h2>}
            />,
        );
        const input = container.querySelector('input');
        await fireEvent.dragEnter(input);

        const overlayTitle = screen.getByTestId('title_as_node');
        expect(overlayTitle).toBeInTheDocument();

        expect(overlayTitle.textContent).toBe('Прикрепить документы к заявке');
        expect(overlayTitle).toMatchSnapshot();
        await fireEvent.dragLeave(input);
    });

    it('Должен отобразиться UploadFiles c overlay description по-умолчанию', async () => {
        const { container } = render(<UploadFiles accept={['png']} />);
        const input = container.querySelector('input');
        await fireEvent.dragEnter(input);
        const overlayDescription = document.querySelector('.UploadFiles__OverlayDescription');
        expect(overlayDescription).toBeInTheDocument();
        expect(overlayDescription.textContent).toBe('');
        await fireEvent.dragLeave(input);
    });

    it('Должен отобразиться UploadFiles c overlay description', async () => {
        const { container } = render(
            <UploadFiles accept={['png']} overlayDescription="Прикрепить документы к заявке" />,
        );
        const input = container.querySelector('input');
        await fireEvent.dragEnter(input);
        const overlayDescription = document.querySelector('.UploadFiles__OverlayDescription');
        expect(overlayDescription).toBeInTheDocument();
        expect(overlayDescription.textContent).toBe('Прикрепить документы к заявке');
        expect(overlayDescription).toMatchSnapshot();
        await fireEvent.dragLeave(input);
    });

    it('Должен отобразиться UploadFiles c overlay description as node', async () => {
        const { container } = render(
            <UploadFiles
                accept={['png']}
                overlayDescription={
                    <div data-testid="description_as_node">Прикрепить документы к заявке</div>
                }
            />,
        );
        const input = container.querySelector('input');
        await fireEvent.dragEnter(input);

        const overlayDescription = screen.getByTestId('description_as_node');
        expect(overlayDescription).toBeInTheDocument();

        expect(overlayDescription.textContent).toBe('Прикрепить документы к заявке');
        expect(overlayDescription).toMatchSnapshot();
        await fireEvent.dragLeave(input);
    });

    it('Должен отобразиться UploadFiles c title', () => {
        const { container } = render(
            <UploadFiles accept={['png']} title="Прикрепить документы к заявке" />,
        );
        const title = container.querySelector('.UploadFiles__Title');
        const titleForTab = title.querySelector('.WToTab');
        const titleForDesktop = title.querySelector('.WFromDesktop');
        expect(titleForTab).toBeInTheDocument();
        expect(titleForTab.textContent).toBe('Прикрепить документы к заявке');
        expect(titleForDesktop).toBeInTheDocument();
        expect(titleForDesktop.textContent).toBe('Прикрепить документы к заявке');
        expect(title).toMatchSnapshot();
    });

    it('Должен отобразиться UploadFiles c title as node', () => {
        render(
            <UploadFiles
                accept={['png']}
                title={<span data-testid="title">Прикрепить документы к заявке</span>}
            />,
        );
        const titles = screen.getAllByTestId('title');
        expect(titles[0].textContent).toBe('Прикрепить документы к заявке');
        expect(titles[1].textContent).toBe('Прикрепить документы к заявке');
    });

    it('Должен отобразиться UploadFiles useDragNDrop = true, получится загрузить файлы через drop', async () => {
        const handleDrop = jest.fn();
        const useDragNDrop = true;
        const { container } = render(
            <UploadFiles accept={['png']} useDragNDrop={useDragNDrop} onDrop={handleDrop} />,
        );
        const input = container.querySelector('input');
        const file = new File(['png'], 'file.png', { type: 'image/png' });
        await fireEvent.drop(input, {
            dataTransfer: {
                file,
            },
        });
        expect(handleDrop).toBeCalledTimes(1);
    });

    it('Должен отобразиться UploadFiles disableDragNDrop = true, не получится загрузить файлы через drop', async () => {
        const handleDrop = jest.fn();
        const { container } = render(
            <UploadFiles accept={['png']} disableDragNDrop onDrop={handleDrop} />,
        );
        const input = container.querySelector('input');
        const file = new File(['png'], 'file.png', { type: 'image/png' });
        await fireEvent.drop(input, {
            dataTransfer: {
                file,
            },
        });
        expect(handleDrop).toBeCalledTimes(0);
    });

    it('Должен отобразиться UploadFiles disableDragNDrop = false, получится загрузить файлы через openFileDialog', async () => {
        const handleDrop = jest.fn();
        const disableDragNDrop = false;
        const { container } = render(
            <UploadFiles
                accept={['png']}
                disableDragNDrop={disableDragNDrop}
                onDrop={handleDrop}
            />,
        );
        const input = container.querySelector('input');
        const file = new File(['png'], 'file.png', { type: 'image/png' });
        await fireEvent.change(input, { target: { files: [file] } });
        expect(handleDrop).toBeCalledTimes(1);
        expect(handleDrop).toBeCalledWith([expect.any(File)], expect.any(Object));
    });

    it('Должен отобразиться UploadFiles c onDrop несколько файлов только png', async () => {
        const handleDrop = jest.fn();
        const { container } = render(<UploadFiles accept={['png']} onDrop={handleDrop} />);
        const input = container.querySelector('.UploadFiles__File');
        const files = [
            new File(['png'], 'file.png', { type: 'image/png' }),
            new File(['xml'], 'file.xml', { type: 'application/xml' }),
        ];
        await fireEvent.drop(input, {
            dataTransfer: {
                files,
            },
        });
        expect(handleDrop).toBeCalledTimes(1);
        expect(handleDrop).toBeCalledWith([expect.any(File)], expect.any(Object));
    });
});
