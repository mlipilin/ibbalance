import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import Checkbox from './index';

import { render } from '../../hoc/with-theme';

describe('Тестирование компонента Checkbox', () => {
    it('Должен отобразиться Checkbox', () => {
        const { container } = render(<Checkbox />);
        expect(screen.getByRole('checkbox')).toBeInTheDocument();
        expect(screen.getByLabelText('')).toBeInTheDocument();
        expect(screen.getByRole('checkbox')).not.toBeChecked();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Checkbox c input type = "checkbox"', () => {
        const { container } = render(<Checkbox />);
        const input = container.getElementsByTagName('input')[0];
        expect(input.getAttribute('type')).toBe('checkbox');
        expect(screen.getByRole('checkbox')).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Checkbox с label = "Checkbox"', () => {
        const { container } = render(<Checkbox>Checkbox</Checkbox>);
        expect(screen.getByRole('checkbox')).toBeInTheDocument();
        expect(screen.getByLabelText('Checkbox')).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Checkbox с label = "Checkbox" и ошибкой error = "Проверка ошибки"', () => {
        const { container } = render(<Checkbox error="Проверка ошибки">Checkbox</Checkbox>);
        expect(screen.getByRole('checkbox')).toBeInTheDocument();
        expect(screen.getByLabelText('Checkbox')).toBeInTheDocument();
        expect(screen.getByText('Проверка ошибки')).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Checkbox в состоянии disabled', () => {
        const { container } = render(<Checkbox disabled>Checkbox</Checkbox>);
        expect(screen.getByRole('checkbox')).toBeInTheDocument();
        expect(screen.getByRole('checkbox')).toBeDisabled();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Checkbox в состоянии focus', () => {
        render(<Checkbox>Checkbox</Checkbox>);
        expect(screen.getByRole('checkbox')).toBeInTheDocument();
        screen.getByRole('checkbox').focus();
        expect(screen.getByRole('checkbox')).toHaveFocus();
    });

    it('Должен отобразиться Checkbox в обычном состоянии после того, как убрали фокус', () => {
        render(<Checkbox>Checkbox</Checkbox>);
        expect(screen.getByRole('checkbox')).toBeInTheDocument();
        screen.getByRole('checkbox').focus();
        expect(screen.getByRole('checkbox')).toHaveFocus();
        userEvent.tab();
        expect(screen.getByRole('checkbox')).not.toHaveFocus();
    });

    it('Должен отобразиться Checkbox в состоянии disabled не происходит focus', () => {
        render(<Checkbox disabled>Checkbox</Checkbox>);
        expect(screen.getByRole('checkbox')).toBeInTheDocument();
        expect(screen.getByRole('checkbox')).toBeDisabled();
        screen.getByRole('checkbox').focus();
        expect(screen.getByRole('checkbox')).not.toHaveFocus();
    });

    it('Должен отобразиться Checkbox в состоянии checked', () => {
        const { container } = render(
            <Checkbox className="Container__Checkbox" checked>
                Checkbox
            </Checkbox>,
        );
        const label = container.getElementsByTagName('label')[0];
        expect(label).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
        const classes = label.className;
        expect(classes).toMatch(/Checkbox__Label_checked/);
    });

    it('Должен отобразиться Checkbox c аттрибутом name = "isOptions"', () => {
        const { container } = render(
            <Checkbox className="Container__Checkbox" name="isOptions">
                Checkbox
            </Checkbox>,
        );
        const input = container.getElementsByTagName('input')[0];
        expect(input.getAttribute('name')).toBe('isOptions');
        expect(screen.getByRole('checkbox')).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Checkbox после click пользователя по нему', () => {
        const handleChange = jest.fn();
        const { container } = render(
            <Checkbox className="Container__Checkbox" onChange={handleChange}>
                Checkbox
            </Checkbox>,
        );
        const label = container.getElementsByTagName('label')[0];
        userEvent.click(label);
        expect(label).toMatchSnapshot();
        expect(handleChange).toHaveBeenCalledTimes(1);
        expect(handleChange).toHaveBeenCalledWith(true, expect.any(Object));
    });

    it('Должен отобразиться Checkbox в состоянии size = "m" по-умолчанию', () => {
        const { container } = render(<Checkbox className="Container__Checkbox">Checkbox</Checkbox>);
        const label = container.getElementsByTagName('label')[0];
        expect(label).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
        const classes = label.className;
        expect(classes).toMatch(/Checkbox__Label_size_m/);
    });

    it('Должен отобразиться Checkbox в состоянии size = "s"', () => {
        const { container } = render(
            <Checkbox className="Container__Checkbox" size="s">
                Checkbox
            </Checkbox>,
        );
        const label = container.getElementsByTagName('label')[0];
        expect(label).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
        const classes = label.className;
        expect(classes).toMatch(/Checkbox__Label_size_s/);
    });

    it('Должен отобразиться Checkbox в состоянии size = "m"', () => {
        const { container } = render(
            <Checkbox className="Container__Checkbox" size="m">
                Checkbox
            </Checkbox>,
        );
        const label = container.getElementsByTagName('label')[0];
        expect(label).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
        const classes = label.className;
        expect(classes).toMatch(/Checkbox__Label_size_m/);
    });

    it('Должен отобразиться Checkbox в состоянии size = "l"', () => {
        const { container } = render(
            <Checkbox className="Container__Checkbox" size="l">
                Checkbox
            </Checkbox>,
        );
        const label = container.getElementsByTagName('label')[0];
        expect(label).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
        const classes = label.className;
        expect(classes).toMatch(/Checkbox__Label_size_l/);
    });

    it('Должен отобразиться Checkbox в состоянии type = "primary" по-умолчанию', () => {
        const { container } = render(<Checkbox className="Container__Checkbox">Checkbox</Checkbox>);
        const label = container.getElementsByTagName('label')[0];
        expect(label).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
        const classes = label.className;
        expect(classes).toMatch(/Checkbox__Label_type_primary/);
    });

    it('Должен отобразиться Checkbox в состоянии type = "primary"', () => {
        const { container } = render(
            <Checkbox className="Container__Checkbox" type="primary">
                Checkbox
            </Checkbox>,
        );
        const label = container.getElementsByTagName('label')[0];
        expect(label).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
        const classes = label.className;
        expect(classes).toMatch(/Checkbox__Label_type_primary/);
    });

    it('Должен отобразиться Checkbox в состоянии type = "secondary"', () => {
        const { container } = render(
            <Checkbox className="Container__Checkbox" type="secondary">
                Checkbox
            </Checkbox>,
        );
        const label = container.getElementsByTagName('label')[0];
        expect(label).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
        const classes = label.className;
        expect(classes).toMatch(/Checkbox__Label_type_secondary/);
    });

    it('Должен отобразиться Checkbox в состоянии checked после focus+space', async () => {
        let checkedValue = false;
        const onChange = () => {
            checkedValue = !checkedValue;
        };
        const { container, rerender } = render(
            <Checkbox checked={checkedValue} onChange={onChange}>
                Checkbox
            </Checkbox>,
        );
        const checkbox = screen.getByRole('checkbox');
        expect(checkbox).toBeInTheDocument();
        checkbox.focus();
        userEvent.keyboard('{space}');

        rerender(
            <Checkbox onChange={onChange} checked={checkedValue}>
                Checkbox
            </Checkbox>,
        );
        expect(container.firstChild).toMatchSnapshot();
        const label = container.getElementsByTagName('label')[0];
        expect(label).toHaveClass('Checkbox__Label_checked');
        const isCheckedClass = label.classList.contains('Checkbox__Label_checked');
        expect(isCheckedClass).toBeTruthy();
    });

    it('Должен отобразиться Checkbox c классом "Checkbox__Style"', () => {
        const { container } = render(<Checkbox className="Checkbox__Style">Checkbox</Checkbox>);
        const checkbox = container.querySelector('.Checkbox');
        expect(checkbox.classList.contains('Checkbox__Style')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });
});
