# Размер чекбокса

Свойство size может принимать одно из следующих значений: `s`, `m` и `l`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Checkbox, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/ozon/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Checkbox size="l">Checkbox L</Checkbox>
    <Checkbox size="m">Checkbox M</Checkbox>
    <Checkbox size="s">Checkbox S</Checkbox>
  </ThemeProvider>,
  document.getElementById('root')
);
```
