# Тип чекбокса

Свойство type может принимать одно из следующих значений: `primary` и `secondary`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Checkbox, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/vkusvill/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Checkbox type="primary">Primary checkbox</Checkbox>
    <Checkbox type="secondary">Secondary checkbox</Checkbox>
  </ThemeProvider>,
  document.getElementById('root')
);
```
