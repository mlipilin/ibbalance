# Наличие ошибки

В поле `error` можно передать текст ошибки

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Checkbox, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/vkusvill/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Checkbox error="Скидка за быстрый выход на сделку не применима для зарплатных клиентов">
        Скидка за быстрый выход на сделку
    </Checkbox>
  </ThemeProvider>,
  document.getElementById('root')
);
```
