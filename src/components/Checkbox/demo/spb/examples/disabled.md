# "disabled" состояние

`true`: чекбокс становится 'disabled' (выключенный)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Checkbox, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/spb/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Checkbox disabled>Disabled чекбокс</Checkbox>
  </ThemeProvider>,
  document.getElementById('root')
);
```
