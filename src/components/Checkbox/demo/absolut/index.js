import React from 'react';

// Components
import Checkbox from '../../index';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';

// Examples
import disabled from './examples/disabled.md';
import error from './examples/error.md';
import size from './examples/size.md';

// HOC
import { withState } from '../../../../hoc';

// Wrappers
import { Col, Row } from '../../../../wrappers/WGrid';

const CheckboxWithState = withState(Checkbox, 'checked', false);

const DemoCheckbox = () => {
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>

            <div id="example">
                <Row
                    cols={{ mobile: [1], tab: [1, 1], desktop: [1], largeDesktop: [1, 1] }}
                    gap="20px"
                >
                    <Col>
                        <Example readme={size}>
                            <CheckboxWithState size="l">Checkbox L</CheckboxWithState>
                            <CheckboxWithState size="m">Checkbox M</CheckboxWithState>
                            <CheckboxWithState size="s">Checkbox S</CheckboxWithState>
                        </Example>
                        <Example readme={error}>
                            <CheckboxWithState error="Скидка за быстрый выход на сделку не применима для зарплатных клиентов">
                                Скидка за быстрый выход на сделку
                            </CheckboxWithState>
                        </Example>
                    </Col>
                    <Col>
                        <Example readme={disabled}>
                            <CheckboxWithState disabled>Checkbox disabled</CheckboxWithState>
                        </Example>
                    </Col>
                </Row>
            </div>
        </DemoComponent>
    );
};

export default DemoCheckbox;
