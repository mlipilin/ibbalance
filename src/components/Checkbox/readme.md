## Usage

```jsx
<Checkbox size="s" type="primary" />
```

With content:

```jsx
<Checkbox size="l" type="secondary">
    Check me
</Checkbox>
```

## Props

Prop name | Prop type | Default value
--- | --- | ---
*checked*|bool|`false`
*disabled*|bool|`false`
*size*|`s`, `m`, `l`|`m`
*type*|`primary`, `secondary`|`primary`
*onChange*|func|`_ => _`

## CSS Selectors

```css
.Checkbox {}

/* Label */
.Checkbox__Label {}
.Checkbox__Label:after {}
.Checkbox__Label:before {}

/* Label: By size */
.Checkbox__Label_size_s {}
.Checkbox__Label_size_m {}
.Checkbox__Label_size_l {}

/* Label: By type */
.Checkbox__Label_type_primary {}
.Checkbox__Label_type_secondary {}

/* Label: Booleans */
.Checkbox__Label_checked {}
.Checkbox__Label_disabled {}
```
