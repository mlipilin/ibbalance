import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

// Constants
import { SIZES, TYPES } from '../../constants/props';

import { withTheme } from '../../theme-provider';

@withTheme
class Checkbox extends Component {
    handleChange = e => {
        const { onChange } = this.props;
        onChange(e.target.checked, this.props);
    };

    render() {
        const {
            className,
            checked,
            children,
            disabled,
            error,
            field,
            size,
            type,
            applyTheme,
            ...otherProps
        } = this.props;

        const componentClass = applyTheme(cn('Checkbox'), className);
        const labelClass = applyTheme(
            cn('Checkbox__Label', `Checkbox__Label_size_${size}`, `Checkbox__Label_type_${type}`, {
                Checkbox__Label_checked: checked,
                Checkbox__Label_disabled: disabled,
            }),
        );
        const errorClass = applyTheme(cn('Checkbox__Error', `Checkbox__Error_size_${size}`));

        return (
            <div className={componentClass}>
                <label className={labelClass}>
                    <input
                        {...otherProps}
                        checked={checked}
                        disabled={disabled}
                        type="checkbox"
                        onChange={this.handleChange}
                    />
                    {children}
                </label>
                {/* Error */}
                {!!error && <div className={errorClass}>{error}</div>}
            </div>
        );
    }
}

Checkbox.propTypes = {
    checked: PropTypes.bool,
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    className: PropTypes.string,
    disabled: PropTypes.bool,
    error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool, PropTypes.object]),
    field: PropTypes.string,
    size: PropTypes.oneOf([SIZES.S, SIZES.M, SIZES.L]),
    type: PropTypes.oneOf([TYPES.PRIMARY, TYPES.SECONDARY]),
    applyTheme: PropTypes.func,
    onChange: PropTypes.func,
};

Checkbox.defaultProps = {
    checked: false,
    children: null,
    className: '',
    disabled: false,
    error: null,
    field: null,
    size: SIZES.M,
    type: TYPES.PRIMARY,
    applyTheme: _ => _,
    onChange: (checked, props) => {}, // eslint-disable-line no-unused-vars
};

export default Checkbox;
