import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean, select } from '@storybook/addon-knobs';

// Constants
import { LOCAL_STORAGE_THEME_KEY } from '../../../constants/local-storage';
import { SIZES, TYPES } from '../../../constants/props';
import { THEMES } from '../../../constants/themes';

// HOC
import { withState } from '../../../hoc';

import Switch from '../index';

import notes from '../readme.md';

const SwitchWithState = withState(Switch, 'checked', false);
const theme = localStorage.getItem(LOCAL_STORAGE_THEME_KEY);

let sizesList = [];
let typesList = [];

switch (theme) {
    case THEMES.DEFAULT:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        typesList = [TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    case THEMES.ALFA:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        typesList = [TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    case THEMES.ABSOLUT:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        typesList = [TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    case THEMES.SPB:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        typesList = [TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    case THEMES.TKB:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        typesList = [TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    case THEMES.TKB_LKZ:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        typesList = [TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    case THEMES.KYC:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        typesList = [TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    case THEMES.KYC_LANDING:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        typesList = [TYPES.PRIMARY, TYPES.SECONDARY];
        break;
    default:
        break;
}

const stories = storiesOf('Switch', module);

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories
    .add(
        'Default',
        () => (
            <SwitchWithState
                disabled={boolean('Disabled', false)}
                size={select('Size', sizesList, SIZES.M)}
                type={select('Type', typesList, TYPES.PRIMARY)}
            />
        ),
        { notes },
    )
    .add(
        'With text',
        () => (
            <SwitchWithState
                disabled={boolean('Disabled', false)}
                size={select('Size', sizesList, SIZES.M)}
                type={select('Type', typesList, TYPES.PRIMARY)}
            >
                Check me
            </SwitchWithState>
        ),
        { notes },
    );
