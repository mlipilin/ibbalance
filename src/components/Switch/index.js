import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

// Constants
import { SIZES, TYPES } from '../../constants/props';

import { withTheme } from '../../theme-provider';

@withTheme
class Switch extends Component {
    handleChange = e => {
        const { onChange } = this.props;
        onChange(e.target.checked, this.props);
    };

    render() {
        const {
            checked,
            children,
            className,
            disabled,
            size,
            type,
            applyTheme,
            ...otherProps
        } = this.props;

        const componentClass = applyTheme(cn('Switch'), className);
        const labelClass = applyTheme(
            cn('Switch__Label', `Switch__Label_size_${size}`, `Switch__Label_type_${type}`, {
                Switch__Label_checked: checked,
                Switch__Label_disabled: disabled,
            }),
        );
        const contentClass = applyTheme(cn('Switch__Content', `Switch__Content_size_${size}`));

        return (
            <div className={componentClass}>
                <label className={labelClass}>
                    <input
                        {...otherProps}
                        checked={checked}
                        disabled={disabled}
                        type="checkbox"
                        onChange={this.handleChange}
                    />
                </label>

                {/* Content */}
                {!!children && <div className={contentClass}>{children}</div>}
            </div>
        );
    }
}

Switch.propTypes = {
    checked: PropTypes.bool,
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    className: PropTypes.string,
    disabled: PropTypes.bool,
    field: PropTypes.string,
    size: PropTypes.oneOf([SIZES.S, SIZES.M, SIZES.L]),
    type: PropTypes.oneOf([TYPES.PRIMARY, TYPES.SECONDARY]),
    applyTheme: PropTypes.func,
    onChange: PropTypes.func,
};

Switch.defaultProps = {
    checked: false,
    children: null,
    className: '',
    disabled: false,
    field: null,
    size: SIZES.M,
    type: TYPES.PRIMARY,
    applyTheme: _ => _,
    onChange: (checked, props) => {}, // eslint-disable-line no-unused-vars
};

export default Switch;
