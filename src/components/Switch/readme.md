## Usage

```jsx
<Switch size="l" type="primary" />
```

With content:

```jsx
<Switch size="s" type="secondary">
    Check me
</Switch>
```

## Props

Prop name | Prop type | Default value
--- | --- | ---
*checked*|bool|`false`
*disabled*|bool|`false`
*size*|`s`, `m`, `l`|`m`
*type*|`primary`, `secondary`|`primary`
*onChange*|func|`_ => _`

## CSS Selectors

```css
.Switch {}

/* Label */
.Switch__Label {}

/* Label: Size */
.Switch__Label_size_s {}
.Switch__Label_size_m {}
.Switch__Label_size_l {}

/* Label: Type */
.Switch__Label_type_primary {}
.Switch__Label_type_secondary {}

/* Label: Booleans */
.Switch__Label_checked {}
.Switch__Label_disabled {}

/* Content */
.Switch__Content {}

/* Content: Size */
.Switch__Content_size_s {}
.Switch__Content_size_m {}
.Switch__Content_size_l {}
```
