import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import Switch from './index';

import { render } from '../../hoc/with-theme';

describe('Тестирование компонента Switch', () => {
    it('Должен отобразиться Switch', () => {
        const { container } = render(<Switch />);
        expect(screen.getByRole('checkbox')).toBeInTheDocument();
        expect(screen.getByLabelText('')).toBeInTheDocument();
        expect(screen.getByRole('checkbox')).not.toBeChecked();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Switch с размером "m" по-умолчанию', () => {
        const { container } = render(<Switch />);
        const label = container.querySelector('label');
        expect(label.classList.contains('Switch__Label_size_m')).toBeTruthy();
    });

    it('Должен отобразиться Switch с размером "s"', () => {
        const { container } = render(<Switch size="s" />);
        const label = container.querySelector('label');
        expect(label.classList.contains('Switch__Label_size_s')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Switch с размером "m"', () => {
        const { container } = render(<Switch size="m" />);
        const label = container.querySelector('label');
        expect(label.classList.contains('Switch__Label_size_m')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Switch с размером "l"', () => {
        const { container } = render(<Switch size="l" />);
        const label = container.querySelector('label');
        expect(label.classList.contains('Switch__Label_size_l')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Switch с типом "primary" по-умолчанию', () => {
        const { container } = render(<Switch />);
        const label = container.querySelector('label');
        expect(label.classList.contains('Switch__Label_type_primary')).toBeTruthy();
    });

    it('Должен отобразиться Switch с типом "primary"', () => {
        const { container } = render(<Switch type="primary" />);
        const label = container.querySelector('label');
        expect(label.classList.contains('Switch__Label_type_primary')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Switch с типом "secondary"', () => {
        const { container } = render(<Switch type="secondary" />);
        const label = container.querySelector('label');
        expect(label.classList.contains('Switch__Label_type_secondary')).toBeTruthy();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Switch в состоянии checked', () => {
        const { container } = render(<Switch checked />);
        const label = container.querySelector('label');
        expect(label.classList.contains('Switch__Label_checked')).toBeTruthy();
        expect(screen.getByRole('checkbox')).toBeChecked();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Switch в состоянии disabled', () => {
        const onChange = jest.fn();
        const { container } = render(<Switch disabled onChange={onChange} />);
        const label = container.querySelector('label');
        expect(label.classList.contains('Switch__Label_disabled')).toBeTruthy();
        expect(screen.getByRole('checkbox')).toBeDisabled();
        userEvent.click(label);
        expect(onChange).toHaveBeenCalledTimes(0);
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Switch в состоянии checked после click пользователя ', async () => {
        let checkedValue = false;
        const onChange = () => {
            checkedValue = !checkedValue;
        };
        const { container, rerender } = render(
            <Switch onChange={onChange} checked={checkedValue}>
                Click
            </Switch>,
        );
        const label = container.querySelector('label');
        expect(label.classList.contains('Switch__Label_checked')).toBeFalsy();
        await userEvent.click(label);
        expect(checkedValue).toBeTruthy();
        rerender(<Switch onChange={onChange} checked={checkedValue} />);
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Switch в состоянии checked после {space} пользователя ', async () => {
        let checkedValue = false;
        const onChange = () => {
            checkedValue = !checkedValue;
        };
        const { container, rerender } = render(
            <Switch checked={checkedValue} key="render" onChange={onChange}>
                Click
            </Switch>,
        );
        const input = screen.getByRole('checkbox');
        const label = container.querySelector('label');
        expect(label.classList.contains('Switch__Label_checked')).toBeFalsy();
        input.focus();
        userEvent.keyboard('{space}');
        expect(checkedValue).toBeTruthy();
        rerender(<Switch onChange={onChange} checked={checkedValue} />);
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Switch в состоянии focus ', () => {
        const onFocus = jest.fn();
        const { container } = render(<Switch onFocus={onFocus} />);
        const input = container.querySelector('input');
        input.focus();
        expect(onFocus).toHaveBeenCalledTimes(1);
    });

    it('Должен отобразиться Switch с атрибутом name ', () => {
        const { container } = render(<Switch name="theme" />);
        const input = container.querySelector('input');
        expect(input.getAttribute('name')).toBe('theme');
    });

    it('Должен отобразиться Switch с классом "Switch__Style" ', () => {
        const { container } = render(<Switch className="Switch__Style" />);
        const label = container.querySelector('.Switch__Style');
        expect(label).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Switch co свойством "field" ', () => {
        const { container } = render(<Switch field="filedName" />);
        const input = container.querySelector('input');
        expect(input.getAttribute('field')).toBe('filedName');
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Switch с контентом ', () => {
        const { container } = render(
            <Switch>
                <div className="Switch__ContentName">Название</div>
            </Switch>,
        );
        const content = container.querySelector('.Switch__Content');
        const innerContent = content.querySelector('.Switch__ContentName');
        expect(innerContent.textContent).toBe('Название');
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться Switch проверка события click ', () => {
        const onChange = jest.fn();
        const { container } = render(
            <Switch onChange={onChange}>
                <div className="Switch__ContentName">Название</div>
            </Switch>,
        );
        const label = container.querySelector('label');
        expect(label).toBeInTheDocument();
        label.focus();
        userEvent.click(label);
        expect(onChange).toHaveBeenCalledTimes(1);
        expect(onChange).toBeCalledWith(true, expect.any(Object));
    });
});
