# "disabled" состояние

`true`: свитчер становится 'disabled' (выключенный)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Switch, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/tkb_lkz/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Switch disabled>Disabled свитчер</Switch>
  </ThemeProvider>,
  document.getElementById('root')
);
```
