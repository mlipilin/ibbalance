# Размер свитчера

Свойство size может принимать одно из следующих значений: `s`, `m` и `l`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Switch, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/default/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Switch size="l">Switch L</Switch>
    <Switch size="m">Switch M</Switch>
    <Switch size="s">Switch S</Switch>
  </ThemeProvider>,
  document.getElementById('root')
);
```
