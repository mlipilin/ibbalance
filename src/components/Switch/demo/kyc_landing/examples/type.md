# Тип свитчера

Свойство type может принимать одно из следующих значений: `primary` и `secondary`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Switch, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/alfa/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Switch type="primary">Primary свитчер</Switch>
    <Switch type="secondary">Secondary свитчер</Switch>
  </ThemeProvider>,
  document.getElementById('root')
);
```
