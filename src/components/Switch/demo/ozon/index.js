import React from 'react';

// Components
import Switch from '../../index';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';

// Examples
import disabled from './examples/disabled.md';
import size from './examples/size.md';
import type from './examples/type.md';

// HOC
import { withState } from '../../../../hoc';

// Wrappers
import { Col, Row } from '../../../../wrappers/WGrid';

const SwitchWithState = withState(Switch, 'checked', false);

const DemoSwitch = () => {
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>

            <div id="example">
                <Row
                    cols={{ mobile: [1], tab: [1, 1], desktop: [1], largeDesktop: [1, 1] }}
                    gap="20px"
                >
                    <Col>
                        <Example readme={size}>
                            <SwitchWithState size="l">Switch L</SwitchWithState>
                            <SwitchWithState size="m">Switch M</SwitchWithState>
                            <SwitchWithState size="s">Switch S</SwitchWithState>
                        </Example>
                        <Example readme={type}>
                            <SwitchWithState type="primary">Switch primary</SwitchWithState>
                            <SwitchWithState type="secondary">Switch secondary</SwitchWithState>
                        </Example>
                    </Col>
                    <Col>
                        <Example readme={disabled}>
                            <SwitchWithState disabled>Switch disabled</SwitchWithState>
                        </Example>
                    </Col>
                </Row>
            </div>
        </DemoComponent>
    );
};

export default DemoSwitch;
