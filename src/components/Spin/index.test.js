import React from 'react';

import Spin from './index';

import { render } from '../../hoc/with-theme';

describe('Тестирование компонента Spin', () => {
    it('Должен отобразиться Spin с параметрами по-умолчанию', () => {
        const { container } = render(<Spin />);
        const spin = container.querySelector('.Spin');
        expect(spin).toBeInTheDocument();
        expect(spin).toHaveClass('Spin_size_m');
        expect(spin).toHaveClass('Spin_type_default');
        expect(container.firstChild).toMatchSnapshot();
    });
    it('Должен отобразиться Spin с классом "Spin__Styled"', () => {
        const { container } = render(<Spin className="Spin__Styled" />);
        const spin = container.querySelector('.Spin');
        expect(spin).toBeInTheDocument();
        expect(spin).toHaveClass('Spin__Styled');
    });
    it('Должен отобразиться Spin с размером size="xs"', () => {
        const { container } = render(<Spin size="xs" />);
        const spin = container.querySelector('.Spin');
        expect(spin).toHaveClass('Spin_size_xs');
    });
    it('Должен отобразиться Spin с размером size="s"', () => {
        const { container } = render(<Spin size="s" />);
        const spin = container.querySelector('.Spin');
        expect(spin).toHaveClass('Spin_size_s');
    });
    it('Должен отобразиться Spin с размером size="m"', () => {
        const { container } = render(<Spin size="m" />);
        const spin = container.querySelector('.Spin');
        expect(spin).toHaveClass('Spin_size_m');
    });
    it('Должен отобразиться Spin с размером size="l"', () => {
        const { container } = render(<Spin size="l" />);
        const spin = container.querySelector('.Spin');
        expect(spin).toHaveClass('Spin_size_l');
    });
    it('Должен отобразиться Spin с размером size="xl"', () => {
        const { container } = render(<Spin size="xl" />);
        const spin = container.querySelector('.Spin');
        expect(spin).toHaveClass('Spin_size_xl');
    });
    it('Должен отобразиться Spin с размером size="xxl"', () => {
        const { container } = render(<Spin size="xxl" />);
        const spin = container.querySelector('.Spin');
        expect(spin).toHaveClass('Spin_size_xxl');
    });
    it('Должен отобразиться Spin с типом primary', () => {
        const { container } = render(<Spin type="primary" />);
        const spin = container.querySelector('.Spin');
        expect(spin).toHaveClass('Spin_type_primary');
    });
    it('Должен отобразиться Spin с типом secondary', () => {
        const { container } = render(<Spin type="secondary" />);
        const spin = container.querySelector('.Spin');
        expect(spin).toHaveClass('Spin_type_secondary');
    });
    it('Должен отобразиться Spin с типом danger', () => {
        const { container } = render(<Spin type="danger" />);
        const spin = container.querySelector('.Spin');
        expect(spin).toHaveClass('Spin_type_danger');
    });
    it('Должен отобразиться Spin с типом success', () => {
        const { container } = render(<Spin type="success" />);
        const spin = container.querySelector('.Spin');
        expect(spin).toHaveClass('Spin_type_success');
    });
    it('Должен отобразиться Spin с типом info', () => {
        const { container } = render(<Spin type="info" />);
        const spin = container.querySelector('.Spin');
        expect(spin).toHaveClass('Spin_type_info');
    });
    it('Должен отобразиться Spin с типом warning', () => {
        const { container } = render(<Spin type="warning" />);
        const spin = container.querySelector('.Spin');
        expect(spin).toHaveClass('Spin_type_warning');
    });
});
