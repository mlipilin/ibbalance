## Usage

```jsx
<Spin size="s" type="primary" />
```

## Props

Prop name | Prop type | Default value
--- | --- | ---
*size*|`xs`, `s`, `m`, `l`, `xl`, `xxl`|`m`
*type*|`default`, `primary`, `secondary`, `danger`, `success`, `info`, `warning`|`default`

## CSS Selectors

```css
.Spin {}

/* Size */
.Spin_size_xs {}
.Spin_size_s {}
.Spin_size_m {}
.Spin_size_l {}
.Spin_size_xl {}
.Spin_size_xxl {}

/* Type */
.Spin_type_default {}
.Spin_type_primary {}
.Spin_type_secondary {}
.Spin_type_danger {}
.Spin_type_success {}
.Spin_type_info {}
.Spin_type_warning {}
```
