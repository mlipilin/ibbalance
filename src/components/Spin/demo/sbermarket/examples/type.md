# Тип спиннера

Свойство type может принимать одно из следующих значений: `default`, `primary`, `secondary`, `danger`, `success`, `info` и `warning`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Spin, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/sbermarket/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Spin type="default" />
    <Spin type="primary" />
    <Spin type="secondary" />
    <Spin type="danger" />
    <Spin type="success" />
    <Spin type="info" />
    <Spin type="warning" />
  </ThemeProvider>,
  document.getElementById('root')
);
```
