import React from 'react';

// Components
import Spin from '../../index';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';

// Examples
import size from './examples/size.md';
import type from './examples/type.md';

// Wrappers
import { Col, Row } from '../../../../wrappers/WGrid';

const DemoSpin = () => {
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>

            <div id="example">
                <Row
                    cols={{ mobile: [1], tab: [1, 1], desktop: [1], largeDesktop: [1, 1] }}
                    gap="20px"
                >
                    <Col>
                        <Example readme={size}>
                            <div style={{ display: 'flex' }}>
                                <Spin size="xxl" />
                                <Spin size="xl" />
                                <Spin size="l" />
                                <Spin size="m" />
                                <Spin size="s" />
                                <Spin size="xs" />
                            </div>
                        </Example>
                    </Col>
                    <Col>
                        <Example readme={type}>
                            <div style={{ display: 'flex' }}>
                                <Spin type="default" />
                                <Spin type="primary" />
                                <Spin type="secondary" />
                                <Spin type="danger" />
                                <Spin type="success" />
                                <Spin type="info" />
                                <Spin type="warning" />
                            </div>
                        </Example>
                    </Col>
                </Row>
            </div>
        </DemoComponent>
    );
};

export default DemoSpin;
