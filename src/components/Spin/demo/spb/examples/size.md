# Размер спиннера

Свойство size может принимать одно из следующих значений: `xs`, `s`, `m`, `l`, `xl` и `xxl`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Spin, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/spb/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Spin size="xxl" />
    <Spin size="xl" />
    <Spin size="l" />
    <Spin size="m" />
    <Spin size="s" />
    <Spin size="xs" />
  </ThemeProvider>,
  document.getElementById('root')
);
```
