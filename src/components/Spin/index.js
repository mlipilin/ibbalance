import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

// Constants
import { SIZES, TYPES } from '../../constants/props';

import { withTheme } from '../../theme-provider';

@withTheme
class Spin extends Component {
    render() {
        const { className, size, type, applyTheme } = this.props;

        const componentClass = applyTheme(
            cn('Spin', `Spin_size_${size}`, `Spin_type_${type}`),
            className,
        );

        return <div className={componentClass} />;
    }
}

Spin.propTypes = {
    className: PropTypes.string,
    size: PropTypes.oneOf([SIZES.XS, SIZES.S, SIZES.M, SIZES.L, SIZES.XL, SIZES.XXL]),
    type: PropTypes.oneOf([
        TYPES.DEFAULT,
        TYPES.PRIMARY,
        TYPES.SECONDARY,
        TYPES.DANGER,
        TYPES.SUCCESS,
        TYPES.INFO,
        TYPES.WARNING,
    ]),
    applyTheme: PropTypes.func,
};

Spin.defaultProps = {
    className: '',
    size: SIZES.M,
    type: TYPES.DEFAULT,
    applyTheme: _ => _,
};

export default Spin;
