import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, select } from '@storybook/addon-knobs';

// Constants
import { LOCAL_STORAGE_THEME_KEY } from '../../../constants/local-storage';
import { SIZES, TYPES } from '../../../constants/props';
import { THEMES } from '../../../constants/themes';

import Spin from '../index';

import notes from '../readme.md';

const theme = localStorage.getItem(LOCAL_STORAGE_THEME_KEY);

let sizesList = [];
let typesList = [];

switch (theme) {
    case THEMES.DEFAULT:
        sizesList = [SIZES.XS, SIZES.S, SIZES.M, SIZES.L, SIZES.XL, SIZES.XXL];
        typesList = Object.values(TYPES);
        break;
    case THEMES.ALFA:
        sizesList = [SIZES.XS, SIZES.S, SIZES.M, SIZES.L, SIZES.XL, SIZES.XXL];
        typesList = Object.values(TYPES);
        break;
    case THEMES.SPB:
        sizesList = [SIZES.XS, SIZES.S, SIZES.M, SIZES.L, SIZES.XL, SIZES.XXL];
        typesList = Object.values(TYPES);
        break;
    case THEMES.TKB:
        sizesList = [SIZES.XS, SIZES.S, SIZES.M, SIZES.L, SIZES.XL, SIZES.XXL];
        typesList = Object.values(TYPES);
        break;
    case THEMES.TKB_LKZ:
        sizesList = [SIZES.XS, SIZES.S, SIZES.M, SIZES.L, SIZES.XL, SIZES.XXL];
        typesList = Object.values(TYPES);
        break;
    case THEMES.KYC:
        sizesList = [SIZES.XS, SIZES.S, SIZES.M, SIZES.L, SIZES.XL, SIZES.XXL];
        typesList = Object.values(TYPES);
        break;
    case THEMES.KYC_LANDING:
        sizesList = [SIZES.XS, SIZES.S, SIZES.M, SIZES.L, SIZES.XL, SIZES.XXL];
        typesList = Object.values(TYPES);
        break;
    default:
        break;
}

const stories = storiesOf('Spin', module);

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories.add(
    'Default',
    () => (
        <Spin
            size={select('Size', sizesList, SIZES.M)}
            type={select('Type', typesList, TYPES.DEFAULT)}
        />
    ),
    { notes },
);
