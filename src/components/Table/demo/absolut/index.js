import React from 'react';

// Components
import Table from '../../index';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';

// Examples
import basic from './examples/basic.md';
import bordered from './examples/bordered.md';
import hovered from './examples/hovered.md';
import striped from './examples/striped.md';

const DemoButton = () => {
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>

            <div id="example">
                <Example readme={basic}>
                    <Table>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Имя</th>
                                <th>Проекты</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Михаил Липилин</td>
                                <td>Альфа, ТКБ</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Руслан Зарипов</td>
                                <td>Абсолют</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Алёна Кравец</td>
                                <td>Абсолют</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Никита Наумов</td>
                                <td>Пик</td>
                            </tr>
                        </tbody>
                    </Table>
                </Example>
                <Example readme={bordered}>
                    <Table bordered>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Имя</th>
                                <th>Проекты</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Михаил Липилин</td>
                                <td>Альфа, ТКБ</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Руслан Зарипов</td>
                                <td>Абсолют</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Алёна Кравец</td>
                                <td>Абсолют</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Никита Наумов</td>
                                <td>Пик</td>
                            </tr>
                        </tbody>
                    </Table>
                </Example>
                <Example readme={hovered}>
                    <Table hovered>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Имя</th>
                                <th>Проекты</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Михаил Липилин</td>
                                <td>Альфа, ТКБ</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Руслан Зарипов</td>
                                <td>Абсолют</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Алёна Кравец</td>
                                <td>Абсолют</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Никита Наумов</td>
                                <td>Пик</td>
                            </tr>
                        </tbody>
                    </Table>
                </Example>
                <Example readme={striped}>
                    <Table striped>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Имя</th>
                                <th>Проекты</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Михаил Липилин</td>
                                <td>Альфа, ТКБ</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Руслан Зарипов</td>
                                <td>Абсолют</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Алёна Кравец</td>
                                <td>Абсолют</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Никита Наумов</td>
                                <td>Пик</td>
                            </tr>
                        </tbody>
                    </Table>
                </Example>
            </div>
        </DemoComponent>
    );
};

export default DemoButton;
