# Bordered

Если нужно добавить каждой ячейке свойство `border`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Table, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/absolut/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Table bordered>
      <thead>
        <tr>
          <th>#</th>
          <th>Имя</th>
          <th>Проекты</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td>Михаил Липилин</td>
          <td>Альфа, ТКБ</td>
        </tr>
        <tr>
          <td>2</td>
          <td>Руслан Зарипов</td>
          <td>Абсолют</td>
        </tr>
        <tr>
          <td>3</td>
          <td>Алёна Кравец</td>
          <td>Абсолют</td>
        </tr>
        <tr>
          <td>4</td>
          <td>Никита Наумов</td>
          <td>Пик</td>
        </tr>
      </tbody>
    </Table>
  </ThemeProvider>,
  document.getElementById('root')
);
```
