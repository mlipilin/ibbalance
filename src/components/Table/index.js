import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import { withTheme } from '../../theme-provider';

@withTheme
class Table extends Component {
    render() {
        const {
            bordered,
            children,
            className,
            hovered,
            striped,
            applyTheme,
            ...otherProps
        } = this.props;

        const componentClass = applyTheme(
            cn('Table', {
                Table_bordered: bordered,
                Table_hovered: hovered,
                Table_striped: striped,
            }),
            className,
        );

        return (
            <table className={componentClass} {...otherProps}>
                {children}
            </table>
        );
    }
}

Table.propTypes = {
    bordered: PropTypes.bool,
    className: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    hovered: PropTypes.bool,
    striped: PropTypes.bool,
    applyTheme: PropTypes.func,
};

Table.defaultProps = {
    bordered: false,
    className: '',
    children: null,
    hovered: false,
    striped: false,
    applyTheme: _ => _,
};

export default Table;
