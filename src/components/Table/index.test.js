import React from 'react';

import { screen } from '@testing-library/dom';
import { render } from '../../hoc/with-theme';
import Table from './index';

describe('Table', () => {
    it('ничего не передали -> есть класс Table', () => {
        const { container } = render(<Table />);
        expect(container.firstChild).toHaveClass('Table');
    });
    it('передали bordered -> есть класс Table_bordered', () => {
        const { container } = render(<Table bordered />);
        expect(container.firstChild).toHaveClass('Table_bordered');
    });
    it('передали bordered=false -> нет класса Table_bordered', () => {
        const { container } = render(<Table bordered={false} />);
        expect(container.firstChild).not.toHaveClass('Table_bordered');
    });
    it('передали className -> есть переданный класс', () => {
        const CLASSNAME = 'class';
        const { container } = render(<Table className={CLASSNAME} />);
        expect(container.firstChild).toHaveClass(CLASSNAME);
    });
    it('передали children -> есть переданные children as node', () => {
        const CHILDREN = 'test-children';
        render(
            <Table>
                <thead data-testid={CHILDREN}>
                    <tr>
                        <th>{CHILDREN}</th>
                    </tr>
                </thead>
            </Table>,
        );
        const children = screen.getByTestId(CHILDREN);
        expect(children).toHaveTextContent(CHILDREN);
    });
    it('передали children -> есть переданные children', () => {
        const CHILDREN = 'test-children';
        render(
            <Table>
                <thead data-testid={CHILDREN} />
                <tbody data-testid={CHILDREN} />
            </Table>,
        );
        const children = screen.getAllByTestId(CHILDREN);
        expect(children).toHaveLength(2);
    });
    it('передали hovered -> есть класс Table_hovered', () => {
        const { container } = render(<Table hovered />);
        expect(container.firstChild).toHaveClass('Table_hovered');
    });
    it('передали hovered=false -> нет класса Table_hovered', () => {
        const { container } = render(<Table hovered={false} />);
        expect(container.firstChild).not.toHaveClass('Table_hovered');
    });
    it('передали striped -> есть класс Table_striped', () => {
        const { container } = render(<Table striped />);
        expect(container.firstChild).toHaveClass('Table_striped');
    });
    it('передали striped=false -> нет класса Table_striped', () => {
        const { container } = render(<Table striped={false} />);
        expect(container.firstChild).not.toHaveClass('Table_striped');
    });
});
