## Usage

```jsx
<Table bordered>
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Role</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>John Peters</td>
            <td>Developer</td>
        </tr>
        <tr>
            <td>2</td>
            <td>Denise Herrmann</td>
            <td>Designer</td>
        </tr>
        <tr>
            <td>3</td>
            <td>Alex Kane</td>
            <td>QA</td>
        </tr>
        <tr>
            <td>4</td>
            <td>Alan McQuinn</td>
            <td>Manager</td>
        </tr>
    </tbody>
</Table>
```

## Props

Prop name | Prop type | Default value
--- | --- | ---
*bordered*|bool|`false`
*hovered*|bool|`false`
*striped*|bool|`false`

## CSS Selectors

```css
.Table {}
```
