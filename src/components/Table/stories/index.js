import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean } from '@storybook/addon-knobs';

import Table from '..';

import notes from '../readme.md';

const stories = storiesOf('Table', module);

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories.add(
    'Default',
    () => (
        <Table
            bordered={boolean('bordered', false)}
            hovered={boolean('hovered', false)}
            striped={boolean('striped', false)}
        >
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Role</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>John Peters</td>
                    <td>Developer</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Denise Herrmann</td>
                    <td>Designer</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Alex Kane</td>
                    <td>QA</td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Alan McQuinn</td>
                    <td>Manager</td>
                </tr>
            </tbody>
        </Table>
    ),
    { notes },
);
