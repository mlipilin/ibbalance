import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean, number, select, text } from '@storybook/addon-knobs';
import dotProp from 'dot-prop';

// Constants
import { LOCAL_STORAGE_THEME_KEY } from '../../../constants/local-storage';
import { SIZES } from '../../../constants/props';
import { THEMES } from '../../../constants/themes';

// HOC
import { withState } from '../../../hoc';

// Get options method
import { getOptionsOrganization, getOptionsSurname } from './dadata';
import { getOptionsPosition } from './hh';

import InputSuggest from '../index';

const InputSuggestWithState = withState(InputSuggest);

const theme = localStorage.getItem(LOCAL_STORAGE_THEME_KEY);

let sizesList = [];

switch (theme) {
    case THEMES.DEFAULT:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        break;
    case THEMES.ALFA:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.ABSOLUT:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.SPB:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.TKB:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.TKB_LKZ:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.KYC:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.KYC_LANDING:
        sizesList = [SIZES.S, SIZES.M];
        break;
    default:
        break;
}

const stories = storiesOf('InputSuggest', module);

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories
    .add('With label', () => (
        <InputSuggestWithState
            disabled={boolean('disabled', false)}
            error={text('error', '')}
            errorDetail={text('errorDetail', '')}
            label={text('label', 'Страна')}
            readOnly={boolean('readOnly', false)}
            size={select('size', sizesList, SIZES.M)}
            success={boolean('success', false)}
        />
    ))
    .add('Without label', () => (
        <InputSuggestWithState
            disabled={boolean('disabled', false)}
            error={text('error', '')}
            errorDetail={text('errorDetail', '')}
            readOnly={boolean('readOnly', false)}
            size={select('size', sizesList, SIZES.M)}
            success={boolean('success', false)}
        />
    ))
    .add('Dadata', () => (
        <InputSuggestWithState
            disabled={boolean('disabled', false)}
            error={text('error', '')}
            errorDetail={text('errorDetail', '')}
            label={text('label', 'Фамилия')}
            minLengthToCallGet={number('minLengthToCallGet', 2)}
            optionsToShow={number('optionsToShow', 5)}
            readOnly={boolean('readOnly', false)}
            size={select('size', sizesList, SIZES.M)}
            success={boolean('success', false)}
            getOptionsMethod={getOptionsSurname}
        />
    ))
    .add('Dadata (with formatOption)', () => (
        <InputSuggestWithState
            disabled={boolean('disabled', false)}
            error={text('error', '')}
            errorDetail={text('errorDetail', '')}
            label={text('label', 'Работодатель (название, ИНН)')}
            minLengthToCallGet={number('minLengthToCallGet', 2)}
            optionsToShow={number('optionsToShow', 5)}
            readOnly={boolean('readOnly', false)}
            size={select('size', sizesList, SIZES.M)}
            success={boolean('success', false)}
            formatOption={(name, option) => {
                const address = dotProp.get(option, 'data.data.address.value');
                const inn = dotProp.get(option, 'data.data.inn');
                return (
                    <>
                        {name}{' '}
                        <em>
                            {address}
                            {'  ИНН: '}
                            {inn}
                        </em>
                    </>
                );
            }}
            getOptionsMethod={getOptionsOrganization}
        />
    ))
    .add('HH', () => (
        <InputSuggestWithState
            disabled={boolean('disabled', false)}
            error={text('error', '')}
            errorDetail={text('errorDetail', '')}
            label={text('label', 'Должность')}
            minLengthToCallGet={number('minLengthToCallGet', 2)}
            optionsToShow={number('optionsToShow', 5)}
            readOnly={boolean('readOnly', false)}
            size={select('size', sizesList, SIZES.M)}
            success={boolean('success', false)}
            getOptionsMethod={getOptionsPosition}
        />
    ));
