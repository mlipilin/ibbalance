export const getOptionsOrganization = (query, count) =>
    fetch('https://s-latest.balance-pl.ru/suggestions/api/4_1/rs/suggest/party/', {
        headers: {
            'Content-Type': 'application/json',
        },
        method: 'POST',
        body: JSON.stringify({
            count,
            query,
        }),
    })
        .then(response => response.json())
        .then(response =>
            response.suggestions.map((item, index) => ({
                id: index,
                name: item.value,
                data: item,
            })),
        )
        .catch(error => {
            console.log('error', error);
            return [];
        });

export const getOptionsSurname = (query, count) =>
    fetch('https://s-latest.balance-pl.ru/suggestions/api/4_1/rs/suggest/fio', {
        headers: {
            'Content-Type': 'application/json',
        },
        method: 'POST',
        body: JSON.stringify({
            count,
            parts: ['SURNAME'],
            query,
        }),
    })
        .then(response => response.json())
        .then(response =>
            response.suggestions.map((item, index) => ({
                id: index,
                name: item.value,
                data: item,
            })),
        )
        .catch(error => {
            console.log('error', error);
            return [];
        });
