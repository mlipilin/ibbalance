# Базовое использование поля с подсказкой

Подходит для большинства ситуаций

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { InputSuggest, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/ozon/theme';

const getOptionsMethod = (query, count) =>
  fetch('https://suggestions.project30.pro/suggestions/api/4_1/rs/suggest/fio/', {
    headers: { 'Content-Type': 'application/json' },
    method: 'POST',
    body: JSON.stringify({ count, parts: ['SURNAME'], query }),
  })
    .then(response => response.json())
    .then(response =>
      response.suggestions.map((item, index) => ({
        id: index,
        name: item.value
      })),
    )
    .catch(error => []);

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <InputSuggest
      label="Фамилия"
      getOptionsMethod={getOptionsMethod}
    />
  </ThemeProvider>,
  document.getElementById('root')
);
```
