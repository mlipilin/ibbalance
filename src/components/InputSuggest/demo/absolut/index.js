import React from 'react';

// Components
import InputSuggest from '../../index';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';

// Examples
import base from './examples/base.md';
import disabled from './examples/disabled.md';
import error from './examples/error.md';
import predefined from './examples/predefined.md';
import readOnly from './examples/readonly.md';
import success from './examples/success.md';

// Get options method
import { getOptionsSurname } from '../../stories/dadata';

// HOC
import { withState } from '../../../../hoc';

// Wrappers
import { Col, Row } from '../../../../wrappers/WGrid';

const PREDEFINED_OPTIONS = [
    'ЖК Альбатрос',
    'ЖК Бунинские Луга',
    'ЖК Золотое Кольцо',
    'ЖК Ленинский Массив',
    'ЖК Митино',
    'ЖК Новая Москва',
    'ЖК Павшинская Пойма',
    'ЖК Российские Просторы',
    'ЖК Трущебы Обыкновенные',
    'ЖК Янтарь',
];

const InputSuggestWithState = withState(InputSuggest);

const DemoInputSuggest = () => {
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>

            <div id="example">
                <Row
                    cols={{ mobile: [1], tab: [1, 1], desktop: [1], largeDesktop: [1, 1] }}
                    gap="20px"
                >
                    <Col>
                        <Example readme={base}>
                            <InputSuggestWithState
                                label="Фамилия"
                                getOptionsMethod={getOptionsSurname}
                            />
                        </Example>
                        <Example readme={disabled}>
                            <InputSuggestWithState
                                disabled
                                label="Фамилия"
                                getOptionsMethod={getOptionsSurname}
                            />
                        </Example>
                        <Example readme={readOnly}>
                            <InputSuggestWithState
                                label="Фамилия"
                                getOptionsMethod={getOptionsSurname}
                                readOnly
                            />
                        </Example>
                    </Col>
                    <Col>
                        <Example readme={error}>
                            <InputSuggestWithState
                                error="В поле ошибка"
                                label="Фамилия"
                                getOptionsMethod={getOptionsSurname}
                            />
                        </Example>
                        <Example readme={success}>
                            <InputSuggestWithState
                                label="Фамилия"
                                getOptionsMethod={getOptionsSurname}
                                success
                            />
                        </Example>
                        {/* eslint-disable */}
                        <Example readme={predefined}>
                            <InputSuggestWithState
                                label="Жилой комплекс"
                                getOptionsMethod={(query, count) =>
                                    Promise.resolve(
                                        PREDEFINED_OPTIONS.filter(
                                            option =>
                                                option
                                                    .toLowerCase()
                                                    .indexOf(query.toLowerCase()) !== -1,
                                        ).map((option, index) => ({ id: index, name: option })),
                                    )
                                }
                            />
                        </Example>
                        {/* eslint-enable */}
                    </Col>
                </Row>
            </div>
        </DemoComponent>
    );
};

export default DemoInputSuggest;
