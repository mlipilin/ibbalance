# Подсказка, формируемая на клиенте

Подходит для ситуаций, когда все возможные варианты подсказки получены заранее (например, когда этих вариантов относительно немного)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { InputSuggest, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/alfa/theme';

const PREDEFINED_OPTIONS = [
  'ЖК Альбатрос',
  'ЖК Бунинские Луга',
  'ЖК Золотое Кольцо',
  'ЖК Ленинский Массив',
  'ЖК Митино',
  'ЖК Новая Москва',
  'ЖК Павшинская Пойма',
  'ЖК Российские Просторы',
  'ЖК Трущебы Обыкновенные',
  'ЖК Янтарь',
];

const getOptionsMethod = (query, count) => Promise.resolve(
  PREDEFINED_OPTIONS
    .filter(option => option
      .toLowerCase()
      .indexOf(query.toLowerCase()) !== -1
    )
    .map((option, index) => ({ id: index, name: option }))
);

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <InputSuggest
      label="Фамилия"
      getOptionsMethod={getOptionsMethod}
    />
  </ThemeProvider>,
  document.getElementById('root')
);
```
