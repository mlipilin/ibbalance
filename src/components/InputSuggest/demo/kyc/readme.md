# InputSuggest

Поле для ввода текста с подсказкой

## Когда использовать

Когда нужен контрол для ввода текста, подсказывающий пользователю возможные варианты, дополняющие введенное значение. Полезен при интеграции с сервисом подсказок Дадата.

## Примеры использования

## Свойства

Название | Описание | Тип | Значение по умолчанию
--- | --- | --- | ---
**disabled**|`Disabled` состояние контрола|*bool*|`false`
**error**|Текст ошибки для контрола|*string*|`null`
**errorDetail**|Текст длинной ошибки, которую можно посмотреть в открывающимся тултипе |*string*|`null`
**errorDetailIcon**|Иконка (компонент), которая используется для открытия тултипа с ошибкой|*object*|`<FeatherIcon name={'alert-circle'} />`
**label**|Заголовок поля|*string,node,node[]*|`null`
**minLengthToCallGet**|Минимальная длина вводимого текста, при которой будет появляться подсказка|*number*|`2`
**optionsToShow**|Количество вариантов в подсказке|*number*|`5`
**readOnly**|`ReadOnly` состояние контрола|*bool*|`false`
**size**|Размер контрола|Один из: `s`, `m`|`m`
**success**|`Success` состояние контрола (как правило чтобы показать, что в поле валидное значение)|*bool*|`false`
**value**|Значение (текст внутри поля)|*number* или *string*|`null`
**formatOption**|Функция, преобразующая значение перед отображением значений в подсказке|*func*|`(name, option) => name`
**getOptionsMethod**|Метод, вызываемый для получения подсказки|*func*|`(query, count) => Promise.resolve([])`
**prepareValueToGetOptions**|Подготовка значения к передаче для поиска подсказок|*func*|`value => value`
**onBlur**|Обработчик события "blur"|*func*|`props => {}`
**onChange**|Обработчик события "change"|*func*|`(name, props, data) => {}`
**onEnter**|Обработчик события "выбор подсказки из списка"|*func*|`(value, fromSuggest, props, data) => {}`
**onFocus**|Обработчик события "focus"|*func*|`props => {}`
