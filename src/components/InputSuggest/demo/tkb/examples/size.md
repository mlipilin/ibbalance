# Размер инпута

Свойство size может принимать одно из следующих значений: `s` и `m`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { InputSuggest, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/tkb/theme';

const getOptionsMethod = (query, count) =>
  fetch('https://suggestions.project30.pro/suggestions/api/4_1/rs/suggest/fio/', {
    headers: { 'Content-Type': 'application/json' },
    method: 'POST',
    body: JSON.stringify({ count, parts: ['SURNAME'], query }),
  })
    .then(response => response.json())
    .then(response =>
      response.suggestions.map((item, index) => ({
        id: index,
        name: item.value
      })),
    )
    .catch(error => []);

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <InputSuggestWithState
      label="Фамилия"
      getOptionsMethod={getOptionsMethod}
      size="m"
    />
    <InputSuggestWithState
      label="Фамилия"
      getOptionsMethod={getOptionsMethod}
      size="s"
    />
  </ThemeProvider>,
  document.getElementById('root')
);
```
