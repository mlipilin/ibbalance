import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import MutationObserver from '@sheerun/mutationobserver-shim';
import FeatherIcon from '../FeatherIcon';
import InputSuggest from './index';

import { render } from '../../hoc/with-theme';

import countries from './stories/countries.json';

window.MutationObserver = MutationObserver;

describe('Тестирование компонента InputSuggest', () => {
    it('Должен отобразиться InputSuggest', () => {
        const { container } = render(<InputSuggest />);
        expect(screen.getByLabelText('')).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться InputSuggest с заголовком Страны', () => {
        render(<InputSuggest label="Страны" />);
        expect(screen.getByLabelText('Страны')).toBeInTheDocument();
        expect(screen.getByLabelText('Страны')).toMatchSnapshot();
    });

    it('Должен отобразиться InputSuggest с заголовком-нодой', () => {
        const { container } = render(<InputSuggest label={<span>Страны</span>} />);
        expect(screen.getByLabelText('Страны')).toBeInTheDocument();
        const labelText = container.querySelector('.Input__Label > span');
        expect(labelText).toBeInTheDocument();
        expect(screen.getByLabelText('Страны')).toMatchSnapshot();
    });

    it('Должен отобразиться InputSuggest с заголовком-массивом нод', () => {
        const { container } = render(
            <InputSuggest
                label={[<span key="страны">Страны</span>, <span key="участники">участники</span>]}
            />,
        );
        const labelText = container.querySelector('.Input__LabelText');
        expect(labelText.firstChild.textContent).toBe('Страны');
        expect(labelText.lastChild.textContent).toBe('участники');
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться InputSuggest в состоянии disabled', () => {
        const { container } = render(<InputSuggest disabled />);
        const disabledElem = container.querySelector('.InputSuggest_disabled');
        expect(disabledElem).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться InputSuggest в состоянии disabled - не получится выбрать', () => {
        const handleFocus = jest.fn();
        const { container } = render(<InputSuggest disabled onFocus={handleFocus} />);
        const disabledElem = container.querySelector('.InputSuggest_disabled');
        userEvent.click(disabledElem);
        expect(handleFocus).toHaveBeenCalledTimes(0);
    });

    it('Должен отобразиться InputSuggest в состоянии readonly', () => {
        const { container } = render(<InputSuggest readOnly />);
        const readOnlyElem = container.querySelector('.InputSuggest_readonly');
        expect(readOnlyElem).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться InputSuggest в состоянии readonly - не получится раскрыть', () => {
        const handleFocus = jest.fn();
        const { container } = render(<InputSuggest readOnly onFocus={handleFocus} />);
        const readOnlyElem = container.querySelector('.InputSuggest_readonly');
        expect(readOnlyElem).toBeInTheDocument();
        userEvent.click(readOnlyElem);
        expect(handleFocus).toHaveBeenCalledTimes(0);
    });

    it('Должен отобразиться InputSuggest с error = "Обязательное поле"', () => {
        const { container } = render(<InputSuggest error="Обязательное поле" />);
        const errorElem = container.querySelector('.Input__Error');
        expect(errorElem).toBeInTheDocument();
        expect(errorElem.textContent).toBe('Обязательное поле');
        expect(errorElem).toMatchSnapshot();
    });

    it('Должен отобразиться InputSuggest со скрытым полем errorDetail = "Обязательное поле"', () => {
        const { container } = render(<InputSuggest errorDetail="Обязательное поле" />);
        const errorElem = container.querySelector('.Input__Tooltip');
        expect(errorElem).toBeInTheDocument();
        expect(errorElem.textContent).toBe('Обязательное поле');
        expect(errorElem).toMatchSnapshot();
        const iconError = container.querySelector('.feather-alert-circle');
        expect(iconError).toBeInTheDocument();
    });

    it('Должен отобразиться InputSuggest с errorDetail = "Обязательное поле" ', async () => {
        const { container } = render(<InputSuggest errorDetail="Обязательное поле" />);
        const iconError = container.querySelector('.feather-alert-circle');
        await userEvent.click(iconError);
        const errorElem = container.querySelector('.Input__Tooltip_visible');
        expect(errorElem).toBeInTheDocument();
        expect(errorElem.textContent).toBe('Обязательное поле');
        expect(errorElem).toMatchSnapshot();
    });

    it('Должен отобразиться InputSuggest с errorDetailIcon = "alert-octagon" /> ', () => {
        const { container } = render(
            <InputSuggest
                errorDetail="Обязательное поле"
                errorDetailIcon={<FeatherIcon name="alert-octagon" />}
            />,
        );
        const iconError = container.querySelector('.feather-alert-octagon');
        expect(iconError).toBeInTheDocument();
    });

    it('Должен отобразиться InputSuggest с size="m" по-умолчанию ', () => {
        const { container } = render(<InputSuggest />);
        const conteinerSizeElem = container.querySelector('.InputSuggest_size_m');
        expect(conteinerSizeElem).toBeInTheDocument();
    });

    it('Должен отобразиться InputSuggest с size="s" ', () => {
        const { container } = render(<InputSuggest size="s" />);
        const conteinerSizeElem = container.querySelector('.InputSuggest_size_s');
        expect(conteinerSizeElem).toBeInTheDocument();
        expect(conteinerSizeElem).toMatchSnapshot();
    });

    it('Должен отобразиться InputSuggest с size="m" ', () => {
        const { container } = render(<InputSuggest size="m" />);
        const conteinerSizeElem = container.querySelector('.InputSuggest_size_m');
        expect(conteinerSizeElem).toBeInTheDocument();
        expect(conteinerSizeElem).toMatchSnapshot();
    });

    it('Должен отобразиться InputSuggest с size="l" ', () => {
        const { container } = render(<InputSuggest size="l" />);
        const conteinerSizeElem = container.querySelector('.InputSuggest_size_l');
        expect(conteinerSizeElem).toBeInTheDocument();
        expect(conteinerSizeElem).toMatchSnapshot();
    });

    it('Должен отобразиться InputSuggest в состоянии success', () => {
        const { container } = render(<InputSuggest success />);
        const inputSuccess = container.querySelector('.Input__Input_success');
        expect(inputSuccess).toBeInTheDocument();
        expect(inputSuccess).toMatchSnapshot();
    });

    it('Должен отобразиться InputSuggest c value = "Россия"', () => {
        const { container } = render(<InputSuggest value="Россия" />);
        const input = container.querySelector('input');
        expect(input).toBeInTheDocument();
        expect(input.value).toBe('Россия');
        expect(input).toMatchSnapshot();
    });

    it('Должен отобразиться InputSuggest со свойством name = "Countries"', () => {
        const { container } = render(<InputSuggest name="Countries" />);
        const input = container.querySelector('input');
        expect(input).toBeInTheDocument();
        expect(input.name).toBe('Countries');
        expect(input).toMatchSnapshot();
    });

    it('Должен отобразиться InputSuggest с className = "InputSuggest__Styles"', () => {
        const { container } = render(<InputSuggest className="InputSuggest__Styles" />);
        const wrapper = container.querySelector('.InputSuggest__Styles');
        expect(wrapper).toBeInTheDocument();
    });

    it('Должен отобразиться InputSuggest со свойством minLengthToCallGet = 2', async () => {
        const onChange = jest.fn();
        const getOptionsMethod = query =>
            Promise.resolve(
                ['Россия', 'Российская Федерация']
                    .filter(option => option.toLowerCase().indexOf(query.toLowerCase()) !== -1)
                    .map((option, index) => ({ id: index, name: option })),
            );
        jest.useFakeTimers();
        const { rerender } = render(
            <InputSuggest
                minLengthToCallGet={2}
                getOptionsMethod={getOptionsMethod}
                key="first"
                value="Р"
                onChange={onChange}
            />,
        );
        let input = screen.getByDisplayValue('Р');
        input.focus();
        await jest.advanceTimersByTime(200);

        const listOptions = screen.getByRole('list');
        expect(listOptions.childNodes.length).toBe(0);

        await rerender(
            <InputSuggest
                minLengthToCallGet={2}
                getOptionsMethod={getOptionsMethod}
                key="second"
                value="Рос"
                onChange={onChange}
            />,
        );
        input = screen.getByDisplayValue('Рос');
        input.focus();
        await jest.advanceTimersByTime(200);
        screen.debug();

        const listOptionsAfterChange = await screen.getByRole('list');
        expect(listOptionsAfterChange.childNodes.length).toBe(2);
        const options = await screen.getAllByRole('listitem');

        expect(options[0].textContent).toBe('Россия');
        expect(options[1].textContent).toBe('Российская Федерация');
    });

    it('Должен отобразиться InputSuggest со свойством optionsToShow = 1', async () => {
        const onChange = jest.fn();
        const getOptionsMethod = (query, count) => {
            expect(count).toBe(1);
            return Promise.resolve(
                ['Россия', 'Российская Федерация']
                    .filter(option => option.toLowerCase().indexOf(query.toLowerCase()) !== -1)
                    .map((option, index) => ({ id: index, name: option }))
                    .slice(0, count),
            );
        };
        jest.useFakeTimers();
        const { container, rerender } = render(
            <InputSuggest
                minLengthToCallGet={2}
                getOptionsMethod={getOptionsMethod}
                key="render"
                optionsToShow={1}
                value="Pос"
                onChange={onChange}
            />,
        );
        let input = container.querySelector('input');
        input.focus();

        rerender(
            <InputSuggest
                minLengthToCallGet={2}
                getOptionsMethod={getOptionsMethod}
                value="Рос"
                optionsToShow={1}
                onChange={onChange}
            />,
        );
        input = container.querySelector('input');
        input.focus();
        await jest.advanceTimersByTime(200);

        const listOptionsAfterChange = await screen.getByRole('list');
        expect(listOptionsAfterChange.childNodes.length).toBe(1);
        const options = await screen.getAllByRole('listitem');
        expect(options[0].textContent).toBe('Россия');
    });

    it('Должен отобразиться InputSuggest с форматированным списком formatOption', async () => {
        const onChange = jest.fn();
        const getOptionsMethod = jest.fn(() =>
            Promise.resolve([
                { id: 0, name: 'Россия' },
                { id: 1, name: 'Российская Федерация' },
            ]),
        );
        const onFormatOption = name => [name[0].toLowerCase()];
        jest.useFakeTimers();
        const { container, rerender } = render(
            <InputSuggest
                formatOption={onFormatOption}
                getOptionsM
                ethod={getOptionsMethod}
                key="render"
                minLengthToCallGet={0}
                onChange={onChange}
            />,
        );
        let input = container.querySelector('input');
        input.focus();

        rerender(
            <InputSuggest
                formatOption={onFormatOption}
                minLengthToCallGet={0}
                getOptionsMethod={getOptionsMethod}
                value=" "
                optionsToShow={1}
                onChange={onChange}
            />,
        );
        input = container.querySelector('input');
        input.focus();
        await jest.advanceTimersByTime(200);
        const options = container.querySelectorAll('.Suggest__Option');
        expect(options[0].textContent).toBe('россия');
        expect(options[1].textContent).toBe('российская федерация');
    });

    it('Должен отобразиться InputSuggest с активный элементом "Россия"', async () => {
        const onChange = jest.fn();
        const getOptionsMethod = jest.fn(() =>
            Promise.resolve([
                { id: 0, name: 'Россия' },
                { id: 1, name: 'Российская Федерация' },
            ]),
        );
        jest.useFakeTimers();
        const { container, rerender } = render(
            <InputSuggest
                getOptionsMethod={getOptionsMethod}
                key="render"
                minLengthToCallGet={0}
                onChange={onChange}
            />,
        );
        let input = container.querySelector('input');
        input.focus();

        rerender(
            <InputSuggest
                minLengthToCallGet={0}
                getOptionsMethod={getOptionsMethod}
                value={' '}
                optionsToShow={1}
                onChange={onChange}
            />,
        );
        input = container.querySelector('input');
        input.focus();
        await jest.advanceTimersByTime(200);
        const activeOption = container.querySelector('.SuggestOption_active');
        expect(activeOption.textContent).toBe('Россия');
    });

    it('Должен отобразиться InputSuggest со значением "Австрия"', async () => {
        const onChange = jest.fn();
        const onEnter = jest.fn();
        const getOptionsMethod = jest.fn(() => Promise.resolve(countries));
        jest.useFakeTimers();
        const { container, rerender } = render(
            <InputSuggest
                getOptionsMethod={getOptionsMethod}
                key="render"
                minLengthToCallGet={0}
                value=""
                onChange={onChange}
                onEnter={onEnter}
            />,
        );
        let input = container.querySelector('input');
        input.focus();
        await jest.advanceTimersByTime(200);

        rerender(
            <InputSuggest
                getOptionsMethod={getOptionsMethod}
                onChange={onChange}
                minLengthToCallGet={0}
                value={' '}
                onEnter={onEnter}
            />,
        );
        input = container.querySelector('input');
        input.focus();
        await jest.advanceTimersByTime(200);

        await userEvent.type(input, '{arrowdown}{arrowup}{arrowdown}{enter}');
        expect(onEnter).toHaveBeenCalledTimes(1);
        expect(onEnter).toBeCalledWith('Австрия', true, expect.any(Object), expect.any(Object));
        expect(onChange).toHaveBeenCalledTimes(1);
        expect(onChange).toBeCalledWith('Австрия', expect.any(Object), expect.any(Object));
    });

    it('Должен отобразиться InputSuggest у которого при проходе стрелками disabled (Азербайджан) пропускается', async () => {
        const onChange = jest.fn();
        const onEnter = jest.fn();
        const getOptionsMethod = jest.fn(() => Promise.resolve(countries));
        jest.useFakeTimers();
        const { container, rerender } = render(
            <InputSuggest
                getOptionsMethod={getOptionsMethod}
                key="render"
                minLengthToCallGet={0}
                value=""
                onChange={onChange}
                onEnter={onEnter}
            />,
        );
        let input = container.querySelector('input');
        input.focus();
        await jest.advanceTimersByTime(2000);

        rerender(
            <InputSuggest
                getOptionsMethod={getOptionsMethod}
                minLengthToCallGet={0}
                value=" "
                onChange={onChange}
                onEnter={onEnter}
            />,
        );
        input = container.querySelector('input');
        input.focus();
        await jest.advanceTimersByTime(200);

        await userEvent.type(input, '{arrowdown}{arrowdown}{arrowdown}{enter}');
        expect(onEnter).toHaveBeenCalledTimes(1);
        expect(onEnter).toBeCalledWith('Албания', true, expect.any(Object), expect.any(Object));
        expect(onChange).toHaveBeenCalledTimes(1);
        expect(onChange).toBeCalledWith('Албания', expect.any(Object), expect.any(Object));
    });

    it('Должен отобразиться InputSuggest с активным элементом "Австралия" после прохода стрелками до самого верхнего элемента', async () => {
        const onChange = jest.fn();
        const onEnter = jest.fn();
        const getOptionsMethod = jest.fn(() => Promise.resolve(countries));
        jest.useFakeTimers();
        const { container, rerender } = render(
            <InputSuggest
                getOptionsMethod={getOptionsMethod}
                key="render"
                minLengthToCallGet={0}
                value=""
                onChange={onChange}
                onEnter={onEnter}
            />,
        );
        let input = container.querySelector('input');
        input.focus();
        await jest.advanceTimersByTime(200);

        rerender(
            <InputSuggest
                getOptionsMethod={getOptionsMethod}
                onChange={onChange}
                minLengthToCallGet={0}
                value={' '}
                onEnter={onEnter}
            />,
        );
        input = container.querySelector('input');
        input.focus();
        await jest.advanceTimersByTime(200);

        await userEvent.type(input, '{arrowup}{arrowup}');
        const activeElem = await container.querySelector('.SuggestOption_active');
        expect(activeElem.textContent).toBe('Австралия');
    });

    it('Должен отобразиться InputSuggest с активным элементом Албания после прохода стрелками до самого нижнего элемента', async () => {
        const onChange = jest.fn();
        const onEnter = jest.fn();
        const getOptionsMethod = jest.fn((query, count) =>
            Promise.resolve(countries.slice(0, count)),
        );
        jest.useFakeTimers();
        const { container, rerender } = render(
            <InputSuggest
                getOptionsMethod={getOptionsMethod}
                key="render"
                minLengthToCallGet={0}
                optionsToShow={4}
                value=""
                onChange={onChange}
                onEnter={onEnter}
            />,
        );
        let input = container.querySelector('input');
        input.focus();
        await jest.advanceTimersByTime(200);

        rerender(
            <InputSuggest
                getOptionsMethod={getOptionsMethod}
                onChange={onChange}
                minLengthToCallGet={0}
                value={' '}
                onEnter={onEnter}
                optionsToShow={4}
            />,
        );
        input = container.querySelector('input');
        input.focus();
        await jest.advanceTimersByTime(200);

        await userEvent.type(input, '{arrowdown}{arrowdown}{arrowdown}{arrowdown}{arrowdown}');
        const activeElem = await container.querySelector('.SuggestOption_active');
        expect(activeElem.textContent).toBe('Албания');
    });

    it('Должен отобразиться InputSuggest c правльно отсортированным списком', async () => {
        const onChange = jest.fn();
        const onEnter = jest.fn();
        const getOptionsMethod = jest.fn((query, count) =>
            Promise.resolve(
                countries
                    .filter(option => option.name.toLowerCase().indexOf(query.toLowerCase()) !== -1)
                    .map(option => ({ id: option.id, name: option.name }))
                    .slice(0, count),
            ),
        );
        jest.useFakeTimers();
        const { container, rerender } = render(
            <InputSuggest
                getOptionsMethod={getOptionsMethod}
                key="render"
                minLengthToCallGet={0}
                optionsToShow={9}
                onChange={onChange}
                onEnter={onEnter}
            />,
        );
        let input = container.querySelector('input');
        input.focus();
        await jest.advanceTimersByTime(200);

        rerender(
            <InputSuggest
                getOptionsMethod={getOptionsMethod}
                onChange={onChange}
                minLengthToCallGet={0}
                value="Ро"
                onEnter={onEnter}
                optionsToShow={9}
            />,
        );
        input = container.querySelector('input');
        input.focus();
        await jest.advanceTimersByTime(200);

        await userEvent.type(input, 'Ро');

        const array = [
            'Багамские Острова',
            'Марокко',
            'Маршалловы Острова',
            'Россия',
            'Сейшельские Острова',
            'Соломоновы Острова',
            'Федеративные Штаты Микронезии',
            'Приднестровье',
            'Острова Кука',
        ];
        const selectCountries = await container.querySelectorAll('.SuggestOption');
        expect(Array.from(selectCountries).map(option => option.textContent)).toEqual(array);
    });

    it('Должен отобразиться InputSuggest onFocus', async () => {
        const onFocus = jest.fn();
        jest.useFakeTimers();
        const { container } = render(<InputSuggest onFocus={onFocus} />);
        const input = container.querySelector('input');
        input.focus();
        await jest.advanceTimersByTime(200);

        expect(onFocus).toBeCalledWith(expect.any(Object));
        expect(onFocus).toBeCalledTimes(1);
    });

    it('Должен отобразиться InputSuggest prepareValueToGetOptions', async () => {
        const onChange = jest.fn();
        const onEnter = jest.fn();
        const prepareValueToGetOptions = jest.fn(value => value.toLowerCase());
        const getOptionsMethod = jest.fn((query, count) =>
            Promise.resolve(
                countries
                    .filter(option => option.name.toLowerCase().indexOf(query) !== -1)
                    .map(option => ({ id: option.id, name: option.name }))
                    .slice(0, count),
            ),
        );
        jest.useFakeTimers();
        const { container, rerender } = render(<InputSuggest key="render" />);
        const input = container.querySelector('input');
        input.focus();
        await jest.advanceTimersByTime(200);

        rerender(
            <InputSuggest
                getOptionsMethod={getOptionsMethod}
                onChange={onChange}
                minLengthToCallGet={0}
                value="Ро"
                onEnter={onEnter}
                optionsToShow={9}
                prepareValueToGetOptions={prepareValueToGetOptions}
            />,
        );
        const inputRerender = container.querySelector('input');
        inputRerender.focus();
        await jest.advanceTimersByTime(200);

        expect(prepareValueToGetOptions).toBeCalledWith('Ро');
        expect(prepareValueToGetOptions).toBeCalledTimes(1);
    });

    it('Должен отобразиться InputSuggest onBlur', async () => {
        const onBlur = jest.fn();

        jest.useFakeTimers();
        const { container, rerender } = render(<InputSuggest onBlur={onBlur} />);
        const input = container.querySelector('input');
        input.focus();
        await jest.advanceTimersByTime(200);
        await userEvent.tab();

        rerender(<InputSuggest onBlur={onBlur} />);
        await jest.advanceTimersByTime(200);

        expect(onBlur).toBeCalledWith(expect.any(Object));
        expect(onBlur).toBeCalledTimes(1);
    });

    it('Должен отобразиться InputSuggest с закрытием по ESC key', async () => {
        const onBlur = jest.fn();

        jest.useFakeTimers();
        const { container, rerender } = render(<InputSuggest onBlur={onBlur} />);
        const input = container.querySelector('input');
        input.focus();
        await userEvent.type(input, '{esc}');
        rerender(<InputSuggest onBlur={onBlur} />);
        await jest.advanceTimersByTime(2000);

        expect(onBlur).toBeCalledWith(undefined);
        expect(onBlur).toBeCalledTimes(1);
    });

    it.skip('Должен отобразиться InputSuggest у которого при клике на стрелки - происходит scroll', () => {});
});
