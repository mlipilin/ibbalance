import React from 'react';
import userEvent from '@testing-library/user-event';

import Tooltip from './index';

import { render } from '../../hoc/with-theme';

window.IntersectionObserver = jest.fn(() => ({
    disconnect: jest.fn(),
    observe: jest.fn(),
    unobserve: jest.fn(),
}));
window.IntersectionObserver.prototype.disconnect = jest.fn();
document.elementFromPoint = jest.fn();

describe('Тестирование компонента Tooltip', () => {
    it('Должен отобразиться Tooltip', () => {
        const { container } = render(
            <Tooltip>
                <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                    Click
                </div>
            </Tooltip>,
        );
        const tooltip = document.querySelector('.Tooltip');
        expect(tooltip).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
        expect(tooltip).toMatchSnapshot();
    });

    it('Должен отобразиться Tooltip с className="Tooltip__Style"', () => {
        render(
            <Tooltip className="Tooltip__Style">
                <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                    Click
                </div>
            </Tooltip>,
        );
        const tooltip = document.querySelector('.Tooltip');
        expect(tooltip).toBeInTheDocument();
        expect(tooltip.classList.contains('Tooltip__Style')).toBeTruthy();
        expect(tooltip).toMatchSnapshot();
    });

    it('Должен отобразиться Tooltip c content = "Содержимое тултипа"', () => {
        render(
            <Tooltip content="Содержимое тултипа">
                <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                    Click
                </div>
            </Tooltip>,
        );
        const tooltip = document.querySelector('.Tooltip');
        expect(tooltip).toBeInTheDocument();
        expect(tooltip.textContent).toBe('Содержимое тултипа');
        expect(tooltip).toMatchSnapshot();
    });

    it('Должен отобразиться Tooltip c content = "<span>Содержимое тултипа</span>"', () => {
        render(
            <Tooltip content={<span>Содержимое тултипа</span>}>
                <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                    Click
                </div>
            </Tooltip>,
        );
        const tooltip = document.querySelector('.Tooltip');
        const content = tooltip.querySelector('span');
        expect(content).toBeInTheDocument();
        expect(tooltip).toMatchSnapshot();
    });

    it('Должен отобразиться Tooltip c расположением по-умолчанию "bottom"', () => {
        render(
            <Tooltip>
                <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                    Click
                </div>
            </Tooltip>,
        );
        const tooltip = document.querySelector('.Tooltip_placement_bottom');
        expect(tooltip).toBeInTheDocument();
    });

    it('Должен отобразиться Tooltip c расположением "top"', async () => {
        const { container } = render(
            <Tooltip placement="top">
                <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                    Click
                </div>
            </Tooltip>,
        );
        const button = container.querySelector('.Tooltip__Btn');
        await userEvent.click(button);
        const tooltip = document.querySelector('.Tooltip_placement_top');
        expect(tooltip).toBeInTheDocument();
    });

    it('Должен отобразиться Tooltip c расположением "left"', async () => {
        const { container } = render(
            <Tooltip placement="left">
                <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                    Click
                </div>
            </Tooltip>,
        );
        const button = container.querySelector('.Tooltip__Btn');
        await userEvent.click(button);
        const tooltip = document.querySelector('.Tooltip_placement_left');
        expect(tooltip).toBeInTheDocument();
    });

    it('Должен отобразиться Tooltip c расположением "right"', async () => {
        const { container } = render(
            <Tooltip placement="right">
                <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                    Click
                </div>
            </Tooltip>,
        );
        const button = container.querySelector('.Tooltip__Btn');
        await userEvent.click(button);
        const tooltip = document.querySelector('.Tooltip_placement_right');
        expect(tooltip).toBeInTheDocument();
    });

    it('Должен отобразиться Tooltip c расположением "bottom"', async () => {
        const { container } = render(
            <Tooltip placement="bottom">
                <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                    Click
                </div>
            </Tooltip>,
        );
        const button = container.querySelector('.Tooltip__Btn');
        await userEvent.click(button);
        const tooltip = document.querySelector('.Tooltip_placement_bottom');
        expect(tooltip).toBeInTheDocument();
    });

    it('Должен отобразиться Tooltip c позицией "absolute" по-умолчанию', () => {
        render(
            <Tooltip>
                <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                    Click
                </div>
            </Tooltip>,
        );
        const tooltip = document.querySelector('.Tooltip');
        expect(tooltip).toHaveStyle({ position: 'absolute' });
    });

    it('Должен отобразиться Tooltip c позицией "absolute"', () => {
        render(
            <Tooltip position="absolute">
                <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                    Click
                </div>
            </Tooltip>,
        );
        const tooltip = document.querySelector('.Tooltip');
        expect(tooltip).toHaveStyle({ position: 'absolute' });
    });

    it('Должен отобразиться Tooltip c позицией "fixed"', () => {
        render(
            <Tooltip position="fixed">
                <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                    Click
                </div>
            </Tooltip>,
        );
        const tooltip = document.querySelector('.Tooltip');
        expect(tooltip).toHaveStyle({ position: 'fixed' });
    });

    it('Должен отобразиться Tooltip таргетным элементом Click', () => {
        const { container } = render(<Tooltip>Click</Tooltip>);
        const targetString = container.querySelector('span');
        expect(targetString).toBeInTheDocument();
        expect(targetString.textContent).toBe('Click');
    });

    it('Должен отобразиться Tooltip открытым', async () => {
        const { container } = render(
            <Tooltip>
                <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                    Click
                </div>
            </Tooltip>,
        );
        const tooltip = document.querySelector('.Tooltip');
        expect(tooltip.classList.contains('Tooltip_visible')).toBeFalsy();
        const button = container.querySelector('.Tooltip__Btn');
        await userEvent.click(button);
        expect(
            document.querySelector('.Tooltip').classList.contains('Tooltip_visible'),
        ).toBeTruthy();
    });

    it('Должен отобразиться Tooltip закрытым', async () => {
        const { container } = render(
            <Tooltip>
                <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                    Click
                </div>
            </Tooltip>,
        );
        const tooltip = document.querySelector('.Tooltip');
        expect(tooltip.classList.contains('Tooltip_visible')).toBeFalsy();
        const button = container.querySelector('.Tooltip__Btn');
        await userEvent.click(button);
        await userEvent.click(button);
        expect(
            document.querySelector('.Tooltip').classList.contains('Tooltip_visible'),
        ).toBeFalsy();
    });
});
