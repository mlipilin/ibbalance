# Расположение тултипа

Свойство placement может принимать одно из следующих значений: `top`, `right`, `bottom`, `left`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Button, FeatherIcon, Tooltip, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/ozon/theme';

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <Tooltip placement="left" content="Content">
            <Button>
                <FeatherIcon name="arrow-left" />
            </Button>
        </Tooltip>
        <div style={{ display: 'inline-block' }}>
            <Tooltip placement="top" content="Content">
                <Button>
                    <FeatherIcon name="arrow-up" />
                </Button>
            </Tooltip>
            <br />
            <br />
            <Tooltip placement="bottom" content="Content">
                <Button>
                    <FeatherIcon name="arrow-down" />
                </Button>
            </Tooltip>
        </div>
        <Tooltip placement="right" content="Content">
            <Button>
                <FeatherIcon name="arrow-right" />
            </Button>
        </Tooltip>
    </ThemeProvider>,
    document.getElementById('root'),
);
```
