import React from 'react';

// Components
import Button from '../../../Button';
import FeatherIcon from '../../../FeatherIcon';
import Tooltip from '../../index';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';

// Examples
import placement from './examples/placement.md';

// Wrappers
import { Col, Row } from '../../../../wrappers/WGrid';

const DemoTooltip = () => {
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>

            <div id="example">
                <Row
                    cols={{ mobile: [1], tab: [1, 1], desktop: [1], largeDesktop: [1, 1] }}
                    gap="20px"
                >
                    <Col>
                        <Example readme={placement}>
                            <Tooltip placement="left" content="Content">
                                <Button>
                                    <FeatherIcon name="arrow-left" />
                                </Button>
                            </Tooltip>
                            <div style={{ display: 'inline-block' }}>
                                <Tooltip placement="top" content="Content">
                                    <Button>
                                        <FeatherIcon name="arrow-up" />
                                    </Button>
                                </Tooltip>
                                <br />
                                <br />
                                <Tooltip placement="bottom" content="Content">
                                    <Button>
                                        <FeatherIcon name="arrow-down" />
                                    </Button>
                                </Tooltip>
                            </div>
                            <Tooltip placement="right" content="Content">
                                <Button>
                                    <FeatherIcon name="arrow-right" />
                                </Button>
                            </Tooltip>
                        </Example>
                    </Col>
                </Row>
            </div>
        </DemoComponent>
    );
};

export default DemoTooltip;
