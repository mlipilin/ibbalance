import { PLACEMENTS } from '../../constants/props';

const getPlacement = elem => {
    const box = elem.getBoundingClientRect();

    return {
        top: box.top + window.pageYOffset,
        left: box.left + window.pageXOffset,
    };
};

function moveTooltipTop(target) {
    const targetBoundingClientRect = target.getBoundingClientRect();
    const viewportY = targetBoundingClientRect.top;
    const targetHeight = targetBoundingClientRect.height;
    const windowScrollY = window.pageYOffset;

    const tooltipTop = windowScrollY + viewportY + targetHeight;

    return tooltipTop;
}

function getTooltipTopCoord(target) {
    const targetBoundingClientRect = target.getBoundingClientRect();
    const viewportY = targetBoundingClientRect.top;
    const targetHeight = targetBoundingClientRect.height;

    const tooltipTop = viewportY + targetHeight;

    return tooltipTop;
}

function checkVisibleTarget(targetProps, tooltipProps) {
    const postTarget = targetProps.getBoundingClientRect();
    const x = postTarget.left + postTarget.width / 2;
    const y = postTarget.top + postTarget.height / 2;
    const visibleElement = document.elementFromPoint(x, y);
    const isParentTarget = targetProps.contains(visibleElement);
    const isParentTooltip = tooltipProps.contains(visibleElement);
    return visibleElement && !isParentTarget && !isParentTooltip;
}

const getTooltipPlacementBottom = (targetElement, tooltipElement) => {
    if (!(targetElement && tooltipElement)) {
        return undefined;
    }
    const arrow = {};

    const tooltipLeftDelta = targetElement.offsetWidth / 2 - tooltipElement.offsetWidth / 2;
    const targetPlacement = getPlacement(targetElement);

    let tooltipLeft = targetPlacement.left + tooltipLeftDelta;

    arrow.left = targetPlacement.left - tooltipLeft + targetElement.offsetWidth / 2;

    // Тултип вышел за пределы ПРАВОЙ границы окна
    if (tooltipLeft + tooltipElement.offsetWidth > document.documentElement.clientWidth) {
        tooltipLeft = document.documentElement.clientWidth - tooltipElement.offsetWidth;

        arrow.left = targetPlacement.left - tooltipLeft + targetElement.offsetWidth / 2;
    }

    // Тултип вышел за пределы ЛЕВОЙ границы окна
    if (tooltipLeft < 0) {
        tooltipLeft = 0;

        arrow.left = targetPlacement.left - tooltipLeft + targetElement.offsetWidth / 2;
    }

    const tooltipTopDelta = targetElement.offsetHeight;
    let tooltipTop = targetPlacement.top + tooltipTopDelta;

    let placementStyle = PLACEMENTS.BOTTOM;

    const isOverflow = (targetProp, tooltipProp) => {
        const tooltipTopCalc = getTooltipTopCoord(targetProp);
        return tooltipTopCalc + tooltipProp.offsetHeight > window.innerHeight;
    };

    const isSpaceTop = (targetProp, tooltipProp) => {
        const tooltipTopCalc = moveTooltipTop(targetProp);
        return tooltipTopCalc - tooltipProp.offsetHeight > 0;
    };

    // Тултип вышел за пределы окна снизу, будет отображаться сверху, если есть место
    if (isOverflow(targetElement, tooltipElement) && isSpaceTop(targetElement, tooltipElement)) {
        placementStyle = PLACEMENTS.TOP;
        tooltipTop =
            moveTooltipTop(targetElement) -
            tooltipElement.clientHeight -
            targetElement.clientHeight;
    }

    const tooltip = {
        transform: `translate3d(${parseInt(tooltipLeft, 10)}px, ${parseInt(tooltipTop, 10)}px, 0)`,
    };

    // Если таргет перекрыт, то скрываем тултип
    const isNeedClose = checkVisibleTarget(targetElement, tooltipElement);

    return { arrow, tooltip, placementStyle, isNeedClose };
};

const getTooltipPlacementLeft = (targetElement, tooltipElement) => {
    if (!(targetElement && tooltipElement)) {
        return undefined;
    }
    const arrow = {};

    const tooltipLeftDelta = tooltipElement.offsetWidth;
    const targetPlacement = getPlacement(targetElement);

    let tooltipLeft = targetPlacement.left - tooltipLeftDelta;

    const tooltipTopDelta = tooltipElement.offsetHeight / 2 + targetElement.offsetHeight / 2;
    let tooltipTop = moveTooltipTop(targetElement) - tooltipTopDelta;

    // Тултип вышел за пределы НИЖНЕЙ границы окна
    if (
        tooltipTop + tooltipElement.offsetHeight >
        document.documentElement.clientHeight + window.pageYOffset
    ) {
        tooltipTop =
            document.documentElement.clientHeight +
            window.pageYOffset -
            tooltipElement.offsetHeight;
        arrow.top = targetPlacement.top - tooltipTop + targetElement.offsetHeight / 2;
    }

    // Тултип вышел за пределы ВЕРХНЕЙ границы окна
    if (tooltipTop < window.pageYOffset) {
        tooltipTop = window.pageYOffset;
        arrow.top = targetPlacement.top - tooltipTop + targetElement.offsetHeight / 2;
    }

    let placementStyle = PLACEMENTS.LEFT;

    // Тултип вышел за границу окна слева
    const isNotSpaceLeft = tooltipLeft < 0;

    const isSpaceRight =
        targetPlacement.left + tooltipElement.offsetWidth + targetElement.offsetWidth <
        document.documentElement.clientWidth;

    if (isNotSpaceLeft && isSpaceRight) {
        placementStyle = PLACEMENTS.RIGHT;
        tooltipLeft = targetPlacement.left + targetElement.offsetWidth;
    }

    // Если таргет перекрыт, то скрываем тултип
    const isNeedClose = checkVisibleTarget(targetElement, tooltipElement);

    const tooltip = {
        transform: `translate3d(${parseInt(tooltipLeft, 10)}px, ${parseInt(tooltipTop, 10)}px, 0)`,
    };

    return { arrow, tooltip, placementStyle, isNeedClose };
};

const getTooltipPlacementRight = (targetElement, tooltipElement) => {
    if (!(targetElement && tooltipElement)) {
        return undefined;
    }
    const arrow = {};

    const tooltipLeftDelta = targetElement.offsetWidth;
    const targetPlacement = getPlacement(targetElement);

    let tooltipLeft = targetPlacement.left + tooltipLeftDelta;

    const tooltipTopDelta = tooltipElement.offsetHeight / 2;
    let tooltipTop =
        moveTooltipTop(targetElement) - tooltipTopDelta - targetElement.offsetHeight / 2;

    // Тултип вышел за пределы НИЖНЕЙ границы окна
    if (
        tooltipTop + tooltipElement.offsetHeight >
        document.documentElement.clientHeight + window.pageYOffset
    ) {
        tooltipTop =
            document.documentElement.clientHeight +
            window.pageYOffset -
            tooltipElement.offsetHeight;
        arrow.top = targetPlacement.top - tooltipTop + targetElement.offsetHeight / 2;
    }

    // Тултип вышел за пределы ВЕРХНЕЙ границы окна
    if (tooltipTop < window.pageYOffset) {
        tooltipTop = window.pageYOffset;
        arrow.top = targetPlacement.top - tooltipTop + targetElement.offsetHeight / 2;
    }

    let placementStyle = PLACEMENTS.RIGHT;

    const isNotSpaceRight =
        tooltipLeft + tooltipElement.offsetWidth - document.documentElement.clientWidth > 0;
    const isSpaceLeft = tooltipLeft - tooltipElement.offsetWidth - targetElement.offsetWidth > 0;
    if (isNotSpaceRight && isSpaceLeft) {
        placementStyle = PLACEMENTS.LEFT;
        tooltipLeft = targetPlacement.left - tooltipElement.offsetWidth;
    }

    // Если таргет перекрыт, то скрываем тултип
    const isNeedClose = checkVisibleTarget(targetElement, tooltipElement);

    const tooltip = {
        transform: `translate3d(${parseInt(tooltipLeft, 10)}px, ${parseInt(tooltipTop, 10)}px, 0)`,
    };

    return { arrow, tooltip, placementStyle, isNeedClose };
};

const getTooltipPlacementTop = (targetElement, tooltipElement) => {
    if (!(targetElement && tooltipElement)) {
        return undefined;
    }
    const arrow = {};
    const tooltipLeftDelta = targetElement.offsetWidth / 2 - tooltipElement.offsetWidth / 2;
    const targetPlacement = getPlacement(targetElement);

    let tooltipLeft = targetPlacement.left + tooltipLeftDelta;

    // Тултип вышел за пределы ПРАВОЙ границы окна
    if (tooltipLeft + tooltipElement.offsetWidth > document.documentElement.clientWidth) {
        tooltipLeft = document.documentElement.clientWidth - tooltipElement.offsetWidth;

        arrow.left = targetPlacement.left - tooltipLeft + targetElement.offsetWidth / 2;
    }

    // Тултип вышел за пределы ЛЕВОЙ границы окна
    if (tooltipLeft < 0) {
        tooltipLeft = 0;

        arrow.left = targetPlacement.left - tooltipLeft + targetElement.offsetWidth / 2;
    }

    const tooltipTopDelta = tooltipElement.offsetHeight;
    let tooltipTop = targetPlacement.top - tooltipTopDelta;

    let placementStyle = PLACEMENTS.TOP;

    // Тултип вышел за пределы окна сверху, будет отображаться снизу
    if (tooltipTop < 0 || tooltipTop - window.pageYOffset < 0) {
        tooltipTop = moveTooltipTop(targetElement);
        // tooltip.transform = `translate3d(${tooltipLeft}px, ${moveTooltipTop(targetElement)}px, 0)`;
        placementStyle = PLACEMENTS.BOTTOM;
    }

    // Если таргет перекрыт, то скрываем тултип
    const isNeedClose = checkVisibleTarget(targetElement, tooltipElement);

    const tooltip = {
        transform: `translate3d(${parseInt(tooltipLeft, 10)}px, ${parseInt(tooltipTop, 10)}px, 0)`,
    };

    return { arrow, tooltip, placementStyle, isNeedClose };
};

export const getTooltipPlacement = (targetElement, tooltipElement, placement) => {
    if (!targetElement) {
        return getDefaultTooltipPlacement();
    }
    if (!tooltipElement) {
        return getDefaultTooltipPlacement();
    }

    switch (placement) {
        case PLACEMENTS.BOTTOM:
            return getTooltipPlacementBottom(targetElement, tooltipElement);
        case PLACEMENTS.LEFT:
            return getTooltipPlacementLeft(targetElement, tooltipElement);
        case PLACEMENTS.RIGHT:
            return getTooltipPlacementRight(targetElement, tooltipElement);
        case PLACEMENTS.TOP:
            return getTooltipPlacementTop(targetElement, tooltipElement);
        default:
            return getTooltipPlacementBottom(targetElement, tooltipElement);
    }
};

export const getDefaultTooltipPlacement = () => ({
    arrow: {},
    tooltip: {
        transform: 'unset',
    },
    placementStyle: PLACEMENTS.BOTTOM,
});
