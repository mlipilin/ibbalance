import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import cn from 'classnames';
import debounce from 'lodash.debounce';

// Constants
import { PLACEMENTS } from '../../constants/props';

// Helpers
import { getDefaultTooltipPlacement, getTooltipPlacement } from './helpers';
import { hasNodeInTree } from '../../helpers/dom';

import { withTheme } from '../../theme-provider';

@withTheme
class Tooltip extends Component {
    elementsHasListnerOnScrollEvent = [];

    observers = [];

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            placementTooltip: getDefaultTooltipPlacement(),
        };

        this.handleScrollDebounced = debounce(this.handleWindowScrollEvent, 500).bind(this);
        this.handleMoveDebounced = debounce(this.handleTouchMove, 500).bind(this);
    }

    componentDidMount() {
        const { children } = this.props;
        // eslint-disable-next-line
        this.targetElement = children ? ReactDOM.findDOMNode(this.target) : null;

        if (this.targetElement) {
            this.init();
        }
    }

    componentWillUnmount() {
        if (this.targetElement) {
            this.targetElement.removeEventListener('click', this.handleTargetClickEvent);
        }
        window.removeEventListener('click', this.handleWindowClickEvent, true);

        this.removeListeners();
        this.elementsHasListnerOnScrollEvent.forEach(elem =>
            elem.removeEventListener('scroll', this.handleWindowScrollEvent),
        );
        this.observers.forEach(observer => {
            observer.disconnect();
        });
    }

    init = () => {
        this.calculatePlacement();
        this.targetElement.addEventListener('click', this.handleTargetClickEvent);
        window.addEventListener('click', this.handleWindowClickEvent, true);

        const parentsScroll = this.findFirstParentScroll(this.targetElement);
        parentsScroll.addEventListener('scroll', this.handleWindowScrollEvent, false);
        this.elementsHasListnerOnScrollEvent.push(parentsScroll);

        const options = {
            root: parentsScroll,
            rootMargin: '0px',
            threshold: 1.0,
        };
        const observer = new IntersectionObserver(this.hasVisibleTarget, options);
        this.observers.push(observer);

        observer.observe(this.targetElement);
    };

    addListeners = () => {
        window.addEventListener('resize', this.handleResizeOrOrientation, false);
        window.addEventListener('orientationchange', this.handleResizeOrOrientation, false);
        window.addEventListener('scroll', this.handleWindowScrollEvent, false);
        window.addEventListener('touchstart', this.handleTouchStart, { passive: true });
        window.addEventListener('touchend', this.handleTouchEnd);
    };

    removeListeners = () => {
        window.removeEventListener('resize', this.handleResizeOrOrientation);
        window.removeEventListener('orientationchange', this.handleResizeOrOrientation);
        window.removeEventListener('scroll', this.handleScrollDebounced);
        window.removeEventListener('touchstart', this.handleTouchStart);
        window.removeEventListener('touchend', this.handleTouchEnd);
    };

    hasVisibleTarget = () => {
        const { isOpen } = this.state;
        if (isOpen) {
            this.setState({ isOpen: false });
        }
    };

    handleResizeOrOrientation = () => {
        const { isOpen } = this.state;
        if (isOpen) {
            this.calculatePlacement(true);
            setTimeout(this.calculatePlacement, 400);
        }
    };

    handleTouchStart = () => {
        window.addEventListener('touchmove', this.handleMoveDebounced, false);
    };

    handleTouchEnd = () => {
        window.removeEventListener('touchmove', this.handleMoveDebounced);
        // обрабатывается инерционный скролл на touch устройствах. После толчка, события не происходит, а скролл контента есть
        const counter = 10;
        const checkScroll = counterProp => {
            this.calculatePlacement();
            if (counterProp > 0) {
                setTimeout(checkScroll, 500, [counterProp - 1]);
            }
        };
        setTimeout(checkScroll, 500, [counter]);
    };

    handleTouchMove = () => {
        const { isOpen } = this.state;
        if (isOpen) {
            this.calculatePlacement();
        }
    };

    getParentsNode = (nodeProps, ps) => {
        if (nodeProps.parentNode === null || nodeProps.isEqualNode(document.documentElement)) {
            return ps;
        }
        return this.getParentsNode(nodeProps.parentNode, ps.concat([nodeProps]));
    };

    // у target ищем первого родителя, у которого есть возможность scroll. Для корректной нативной работы
    findFirstParentScroll = target => {
        const getScrollParent = node => {
            const regex = /(auto|scroll)/;

            const style = (nodeProps, prop) =>
                getComputedStyle(nodeProps, null).getPropertyValue(prop);
            const overflow = nodeProps =>
                style(nodeProps, 'overflow') +
                style(nodeProps, 'overflow-y') +
                style(nodeProps, 'overflow-x');
            const scroll = nodeProps => regex.test(overflow(nodeProps));

            /* eslint-disable consistent-return */
            const scrollParent = nodeProps => {
                if (!(nodeProps instanceof HTMLElement || nodeProps instanceof SVGElement)) {
                    return;
                }

                const ps = this.getParentsNode(nodeProps.parentNode, []);

                for (let i = 0; i < ps.length; i += 1) {
                    if (scroll(ps[i])) {
                        return ps[i];
                    }
                }

                return document.scrollingElement || document.documentElement;
            };
            return scrollParent(node);
            /* eslint-enable consistent-return */
        };
        return getScrollParent(target);
    };

    handleTargetClickEvent = e => {
        e.preventDefault();
        this.setState(
            prevState => ({ isOpen: !prevState.isOpen }),
            () => {
                const { isOpen } = this.state;
                if (isOpen) {
                    this.openTooltip();
                } else {
                    this.closeTooltip();
                }
            },
        );
    };

    handleWindowScrollEvent = () => {
        const { isOpen } = this.state;
        if (isOpen) {
            this.calculatePlacement();
        }
    };

    handleWindowClickEvent = e => {
        const isClickOnTarget = hasNodeInTree(e.target, this.targetElement);
        const isClickOnTooltip = hasNodeInTree(e.target, this.tooltip);

        if (!isClickOnTooltip && !isClickOnTarget) {
            this.closeTooltip();
        }
    };

    objectIsEmpty = object => {
        return Object.keys(object).length === 0;
    };

    calculatePlacement = isOpenProps => {
        const { placement } = this.props;
        const { isOpen, placementTooltip } = this.state;

        if (!isOpen) {
            return;
        }

        const newPlacementTooltip = getTooltipPlacement(
            this.targetElement,
            this.tooltip,
            placement,
        );

        const placementArrow = placementTooltip.arrow;
        if (!newPlacementTooltip) {
            return;
        }

        if (newPlacementTooltip.isNeedClose) {
            this.setState({ isOpen: false });
            return;
        }

        if (newPlacementTooltip.placementStyle !== placementTooltip.placementStyle) {
            this.setState({ placementTooltip: newPlacementTooltip });
            return;
        }

        if (isOpenProps) {
            this.setState({ placementTooltip: newPlacementTooltip });
            return;
        }
        const isNullArrow = this.objectIsEmpty(placementTooltip.arrow);
        const isNullPlacementArrow = this.objectIsEmpty(placementArrow);
        if (isNullPlacementArrow && !isNullArrow) {
            this.setState({ placementTooltip });
            return;
        }
        if (
            newPlacementTooltip.arrow.left !== placementArrow.left ||
            newPlacementTooltip.arrow.top !== placementArrow.top
        ) {
            this.setState({ placementTooltip: newPlacementTooltip });
            return;
        }
        if (this.tooltip.style.transform !== newPlacementTooltip.tooltip.transform) {
            this.tooltip.style.transform = newPlacementTooltip.tooltip.transform;
        }
    };

    closeTooltip = () => {
        this.setState({ isOpen: false });
    };

    openTooltip = () => {
        this.addListeners();
        this.setState({ isOpen: true }, () => {
            this.calculatePlacement(true);
        });
    };

    render() {
        const { children, className, content, position, applyTheme } = this.props;
        const { isOpen, placementTooltip } = this.state;

        const componentClass = applyTheme(
            cn('Tooltip', `Tooltip_placement_${placementTooltip.placementStyle}`, {
                Tooltip_visible: isOpen,
            }),
            className,
        );

        const arrowClass = applyTheme(
            cn('Tooltip__Arrow', `Tooltip__Arrow_placement_${placementTooltip.placementStyle}`),
        );

        const componentStyles = {
            ...placementTooltip.tooltip,
            position,
            top: 0,
            left: 0,
            willChange: 'transform',
        };

        const arrowStyles = {
            ...placementTooltip.arrow,
        };

        const childrenProps = {
            ref: el => {
                this.target = el;
                return this.target;
            },
        };

        return (
            <>
                {typeof children === 'string' ? (
                    <span {...childrenProps}>{children}</span>
                ) : (
                    React.cloneElement(children, childrenProps)
                )}

                {ReactDOM.createPortal(
                    <div
                        className={componentClass}
                        ref={el => {
                            this.tooltip = el;
                            return this.tooltip;
                        }}
                        style={componentStyles}
                    >
                        {content}
                        <div className={arrowClass} style={arrowStyles} />
                    </div>,
                    document.body,
                )}
            </>
        );
    }
}

Tooltip.propTypes = {
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    className: PropTypes.string,
    content: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
    placement: PropTypes.oneOf([
        PLACEMENTS.TOP,
        PLACEMENTS.RIGHT,
        PLACEMENTS.BOTTOM,
        PLACEMENTS.LEFT,
    ]),
    position: PropTypes.oneOf(['absolute', 'fixed']),
    applyTheme: PropTypes.func,
};

Tooltip.defaultProps = {
    children: null,
    className: '',
    content: null,
    placement: PLACEMENTS.BOTTOM,
    position: 'absolute',
    applyTheme: _ => _,
};

export default Tooltip;
