## Usage

```jsx
<Tooltip placement="top" _position_="fixed">
    <div>Click me and you will see the tooltip</div>
</Tooltip>
```

## Props

| Prop name   | Prop type                        | Default value |
| ----------- | -------------------------------- | ------------- |
| _content_   | node or string                   | `null`        |
| _placement_ | `top`, `right`, `bottom`, `left` | `bottom`      |
| _position_  | `absolute`, `fixed`              | `absolute`    |

## CSS Selectors

```css
.Tooltip {
}
.Tooltip_visible {
}
.Tooltip_placement_top {
}
.Tooltip_placement_right {
}
.Tooltip_placement_bottom {
}
.Tooltip_placement_left {
}

/* Arrow */
.Tooltip__Arrow {
}
.Tooltip__Arrow_placement_top {
}
.Tooltip__Arrow_placement_right {
}
.Tooltip__Arrow_placement_bottom {
}
.Tooltip__Arrow_placement_left {
}
```
