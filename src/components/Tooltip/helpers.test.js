import React from 'react';

import { getTooltipPlacement, getDefaultTooltipPlacement } from './helpers';

import { render } from '../../hoc/with-theme';
import Tooltip from './index';

window.IntersectionObserver = jest.fn(() => ({
    disconnect: jest.fn(),
    observe: jest.fn(),
    unobserve: jest.fn(),
}));
window.IntersectionObserver.prototype.disconnect = jest.fn();
document.elementFromPoint = jest.fn();

describe('Тестирование helpers функций для Tooltip', () => {
    describe('Тестирование функции getDefaultTooltipPlacement', () => {
        it('Должны отобразиться значения для тултипа по-умолчанию', () => {
            const output = {
                arrow: {},
                placementStyle: 'bottom',
                tooltip: {
                    transform: 'unset',
                },
            };
            expect(getDefaultTooltipPlacement()).toEqual(output);
        });
    });
    describe('Тестирование функции getTooltipPlacement', () => {
        it('Должны отобразиться значения для тултипа по-умолчанию, функция вызывается без параметров', () => {
            const output = {
                arrow: {},
                placementStyle: 'bottom',
                tooltip: {
                    transform: 'unset',
                },
            };
            expect(getTooltipPlacement()).toEqual(output);
        });
        it('Должны отобразиться значения для тултипа по-умолчанию, передаём элементы target и tooltip', () => {
            const { container } = render(
                <Tooltip className="Tooltip__Style">
                    <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                        Click
                    </div>
                </Tooltip>,
            );
            const target = container.querySelector('.Tooltip__Btn');
            target.getBoundingClientRect = jest.fn(() => {
                return { bottom: 1000, height: 20, left: 20, right: 1000, top: 30, width: 40 };
            });
            document.elementFromPoint = jest.fn(() => target);
            const tooltip = document.querySelector('.Tooltip');
            const output = {
                arrow: {
                    left: 20,
                },
                placementStyle: 'bottom',
                isNeedClose: false,
                tooltip: {
                    transform: 'translate3d(0px, 30px, 0)',
                },
            };
            expect(getTooltipPlacement(target, tooltip, 'placement')).toEqual(output);
        });
        it('Должны отобразиться значения для тултипа по-умолчанию, передаём элементы target', () => {
            const { container } = render(
                <Tooltip className="Tooltip__Style">
                    <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                        Click
                    </div>
                </Tooltip>,
            );
            const target = container.querySelector('.Tooltip__Btn');
            const output = {
                arrow: {},
                placementStyle: 'bottom',
                tooltip: {
                    transform: 'unset',
                },
            };
            expect(getTooltipPlacement(target, null, 'placement')).toEqual(output);
        });
        it('Должны отобразиться значения для тултипа по-умолчанию, передаём элементы tooltip', () => {
            render(
                <Tooltip className="Tooltip__Style">
                    <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                        Click
                    </div>
                </Tooltip>,
            );
            const tooltip = document.querySelector('.Tooltip');
            const output = {
                arrow: {},
                placementStyle: 'bottom',
                tooltip: {
                    transform: 'unset',
                },
            };
            expect(getTooltipPlacement(null, tooltip, 'placement')).toEqual(output);
        });
        it('Должны отобразиться значения для тултипа в положение "bottom"', () => {
            const { container } = render(
                <Tooltip className="Tooltip__Style">
                    <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                        Click
                    </div>
                </Tooltip>,
            );
            const target = container.querySelector('.Tooltip__Btn');
            target.getBoundingClientRect = jest.fn(() => {
                return { bottom: 1000, height: 20, left: 20, right: 1000, top: 30, width: 40 };
            });
            document.elementFromPoint = jest.fn(() => target);
            const tooltip = document.querySelector('.Tooltip');
            const output = {
                arrow: {
                    left: 20,
                },
                placementStyle: 'bottom',
                isNeedClose: false,
                tooltip: {
                    transform: 'translate3d(0px, 30px, 0)',
                },
            };
            expect(getTooltipPlacement(target, tooltip, 'bottom')).toEqual(output);
        });
        it('Должны отобразиться значения для тултипа в положение "top"', () => {
            const { container } = render(
                <Tooltip className="Tooltip__Style">
                    <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                        Click
                    </div>
                </Tooltip>,
            );
            const target = container.querySelector('.Tooltip__Btn');
            target.getBoundingClientRect = jest.fn(() => {
                return { bottom: 1000, height: 20, left: 20, right: 1000, top: 30, width: 40 };
            });
            document.elementFromPoint = jest.fn(() => target);
            const tooltip = document.querySelector('.Tooltip');
            const output = {
                arrow: {
                    left: 20,
                },
                placementStyle: 'top',
                isNeedClose: false,
                tooltip: {
                    transform: 'translate3d(0px, 30px, 0)',
                },
            };
            expect(getTooltipPlacement(target, tooltip, 'top')).toEqual(output);
        });
        it('Должны отобразиться значения для тултипа в положение "left"', () => {
            const { container } = render(
                <Tooltip className="Tooltip__Style">
                    <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                        Click
                    </div>
                </Tooltip>,
            );
            const target = container.querySelector('.Tooltip__Btn');
            target.getBoundingClientRect = jest.fn(() => {
                return { bottom: 1000, height: 20, left: 20, right: 1000, top: 30, width: 40 };
            });
            document.elementFromPoint = jest.fn(() => target);
            const tooltip = document.querySelector('.Tooltip');
            const output = {
                arrow: {
                    top: 30,
                },
                placementStyle: 'left',
                isNeedClose: false,
                tooltip: {
                    transform: 'translate3d(20px, 0px, 0)',
                },
            };
            expect(getTooltipPlacement(target, tooltip, 'left')).toEqual(output);
        });
        it('Должны отобразиться значения для тултипа в положение "right"', () => {
            const { container } = render(
                <Tooltip className="Tooltip__Style">
                    <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                        Click
                    </div>
                </Tooltip>,
            );
            const target = container.querySelector('.Tooltip__Btn');
            target.getBoundingClientRect = jest.fn(() => {
                return { bottom: 1000, height: 20, left: 50, right: 1000, top: 30, width: 40 };
            });
            Object.defineProperty(document.documentElement, 'clientWidth', {
                value: 200,
                configurable: true,
            });
            document.elementFromPoint = jest.fn(() => target);
            const tooltip = document.querySelector('.Tooltip');
            const output = {
                arrow: {
                    top: 30,
                },
                placementStyle: 'right',
                isNeedClose: false,
                tooltip: {
                    transform: 'translate3d(50px, 0px, 0)',
                },
            };
            expect(getTooltipPlacement(target, tooltip, 'right')).toEqual(output);
        });
        it('Должны отобразиться значения для тултипа с placement="right" в положение "left"', () => {
            const { container } = render(
                <Tooltip className="Tooltip__Style">
                    <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                        Click
                    </div>
                </Tooltip>,
            );
            const target = container.querySelector('.Tooltip__Btn');
            target.getBoundingClientRect = jest.fn(() => {
                return { bottom: 1000, height: 20, left: 400, right: 20, top: 30, width: 40 };
            });
            const tooltip = document.querySelector('.Tooltip');
            Object.defineProperty(tooltip, 'offsetWidth', {
                value: 130,
            });
            Object.defineProperty(document.documentElement, 'clientWidth', {
                value: 500,
            });
            document.elementFromPoint = jest.fn(() => target);
            const output = {
                arrow: {
                    top: 30,
                },
                placementStyle: 'left',
                isNeedClose: false,
                tooltip: {
                    transform: 'translate3d(270px, 0px, 0)',
                },
            };
            expect(getTooltipPlacement(target, tooltip, 'right')).toEqual(output);
        });

        it('Должны отобразиться значения для тултипа c placement="left" в положение "right"', () => {
            const { container } = render(
                <Tooltip className="Tooltip__Style">
                    <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                        Click
                    </div>
                </Tooltip>,
            );
            const target = container.querySelector('.Tooltip__Btn');
            target.getBoundingClientRect = jest.fn(() => {
                return { bottom: 1000, height: 20, left: 0, right: 250, top: 30, width: 40 };
            });
            Object.defineProperty(document.documentElement, 'clientWidth', {
                value: 500,
            });
            document.elementFromPoint = jest.fn(() => target);
            const tooltip = document.querySelector('.Tooltip');
            Object.defineProperty(tooltip, 'offsetWidth', {
                value: 30,
            });
            const output = {
                arrow: {
                    top: 30,
                },
                placementStyle: 'right',
                isNeedClose: false,
                tooltip: {
                    transform: 'translate3d(0px, 0px, 0)',
                },
            };
            expect(getTooltipPlacement(target, tooltip, 'left')).toEqual(output);
        });

        it('Должны отобразиться значения для тултипа c placement="top" в положение "bottom"', () => {
            const { container } = render(
                <Tooltip className="Tooltip__Style">
                    <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                        Click
                    </div>
                </Tooltip>,
            );
            const target = container.querySelector('.Tooltip__Btn');
            target.getBoundingClientRect = jest.fn(() => {
                return { bottom: 1000, height: 20, left: 0, right: 250, top: 30, width: 40 };
            });
            document.elementFromPoint = jest.fn(() => target);
            const tooltip = document.querySelector('.Tooltip');
            Object.defineProperty(tooltip, 'offsetHeight', {
                value: 40,
            });
            const output = {
                arrow: {},
                placementStyle: 'bottom',
                isNeedClose: false,
                tooltip: {
                    transform: 'translate3d(0px, 50px, 0)',
                },
            };
            expect(getTooltipPlacement(target, tooltip, 'top')).toEqual(output);
        });

        it('Должны отобразиться значения для тултипа c placement="bottom" в положение "top"', () => {
            const { container } = render(
                <Tooltip className="Tooltip__Style">
                    <div className="Tooltip__Btn" style={{ width: '40px', height: '20px' }}>
                        Click
                    </div>
                </Tooltip>,
            );
            const target = container.querySelector('.Tooltip__Btn');
            target.getBoundingClientRect = jest.fn(() => {
                return { bottom: 20, height: 20, left: 0, right: 250, top: 480, width: 40 };
            });
            document.elementFromPoint = jest.fn(() => target);
            const tooltip = document.querySelector('.Tooltip');
            Object.defineProperty(tooltip, 'offsetHeight', {
                value: 40,
            });
            Object.defineProperty(window, 'innerHeight', {
                value: 500,
            });
            const output = {
                arrow: {
                    left: 0,
                },
                placementStyle: 'top',
                isNeedClose: false,
                tooltip: {
                    transform: 'translate3d(0px, 500px, 0)',
                },
            };
            expect(getTooltipPlacement(target, tooltip, 'bottom')).toEqual(output);
        });
    });
});
