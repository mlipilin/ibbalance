import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, select, text } from '@storybook/addon-knobs';

// Components
import Button from '../../Button';

// Constants
import { LOCAL_STORAGE_THEME_KEY } from '../../../constants/local-storage';
import { PLACEMENTS } from '../../../constants/props';
import { THEMES } from '../../../constants/themes';

// HOC
import { withState } from '../../../hoc';

import Tooltip from '../index';

import notes from '../readme.md';

const TooltipWithState = withState(Tooltip);
const theme = localStorage.getItem(LOCAL_STORAGE_THEME_KEY);

let placementsList = [];

switch (theme) {
    case THEMES.DEFAULT:
        placementsList = [PLACEMENTS.TOP, PLACEMENTS.RIGHT, PLACEMENTS.BOTTOM, PLACEMENTS.LEFT];
        break;
    case THEMES.ABSOLUT:
        placementsList = [PLACEMENTS.TOP, PLACEMENTS.RIGHT, PLACEMENTS.BOTTOM, PLACEMENTS.LEFT];
        break;
    case THEMES.ALFA:
        placementsList = [PLACEMENTS.TOP, PLACEMENTS.RIGHT, PLACEMENTS.BOTTOM, PLACEMENTS.LEFT];
        break;
    case THEMES.SPB:
        placementsList = [PLACEMENTS.TOP, PLACEMENTS.RIGHT, PLACEMENTS.BOTTOM, PLACEMENTS.LEFT];
        break;
    case THEMES.TKB:
        placementsList = [PLACEMENTS.TOP, PLACEMENTS.RIGHT, PLACEMENTS.BOTTOM, PLACEMENTS.LEFT];
        break;
    case THEMES.TKB_LKZ:
        placementsList = [PLACEMENTS.TOP, PLACEMENTS.RIGHT, PLACEMENTS.BOTTOM, PLACEMENTS.LEFT];
        break;
    case THEMES.KYC:
        placementsList = [PLACEMENTS.TOP, PLACEMENTS.RIGHT, PLACEMENTS.BOTTOM, PLACEMENTS.LEFT];
        break;
    default:
        break;
}

const stories = storiesOf('Tooltip', module);

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories.add(
    'Default',
    () => (
        <TooltipWithState
            content={text('Content', 'Write your content here')}
            placement={select('Placement', placementsList, PLACEMENTS.BOTTOM)}
            position={select('Position', ['absolute', 'fixed'], 'absolute')}
        >
            <Button>{text('Child', 'Write child content here')}</Button>
        </TooltipWithState>
    ),
    { notes },
);
