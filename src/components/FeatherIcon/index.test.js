import React from 'react';

import FeatherIcon from './index';

// Constants
import NAMES_ARRAY from './constants';

import { render } from '../../hoc/with-theme';

describe('Тестирование компонента FeatherIcon', () => {
    it('Должен отобразиться null', () => {
        const { container } = render(<FeatherIcon />);
        expect(container.firstChild).toMatchSnapshot();
    });
    it('Должен отобразиться null name не найден', () => {
        const { container } = render(<FeatherIcon name="email" />);
        expect(container.firstChild).toMatchSnapshot();
    });
    it('Должен отобразиться FeatherIcon c className="FeatherIcon__Style"', () => {
        const { container } = render(<FeatherIcon className="FeatherIcon__Style" name="mail" />);
        const icon = container.querySelector('.FeatherIcon__Style');
        expect(icon).toBeInTheDocument();
        expect(container).toMatchSnapshot();
    });
    it('Должен отобразиться FeatherIcon c name из перечня NAMES_ARRAY', () => {
        NAMES_ARRAY.forEach(name => {
            const { container } = render(
                <FeatherIcon className="FeatherIcon__Style" name={name} />,
            );
            const icon = container.querySelector(`.feather-${name}`);
            expect(icon).toBeInTheDocument();
        });
    });
});
