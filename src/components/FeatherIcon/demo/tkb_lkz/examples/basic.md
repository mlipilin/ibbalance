# Базовое использование

В свойстве `name` нужно передать название иконки

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { FeatherIcon, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/tkb_lkz/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <FeatherIcon name="github" />
  </ThemeProvider>,
  document.getElementById('root')
);
```
