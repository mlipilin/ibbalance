import React from 'react';

// Components
import FeatherIcon from '../../index';
import Icons from '../../components/Icons';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';

// Examples
import basic from './examples/basic.md';

// Wrappers
import { Col, Row } from '../../../../wrappers/WGrid';

const DemoFeatherIcon = () => {
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>

            <div id="example">
                <Row
                    cols={{ mobile: [1], tab: [1, 1], desktop: [1], largeDesktop: [1, 1] }}
                    gap="20px"
                >
                    <Col>
                        <Example readme={basic}>
                            <FeatherIcon name="github" />
                        </Example>
                    </Col>
                    <Col />
                </Row>
            </div>

            {/* Полный список */}
            <Icons />
        </DemoComponent>
    );
};

export default DemoFeatherIcon;
