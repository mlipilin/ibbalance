import React, { Component } from 'react';

// Components
import { Notification, NotificationWrapper } from '../../../Notification';

// Constants
import NAMES_ARRAY from '../../constants';

import FeatherIcon from '../../index';

import styles from './styles.sass';

class Icons extends Component {
    constructor(props) {
        super(props);
        this.state = { notifications: [] };
    }

    handleIconClick = name => () => {
        const componentCode = `<FeatherIcon name="${name}" />`;
        this.clipboardInput.value = componentCode;
        this.clipboardInput.select();
        document.execCommand('copy');

        this.setState(prevState => {
            const notifications = [
                ...prevState.notifications,
                {
                    title: 'Код скопирован в буфер обмена',
                    description: componentCode,
                },
            ];
            return { notifications };
        });
    };

    render() {
        const { notifications } = this.state;

        return (
            <>
                <div className={styles.ClipboardInputWrapper}>
                    <input
                        className={styles.ClipboardInput}
                        ref={el => {
                            this.clipboardInput = el;
                            return this.clipboardInput;
                        }}
                    />
                </div>
                <div className={styles.Icons}>
                    {NAMES_ARRAY.map(name => (
                        <div
                            key={name}
                            className={styles.Icon}
                            onClick={this.handleIconClick(name)}
                        >
                            <div className={styles.Icon__Icon}>
                                <FeatherIcon name={name} />
                            </div>
                            <div className={styles.Icon__Title}>
                                <i>{name}</i>
                            </div>
                        </div>
                    ))}
                </div>
                <NotificationWrapper>
                    {notifications.map(({ title }) => (
                        <Notification key={title} title={title} />
                    ))}
                </NotificationWrapper>
            </>
        );
    }
}

export default Icons;
