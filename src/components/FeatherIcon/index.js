import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { icons } from 'feather-icons';

// Constants
import NAMES_ARRAY from './constants';

import { withTheme } from '../../theme-provider';

@withTheme
class FeatherIcon extends Component {
    render() {
        const { className, name, applyTheme, ...otherProps } = this.props;

        if (!name) return null;
        if (!icons[name]) return null;

        const componentClass = applyTheme(cn('FeatherIcon'), className);

        return (
            <span
                {...otherProps}
                className={componentClass}
                dangerouslySetInnerHTML={{
                    __html: icons[name].toSvg(),
                }}
            />
        );
    }
}

FeatherIcon.propTypes = {
    className: PropTypes.string,
    name: PropTypes.oneOf(NAMES_ARRAY),
    applyTheme: PropTypes.func,
};

FeatherIcon.defaultProps = {
    className: '',
    name: null,
    applyTheme: _ => _,
};

export default FeatherIcon;
