import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import FeatherIcon from '../FeatherIcon';
import SelectBox from './index';

import { render } from '../../hoc/with-theme';

import countries from './stories/countries.json';

describe('Тестирование компонента SelectBox', () => {
    it('Должен отобразиться SelectBox', () => {
        const { container } = render(<SelectBox />);
        expect(screen.getByLabelText('')).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться SelectBox с заголовком Страны', () => {
        render(<SelectBox label="Страны" />);
        expect(screen.getByLabelText('Страны')).toBeInTheDocument();
        expect(screen.getByLabelText('Страны')).toMatchSnapshot();
    });

    it('Должен отобразиться SelectBox с заголовком-нодой', () => {
        const { container } = render(<SelectBox label={<span>Страны</span>} />);
        expect(screen.getByLabelText('Страны')).toBeInTheDocument();
        const labelText = container.querySelector('.Input__Label > span');
        expect(labelText).toBeInTheDocument();
        expect(screen.getByLabelText('Страны')).toMatchSnapshot();
    });

    it('Должен отобразиться SelectBox с заголовком-массивом нод', () => {
        const { container } = render(
            <SelectBox
                label={[<span key="страны">Страны</span>, <span key="участники">участники</span>]}
            />,
        );
        const labelText = container.querySelector('.Input__LabelText');
        expect(labelText.firstChild.textContent).toBe('Страны');
        expect(labelText.lastChild.textContent).toBe('участники');
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться SelectBox с текстом "Страна не найдена"', () => {
        const { container } = render(<SelectBox notFoundText="Страна не найдена" />);
        const notFoundText = container.querySelector('.SelectBox__NotFound');
        expect(notFoundText).toBeInTheDocument();
        expect(notFoundText.textContent).toBe('Страна не найдена');
        expect(notFoundText).toMatchSnapshot();
    });

    it('Должен отобразиться SelectBox в состоянии disabled', () => {
        const { container } = render(<SelectBox disabled />);
        const disabledElem = container.querySelector('.SelectBox_disabled');
        expect(disabledElem).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться SelectBox в состоянии disabled - не получится выбрать', () => {
        const handleFocus = jest.fn();
        const { container } = render(<SelectBox disabled onFocus={handleFocus} />);
        const disabledElem = container.querySelector('.SelectBox_disabled');
        userEvent.click(disabledElem);
        expect(handleFocus).toHaveBeenCalledTimes(0);
    });

    it('Должен отобразиться SelectBox в состоянии editable', () => {
        const { container } = render(<SelectBox options={countries} editable />);
        const input = container.querySelector('input');
        expect(input).toBeInTheDocument();
        userEvent.type(input, 'Росс');
        expect(screen.getByText('Росс')).toMatchSnapshot();
    });

    it('Должен отобразиться SelectBox в состоянии readonly', () => {
        const { container } = render(<SelectBox readOnly />);
        const readOnlyElem = container.querySelector('.SelectBox_readonly');
        expect(readOnlyElem).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться SelectBox в состоянии readonly - не получится раскрыть', () => {
        const handleFocus = jest.fn();
        const { container } = render(<SelectBox readOnly onFocus={handleFocus} />);
        const readOnlyElem = container.querySelector('.SelectBox_readonly');
        expect(readOnlyElem).toBeInTheDocument();
        userEvent.click(readOnlyElem);
        expect(handleFocus).toHaveBeenCalledTimes(0);
    });

    it('Должен отобразиться SelectBox с error = "Обязательное поле"', () => {
        const { container } = render(<SelectBox error="Обязательное поле" />);
        const errorElem = container.querySelector('.Input__Error');
        expect(errorElem).toBeInTheDocument();
        expect(errorElem.textContent).toBe('Обязательное поле');
        expect(errorElem).toMatchSnapshot();
    });

    it('Должен отобразиться SelectBox со скрытым полем errorDetail = "Обязательное поле"', () => {
        const { container } = render(<SelectBox errorDetail="Обязательное поле" />);
        const errorElem = container.querySelector('.Input__Tooltip');
        expect(errorElem).toBeInTheDocument();
        expect(errorElem.textContent).toBe('Обязательное поле');
        expect(errorElem).toMatchSnapshot();
        const iconError = container.querySelector('.feather-alert-circle');
        expect(iconError).toBeInTheDocument();
    });

    it('Должен отобразиться SelectBox с errorDetail = "Обязательное поле" ', async () => {
        const { container } = render(<SelectBox errorDetail="Обязательное поле" />);
        const iconError = container.querySelector('.feather-alert-circle');
        await userEvent.click(iconError);
        const errorElem = container.querySelector('.Input__Tooltip_visible');
        expect(errorElem).toBeInTheDocument();
        expect(errorElem.textContent).toBe('Обязательное поле');
        expect(errorElem).toMatchSnapshot();
    });

    it('Должен отобразиться SelectBox с errorDetailIcon = "alert-octagon" /> ', () => {
        const { container } = render(
            <SelectBox
                errorDetail="Обязательное поле"
                errorDetailIcon={<FeatherIcon name="alert-octagon" />}
            />,
        );
        const iconError = container.querySelector('.feather-alert-octagon');
        expect(iconError).toBeInTheDocument();
    });

    it('Должен отобразиться SelectBox с options ', () => {
        const { container } = render(<SelectBox options={countries} />);
        const options = container.querySelectorAll('.Suggest__Option');
        expect(options.length).toBe(209);
    });

    it('Должен отобразиться SelectBox с size="m" по-умолчанию ', () => {
        const { container } = render(<SelectBox />);
        const conteinerSizeElem = container.querySelector('.SelectBox_size_m');
        expect(conteinerSizeElem).toBeInTheDocument();
    });

    it('Должен отобразиться SelectBox с size="s" ', () => {
        const { container } = render(<SelectBox size="s" />);
        const conteinerSizeElem = container.querySelector('.SelectBox_size_s');
        expect(conteinerSizeElem).toBeInTheDocument();
        expect(conteinerSizeElem).toMatchSnapshot();
    });

    it('Должен отобразиться SelectBox с size="m" ', () => {
        const { container } = render(<SelectBox size="m" />);
        const conteinerSizeElem = container.querySelector('.SelectBox_size_m');
        expect(conteinerSizeElem).toBeInTheDocument();
        expect(conteinerSizeElem).toMatchSnapshot();
    });

    it('Должен отобразиться SelectBox с size="l" ', () => {
        const { container } = render(<SelectBox size="l" />);
        const conteinerSizeElem = container.querySelector('.SelectBox_size_l');
        expect(conteinerSizeElem).toBeInTheDocument();
        expect(conteinerSizeElem).toMatchSnapshot();
    });

    it('Должен отобразиться SelectBox в состоянии success', () => {
        const { container } = render(<SelectBox success />);
        const inputSuccess = container.querySelector('.Input__Input_success');
        expect(inputSuccess).toBeInTheDocument();
        expect(inputSuccess).toMatchSnapshot();
    });

    it('Должен отобразиться SelectBox c value = {109}', () => {
        const { container } = render(<SelectBox options={countries} value={10} />);
        const input = container.querySelector('input');
        expect(input).toBeInTheDocument();
        expect(input.value).toBe('Афганистан');
        expect(input).toMatchSnapshot();
    });

    it('Должен отобразиться SelectBox c mask', async () => {
        const { container } = render(<SelectBox mask="+7 (999) 999-99-99" />);
        const input = container.querySelector('input');
        expect(input).toBeInTheDocument();
        await userEvent.type(input, '{space}');
        expect(input.value).toBe('+7 (');
        expect(input).toMatchSnapshot();
    });

    it('Должен отобразиться SelectBox co свойством isSuggestWrapperVisible', () => {
        const { container } = render(<SelectBox isSuggestWrapperVisible />);
        const wrapper = container.querySelector('.SelectBox__Icon_suggest_visible');
        expect(wrapper).toBeInTheDocument();
        expect(container.firstChild).toMatchSnapshot();
    });

    it('Должен отобразиться SelectBox с форматированным списком formatOption', () => {
        const onFormatOption = name => [name[0].toLowerCase()];
        const { container } = render(
            <SelectBox formatOption={onFormatOption} options={countries} />,
        );
        const firstCountry = container.querySelector('#select_box_option_0');
        expect(firstCountry).toBeInTheDocument();
        expect(firstCountry.textContent).toBe('австралия');
        expect(firstCountry).toMatchSnapshot();
    });

    it('Должен отобразиться SelectBox со значением "Австрия"', async () => {
        const onChange = jest.fn();
        const { container } = render(<SelectBox options={countries} onChange={onChange} />);
        const input = container.querySelector('input');
        expect(input).toBeInTheDocument();
        input.focus();
        const suggestOptions = container.querySelector('.Suggest__Options');
        expect(suggestOptions).toBeInTheDocument();
        await userEvent.type(input, '{arrowdown}{arrowup}{arrowdown}{enter}');
        expect(onChange).toHaveBeenCalledTimes(1);
        expect(onChange).toBeCalledWith(1, expect.any(Object));
    });

    it('Должен отобразиться SelectBox у которого при проходе стрелками disabled (Азербайджан) пропускается', async () => {
        const onChange = jest.fn();
        const { container } = render(<SelectBox options={countries} onChange={onChange} />);
        const input = container.querySelector('input');
        await userEvent.type(input, '{arrowdown}{arrowdown}{arrowdown}{enter}');
        expect(onChange).toHaveBeenCalledTimes(1);
        expect(onChange).toBeCalledWith(3, expect.any(Object));
        expect(countries[3].name).toBe('Албания');
    });

    it('Должен отобразиться SelectBox с активным элементом Австралия после прохода стрелками до самого верхнего элемента', async () => {
        const onChange = jest.fn();
        const { container } = render(<SelectBox options={countries} onChange={onChange} />);
        const input = container.querySelector('input');
        await userEvent.type(input, '{arrowup}{arrowup}');
        const activeElem = container.querySelector('.SuggestOption_active');
        expect(activeElem.textContent).toBe('Австралия');
    });

    it('Должен отобразиться SelectBox с активным элементом Австралия после прохода стрелками до самого нижнего элемента', async () => {
        const onChange = jest.fn();
        const smallCountries = [
            {
                id: 0,
                name: 'Австралия',
            },
            {
                id: 1,
                name: 'Австрия',
            },
            {
                disabled: true,
                id: 2,
                name: 'Азербайджан',
            },
            {
                id: 3,
                name: 'Албания',
            },
            {
                disabled: true,
                id: 4,
                name: 'Алжир',
            },
            {
                disabled: true,
                id: 5,
                name: 'Ангола',
            },
            {
                id: 6,
                name: 'Андорра',
            },
            {
                id: 7,
                name: 'Антигуа и Барбуда',
            },
        ];
        const { container } = render(<SelectBox options={smallCountries} onChange={onChange} />);
        const input = container.querySelector('input');
        await userEvent.type(
            input,
            '{arrowdown}{arrowdown}{arrowdown}{arrowdown}{arrowdown}{arrowdown}{arrowdown}{arrowdown}{arrowdown}',
        );
        const activeElem = container.querySelector('.SuggestOption_active');
        expect(activeElem.textContent).toBe('Антигуа и Барбуда');
    });

    it('Должен отобразиться SelectBox editable c правльно отсортированным списком', async () => {
        const onChange = jest.fn();
        const { container } = render(
            <SelectBox options={countries} onChange={onChange} editable />,
        );
        const input = container.querySelector('input');
        await userEvent.type(input, 'Ро');
        const selectCountries = container.querySelectorAll('.SuggestOption');
        const array = [
            'Багамские Острова',
            'Марокко',
            'Маршалловы Острова',
            'Россия',
            'Сейшельские Острова',
            'Соломоновы Острова',
            'Федеративные Штаты Микронезии',
            'Приднестровье',
            'Острова Кука',
        ];
        expect(Array.from(selectCountries).map(option => option.textContent)).toEqual(array);
    });

    it.skip('Должен отобразиться SelectBox у которого при клике на стрелки - происходит scroll', () => {});
});
