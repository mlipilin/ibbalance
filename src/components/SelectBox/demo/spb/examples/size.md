# Размер селект-бокса

Свойство size может принимать одно из следующих значений: `s`, `m` и `l`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { SelectBox, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/spb/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <SelectBox size="m" label="Страна" options={[
      { id: 1, name: 'Австралия' },
      { id: 2, name: 'Австрия' },
      { id: 3, name: 'Азербайджан' },
      // ...more items
    ]} />
    <SelectBox size="s" label="Страна" options={[
      { id: 1, name: 'Австралия' },
      { id: 2, name: 'Австрия' },
      { id: 3, name: 'Азербайджан' },
      // ...more items
    ]} />
  </ThemeProvider>,
  document.getElementById('root')
);
```
