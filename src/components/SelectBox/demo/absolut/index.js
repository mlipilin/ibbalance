import React from 'react';

// Components
import SelectBox from '../../index';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';

// Examples
import base from './examples/base.md';
import disabled from './examples/disabled.md';
import editable from './examples/editable.md';
import error from './examples/error.md';
import readOnly from './examples/readonly.md';
import success from './examples/success.md';

// HOC
import { withState } from '../../../../hoc';

// Wrappers
import { Col, Row } from '../../../../wrappers/WGrid';

// Countries
import countries from '../../stories/countries.json';

const SelectBoxWithState = withState(SelectBox);

const DemoSelectBox = () => {
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>

            <div id="example">
                <Row
                    cols={{ mobile: [1], tab: [1, 1], desktop: [1], largeDesktop: [1, 1] }}
                    gap="20px"
                >
                    <Col>
                        <Example readme={base}>
                            <SelectBoxWithState label="Страна" options={countries} />
                        </Example>
                        <Example readme={editable}>
                            <SelectBoxWithState
                                editable
                                label="Страна"
                                notFoundText="Ничего не нашел ¯\_(ツ)_/¯"
                                options={countries}
                            />
                        </Example>
                        <Example readme={error}>
                            <SelectBoxWithState
                                error="В поле ошибка"
                                label="Страна"
                                options={countries}
                            />
                        </Example>
                    </Col>
                    <Col>
                        <Example readme={success}>
                            <SelectBoxWithState label="Страна" options={countries} success />
                        </Example>
                        <Example readme={disabled}>
                            <SelectBoxWithState disabled label="Страна" options={countries} />
                        </Example>
                        <Example readme={readOnly}>
                            <SelectBoxWithState label="Страна" options={countries} readOnly />
                        </Example>
                    </Col>
                </Row>
            </div>
        </DemoComponent>
    );
};

export default DemoSelectBox;
