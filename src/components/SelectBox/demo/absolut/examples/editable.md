# Селект-бокс с возможностью поиска

Допускается ввод текста в поле для поиска по списку вариантов

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { SelectBox, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/absolut/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <SelectBox
      editable
      label="Страна"
      notFoundText="Ничего не нашел ¯\_(ツ)_/¯"
      options={[
        { id: 1, name: 'Австралия' },
        { id: 2, name: 'Австрия' },
        { id: 3, name: 'Азербайджан' },
        // ...more items
      ]}
    />
  </ThemeProvider>,
  document.getElementById('root')
);
```
