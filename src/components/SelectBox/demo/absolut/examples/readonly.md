# "read only" состояние

`true`: селект-бокс становится 'read only' (только для чтения)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { SelectBox, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/absolut/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <SelectBox readOnly label="Страна" options={[
      { id: 1, name: 'Австралия' },
      { id: 2, name: 'Австрия' },
      { id: 3, name: 'Азербайджан' },
      // ...more items
    ]} />
  </ThemeProvider>,
  document.getElementById('root')
);
```
