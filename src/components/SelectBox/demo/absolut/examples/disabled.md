# "disabled" состояние

`true`: селект-бокс становится 'disabled' (недоступен для редактирования)

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { SelectBox, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/absolut/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <SelectBox disabled label="Страна" options={[
      { id: 1, name: 'Австралия' },
      { id: 2, name: 'Австрия' },
      { id: 3, name: 'Азербайджан' },
      // ...more items
    ]} />
  </ThemeProvider>,
  document.getElementById('root')
);
```
