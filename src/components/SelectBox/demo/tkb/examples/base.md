# Простой селект-бокс

Подходит для большинства ситуаций

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { SelectBox, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/tkb/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <SelectBox label="Страна" options={[
      { id: 1, name: 'Австралия' },
      { id: 2, name: 'Австрия' },
      { id: 3, name: 'Азербайджан' },
      // ...more items
    ]} />
  </ThemeProvider>,
  document.getElementById('root')
);
```
