import React, { useEffect, useState } from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean, select, text } from '@storybook/addon-knobs';

// Constants
import { LOCAL_STORAGE_THEME_KEY } from '../../../constants/local-storage';
import { SIZES } from '../../../constants/props';
import { THEMES } from '../../../constants/themes';

// HOC
import { withState } from '../../../hoc';

import SelectBox from '../index';

import countries from './countries.json';

const countriesObject = countries.reduce(
    (res, c) => ({
        ...res,
        [c.name]: c.id,
    }),
    {},
);
const countriesWithDisabledOptions = countries.map(country =>
    country.id > 5 && country.id <= 10 ? { ...country, disabled: true } : country,
);

const SelectBoxWithState = withState(SelectBox);

const theme = localStorage.getItem(LOCAL_STORAGE_THEME_KEY);

let sizesList = [];

switch (theme) {
    case THEMES.DEFAULT:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        break;
    case THEMES.ALFA:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.ABSOLUT:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.SPB:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.TKB:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.TKB_LKZ:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.KYC:
        sizesList = [SIZES.S, SIZES.M];
        break;
    case THEMES.KYC_LANDING:
        sizesList = [SIZES.S, SIZES.M];
        break;
    default:
        break;
}

const stories = storiesOf('SelectBox', module);

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories
    .add('With label', () => (
        <SelectBoxWithState
            disabled={boolean('disabled', false)}
            editable={boolean('editable', false)}
            error={text('error', '')}
            errorDetail={text('errorDetail', '')}
            label={text('label', 'Страна')}
            notFoundText={text('notFoundText', 'Совпадений не найдено')}
            options={countries}
            readOnly={boolean('readOnly', false)}
            size={select('size', sizesList, SIZES.M)}
            success={boolean('success', false)}
        />
    ))
    .add('Without label', () => (
        <SelectBoxWithState
            disabled={boolean('disabled', false)}
            editable={boolean('editable', false)}
            error={text('error', '')}
            errorDetail={text('errorDetail', '')}
            notFoundText={text('notFoundText', 'Совпадений не найдено')}
            options={countries}
            readOnly={boolean('readOnly', false)}
            size={select('size', sizesList, SIZES.M)}
            success={boolean('success', false)}
        />
    ))
    .add('With predefined value', () => (
        <SelectBoxWithState
            disabled={boolean('disabled', false)}
            editable={boolean('editable', false)}
            error={text('error', '')}
            errorDetail={text('errorDetail', '')}
            label={text('label', 'Страна')}
            notFoundText={text('notFoundText', 'Совпадений не найдено')}
            options={countries}
            readOnly={boolean('readOnly', false)}
            size={select('size', sizesList, SIZES.M)}
            success={boolean('success', false)}
            value={select('value', countriesObject, countries[50].id)}
        />
    ))
    .add('Small set of options', () => (
        <SelectBoxWithState
            disabled={boolean('disabled', false)}
            editable={boolean('editable', false)}
            error={text('error', '')}
            errorDetail={text('errorDetail', '')}
            label={text('label', 'Подтверждение дохода')}
            notFoundText={text('notFoundText', 'Совпадений не найдено')}
            options={[
                {
                    id: 1,
                    name: 'Зарплатный клиент',
                },
                {
                    id: 2,
                    name: 'Ипотека по 2 документам',
                },
                {
                    id: 3,
                    name: '2-НДФЛ/Справка по форме банка',
                },
            ]}
            readOnly={boolean('readOnly', false)}
            size={select('size', sizesList, SIZES.M)}
            success={boolean('success', false)}
        />
    ))
    .add('Use formatOption', () => (
        <SelectBoxWithState
            disabled={boolean('disabled', false)}
            editable={boolean('editable', false)}
            error={text('error', '')}
            errorDetail={text('errorDetail', '')}
            label={text('label', 'Подтверждение дохода')}
            notFoundText={text('notFoundText', 'Совпадений не найдено')}
            options={[
                {
                    id: 1,
                    name: 'Зарплатный клиент',
                },
                {
                    id: 2,
                    name: 'Ипотека по 2 документам',
                },
                {
                    id: 3,
                    name: '2-НДФЛ/Справка по форме банка',
                },
            ]}
            readOnly={boolean('readOnly', false)}
            size={select('size', sizesList, SIZES.M)}
            success={boolean('success', false)}
            formatOption={(name, option) => {
                return [option.id, ': ', ...name];
            }}
        />
    ))
    .add('With disabled options', () => (
        <SelectBoxWithState
            disabled={boolean('disabled', false)}
            editable={boolean('editable', false)}
            error={text('error', '')}
            errorDetail={text('errorDetail', '')}
            label={text('label', 'Страна')}
            notFoundText={text('notFoundText', 'Совпадений не найдено')}
            options={countriesWithDisabledOptions}
            readOnly={boolean('readOnly', false)}
            size={select('size', sizesList, SIZES.M)}
            success={boolean('success', false)}
        />
    ))
    .add('Change options by timer', () => <ChangeOptionsByTimer />)
    .add('Change value by timer', () => <ChangeValueByTimer />);

function ChangeOptionsByTimer() {
    const [options, setOptions] = useState([]);

    useEffect(() => {
        setTimeout(() => {
            setOptions([
                {
                    id: 1,
                    name: 'Зарплатный клиент',
                },
                {
                    id: 2,
                    name: 'Ипотека по 2 документам',
                },
                {
                    id: 3,
                    name: '2-НДФЛ/Справка по форме банка',
                },
            ]);
        }, 1500);
    }, []);

    return (
        <SelectBoxWithState
            disabled={boolean('disabled', false)}
            editable={boolean('editable', false)}
            error={text('error', '')}
            errorDetail={text('errorDetail', '')}
            label={text('label', 'Подтверждение дохода')}
            notFoundText={text('notFoundText', 'Совпадений не найдено')}
            options={options}
            readOnly={boolean('readOnly', false)}
            size={select('size', sizesList, SIZES.M)}
            success={boolean('success', false)}
            value={2}
        />
    );
}

function ChangeValueByTimer() {
    const [value, setValue] = useState(1);

    useEffect(() => {
        setTimeout(() => {
            setValue(3);
        }, 1500);
    }, []);

    return (
        <SelectBoxWithState
            disabled={boolean('disabled', false)}
            editable={boolean('editable', false)}
            error={text('error', '')}
            errorDetail={text('errorDetail', '')}
            label={text('label', 'Подтверждение дохода')}
            notFoundText={text('notFoundText', 'Совпадений не найдено')}
            options={[
                {
                    id: 1,
                    name: 'Зарплатный клиент',
                },
                {
                    id: 2,
                    name: 'Ипотека по 2 документам',
                },
                {
                    id: 3,
                    name: '2-НДФЛ/Справка по форме банка',
                },
            ]}
            readOnly={boolean('readOnly', false)}
            size={select('size', sizesList, SIZES.M)}
            success={boolean('success', false)}
            value={value}
        />
    );
}
