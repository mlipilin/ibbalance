# Содержимое большого размера

В этом случае появляется скролл

```jsx
import React, { useState } from 'react';
import ReactDOM from 'react-dom';

import { Button, Modal, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/alfa/theme';

function App() {
  // State
  const [isModalOpen, setIsModalOpen] = useState(false);

  // Handlers
  function handleButtonClick() {
    setIsModalOpen(true);
  }
  function handleModalClose() {
    setIsModalOpen(false);
  }

  return (
    <>
      <Button onClick={handleButtonClick}>Открыть модалку</Button>
      <Modal isOpen={isModalOpen} title="Заголовок" onClose={handleModalClose}>
        {Array.from(new Array(100)).map((item, index) => (
            <p key={index}>Строка {index + 1}</p>
        ))}
      </Modal>
    </>
  )
}

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <App />
  </ThemeProvider>,
  document.getElementById('root')
);
```
