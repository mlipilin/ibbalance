# Код

Свойство hideCloseBtn может принимать одно из следующих значений: `true`, `false`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Spin, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/tkb_lkz/theme';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <>
      <Button onClick={() => this.modal.open()}>Открыть модалку</Button>
      <Modal
          ref={el => (this.modal = el)}
          isOpen={false}
          title="Заголовок"
          hideCloseBtn={false}
          onClose={() => this.modal.close()}
      >
          Тут какой-то текст
          <Button size="s">Кнопка</Button>
      </Modal>
  </>
  </ThemeProvider>,
  document.getElementById('root')
);
```
