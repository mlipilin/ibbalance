# Модалка без возможности закрытия

Для случаев, когда нужно, чтобы пользователь сделал какие-либо действия, а не просто закрыл модалку

```jsx
import React, { useState } from 'react';
import ReactDOM from 'react-dom';

import { Button, Modal, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/vkusvill/theme';

function App() {
  // State
  const [isModalOpen, setIsModalOpen] = useState(false);

  // Handlers
  function handleButtonClick() {
    setIsModalOpen(true);
  }
  function handleButtonCloseClick() {
    setIsModalOpen(false);
  }

  return (
    <>
      <Button onClick={handleButtonClick}>Открыть модалку</Button>
      <Modal
        disableOverlayClick
        hideCloseBtn
        isOpen={isModalOpen}
        title="Заголовок"
      >
        <p>
            Какое-то содержимое модалки, которую нельзя закрыть
            стандартным способом, а можно например только по этой кнопке:
        </p>
        <Button onClick={handleButtonCloseClick}>Закрыть модалку</Button>
      </Modal>
    </>
  )
}

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <App />
  </ThemeProvider>,
  document.getElementById('root')
);
```
