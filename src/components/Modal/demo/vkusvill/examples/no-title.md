# Модалка без заголовка

Когда не нужен заголовок, его его можно просто не передавать

```jsx
import React, { useState } from 'react';
import ReactDOM from 'react-dom';

import { Button, Modal, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/vkusvill/theme';

function App() {
  // State
  const [isModalOpen, setIsModalOpen] = useState(false);

  // Handlers
  function handleButtonClick() {
    setIsModalOpen(true);
  }
  function handleModalClose() {
    setIsModalOpen(false);
  }

  return (
    <>
      <Button onClick={handleButtonClick}>Открыть модалку</Button>
      <Modal isOpen={isModalOpen} onClose={handleModalClose}>
        Какое-то содержимое модалки без заголовка
      </Modal>
    </>
  )
}

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <App />
  </ThemeProvider>,
  document.getElementById('root')
);
```
