import React, { useState } from 'react';

// Components
import PropTypes from 'prop-types';
import Button from '../../../Button';
import Modal from '../../index';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

// Examples
import basic from './examples/basic.md';
import noClose from './examples/no-close.md';
import noTitle from './examples/no-title.md';
import scroll from './examples/scroll.md';
import size from './examples/size.md';

// Typography
import H from '../../../../typography/H';

// Wrappers
import { Col, Row } from '../../../../wrappers/WGrid';

import readme from './readme.md';

const ModalContainer = props => {
    const { children, ...modalProps } = props;

    // State
    const [isModalOpen, setIsModalOpen] = useState(false);

    // Handlers
    function handleButtonClick() {
        setIsModalOpen(true);
    }
    function handleModalClose() {
        setIsModalOpen(false);
    }

    return (
        <>
            <Button onClick={handleButtonClick}>Открыть модалку</Button>
            <Modal {...modalProps} isOpen={isModalOpen} onClose={handleModalClose}>
                {children}
            </Modal>
        </>
    );
};

const ModalNoClose = () => {
    // State
    const [isModalOpen, setIsModalOpen] = useState(false);

    // Handlers
    function handleButtonClick() {
        setIsModalOpen(true);
    }
    function handleButtonCloseClick() {
        setIsModalOpen(false);
    }

    return (
        <>
            <Button onClick={handleButtonClick}>Открыть модалку</Button>
            <Modal disableOverlayClick hideCloseBtn isOpen={isModalOpen} title="Заголовок">
                <p>
                    Какое-то содержимое модалки, которую нельзя закрыть стандартным способом, а
                    можно например только по этой кнопке:
                </p>
                <Button onClick={handleButtonCloseClick}>Закрыть модалку</Button>
            </Modal>
        </>
    );
};

const ModalSize = () => {
    // State
    const [isModalSOpen, setIsModalSOpen] = useState(false);
    const [isModalMOpen, setIsModalMOpen] = useState(false);
    const [isModalLOpen, setIsModalLOpen] = useState(false);

    // Handlers
    function handleButtonSClick() {
        setIsModalSOpen(true);
    }
    function handleModalSClose() {
        setIsModalSOpen(false);
    }
    function handleButtonMClick() {
        setIsModalMOpen(true);
    }
    function handleModalMClose() {
        setIsModalMOpen(false);
    }
    function handleButtonLClick() {
        setIsModalLOpen(true);
    }
    function handleModalLClose() {
        setIsModalLOpen(false);
    }

    return (
        <>
            <H size="2">Открыть модалку</H>
            <>
                <Button onClick={handleButtonSClick}>Размер: S</Button>
                <Modal
                    isOpen={isModalSOpen}
                    size="s"
                    title="Модалка размера S"
                    onClose={handleModalSClose}
                >
                    Какое-то содержимое модалки
                </Modal>
            </>
            <>
                <Button onClick={handleButtonMClick}>Размер: M</Button>
                <Modal
                    isOpen={isModalMOpen}
                    size="m"
                    title="Модалка размера M"
                    onClose={handleModalMClose}
                >
                    Какое-то содержимое модалки
                </Modal>
            </>
            <>
                <Button onClick={handleButtonLClick}>Размер: L</Button>
                <Modal
                    isOpen={isModalLOpen}
                    size="l"
                    title="Модалка размера L"
                    onClose={handleModalLClose}
                >
                    Какое-то содержимое модалки
                </Modal>
            </>
        </>
    );
};

function DemoModal() {
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>

            <div id="example">
                <Row
                    cols={{
                        mobile: [1],
                        tab: [1, 1],
                        desktop: [1],
                        largeDesktop: [1, 1],
                    }}
                    gap="20px"
                >
                    <Col>
                        <Example readme={basic}>
                            <ModalContainer title="Заголовок">
                                Какое-то содержимое модалки
                            </ModalContainer>
                        </Example>
                    </Col>
                    <Col>
                        <Example readme={noTitle}>
                            <ModalContainer>Какое-то содержимое модалки</ModalContainer>
                        </Example>
                    </Col>
                    <Col>
                        <Example readme={noClose}>
                            <ModalNoClose />
                        </Example>
                    </Col>
                    <Col>
                        <Example readme={scroll}>
                            <ModalContainer title="Заголовок">
                                {Array.from(new Array(100)).map((item, index) => (
                                    <p key={`${item + index}`}>Строка {index + 1}</p>
                                ))}
                            </ModalContainer>
                        </Example>
                    </Col>
                    <Col>
                        <Example readme={size}>
                            <ModalSize />
                        </Example>
                    </Col>
                </Row>
            </div>
        </DemoComponent>
    );
}

export default DemoModal;

ModalContainer.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

ModalContainer.defaultProps = {
    children: null,
};
