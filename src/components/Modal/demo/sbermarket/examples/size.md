# Размер модалки

Свойство size может принимать одно из следующих значений: `s`, `m`, `l`

```jsx
import React, { useState } from 'react';
import ReactDOM from 'react-dom';

import { H, Button, Modal, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/sbermarket/theme';

function App() {
  // State
  const [isModalSOpen, setIsModalSOpen] = useState(false);
  const [isModalMOpen, setIsModalMOpen] = useState(false);
  const [isModalLOpen, setIsModalLOpen] = useState(false);

  // Handlers
  function handleButtonSClick() {
    setIsModalSOpen(true);
  }
  function handleModalSClose() {
    setIsModalSOpen(false);
  }
  function handleButtonMClick() {
    setIsModalMOpen(true);
  }
  function handleModalMClose() {
    setIsModalMOpen(false);
  }
  function handleButtonLClick() {
    setIsModalLOpen(true);
  }
  function handleModalLClose() {
    setIsModalLOpen(false);
  }

  return (
    <>
      <H size="2">Открыть модалку</H>
      <>
        <Button onClick={handleButtonSClick}>Размер: S</Button>
        <Modal
          isOpen={isModalSOpen}
          size="s"
          title="Модалка размера S"
          onClose={handleModalSClose}
        >
          Какое-то содержимое модалки
        </Modal>
      </>
      <>
        <Button onClick={handleButtonMClick}>Размер: M</Button>
        <Modal
          isOpen={isModalMOpen}
          size="m"
          title="Модалка размера M"
          onClose={handleModalMClose}
        >
          Какое-то содержимое модалки
        </Modal>
      </>
      <>
        <Button onClick={handleButtonLClick}>Размер: L</Button>
        <Modal
          isOpen={isModalLOpen}
          size="l"
          title="Модалка размера L"
          onClose={handleModalLClose}
        >
          Какое-то содержимое модалки
        </Modal>
      </>
    </>
  )
}

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <App />
  </ThemeProvider>,
  document.getElementById('root')
);
```
