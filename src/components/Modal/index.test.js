import React from 'react';
import userEvent from '@testing-library/user-event';

import Modal from './index';

import { render } from '../../hoc/with-theme';

beforeEach(() => {
    window.disableBodyScroll = jest.fn();
    window.enableBodyScroll = jest.fn();
});

describe('Тестирование компонента Modal', () => {
    it('Должен отобразиться null', () => {
        const { container } = render(<Modal />);
        expect(container.firstChild).toBeNull();
    });
    it('Должно отобразиться модальное окно с классом "Modal__Styled"', () => {
        render(<Modal isOpen className="Modal__Styled" />);
        const modal = document.querySelector('.Modal');
        expect(modal.classList.contains('Modal__Styled')).toBeTruthy();
        expect(modal).toMatchSnapshot();
    });

    it('Должно отобразиться модальное окно с children - string ', () => {
        const modalContent = 'Какое-то содержимое модалки';
        render(<Modal isOpen>{modalContent}</Modal>);
        const modal = document.querySelector('.Modal');
        const content = modal.querySelector('.Modal__Content');
        expect(content.textContent).toBe(modalContent);
    });
    it('Должно отобразиться модальное окно с children - node ', () => {
        const modalContent = 'Какое-то содержимое модалки';
        render(
            <Modal isOpen>
                <span>{modalContent}</span>
            </Modal>,
        );
        const modal = document.querySelector('.Modal');
        const content = modal.querySelector('.Modal__Content');
        expect(content.textContent).toBe(modalContent);
    });

    it('Должно отобразиться модальное окно с children - array node ', () => {
        const modalContent = 'Какое-то содержимое модалки';
        render(
            <Modal isOpen>
                <span data-testid="content1" key="content1">
                    {modalContent}
                </span>
                <span data-testid="content2" key="content2">
                    Test
                </span>
            </Modal>,
        );
        const modal = document.querySelector('.Modal');
        const content1 = modal.querySelector('span[data-testid="content1"]');
        expect(content1.textContent).toBe(modalContent);
        const content2 = modal.querySelector('span[data-testid="content2"]');
        expect(content2.textContent).toBe('Test');
    });

    it('Должно отобразиться модальное окно в открытом положении при клике на overlay (свойство disableOverlayClick)', async () => {
        const modalContent = 'Какое-то содержимое модалки';
        const handleClose = jest.fn();
        render(
            <Modal isOpen disableOverlayClick onClose={handleClose}>
                {modalContent}
            </Modal>,
        );
        const overlay = document.querySelector('.Modal__Overlay');
        await userEvent.click(overlay);
        expect(handleClose).toBeCalledTimes(0);
    });

    it('Должно отобразиться модальное окно disableOverlayClick = false по-умолчанию', async () => {
        const modalContent = 'Какое-то содержимое модалки';
        const handleClose = jest.fn();
        render(
            <Modal isOpen onClose={handleClose}>
                {modalContent}
            </Modal>,
        );
        const overlay = document.querySelector('.Modal__Overlay');
        await userEvent.click(overlay);
        expect(handleClose).toBeCalledTimes(1);
        expect(handleClose).toHaveBeenCalledWith(expect.any(Object));
    });

    it('Должно отобразиться модальное окно со скрытой кнопкой х', () => {
        const modalContent = 'Какое-то содержимое модалки';
        render(
            <Modal isOpen hideCloseBtn>
                {modalContent}
            </Modal>,
        );
        const iconX = document.querySelector('.feather-x');
        expect(iconX).not.toBeInTheDocument();
    });

    it('Должно отобразиться модальное окно со доступной кнопкой х по-умолчанию', async () => {
        const modalContent = 'Какое-то содержимое модалки';
        const handleClose = jest.fn();
        render(
            <Modal isOpen onClose={handleClose}>
                {modalContent}
            </Modal>,
        );
        const iconX = document.querySelector('.feather-x');
        expect(iconX).toBeInTheDocument();
        await userEvent.click(iconX);
        expect(handleClose).toBeCalledTimes(1);
    });

    it('Должно отобразиться модальное окно закрытым', async () => {
        const modalContent = 'Какое-то содержимое модалки';
        let isOpenState = true;
        const handleClose = jest.fn(() => {
            isOpenState = !isOpenState;
        });
        const { rerender } = render(
            <Modal isOpen={isOpenState} onClose={handleClose}>
                {modalContent}
            </Modal>,
        );
        const modalOpen = document.querySelector('.Modal_open');
        expect(modalOpen).toBeInTheDocument();
        const iconX = document.querySelector('.feather-x');
        await userEvent.click(iconX);
        rerender(
            <Modal isOpen={isOpenState} onClose={handleClose}>
                {modalContent}
            </Modal>,
        );
        const modalOpenAfterClose = document.querySelector('.Modal_open');
        expect(modalOpenAfterClose).not.toBeInTheDocument();
        expect(isOpenState).toBeFalsy();
    });

    it('Должно отобразиться модальное окно меняется состоянии с закрытого на открытый', async () => {
        const modalContent = 'Какое-то содержимое модалки';
        let isOpenState = false;
        const { rerender } = render(<Modal isOpen={isOpenState}>{modalContent}</Modal>);
        isOpenState = true;
        rerender(<Modal isOpen={isOpenState}>{modalContent}</Modal>);
        const modalOpenAfterClose = document.querySelector('.Modal_open');
        expect(modalOpenAfterClose).toBeInTheDocument();
    });

    it('Должно отобразиться модальное окно в размере "m" по-умолчанию', () => {
        const modalContent = 'Какое-то содержимое модалки';
        render(<Modal isOpen>{modalContent}</Modal>);

        const modalSizeM = document.querySelector('.Modal__Container_size_m');
        expect(modalSizeM).toBeInTheDocument();
    });

    it('Должно отобразиться модальное окно в размере "s"', () => {
        const modalContent = 'Какое-то содержимое модалки';
        render(
            <Modal isOpen size="s">
                {modalContent}
            </Modal>,
        );

        const modalSizeS = document.querySelector('.Modal__Container_size_s');
        expect(modalSizeS).toBeInTheDocument();
    });

    it('Должно отобразиться модальное окно в размере "l"', () => {
        const modalContent = 'Какое-то содержимое модалки';
        render(
            <Modal isOpen size="l">
                {modalContent}
            </Modal>,
        );

        const modalSizeL = document.querySelector('.Modal__Container_size_l');
        expect(modalSizeL).toBeInTheDocument();
    });

    it('Должно отобразиться модальное окно с заголовком "Внимание"', () => {
        const modalContent = 'Какое-то содержимое модалки';
        render(
            <Modal isOpen title="Внимание">
                {modalContent}
            </Modal>,
        );

        const modalTitle = document.querySelector('.Modal__Title');
        expect(modalTitle).toBeInTheDocument();
        expect(modalTitle.textContent).toBe('Внимание');
    });

    it('Должно отобразиться модальное окно - проверка onClose', async () => {
        const modalContent = 'Какое-то содержимое модалки';
        const handleClose = jest.fn();
        render(
            <Modal isOpen onClose={handleClose}>
                {modalContent}
            </Modal>,
        );
        const iconX = document.querySelector('.feather-x');
        await userEvent.click(iconX);
        expect(handleClose).toBeCalledTimes(1);
        expect(handleClose).toBeCalledWith(expect.any(Object));
    });
});
