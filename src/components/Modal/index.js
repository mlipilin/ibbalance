import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';

// Components
import FeatherIcon from '../FeatherIcon';

// Constants
import { SIZES } from '../../constants/props';

import { withTheme } from '../../theme-provider';

@withTheme
class Modal extends Component {
    componentDidUpdate(prevProps) {
        const { isOpen } = this.props;
        if (prevProps.isOpen !== isOpen) {
            if (isOpen) {
                disableBodyScroll(this.modal);
            } else {
                enableBodyScroll(this.modal);
            }
        }
    }

    componentWillUnmount() {
        this.close();
    }

    handleClick = e => {
        e.stopPropagation();
    };

    handleCloseClick = () => {
        this.close();
    };

    handleOverlayClick = () => {
        const { disableOverlayClick } = this.props;
        if (!disableOverlayClick) {
            this.close();
        }
    };

    close = () => {
        enableBodyScroll(this.modal);
        const { onClose } = this.props;
        onClose(this.props);
    };

    render() {
        const { children, className, hideCloseBtn, isOpen, size, title, applyTheme } = this.props;
        const componentClass = applyTheme(
            cn('Modal', {
                Modal_open: isOpen,
            }),
            className,
        );
        const overlayClass = applyTheme(cn('Modal__Overlay'));
        const wrapperClass = applyTheme(cn('Modal__Wrapper'));
        const containerClass = applyTheme(cn('Modal__Container', `Modal__Container_size_${size}`));
        const closeBtnClass = applyTheme(cn('Modal__Close'));
        const titleClass = applyTheme(cn('Modal__Title'));
        const contentClass = applyTheme(cn('Modal__Content'));

        return ReactDOM.createPortal(
            <div
                className={componentClass}
                onClick={this.handleClick}
                ref={el => {
                    this.modal = el;
                    return this.modal;
                }}
            >
                <div className={overlayClass} onClick={this.handleOverlayClick} />
                <div className={wrapperClass}>
                    <div className={containerClass}>
                        {/* Close */}
                        {!hideCloseBtn && (
                            <div className={closeBtnClass} onClick={this.handleCloseClick}>
                                <FeatherIcon name="x" />
                            </div>
                        )}

                        {/* Title */}
                        {!!title && <div className={titleClass}>{title}</div>}

                        {/* Children */}
                        {!!children && <div className={contentClass}>{children}</div>}
                    </div>
                </div>
            </div>,
            document.body,
        );
    }
}

Modal.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    className: PropTypes.string,
    disableOverlayClick: PropTypes.bool,
    hideCloseBtn: PropTypes.bool,
    isOpen: PropTypes.bool,
    size: PropTypes.oneOf([SIZES.S, SIZES.M, SIZES.L]),
    title: PropTypes.string,
    applyTheme: PropTypes.func,
    onClose: PropTypes.func,
};

Modal.defaultProps = {
    children: null,
    className: '',
    disableOverlayClick: false,
    hideCloseBtn: false,
    isOpen: false,
    size: SIZES.M,
    title: '',
    applyTheme: _ => _,
    onClose: props => {}, // eslint-disable-line no-unused-vars
};

export default Modal;
