import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean, select, text } from '@storybook/addon-knobs';

// Constants
import { LOREM_TEXT } from './constants';
import { LOCAL_STORAGE_THEME_KEY } from '../../../constants/local-storage';
import { SIZES } from '../../../constants/props';
import { THEMES } from '../../../constants/themes';

import Modal from '../index';

import notes from '../readme.md';

const theme = localStorage.getItem(LOCAL_STORAGE_THEME_KEY);

let sizesList = [];

switch (theme) {
    case THEMES.DEFAULT:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        break;
    case THEMES.ALFA:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        break;
    case THEMES.ABSOLUT:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        break;
    case THEMES.SPB:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        break;
    case THEMES.TKB:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        break;
    case THEMES.TKB_LKZ:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        break;
    case THEMES.KYC:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        break;
    case THEMES.KYC_LANDING:
        sizesList = [SIZES.S, SIZES.M, SIZES.L];
        break;
    default:
        break;
}

const stories = storiesOf('Modal', module);
stories.addDecorator(withKnobs);
stories
    .add(
        'Default',
        () => (
            <Modal
                disableOverlayClick={boolean('disableOverlayClick', false)}
                hideCloseBtn={boolean('hideCloseBtn', false)}
                isOpen={boolean('isOpen', true)}
                size={select('Size', sizesList, SIZES.M)}
                title={text('title', 'Заголовок')}
            >
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                incididunt ut labore et dolore magna aliqua.
            </Modal>
        ),
        { notes },
    )
    .add(
        'No title',
        () => (
            <Modal
                disableOverlayClick={boolean('disableOverlayClick', false)}
                hideCloseBtn={boolean('hideCloseBtn', false)}
                isOpen={boolean('isOpen', true)}
                size={select('Size', sizesList, SIZES.M)}
            >
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                incididunt ut labore et dolore magna aliqua.
            </Modal>
        ),
        { notes },
    )
    .add(
        'No content',
        () => (
            <Modal
                disableOverlayClick={boolean('disableOverlayClick', false)}
                hideCloseBtn={boolean('hideCloseBtn', false)}
                isOpen={boolean('isOpen', true)}
                size={select('Size', sizesList, SIZES.M)}
                title={text('title', 'Заголовок')}
            />
        ),
        { notes },
    )
    .add(
        'With scroll',
        () => (
            <Modal
                disableOverlayClick={boolean('disableOverlayClick', false)}
                hideCloseBtn={boolean('hideCloseBtn', false)}
                isOpen={boolean('isOpen', true)}
                size={select('Size', sizesList, SIZES.M)}
                title={text('title', 'Заголовок')}
            >
                {LOREM_TEXT}
            </Modal>
        ),
        { notes },
    );
