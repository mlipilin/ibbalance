import React, { useState } from 'react';

// Components
import { Notification, NotificationWrapper } from '../../../Notification';

// Constants
import NAMES_ARRAY from '../../constants';

import MaterialIcon from '../../index';

import styles from './styles.sass';

const Icons = () => {
    const [notifications, setNotifications] = useState([]);
    let clipboardInput = null;

    const handleIconClick = name => () => {
        const componentCode = `<MaterialIcon name="${name}" />`;
        clipboardInput.value = componentCode;
        clipboardInput.select();
        document.execCommand('copy');
        const notificationsWillState = [
            ...notifications,
            {
                title: 'Код скопирован в буфер обмена',
                description: componentCode,
            },
        ];
        setNotifications(notificationsWillState);
    };

    return (
        <>
            <div className={styles.ClipboardInputWrapper}>
                <input
                    className={styles.ClipboardInput}
                    ref={el => {
                        clipboardInput = el;
                        return clipboardInput;
                    }}
                />
            </div>
            <div className={styles.Icons}>
                {NAMES_ARRAY.map(name => (
                    <div key={name} className={styles.Icon} onClick={handleIconClick(name)}>
                        <div className={styles.Icon__Icon}>
                            <MaterialIcon name={name} />
                        </div>
                        <div className={styles.Icon__Title}>
                            <i>{name}</i>
                        </div>
                    </div>
                ))}
            </div>
            {/* eslint-disable */}
            <NotificationWrapper>
                {notifications.map(({ title }, index) => (
                    <Notification key={index} title={title} />
                ))}
            </NotificationWrapper>
            {/* eslint-enable */}
        </>
    );
};

export default Icons;
