# Базовое использование

В свойстве `name` нужно передать название иконки

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { MaterialIcon, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/absolut/theme';

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <MaterialIcon name="house" />
    </ThemeProvider>,
    document.getElementById('root'),
);
```
