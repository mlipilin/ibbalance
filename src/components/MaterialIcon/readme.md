## Usage

```jsx
<MaterialIcon name="phone" />
```

## Props

Prop name | Prop type | Default value
--- | --- | ---
*name*|one of Feather icon names|`null`

## CSS Selectors

```css
.MaterialIcon {}
.MaterialIcon > svg {}
```
