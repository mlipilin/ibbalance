import React from 'react';
import createSvgIcon from './utils/createSvgIcon';

export default createSvgIcon(
    <>
        <path fillOpacity=".3" d="M2 22h20V2L2 22z" />
        <path d="M14 10L2 22h12V10z" />
    </>,
    'SignalCellular2BarOutlined',
);
