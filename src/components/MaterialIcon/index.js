import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
// Constants
import cn from 'classnames';
import NAMES_ARRAY from './constants';

import { withTheme } from '../../theme-provider';

const MaterialIcon = props => {
    const { className, name, applyTheme, ...otherProps } = props;

    const [icon, setIcon] = useState(null);

    useEffect(() => {
        import(`./icons/${name}.js`)
            .then(res => {
                setIcon(res.default());
            })
            .catch(() => console.log('fail', name));
    }, []);

    // load();

    if (!name) return null;
    if (!icon) return null;

    const componentClass = applyTheme(cn('MaterialIcon'), className);

    return (
        <span {...otherProps} className={componentClass}>
            {icon}
        </span>
    );
};

MaterialIcon.propTypes = {
    name: PropTypes.oneOf(NAMES_ARRAY),
    applyTheme: PropTypes.func,
    className: PropTypes.string,
};

MaterialIcon.defaultProps = {
    name: null,
    applyTheme: _ => _,
    className: '',
};

export default withTheme(MaterialIcon);
