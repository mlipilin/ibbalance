import React from 'react';
import { storiesOf } from '@storybook/react';

import notes from '../readme.md';

import Icons from '../components/Icons';

const stories = storiesOf('MaterialIcon', module);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories.add('Default', () => <Icons />, { notes });
