import moment from 'moment';

export const dateMoreThanMin = (date = null, min = null) => {
    if (!date || !min) {
        return true;
    }
    const minDate = moment(min, 'DD.MM.YYYY');
    const selectDate = moment(date);
    return minDate.isSameOrBefore(selectDate);
};

export const dateLessThanMax = (date = null, max = null) => {
    if (!date || !max) {
        return true;
    }
    const maxDate = moment(max, 'DD.MM.YYYY');
    const selectDate = moment(date);
    return maxDate.isSameOrAfter(selectDate);
};

export const isDateBetweenMinAndMax = (date = null, min = null, max = null) => {
    if (!date) {
        return true;
    }
    return dateLessThanMax(date, max) && dateMoreThanMin(date, min);
};
