import { isArray, isObject } from './is';

describe('src/helpers/is.js', () => {
    describe('isArray', () => {
        it('boolean -> false', () => expect(isArray(false)).toBeFalsy());
        it('number -> false', () => expect(isArray(1)).toBeFalsy());
        it('string -> false', () => expect(isArray('string')).toBeFalsy());
        it('null -> false', () => expect(isArray(null)).toBeFalsy());
        it('undefined -> false', () => expect(isArray(undefined)).toBeFalsy());
        it('Symbol -> false', () => expect(isArray(Symbol('symbol'))).toBeFalsy());
        it('object -> false', () => expect(isArray({})).toBeFalsy());
        it('array -> false', () => expect(isArray([])).toBeTruthy());
    });
    describe('isObject', () => {
        it('boolean -> false', () => expect(isObject(false)).toBeFalsy());
        it('number -> false', () => expect(isObject(1)).toBeFalsy());
        it('string -> false', () => expect(isObject('string')).toBeFalsy());
        it('null -> false', () => expect(isObject(null)).toBeFalsy());
        it('undefined -> false', () => expect(isObject(undefined)).toBeFalsy());
        it('Symbol -> true', () => expect(isObject(Symbol('symbol'))).toBeFalsy());
        it('object -> true', () => expect(isObject({})).toBeTruthy());
        it('array -> true', () => expect(isObject([])).toBeFalsy());
    });
});
