import { getFileInfo, getExtensionForMimeType } from './files';

describe('helpers/file.js', () => {
    describe('getFileInfo', () => {
        it('NO file', () => {
            const output = { extension: null, title: null, type: null };
            expect(getFileInfo()).toEqual(output);
        });
        it('file with only name (has NO extension)', () => {
            const input = new File([], 'passport', {});
            const output = { extension: null, title: 'passport', type: null };
            expect(getFileInfo(input)).toEqual(output);
        });
        it('file with only name (has extension)', () => {
            const input = new File([], 'passport.pdf', {});
            const output = { extension: 'pdf', title: 'passport.pdf', type: null };
            expect(getFileInfo(input)).toEqual(output);
        });
        it('file with only name (has bad extension)', () => {
            const input = new File([], 'passport.pdfpdpfdpf', {});
            const output = { extension: null, title: 'passport.pdfpdpfdpf', type: null };
            expect(getFileInfo(input)).toEqual(output);
        });
        it('file with only type', () => {
            const input = new File([], '', { type: 'application/pdf' });
            const output = { extension: 'pdf', title: null, type: 'application/pdf' };
            expect(getFileInfo(input)).toEqual(output);
        });
        it('file with only type (bad)', () => {
            const input = new File([], '', { type: 'application/pdfpdpfdpf' });
            const output = { extension: null, title: null, type: 'application/pdfpdpfdpf' };
            expect(getFileInfo(input)).toEqual(output);
        });
        it('file with name and type', () => {
            const input = new File([], 'passport.jpg', { type: 'application/pdf' });
            const output = { extension: 'jpg', title: 'passport.jpg', type: 'application/pdf' };
            expect(getFileInfo(input)).toEqual(output);
        });
        it('file with name and type (bad)', () => {
            const input = new File([], 'passport.pdf', { type: 'application/pdfpdpfdpf' });
            const output = {
                extension: 'pdf',
                title: 'passport.pdf',
                type: 'application/pdfpdpfdpf',
            };
            expect(getFileInfo(input)).toEqual(output);
        });
        it('file with name (bad extension) and type', () => {
            const input = new File([], 'passport.pdfpdpfdpf', { type: 'application/pdf' });
            const output = {
                extension: 'pdf',
                title: 'passport.pdfpdpfdpf',
                type: 'application/pdf',
            };
            expect(getFileInfo(input)).toEqual(output);
        });
        it('file with name (bad extension) and type (bad)', () => {
            const input = new File([], 'passport.pdfpdpfdpf', { type: 'application/pdfpdpfdpf' });
            const output = {
                extension: null,
                title: 'passport.pdfpdpfdpf',
                type: 'application/pdfpdpfdpf',
            };
            expect(getFileInfo(input)).toEqual(output);
        });
    });
    describe('getExtensionForMimeType', () => {
        it('NO content type', () => {
            expect(getExtensionForMimeType()).toBeNull();
        });
        it('invalid content type', () => {
            expect(getExtensionForMimeType('application/pdfpdpfdpf')).toBeNull();
        });
        it('valid content type', () => {
            expect(getExtensionForMimeType('application/pdf')).toBe('pdf');
        });
    });
});
