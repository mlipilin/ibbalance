// Constants
import { EXTENSION, EXTENSION_MIME_TYPES } from '../constants/files';

/* istanbul ignore next */
export const convertImageToJpeg = (file, ext) =>
    new Promise((resolve, reject) => {
        toBase64(file)
            .then(url => {
                const image = new Image();
                image.onload = function() {
                    try {
                        const canvas = document.createElement('canvas');
                        canvas.width = image.width;
                        canvas.height = image.height;
                        canvas.getContext('2d').drawImage(this, 0, 0);
                        resolve(
                            fromBase64(
                                canvas.toDataURL('image/jpeg'),
                                file.name.replace(new RegExp(`.${ext}$`, 'i'), '.jpg'),
                                { type: 'image/jpeg' },
                            ),
                        );
                    } catch (error) {
                        reject(error);
                    }
                };
                image.src = url;
            })
            .catch(error => {
                reject(error);
            });
    });

/* istanbul ignore next */
export const fromBase64 = (base64, name, params) =>
    fetch(base64)
        .then(res => res.blob())
        .then(blob => new File([blob], name, params));

export const getFileInfo = file => {
    let extension = null;
    let title = null;
    let type = null;

    if (file) {
        // Определяем extension по file.name
        if (file.name) {
            const fileNameParts = file.name.split('.');
            const possibleExtension = fileNameParts[fileNameParts.length - 1];
            if (Object.values(EXTENSION).includes(possibleExtension)) {
                extension = possibleExtension.toLowerCase();
            }
        }

        // Если не получилось, определяем extension по file.type
        if (extension === null && file.type) {
            extension = getExtensionForMimeType(file.type);
        }

        title = file.name || null;
        type = file.type || null;
    }

    return { extension, title, type };
};

export const getExtensionForMimeType = contentType => {
    let extension = null;
    Object.entries(EXTENSION_MIME_TYPES).forEach(([key, mimeTypes]) => {
        if (mimeTypes.includes(contentType)) {
            extension = key;
        }
    });
    return extension;
};

/* istanbul ignore next */
export const toBase64 = file =>
    new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
