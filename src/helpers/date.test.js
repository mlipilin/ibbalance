import moment from 'moment';
import { isDateBetweenMinAndMax, dateMoreThanMin, dateLessThanMax } from './date';

describe('src/helpers/date.js', () => {
    const DATE = moment('03.04.2021', 'DD.MM.YYYY');
    describe('isDateBetweenMinAndMax', () => {
        it('ничего не передали -> true', () => {
            expect(isDateBetweenMinAndMax()).toBeTruthy();
        });
        it('передали date -> true', () => {
            expect(isDateBetweenMinAndMax(DATE)).toBeTruthy();
        });
        it('передали date, min == null, max == null -> true', () => {
            expect(isDateBetweenMinAndMax(DATE, null, null)).toBeTruthy();
        });
        it('передали date, min, date > min -> true', () => {
            const min = moment('02.04.2021', 'DD.MM.YYYY');
            expect(isDateBetweenMinAndMax(DATE, min)).toBeTruthy();
        });
        it('передали date, min, date == min -> true', () => {
            const min = moment('03.04.2021', 'DD.MM.YYYY');
            expect(isDateBetweenMinAndMax(DATE, min)).toBeTruthy();
        });
        it('передали date, min, date < min -> false', () => {
            const min = moment('04.04.2021', 'DD.MM.YYYY');
            expect(isDateBetweenMinAndMax(DATE, min)).toBeFalsy();
        });
        it('передали date, max, min == null, date < max -> true', () => {
            const max = moment('05.04.2021', 'DD.MM.YYYY');
            expect(isDateBetweenMinAndMax(DATE, null, max)).toBeTruthy();
        });
        it('передали date, max, min == null, date == max -> true', () => {
            const max = moment('03.04.2021', 'DD.MM.YYYY');
            expect(isDateBetweenMinAndMax(DATE, null, max)).toBeTruthy();
        });
        it('передали date, max, min == null, date > max -> false', () => {
            const max = moment('02.04.2021', 'DD.MM.YYYY');
            expect(isDateBetweenMinAndMax(DATE, null, max)).toBeFalsy();
        });
        it('передали date, max, min, -> true', () => {
            const max = moment('05.04.2021', 'DD.MM.YYYY');
            const min = moment('02.04.2021', 'DD.MM.YYYY');
            expect(isDateBetweenMinAndMax(DATE, min, max)).toBeTruthy();
        });
    });
    describe('dateMoreThanMin', () => {
        it('ничего не передали -> true', () => {
            expect(dateMoreThanMin()).toBeTruthy();
        });
        it('передали date -> true', () => {
            expect(dateMoreThanMin(DATE)).toBeTruthy();
        });
        it('передали date, min == null -> true', () => {
            expect(dateMoreThanMin(DATE, null)).toBeTruthy();
        });
        it('передали date, min, date > min -> true', () => {
            const min = moment('02.04.2021', 'DD.MM.YYYY');
            expect(dateMoreThanMin(DATE, min)).toBeTruthy();
        });
        it('передали date, min, date == min -> true', () => {
            const min = moment('03.04.2021', 'DD.MM.YYYY');
            expect(dateMoreThanMin(DATE, min)).toBeTruthy();
        });
        it('передали date, min, date < min -> false', () => {
            const min = moment('04.04.2021', 'DD.MM.YYYY');
            expect(dateMoreThanMin(DATE, min)).toBeFalsy();
        });
    });

    describe('dateLessThanMax', () => {
        it('ничего не передали -> true', () => {
            expect(dateLessThanMax()).toBeTruthy();
        });
        it('передали date -> true', () => {
            expect(dateLessThanMax(DATE)).toBeTruthy();
        });
        it('передали date, max == null -> true', () => {
            expect(dateLessThanMax(DATE, null)).toBeTruthy();
        });
        it('передали date, max, date < max -> true', () => {
            const max = moment('05.04.2021', 'DD.MM.YYYY');
            expect(dateLessThanMax(DATE, max)).toBeTruthy();
        });
        it('передали date, max, date == max -> true', () => {
            const max = moment('03.04.2021', 'DD.MM.YYYY');
            expect(dateLessThanMax(DATE, max)).toBeTruthy();
        });
        it('передали date, max, date > max -> false', () => {
            const max = moment('02.04.2021', 'DD.MM.YYYY');
            expect(dateLessThanMax(DATE, max)).toBeFalsy();
        });
        it('передали date, max,  -> true', () => {
            const max = moment('05.04.2021', 'DD.MM.YYYY');
            expect(dateLessThanMax(DATE, max)).toBeTruthy();
        });
        it('передали date=null, max -> true', () => {
            const max = moment('05.04.2021', 'DD.MM.YYYY');
            expect(dateLessThanMax(null, max)).toBeTruthy();
        });
        it('передали date=null, min -> true', () => {
            const min = moment('05.04.2021', 'DD.MM.YYYY');
            expect(dateLessThanMax(null, min)).toBeTruthy();
        });
    });
});
