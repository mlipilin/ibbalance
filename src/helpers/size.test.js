import { getCoverSize } from './size';

describe('src/helpers/size.js', () => {
    describe('getCoverSize', () => {
        it('NO params', () => {
            const output = { width: 0, height: 0 };
            expect(getCoverSize()).toEqual(output);
        });
        describe('ZERO', () => {
            it('container height', () => {
                const input = {
                    containerWidth: 100,
                    containerHeight: 0,
                    elementWidth: 640,
                    elementHeight: 480,
                };
                const output = { width: 0, height: 0 };
                expect(getCoverSize(input)).toEqual(output);
            });
            it('container width', () => {
                const input = {
                    containerWidth: 0,
                    containerHeight: 100,
                    elementWidth: 640,
                    elementHeight: 480,
                };
                const output = { width: 0, height: 0 };
                expect(getCoverSize(input)).toEqual(output);
            });
            it('container height & width', () => {
                const input = {
                    containerWidth: 0,
                    containerHeight: 0,
                    elementWidth: 640,
                    elementHeight: 480,
                };
                const output = { width: 0, height: 0 };
                expect(getCoverSize(input)).toEqual(output);
            });
            it('video height', () => {
                const input = {
                    containerWidth: 640,
                    containerHeight: 480,
                    elementWidth: 640,
                    elementHeight: 0,
                };
                const output = { width: 0, height: 0 };
                expect(getCoverSize(input)).toEqual(output);
            });
            it('video width', () => {
                const input = {
                    containerWidth: 640,
                    containerHeight: 480,
                    elementWidth: 0,
                    elementHeight: 480,
                };
                const output = { width: 0, height: 0 };
                expect(getCoverSize(input)).toEqual(output);
            });
            it('video height & width', () => {
                const input = {
                    containerWidth: 640,
                    containerHeight: 480,
                    elementWidth: 0,
                    elementHeight: 0,
                };
                const output = { width: 0, height: 0 };
                expect(getCoverSize(input)).toEqual(output);
            });
        });
        describe('VIDEO aspect ratio < CONTAINER aspect ratio', () => {
            it('video width < container width', () => {
                const input = {
                    containerWidth: 200,
                    containerHeight: 100,
                    elementWidth: 100,
                    elementHeight: 100,
                };
                const output = { width: 200, height: 200 };
                expect(getCoverSize(input)).toEqual(output);
            });
            it('video width === container width', () => {
                const input = {
                    containerWidth: 200,
                    containerHeight: 100,
                    elementWidth: 200,
                    elementHeight: 200,
                };
                const output = { width: 200, height: 200 };
                expect(getCoverSize(input)).toEqual(output);
            });
            it('video width > container width', () => {
                const input = {
                    containerWidth: 200,
                    containerHeight: 100,
                    elementWidth: 300,
                    elementHeight: 300,
                };
                const output = { width: 200, height: 200 };
                expect(getCoverSize(input)).toEqual(output);
            });
        });
        describe('VIDEO aspect ratio > CONTAINER aspect ratio', () => {
            it('video height < container height', () => {
                const input = {
                    containerWidth: 300,
                    containerHeight: 200,
                    elementWidth: 300,
                    elementHeight: 100,
                };
                const output = { width: 600, height: 200 };
                expect(getCoverSize(input)).toEqual(output);
            });
            it('video height === container height', () => {
                const input = {
                    containerWidth: 300,
                    containerHeight: 200,
                    elementWidth: 600,
                    elementHeight: 200,
                };
                const output = { width: 600, height: 200 };
                expect(getCoverSize(input)).toEqual(output);
            });
            it('video height > container height', () => {
                const input = {
                    containerWidth: 300,
                    containerHeight: 200,
                    elementWidth: 900,
                    elementHeight: 300,
                };
                const output = { width: 600, height: 200 };
                expect(getCoverSize(input)).toEqual(output);
            });
        });
        describe('VIDEO aspect ratio === CONTAINER aspect ratio', () => {
            it('video width & height < container width & height', () => {
                const input = {
                    containerWidth: 300,
                    containerHeight: 200,
                    elementWidth: 150,
                    elementHeight: 100,
                };
                const output = { width: 300, height: 200 };
                expect(getCoverSize(input)).toEqual(output);
            });
            it('video width & height === container width & height', () => {
                const input = {
                    containerWidth: 300,
                    containerHeight: 200,
                    elementWidth: 300,
                    elementHeight: 200,
                };
                const output = { width: 300, height: 200 };
                expect(getCoverSize(input)).toEqual(output);
            });
            it('video width & height > container width & height', () => {
                const input = {
                    containerWidth: 300,
                    containerHeight: 200,
                    elementWidth: 600,
                    elementHeight: 400,
                };
                const output = { width: 300, height: 200 };
                expect(getCoverSize(input)).toEqual(output);
            });
        });
    });
});
