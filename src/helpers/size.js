export const getCoverSize = ({
    containerWidth = 0,
    containerHeight = 0,
    elementWidth = 0,
    elementHeight = 0,
} = {}) => {
    if (
        containerWidth === 0 ||
        containerHeight === 0 ||
        elementWidth === 0 ||
        elementHeight === 0
    ) {
        return { width: 0, height: 0 };
    }

    let width = 0;
    let height = 0;
    const videoAspectRatio = elementWidth / elementHeight;
    const containerAspectRatio = containerWidth / containerHeight;

    if (videoAspectRatio < containerAspectRatio) {
        width = containerWidth;
        height = (width / elementWidth) * elementHeight;
    } else if (videoAspectRatio > containerAspectRatio) {
        height = containerHeight;
        width = (height / elementHeight) * elementWidth;
    } else {
        height = containerHeight;
        width = containerWidth;
    }

    return { width, height };
};
