export const hasNodeInTree = (tree, node) => {
    if (tree === node) return true;
    if (!tree.parentNode) return false;
    return hasNodeInTree(tree.parentNode, node);
};
