import useStateComponent from './use-state-component';

export { useStateComponent };
export default { useStateComponent };
