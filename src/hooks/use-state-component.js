/* eslint-disable react/destructuring-assignment */
import React, { useState } from 'react';

export default (Component, valuePropName = 'value', defaultValue = null) => props => {
    const [valuePropValue, change] = useState(props[valuePropName] || defaultValue);
    const handleChange = value => change(value);

    const componentProps = {
        ...props,
        [valuePropName]: valuePropValue,
        onChange: handleChange,
    };

    return <Component {...componentProps} />;
};
