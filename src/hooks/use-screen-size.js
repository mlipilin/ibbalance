import { useEffect, useState } from 'react';

// Constants
import { SCREEN_SIZES } from '../constants/props';

const TAB_BREAKPOINT = 768;
const DESKTOP_BREAKPOINT = 1025;
const LARGE_DESKTOP_BREAKPOINT = 1441;

export default function useScreenSize() {
    const [screenSize, setScreenSize] = useState(null);

    // Effects
    useEffect(() => {
        detectScreenSize(window.innerWidth);
        window.addEventListener('resize', handleWindowResize);
        return () => {
            window.removeEventListener('resize', handleWindowResize);
        };
    }, []);

    // Handlers
    function handleWindowResize(e) {
        detectScreenSize(e.target.innerWidth);
    }

    // Methods
    function detectScreenSize(windowWidth) {
        if (windowWidth >= LARGE_DESKTOP_BREAKPOINT) {
            setScreenSize(SCREEN_SIZES.LARGE_DESKTOP);
        } else if (windowWidth >= DESKTOP_BREAKPOINT) {
            setScreenSize(SCREEN_SIZES.DESKTOP);
        } else if (windowWidth >= TAB_BREAKPOINT) {
            setScreenSize(SCREEN_SIZES.TAB);
        } else {
            setScreenSize(SCREEN_SIZES.MOBILE);
        }
    }

    return screenSize;
}
