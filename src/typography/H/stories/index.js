import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, select } from '@storybook/addon-knobs';

// Constants
import { LOCAL_STORAGE_THEME_KEY } from '../../../constants/local-storage';
import { THEMES } from '../../../constants/themes';

import H from '../index';

import notes from '../readme.md';

const theme = localStorage.getItem(LOCAL_STORAGE_THEME_KEY);

let sizesList = [];

switch (theme) {
    case THEMES.DEFAULT:
        sizesList = [1, 2, 3, 4, 5, 6];
        break;
    case THEMES.ABSOLUT:
        sizesList = [1, 2, 3];
        break;
    case THEMES.ALFA:
        sizesList = [1, 2, 3];
        break;
    case THEMES.SPB:
        sizesList = [1, 2, 3];
        break;
    case THEMES.TKB:
        sizesList = [1, 2, 3];
        break;
    case THEMES.KYC:
        sizesList = [1, 2, 3];
        break;
    case THEMES.KYC_LANDING:
        sizesList = [1, 2, 3];
        break;
    default:
        break;
}

const stories = storiesOf('H', module);

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories.add(
    'Default',
    () => {
        const size = select('Size', sizesList, 1);
        return <H size={size}>H{size} component</H>;
    },
    { notes },
);
