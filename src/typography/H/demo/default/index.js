import React from 'react';

// Components
import H from '../../index';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';

// Examples
import basic from './examples/basic.md';

const DemoH = () => {
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>
            <div id="example">
                <Example readme={basic}>
                    <H size="1">Заголовок H1</H>
                    <H size="2">Заголовок H2</H>
                    <H size="3">Заголовок H3</H>
                    <H size="4">Заголовок H4</H>
                    <H size="5">Заголовок H5</H>
                    <H size="6">Заголовок H6</H>
                </Example>
            </div>
        </DemoComponent>
    );
};

export default DemoH;
