# Заголовки

Компонент для рендеринга HTML-заголовков H1-H6.

## Когда использовать

Когда требуется разместить на странице тот или иной заголовок.

## Примеры использования

## Свойства

| Название | Описание                   | Тип                                   | Значение по умолчанию |
| -------- | -------------------------- | ------------------------------------- | --------------------- |
| **size** | Тип заголовка (обязателен) | Один из: `1`, `2`, `3`, `4`, `5`, `6` | `null`                |
