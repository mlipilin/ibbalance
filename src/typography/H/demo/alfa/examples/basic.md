# Базовое использование

В свойстве `size` нужно передать тип (размер) заголовка

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { H, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/alfa/theme';

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <H size="1">Заголовок H1</H>
        <H size="2">Заголовок H2</H>
        <H size="3">Заголовок H3</H>
    </ThemeProvider>,
    document.getElementById('root'),
);
```
