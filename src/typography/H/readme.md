## Usage

```jsx
<H size="1">Header 1</H>
<H size="2">Header 2</H>
<H size="3">Header 3</H>
<H size="4">Header 4</H>
<H size="5">Header 5</H>
<H size="6">Header 6</H>
```

## Props

Prop name | Prop type | Default value
--- | --- | ---
*size*|`1`, `2`, `3`, `4`, `5`, `6`|isRequired!

## CSS Selectors

```css
.H {}

/* Size */
.H_size_1 {}
.H_size_2 {}
.H_size_3 {}
.H_size_4 {}
.H_size_5 {}
.H_size_6 {}
```
