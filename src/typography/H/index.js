import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import { withTheme } from '../../theme-provider';

@withTheme
class H extends Component {
    render() {
        const { className, size, applyTheme, children, ...otherProps } = this.props;

        const componentClass = applyTheme(cn('H', `H_size_${size}`), className);

        if (size === 1 || size === '1') {
            return (
                <h1 {...otherProps} className={componentClass}>
                    {children}
                </h1>
            );
        }
        if (size === 2 || size === '2') {
            return (
                <h2 {...otherProps} className={componentClass}>
                    {children}
                </h2>
            );
        }
        if (size === 3 || size === '3') {
            return (
                <h3 {...otherProps} className={componentClass}>
                    {children}
                </h3>
            );
        }
        if (size === 4 || size === '4') {
            return (
                <h4 {...otherProps} className={componentClass}>
                    {children}
                </h4>
            );
        }
        if (size === 5 || size === '5') {
            return (
                <h5 {...otherProps} className={componentClass}>
                    {children}
                </h5>
            );
        }
        if (size === 6 || size === '6') {
            return (
                <h6 {...otherProps} className={componentClass}>
                    {children}
                </h6>
            );
        }

        return null;
    }
}

H.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    className: PropTypes.string,
    size: PropTypes.oneOf([1, 2, 3, 4, 5, 6, '1', '2', '3', '4', '5', '6']).isRequired,
    applyTheme: PropTypes.func,
};

H.defaultProps = {
    children: null,
    className: '',
    applyTheme: _ => _,
};

export default H;
