## Usage

```jsx
<Text size="l" />
```

## Props

Prop name | Prop type | Default value
--- | --- | ---
*size*|`xs`, `s`, `m`, `l`, `xl`, `xxl`, `xxxl`|`m`

## CSS Selectors

```css
.Text {}

/* Size */
.Text_size_xs {}
.Text_size_s {}
.Text_size_m {}
.Text_size_l {}
.Text_size_xl {}
.Text_size_xxl {}
.Text_size_xxxl {}
```
