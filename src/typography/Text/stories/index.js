import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, select } from '@storybook/addon-knobs';

// Constants
import { LOCAL_STORAGE_THEME_KEY } from '../../../constants/local-storage';
import { SIZES } from '../../../constants/props';
import { THEMES } from '../../../constants/themes';

import Text from '../index';

import notes from '../readme.md';

const theme = localStorage.getItem(LOCAL_STORAGE_THEME_KEY);

let sizesList = [];

switch (theme) {
    case THEMES.DEFAULT:
        sizesList = [SIZES.XS, SIZES.S, SIZES.M, SIZES.L, SIZES.XL, SIZES.XXL, SIZES.XXXL];
        break;
    case THEMES.ABSOLUT:
        sizesList = [SIZES.XS, SIZES.S, SIZES.M, SIZES.L, SIZES.XL, SIZES.XXL, SIZES.XXXL];
        break;
    case THEMES.ALFA:
        sizesList = [SIZES.XS, SIZES.S, SIZES.M, SIZES.L, SIZES.XL, SIZES.XXL, SIZES.XXXL];
        break;
    case THEMES.SPB:
        sizesList = [SIZES.XS, SIZES.S, SIZES.M, SIZES.L, SIZES.XL, SIZES.XXL, SIZES.XXXL];
        break;
    case THEMES.TKB:
        sizesList = [SIZES.XS, SIZES.S, SIZES.M, SIZES.L, SIZES.XL, SIZES.XXL, SIZES.XXXL];
        break;
    case THEMES.KYC:
        sizesList = [SIZES.XS, SIZES.S, SIZES.M, SIZES.L, SIZES.XL, SIZES.XXL, SIZES.XXXL];
        break;
    case THEMES.KYC_LANDING:
        sizesList = [SIZES.XS, SIZES.S, SIZES.M, SIZES.L, SIZES.XL, SIZES.XXL, SIZES.XXXL];
        break;
    default:
        break;
}

const stories = storiesOf('Text', module);

stories.addDecorator(withKnobs);
stories.addDecorator(store => <div style={{ padding: '10px' }}>{store()}</div>);

stories.add(
    'Default',
    () => (
        <Text size={select('Size', sizesList, SIZES.M)}>
            <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                Ipsum has been the industry&apos;s standard dummy text ever since the 1500s, when an
                unknown printer took a galley of type and scrambled it to make a type specimen book.
                It has survived not only five centuries, but also the leap into electronic
                typesetting, remaining essentially unchanged.
            </p>
            <p>
                It was popularised in the 1960s with the release of Letraset sheets containing Lorem
                Ipsum passages, and more recently with desktop publishing software like Aldus
                PageMaker including versions of Lorem Ipsum.
            </p>
        </Text>
    ),
    { notes },
);
