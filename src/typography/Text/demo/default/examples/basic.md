# Базовое использование

В свойстве `size` нужно передать размер текста

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import { Text, ThemeProvider } from 'ibalance';
import theme from 'ibalance/src/themes/default/theme';

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <Text size="xs">Текст XS</Text>
        <Text size="s">Текст S</Text>
        <Text size="m">Текст M</Text>
        <Text size="l">Текст L</Text>
        <Text size="xl">Текст XL</Text>
        <Text size="xxl">Текст XXL</Text>
        <Text size="xxxl">Текст XXXL</Text>
    </ThemeProvider>,
    document.getElementById('root'),
);
```
