import React from 'react';

// Components
import Text from '../../index';

// Demo
import DemoComponent from '../../../../../demo/components/DemoComponent';
import Example from '../../../../../demo/components/Example';
import ReadmeView from '../../../../../demo/components/ReadmeView';

import readme from './readme.md';

// Examples
import basic from './examples/basic.md';

const DemoText = () => {
    return (
        <DemoComponent>
            <ReadmeView>{readme}</ReadmeView>
            <div id="example">
                <Example readme={basic}>
                    <Text size="xs">Текст XS</Text>
                    <Text size="s">Текст S</Text>
                    <Text size="m">Текст M</Text>
                    <Text size="l">Текст L</Text>
                    <Text size="xl">Текст XL</Text>
                    <Text size="xxl">Текст XXL</Text>
                    <Text size="xxxl">Текст XXXL</Text>
                </Example>
            </div>
        </DemoComponent>
    );
};

export default DemoText;
