import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

// Constants
import { SIZES } from '../../constants/props';

import { withTheme } from '../../theme-provider';

@withTheme
class Text extends Component {
    render() {
        const { className, size, applyTheme, children, ...otherProps } = this.props;

        const componentClass = applyTheme(cn('Text', `Text_size_${size}`), className);

        return (
            <div {...otherProps} className={componentClass}>
                {children}
            </div>
        );
    }
}

Text.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    className: PropTypes.string,
    size: PropTypes.oneOf([SIZES.XS, SIZES.S, SIZES.M, SIZES.L, SIZES.XL, SIZES.XXL, SIZES.XXXL]),
    applyTheme: PropTypes.func,
};

Text.defaultProps = {
    children: null,
    className: '',
    size: SIZES.M,
    applyTheme: _ => _,
};

export default Text;
