// Components
import Button from './components/Button';
import Calendar from './components/Calendar'; // eslint-disable-line
import Checkbox from './components/Checkbox';
import ConfirmModal from './components/ConfirmModal';
import FeatherIcon from './components/FeatherIcon';
import Icon from './components/Icon';
import Image from './components/Image';
import ImageViewer, { withImageViewer } from './components/ImageViewer';
import Input from './components/Input';
import InputDate from './components/InputDate'; // eslint-disable-line import/no-cycle
import InputSlider from './components/InputSlider';
import InputSuggest from './components/InputSuggest'; // eslint-disable-line import/no-cycle
import MaterialIcon from './components/MaterialIcon';
import Modal from './components/Modal';
import Radio from './components/Radio';
import SelectBox from './components/SelectBox';
import Slider from './components/Slider';
import Spin from './components/Spin';
import Switch from './components/Switch';
import Table from './components/Table';
import Textarea from './components/Textarea';
import Tooltip from './components/Tooltip';
import UploadFiles from './components/UploadFiles';

import { Notification, NotificationWrapper } from './components/Notification';

// Layout
import LContainer from './layout/LContainer';
import LContent from './layout/LContent';
import LFooter from './layout/LFooter';
import LHeader from './layout/LHeader';
import LSidebar from './layout/LSidebar';

// Typography
import H from './typography/H';
import Text from './typography/Text';

// Theme
import ThemeProvider, { withTheme } from './theme-provider';

// Wrappers
import { Col, Row } from './wrappers/WGrid';
import WFormRow from './wrappers/WFormRow';
import {
    WMobile,
    WTab,
    WDesktop,
    WLargeDesktop,
    WFromTab,
    WFromDesktop,
    WToTab,
    WToDesktop,
} from './wrappers/WResponsive';

import packageJson from '../package.json';

// Log the version
console.log(`${packageJson.name} v${packageJson.version}`);

export {
    // Components
    Button,
    Calendar,
    Checkbox,
    ConfirmModal,
    FeatherIcon,
    Icon,
    Image,
    ImageViewer,
    Input,
    InputDate,
    InputSlider,
    InputSuggest,
    MaterialIcon,
    Modal,
    Notification,
    NotificationWrapper,
    Radio,
    SelectBox,
    Slider,
    Spin,
    Switch,
    Table,
    Tooltip,
    Textarea,
    UploadFiles,
    // Layout
    LContainer,
    LContent,
    LFooter,
    LHeader,
    LSidebar,
    // Typography
    H,
    Text,
    // Themes
    ThemeProvider,
    withTheme,
    // Wrappers
    Col,
    Row,
    WFormRow,
    WDesktop,
    WFromDesktop,
    WFromTab,
    WLargeDesktop,
    WMobile,
    WTab,
    WToDesktop,
    WToTab,
    // Decorators
    withImageViewer,
};

export default {
    // Components
    Button,
    Calendar,
    Checkbox,
    ConfirmModal,
    FeatherIcon,
    Icon,
    Image,
    ImageViewer,
    Input,
    InputDate,
    InputSlider,
    InputSuggest,
    MaterialIcon,
    Modal,
    Notification,
    NotificationWrapper,
    Radio,
    SelectBox,
    Slider,
    Spin,
    Switch,
    Table,
    Tooltip,
    Textarea,
    UploadFiles,
    // Layout
    LContainer,
    LContent,
    LFooter,
    LHeader,
    LSidebar,
    // Typography
    H,
    Text,
    // Themes
    ThemeProvider,
    withTheme,
    // Wrappers
    Col,
    Row,
    WFormRow,
    WDesktop,
    WFromDesktop,
    WFromTab,
    WLargeDesktop,
    WMobile,
    WTab,
    WToDesktop,
    WToTab,
    // Decorators
    withImageViewer,
};
