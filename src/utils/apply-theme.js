export default theme => (componentClasses, classNameProp) => {
    if (!theme) {
        return classNameProp || '';
    }
    let className = '';
    let result = '';

    componentClasses.split(' ').forEach(c => {
        className = theme[c];
        if (className) {
            result += `${className} `;
        }
    });
    return classNameProp ? `${result.trim()} ${classNameProp}` : result.trim();
};
