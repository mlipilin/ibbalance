const path = require('path');
const fs = require('fs');
const shell = require('shelljs');

const typesConstant = ['colors', 'breakpoints', 'font', 'layout', 'z-index'];

const typeJSPath = type => path.join(__dirname, `../base/ThemeConstants/${type}`);

const themesPath = path.join(__dirname, '../themes');

const getThemeTypeSassPath = (theme, type) =>
    path.join(__dirname, `../themes/${theme}/variables/${type}.sass`);

const getThemeTypeJSPath = (theme, type) =>
    path.join(__dirname, `../base/ThemeConstants/${type}/${theme}.js`);

const writeThemeConstants = async (theme, type, data) => {
    if (!fs.existsSync(typeJSPath(type))) {
        shell.mkdir('-p', typeJSPath(type));
    }

    const constantsData = `export default ${JSON.stringify(data)}`;

    fs.writeFile(getThemeTypeJSPath(theme, type), constantsData, err => {
        if (err) {
            return console.log('write err', err);
        }
        return console.log('write theme', theme);
    });
};

const convertSassToObject = data => {
    const arr = data.split('\n');
    const res = {};

    let currKey = 'default';
    res[currKey] = {};
    arr.forEach(str => {
        if (str === '') {
            return;
        }
        if (str[0] === '/') {
            currKey = str.slice(3);
            res[currKey] = {};
        }
        if (str[0] === '$') {
            const pair = str.replace(/\s+/g, '').split(':');

            res[currKey][pair[0].slice(1)] = pair[1];
        }
    });

    return res;
};

const readThemeConstants = async theme => {
    /* eslint no-restricted-syntax: ["off", "ForOfStatement"] */
    for (const constant of typesConstant) {
        const filePath = getThemeTypeSassPath(theme, constant);
        fs.readFile(filePath, 'UTF-8', (err, data) => {
            if (err) {
                return console.log('readThemeConstants err', err);
            }
            const obj = convertSassToObject(data);
            writeThemeConstants(theme, constant, obj);
            return undefined;
        });
    }
};

const readThemesFolder = async () => {
    fs.readdir(themesPath, (err, themes) => {
        if (err) {
            return console.log(`readThemesFolder err${err}`);
        }

        const themesFormat = themes[0] === '.DS_Store' ? themes.slice(1) : themes;

        themesFormat.forEach(async theme => {
            if (theme !== '.DS_Store') {
                await readThemeConstants(theme);
            }
        });
        return undefined;
    });
};

readThemesFolder();
