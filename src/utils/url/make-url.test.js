import makeUrl from './make-url';

describe('src/utils/url/make-url.js', () => {
    it('no params -> NULL', () => {
        expect(makeUrl()).toBe(null);
    });
    it('has ONLY pattern -> pattern', () => {
        expect(makeUrl('https://balance-pl.ru')).toBe('https://balance-pl.ru');
    });
    it('has pattern and params', () => {
        expect(makeUrl('https://balance-pl.ru/page/:id/show', { id: 1 })).toBe(
            'https://balance-pl.ru/page/1/show',
        );
    });
    it('has pattern and lots of params', () => {
        expect(
            makeUrl('https://balance-pl.ru/page/:id/show/:id/:title', { id: 1, title: 'main' }),
        ).toBe('https://balance-pl.ru/page/1/show/1/main');
    });
    it('has pattern and qs', () => {
        expect(makeUrl('https://balance-pl.ru/item', {}, { page: 1 })).toBe(
            'https://balance-pl.ru/item?page=1',
        );
    });
    it('has pattern and lots of qs items', () => {
        expect(makeUrl('https://balance-pl.ru/item', {}, { page: 1, limit: 10, year: 2019 })).toBe(
            'https://balance-pl.ru/item?page=1&limit=10&year=2019',
        );
    });
    it('has pattern, params and qs', () => {
        expect(
            makeUrl(
                'https://balance-pl.ru/page/:id/show/:title',
                { id: 1, title: 'main' },
                { page: 1, limit: 10 },
            ),
        ).toBe('https://balance-pl.ru/page/1/show/main?page=1&limit=10');
    });
});
