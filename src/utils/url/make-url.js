export default (pattern, params, qs) => {
    if (!pattern) return null;
    if (!params && !qs) return pattern;

    let result = pattern;

    // Params
    if (params) {
        Object.entries(params).forEach(([key, value]) => {
            result = result.replace(new RegExp(`:${key}`, 'g'), value);
        });
    }

    // Query string
    if (qs) {
        const qsArray = [];
        Object.entries(qs).forEach(([key, value]) => {
            qsArray.push(`${key}=${value}`);
        });
        result += `?${qsArray.join('&')}`;
    }

    return result;
};
