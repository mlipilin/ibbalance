import makeUrl from '../make-url';

const about = (...args) => makeUrl('/about', ...args);
const constants = (...args) => makeUrl('/constants', ...args);
const createComponent = (...args) => makeUrl('/create-component', ...args);
const gettingStarted = (...args) => makeUrl('/getting-started', ...args);
const grid = (...args) => makeUrl('/grid', ...args);
const kindOfComponents = (...args) => makeUrl('/kind-of-components', ...args);
const layout = (...args) => makeUrl('/layout', ...args);
const viewItem = (...args) => makeUrl('/view/:item', ...args);
const viewSubitem = (...args) => makeUrl('/view/:item/:subitem', ...args);
const wrappers = (...args) => makeUrl('/wrappers', ...args);

export default {
    about,
    constants,
    createComponent,
    gettingStarted,
    grid,
    kindOfComponents,
    layout,
    viewItem,
    viewSubitem,
    wrappers,
};
