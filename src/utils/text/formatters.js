import { parseFloatValue } from './parsers';

export const capitalize = s => {
    if (typeof s !== 'string') return '';
    if (s === '') return '';
    return `${s[0].toUpperCase()}${s.slice(1)}`;
};

export const formatPriceNumber = (value = '') => {
    if (value === null) return value;

    const [int, fraction] = parseFloatValue(value);
    let integer = int;

    if (fraction !== null) {
        integer = integer === null ? 0 : integer;
    }

    if (integer !== null) {
        const formattedInteger = String(integer).replace(/(.)(?=(\d{3})+$)/g, '$1 ');
        if (fraction !== null) {
            return `${formattedInteger}.${fraction}`;
        }
        const hasDot = /\.|\,/.test(value);
        return hasDot ? `${formattedInteger}.` : formattedInteger;
    }
    return null;
};
