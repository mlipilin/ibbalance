import { parseFloatValue, parsePriceNumber } from './parsers';

describe('src/utils/text/parsers.js', () => {
    describe('parseFloatValue', () => {
        it('null -> [null, null]', () => {
            expect(parseFloatValue('a')).toEqual([null, null]);
        });
        it('a -> [null, null]', () => {
            expect(parseFloatValue('a')).toEqual([null, null]);
        });
        it('123 -> [123, null]', () => {
            expect(parseFloatValue('123')).toEqual([123, null]);
        });
        it('a.a -> [null, null]', () => {
            expect(parseFloatValue('a.a')).toEqual([null, null]);
        });
        it('123.a -> [123, null]', () => {
            expect(parseFloatValue('123.a')).toEqual([123, null]);
        });
        it('a.456 -> [null, 45]', () => {
            expect(parseFloatValue('a.456')).toEqual([null, 45]);
        });
        it('123.456 -> [123, 45]', () => {
            expect(parseFloatValue('123.456')).toEqual([123, 45]);
        });
    });
    describe('parsePriceNumber', () => {
        it('null -> null', () => expect(parsePriceNumber(null)).toBeNull());
        it('"123" -> 123', () => expect(parsePriceNumber('123')).toBe('123'));
        it('"123.1" -> 123.1', () => expect(parsePriceNumber('123.1')).toBe('123.1'));
        it('"123." -> 123.', () => expect(parsePriceNumber('123.')).toBe('123.'));
        it('"123," -> 123.', () => expect(parsePriceNumber('123,')).toBe('123.'));
        it('"123aaa.1aaa" -> 123.1', () => expect(parsePriceNumber('123aaa.1aaa')).toBe('123.1'));
        it('"123aaa.a1aaa" -> 123.1', () => expect(parsePriceNumber('123aaa.a1aaa')).toBe('123.1'));
        it('"123.123" -> 123.12', () => expect(parsePriceNumber('123.123')).toBe('123.12'));
        it('"123.127" -> 123.12', () => expect(parsePriceNumber('123.127')).toBe('123.12'));
        it('"-123.127" -> -123.12', () => expect(parsePriceNumber('-123.127')).toBe('-123.12'));
        it('"-123 45.127" -> -12345.12', () =>
            expect(parsePriceNumber('-123 45.127')).toBe('-12345.12'));
        it('"-" -> null', () => expect(parsePriceNumber('-')).toBe('-'));
        it('"---" -> null', () => expect(parsePriceNumber('---')).toBeNull());
    });
});
