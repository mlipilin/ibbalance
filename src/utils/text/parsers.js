export const parsePriceNumber = (value = '') => {
    if (value === null) return null;

    if (value === '') return null;
    if (value === '-') return '-';

    const [integer, fraction] = parseFloatValue(value);

    if (integer === null) {
        return null;
    }
    if (integer && fraction === null) {
        const hasDot = /\.|\,/.test(value);
        return hasDot ? `${integer}.` : `${integer}`;
    }
    if (integer && fraction) {
        return `${integer}.${fraction}`;
    }
    return undefined;
};

export const parseFloatValue = (value = '') => {
    if (value === null) return [null, null];

    /* eslint-disable */
    const valueFormat = String(value)
    // 1. replace all commas to dots
        .replace(/\,/g, '.')
        // 2. replace all symbols except numbers, dots and dashes to ""
        .replace(/[^\d\.\-]/g, '')
        // 3. replace all dots except the first one to ""
        .replace(/\./g, (i => m => (!i++ ? m : ''))(0))
        // 4. replace all dashed except the first one in value to ""
        .replace(/(?!^)-/g, '');
    /* eslint-enable */

    let [integer, fraction] = valueFormat.split('.');
    integer = parseInt(integer, 10);
    integer = Number.isNaN(integer) ? null : integer;

    fraction = parseInt(fraction, 10);
    fraction = Number.isNaN(fraction) ? null : fraction;
    fraction = fraction !== null ? Number(String(fraction).slice(0, 2)) : null;

    return [integer, fraction];
};
