import { capitalize, formatPriceNumber } from './formatters';

describe('src/utils/text/formatters.js', () => {
    describe('capitalize', () => {
        it('undefined -> ""', () => {
            expect(capitalize()).toBe('');
        });
        it('null -> ""', () => {
            expect(capitalize(null)).toBe('');
        });
        it('NOT string -> ""', () => {
            expect(capitalize([])).toBe('');
        });
        it('a -> "A"', () => {
            expect(capitalize('a')).toBe('A');
        });
        it('abc -> "Abc"', () => {
            expect(capitalize('abc')).toBe('Abc');
        });
    });
    describe('formatPriceNumber', () => {
        it('null -> null', () => expect(formatPriceNumber(null)).toBeNull());
        it('123 -> 123', () => expect(formatPriceNumber('123')).toBe('123'));
        it('12345 -> 12 345', () => expect(formatPriceNumber('12345')).toBe('12 345'));
        it('123.45 -> 123.45', () => expect(formatPriceNumber('123.45')).toBe('123.45'));
        it('123,45 -> 123.45', () => expect(formatPriceNumber('123,45')).toBe('123.45'));
        it('123. -> 123.', () => expect(formatPriceNumber('123.')).toBe('123.'));
        it('-12345 -> -12 345', () => expect(formatPriceNumber('-12345')).toBe('-12 345'));
    });
});
