const path = require('path');
const fs = require('fs');
const shell = require('shelljs');

const typesConstant = ['block', 'breakpoints', 'fonts', 'textarea', 'tooltip'];

const typeJSPath = type => path.join(__dirname, `../base/ThemeMixins/${type}`);

const themesPath = path.join(__dirname, '../themes');

const getThemeTypeSassPath = (theme, type) =>
    path.join(__dirname, `../themes/${theme}/mixins/${type}.sass`);

const getThemeTypeJSPath = (theme, type) =>
    path.join(__dirname, `../base/ThemeMixins/${type}/${theme}.js`);

const writeThemeMixins = async (theme, type, data) => {
    if (!fs.existsSync(typeJSPath(type))) {
        shell.mkdir('-p', typeJSPath(type));
    }

    const constantsData = `export default ${JSON.stringify(data)}`;

    fs.writeFile(getThemeTypeJSPath(theme, type), constantsData, err => {
        if (err) {
            return console.log('write err', err);
        }
        return console.log('write theme mixin', theme);
    });
};

const convertSassToObject = data => {
    const arr = data.split('\n');
    const res = {};

    let currKey = 'default';
    res[currKey] = '';
    arr.forEach(str => {
        if (str === '') {
            return;
        }
        if (str[0] === '@') {
            currKey = str.slice(7);
            res[currKey] = '';
            return;
        }
        if (str[0] === ' ') {
            res[currKey] = res[currKey] ? res[currKey].concat('\n', str) : str;
        }
    });

    return res;
};

const readThemeMixins = async theme => {
    /* eslint no-restricted-syntax: ["off", "ForOfStatement"] */
    for (const constant of typesConstant) {
        const filePath = getThemeTypeSassPath(theme, constant);
        fs.readFile(filePath, 'UTF-8', (err, data) => {
            if (err) {
                return console.log('readThemeMixins err', err);
            }
            const obj = convertSassToObject(data);
            writeThemeMixins(theme, constant, obj);
            return undefined;
        });
    }
};

const readThemesFolder = async () => {
    fs.readdir(themesPath, (err, themes) => {
        if (err) {
            return console.log(`readThemesFolder err${err}`);
        }
        const themesFormat = themes[0] === '.DS_Store' ? themes.slice(1) : themes;

        themesFormat.forEach(async theme => {
            if (theme !== '.DS_Store') {
                await readThemeMixins(theme);
            }
        });
        return undefined;
    });
};

readThemesFolder();
