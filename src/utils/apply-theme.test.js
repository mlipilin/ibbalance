import applyTheme from './apply-theme';

describe('src/utils/apply-theme.js', () => {
    it('no theme', () => {
        expect(applyTheme()('class1 class2 class999')).toBe('');
    });
    it('theme + componentClasses', () => {
        const theme = {
            class1: 'class1_hash',
            class2: 'class2_hash',
            class3: 'class3_hash',
        };
        expect(applyTheme(theme)('class1 class2 class999')).toBe('class1_hash class2_hash');
    });
    it('theme + componentClasses + classNameProp', () => {
        const theme = {
            class1: 'class1_hash',
            class2: 'class2_hash',
            class3: 'class3_hash',
        };
        expect(applyTheme(theme)('class1 class2 class999', 'class4')).toBe(
            'class1_hash class2_hash class4',
        );
    });
});
