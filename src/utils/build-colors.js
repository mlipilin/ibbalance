const path = require('path');
const fs = require('fs');
const shell = require('shelljs');

const END_BASE_COLORS_ROW = '// End base colors';

const colorsJSPath = path.join(__dirname, '../base/Colors/colors');
const themesPath = path.join(__dirname, '../themes');

const getThemeColorsSassPath = theme =>
    path.join(__dirname, `../themes/${theme}/variables/colors.sass`);

const getThemeColorsJSPath = theme => path.join(__dirname, `../base/Colors/colors/${theme}.js`);

const writeThemeColors = async (theme, colors) => {
    if (!fs.existsSync(colorsJSPath)) {
        shell.mkdir('-p', colorsJSPath);
    }

    const themeData = `export default ${JSON.stringify(colors)}`;

    fs.writeFile(getThemeColorsJSPath(theme), themeData, err => {
        if (err) {
            return console.log('writeThemeColors err', err);
        }
        return console.log('write theme', theme);
    });
};

const parseColor = colorRow => {
    const colorRowParts = colorRow.replace(/\s+/g, ' ').split(' ');
    if (colorRowParts.length !== 2) {
        return null;
    }

    return {
        name: colorRowParts[0].replace(/^\$/, ''),
        value: colorRowParts[1],
    };
};

const convertColorsToArray = themeColors => {
    const rows = themeColors.split('\n');

    const themeColorsArray = [];

    for (let i = 0; i < rows.length; i += 1) {
        if (rows[i].indexOf(END_BASE_COLORS_ROW) !== -1) {
            break;
        }
        const color = parseColor(rows[i]);
        if (color) {
            themeColorsArray.push(color);
        }
    }

    return themeColorsArray;
};

const readThemeColors = async theme => {
    const filePath = getThemeColorsSassPath(theme);
    fs.readFile(filePath, 'UTF-8', (err, themeColors) => {
        if (err) {
            return console.log('readThemeColors err', err);
        }
        const themeColorsArray = convertColorsToArray(themeColors);
        writeThemeColors(theme, themeColorsArray);
        return undefined;
    });
};

const readThemesFolder = async () => {
    fs.readdir(themesPath, (err, themes) => {
        if (err) {
            return console.log(`readThemesFolder err${err}`);
        }
        themes.forEach(async theme => {
            if (theme !== '.DS_Store') {
                await readThemeColors(theme);
            }
        });
        return undefined;
    });
};

readThemesFolder();
