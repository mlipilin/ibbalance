import React from 'react';
import { render } from '@testing-library/react';
import ThemeProvider from '../theme-provider';
import theme from '../themes/default/theme.sass';

const withTheme = component => <ThemeProvider theme={theme}>{component}</ThemeProvider>;

function renderWithProviders(ui, options) {
    const { container, rerender: nativeRerender } = render(withTheme(ui, options));
    const rerender = componentRerender => nativeRerender(withTheme(componentRerender));
    return { container, rerender };
}

export { renderWithProviders as render };
