import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import { withTheme } from '../../theme-provider';

@withTheme
class LContainer extends Component {
    render() {
        const { className, hasHeader, sidebarSide, applyTheme, children } = this.props;

        const componentClass = applyTheme(
            cn('LContainer', `LContainer_sidebar-side_${sidebarSide}`, {
                'LContainer_has-header': hasHeader,
            }),
            className,
        );

        return <div className={componentClass}>{children}</div>;
    }
}

LContainer.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    className: PropTypes.string,
    hasHeader: PropTypes.bool,
    sidebarSide: PropTypes.oneOf(['none', 'left', 'right', 'both']),
    applyTheme: PropTypes.func,
};

LContainer.defaultProps = {
    children: null,
    className: '',
    hasHeader: false,
    sidebarSide: 'none',
    applyTheme: _ => _,
};

export default LContainer;
