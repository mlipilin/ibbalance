import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import { withTheme } from '../../theme-provider';

@withTheme
class LSidebar extends Component {
    constructor(props) {
        super(props);
        this.state = { isOpened: false };
    }

    componentDidMount() {
        // console.log('LSidebar MOUNTED');
    }

    componentWillUnmount() {
        const { applyTheme } = this.props;
        document.body.classList.remove(applyTheme('noBodyScroll'));
    }

    handleBackgroundClick = e => {
        e.preventDefault();
        this.close();
    };

    handleTogglerClick = e => {
        e.preventDefault();
        this.open();
    };

    close = () => {
        const { applyTheme } = this.props;
        this.setState({ isOpened: false }, () => {
            document.body.classList.remove(applyTheme('noBodyScroll'));
        });
    };

    open = () => {
        const { applyTheme } = this.props;
        this.setState({ isOpened: true }, () => {
            document.body.classList.add(applyTheme('noBodyScroll'));
        });
    };

    render() {
        const {
            className,
            hasHeader,
            side,
            toggleIcon,
            applyTheme,
            children,
            ...otherProps
        } = this.props;

        const { isOpened } = this.state;

        const componentClass = applyTheme(
            cn('LSidebar', `LSidebar_side_${side}`, {
                LSidebar_opened: isOpened,
                'LSidebar_has-header': hasHeader,
            }),
            className,
        );

        const backgroundClass = applyTheme(
            cn('LSidebar__Background', {
                LSidebar__Background_opened: isOpened,
                'LSidebar__Background_has-header': hasHeader,
            }),
        );

        const togglerClass = applyTheme(
            cn('LSidebar__Toggler', `LSidebar__Toggler_side_${side}`, {
                'LSidebar__Toggler_has-header': hasHeader,
            }),
        );

        return (
            <>
                {/* Sidebar */}
                <aside {...otherProps} className={componentClass}>
                    {children}
                </aside>

                {/* Background */}
                <div className={backgroundClass} onClick={this.handleBackgroundClick} />

                {/* Toggler */}
                <span className={togglerClass} onClick={this.handleTogglerClick}>
                    {toggleIcon}
                </span>
            </>
        );
    }
}

LSidebar.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    className: PropTypes.string,
    hasHeader: PropTypes.bool,
    side: PropTypes.oneOf(['left', 'right']),
    toggleIcon: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    applyTheme: PropTypes.func,
};

LSidebar.defaultProps = {
    children: null,
    className: '',
    hasHeader: false,
    side: 'left',
    toggleIcon: '=',
    applyTheme: _ => _,
};

export default LSidebar;
