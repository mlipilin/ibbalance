import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import { withTheme } from '../../theme-provider';

@withTheme
class LHeader extends Component {
    render() {
        const { className, applyTheme, sidebarSide, children, ...otherProps } = this.props;

        const componentClass = applyTheme(
            cn('LHeader', `LHeader_sidebar-side_${sidebarSide}`),
            className,
        );

        return (
            <header {...otherProps} className={componentClass}>
                {children}
            </header>
        );
    }
}

LHeader.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    className: PropTypes.string,
    sidebarSide: PropTypes.oneOf(['none', 'left', 'right', 'both']),
    applyTheme: PropTypes.func,
};

LHeader.defaultProps = {
    children: null,
    className: '',
    sidebarSide: 'none',
    applyTheme: _ => _,
};

export default LHeader;
