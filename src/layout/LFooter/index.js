import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import { withTheme } from '../../theme-provider';

@withTheme
class LFooter extends Component {
    render() {
        const { className, applyTheme, children, ...otherProps } = this.props;

        const componentClass = applyTheme(cn('LFooter'), className);

        return (
            <footer {...otherProps} className={componentClass}>
                {children}
            </footer>
        );
    }
}

LFooter.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    className: PropTypes.string,
    applyTheme: PropTypes.func,
};

LFooter.defaultProps = {
    children: null,
    className: '',
    applyTheme: _ => _,
};

export default LFooter;
