import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import { withTheme } from '../../theme-provider';

@withTheme
class LContent extends Component {
    render() {
        const { className, applyTheme, children, ...otherProps } = this.props;

        const componentClass = applyTheme(cn('LContent'), className);

        return (
            <div {...otherProps} className={componentClass}>
                {children}
            </div>
        );
    }
}

LContent.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    className: PropTypes.string,
    applyTheme: PropTypes.func,
};

LContent.defaultProps = {
    children: null,
    className: '',
    applyTheme: _ => _,
};

export default LContent;
