import React from 'react';
import { storiesOf } from '@storybook/react';

// Components
import LContainer from '../LContainer';
import LContent from '../LContent';
import LFooter from '../LFooter';
import LHeader from '../LHeader';
import LSidebar from '../LSidebar';

import notes from './readme.md';

const ContentData = () => 'Content';

const stories = storiesOf('Layout', module);

stories
    .add(
        'Header + Content',
        () => (
            <LContainer hasHeader>
                <LHeader>Header FullWidth</LHeader>
                <LContent>
                    <ContentData />
                </LContent>
            </LContainer>
        ),
        { notes },
    )
    .add(
        'Content + Footer',
        () => (
            <LContainer>
                <LContent>
                    <ContentData />
                </LContent>
                <LFooter>Footer</LFooter>
            </LContainer>
        ),
        { notes },
    )
    .add(
        'Header + Content + Footer',
        () => (
            <LContainer hasHeader>
                <LHeader>Header FullWidth</LHeader>
                <LContent>
                    <ContentData />
                </LContent>
                <LFooter>Footer</LFooter>
            </LContainer>
        ),
        { notes },
    )
    .add(
        'Sidebar Left + Content',
        () => (
            <LContainer sidebarSide="left">
                <LSidebar>Sidebar Left</LSidebar>
                <LContent>
                    <ContentData />
                </LContent>
            </LContainer>
        ),
        { notes },
    )
    .add(
        'Sidebar Right + Content',
        () => (
            <LContainer sidebarSide="right">
                <LSidebar side="right">Sidebar Right</LSidebar>
                <LContent>
                    <ContentData />
                </LContent>
            </LContainer>
        ),
        { notes },
    )
    .add(
        'Sidebar Left + Sidebar Right + Content',
        () => (
            <LContainer sidebarSide="both">
                <LSidebar side="left">Sidebar Left</LSidebar>
                <LSidebar side="right">Sidebar Right</LSidebar>
                <LContent>
                    <ContentData />
                </LContent>
            </LContainer>
        ),
        { notes },
    )
    .add(
        'Header + Sidebar Left + Content (V1)',
        () => (
            <LContainer hasHeader sidebarSide="left">
                <LHeader sidebarSide="left">Header</LHeader>
                <LSidebar>Sidebar Left</LSidebar>
                <LContent>
                    <ContentData />
                </LContent>
            </LContainer>
        ),
        { notes },
    )
    .add(
        'Header + Sidebar Left + Content (V2)',
        () => (
            <LContainer hasHeader sidebarSide="left">
                <LHeader>Header FullWidth</LHeader>
                <LSidebar hasHeader>Sidebar Left</LSidebar>
                <LContent>
                    <ContentData />
                </LContent>
            </LContainer>
        ),
        { notes },
    )
    .add(
        'Header + Sidebar Right + Content (V1)',
        () => (
            <LContainer hasHeader sidebarSide="right">
                <LHeader sidebarSide="right">Header</LHeader>
                <LSidebar side="right">Sidebar Right</LSidebar>
                <LContent>
                    <ContentData />
                </LContent>
            </LContainer>
        ),
        { notes },
    )
    .add(
        'Header + Sidebar Right + Content (V2)',
        () => (
            <LContainer hasHeader sidebarSide="right">
                <LHeader>Header FullWidth</LHeader>
                <LSidebar hasHeader side="right">
                    Sidebar Right
                </LSidebar>
                <LContent>
                    <ContentData />
                </LContent>
            </LContainer>
        ),
        { notes },
    )
    .add(
        'Header + Sidebar Left + Sidebar Right + Content (V1)',
        () => (
            <LContainer hasHeader sidebarSide="both">
                <LHeader sidebarSide="both">Header</LHeader>
                <LSidebar side="left">Sidebar Left</LSidebar>
                <LSidebar side="right">Sidebar Right</LSidebar>
                <LContent>
                    <ContentData />
                </LContent>
            </LContainer>
        ),
        { notes },
    )
    .add(
        'Header + Sidebar Left + Sidebar Right + Content (V2)',
        () => (
            <LContainer hasHeader sidebarSide="both">
                <LHeader>Header FullWidth</LHeader>
                <LSidebar hasHeader side="left">
                    Sidebar Left
                </LSidebar>
                <LSidebar hasHeader side="right">
                    Sidebar Right
                </LSidebar>
                <LContent>
                    <ContentData />
                </LContent>
            </LContainer>
        ),
        { notes },
    )
    .add(
        'Header + Sidebar Left + Content + Footer (V1)',
        () => (
            <LContainer hasHeader sidebarSide="left">
                <LHeader sidebarSide="left">Header</LHeader>
                <LSidebar>Sidebar Left</LSidebar>
                <LContent>
                    <ContentData />
                </LContent>
                <LFooter>Footer</LFooter>
            </LContainer>
        ),
        { notes },
    )
    .add(
        'Header + Sidebar Left + Content + Footer (V2)',
        () => (
            <LContainer hasHeader sidebarSide="left">
                <LHeader>Header FullWidth</LHeader>
                <LSidebar hasHeader>Sidebar Left</LSidebar>
                <LContent>
                    <ContentData />
                </LContent>
                <LFooter>Footer</LFooter>
            </LContainer>
        ),
        { notes },
    )
    .add(
        'Header + Sidebar Right + Content + Footer (V1)',
        () => (
            <LContainer hasHeader sidebarSide="right">
                <LHeader sidebarSide="right">Header</LHeader>
                <LSidebar side="right">Sidebar Right</LSidebar>
                <LContent>
                    <ContentData />
                </LContent>
                <LFooter>Footer</LFooter>
            </LContainer>
        ),
        { notes },
    )
    .add(
        'Header + Sidebar Right + Content + Footer (V2)',
        () => (
            <LContainer hasHeader sidebarSide="right">
                <LHeader>Header FullWidth</LHeader>
                <LSidebar hasHeader side="right">
                    Sidebar Right
                </LSidebar>
                <LContent>
                    <ContentData />
                </LContent>
                <LFooter>Footer</LFooter>
            </LContainer>
        ),
        { notes },
    )
    .add(
        'Header + Sidebar Left + Sidebar Right + Content + Footer (V1)',
        () => (
            <LContainer hasHeader sidebarSide="both">
                <LHeader sidebarSide="both">Header</LHeader>
                <LSidebar side="left">Sidebar Left</LSidebar>
                <LSidebar side="right">Sidebar Right</LSidebar>
                <LContent>
                    <ContentData />
                </LContent>
                <LFooter>Footer</LFooter>
            </LContainer>
        ),
        { notes },
    )
    .add(
        'Header + Sidebar Left + Sidebar Right + Content + Footer (V2)',
        () => (
            <LContainer hasHeader sidebarSide="both">
                <LHeader>Header FullWidth</LHeader>
                <LSidebar hasHeader side="left">
                    Sidebar Left
                </LSidebar>
                <LSidebar hasHeader side="right">
                    Sidebar Right
                </LSidebar>
                <LContent>
                    <ContentData />
                </LContent>
                <LFooter>Footer</LFooter>
            </LContainer>
        ),
        { notes },
    );
