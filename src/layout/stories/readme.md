## LContainer

Занимает все возможное пространство на экране и используется для размещения в нем структурных элементов страницы (`LHeader`, `LFooter`, `LSidebar` и `LContent`). Для правильной разметки структуры должен получать в качестве `props` информацию о наличии на странице хедера (`hasHeader`) и сайдбара(ов) (`sidebarSide`).

**Usage:**

```jsx
<LContainer hasHeader>
    <LHeader>Хедер, растянутый по ширине на весь экран</LHeader>
    <LContent>Контент</LContent>
</LContainer>

<LContainer>
    <LContent>Контент</LContent>
    <LFooter>Футер</LFooter>
</LContainer>

<LContainer hasHeader sidebarSide="left">
    <LHeader>Хедер, растянутый по ширине на весь экран</LHeader>
    <LSidebar>Сайдбар, который находится под хедером</LSidebar>
    <LContent>Контент</LContent>
</LContainer>

<LContainer hasHeader sidebarSide="left">
    <LHeader sidebarSide="left">Хедер, который находится СПРАВА от сайдбара</LHeader>
    <LSidebar>Сайдбар, растянутый по высоте на весь экран</LSidebar>
    <LContent>Контент</LContent>
</LContainer>

<LContainer hasHeader sidebarSide="right">
    <LHeader sidebarSide="right">Хедер, который находится СЛЕВА от сайдбара</LHeader>
    <LSidebar>Сайдбар, растянутый по высоте на весь экран</LSidebar>
    <LContent>Контент</LContent>
</LContainer>
```

**Props:**

Prop name | Prop type | Default value
--- | --- | ---
*hasHeader*|bool|`false`
*sidebarSide*|`none`, `left`, `right`, `both`|`none`

**CSS Selectors:**

```css
.LContainer {}
.LContainer_has-header {}
.LContainer_sidebar-side_left {}
.LContainer_sidebar-side_right {}
.LContainer_sidebar-side_both {}
```

## LContent

Занимает все возможное пространство в родительском контейнере и спользуется для задания контентной части. Наряду с `LHeader`, `LFooter` и `LSidebar` является дочерним компонентом для `LContainer`. 

**Usage:**

```jsx
<LContainer>
    <LContent>Контент на всю страницу</LContent>
</LContainer>

<LContainer sidebarSide="left">
    <LSidebar>Сайдбар</LSidebar>
    <LContent>Контент (справа от сайдбара) на всю оставшуюся часть страницы</LContent>
</LContainer>

<LContainer hasHeader>
    <LHeader>Хедер</LHeader>
    <LContent>Контент (под хедером) на всю оставшуюся часть страницы</LContent>
</LContainer>

<LContainer hasHeader>
    <LHeader>Хедер</LHeader>
    <LContent>Контент (под хедером) на всю оставшуюся часть страницы минус размер футера</LContent>
    <LFooter>Футер</LFooter>
</LContainer>
```

**Props:**
—

**CSS Selectors:**

```css
.LContent {}
```

## LFooter

Используется для разметки места под "футер" страницы. Наряду с `LContent`, `LHeader` и `LSidebar` является дочерним компонентом для `LContainer`. 

**Usage:**

```jsx
<LContainer>
    <LContent>Контент на всю оставшуюся часть страницы</LContent>
    <LFooter>Футер</LFooter>
</LContainer>
```

**Props:**
—

**CSS Selectors:**

```css
.LFooter {}
```

## LHeader

Используется для разметки места под **прижатый к верху** "хедер" страницы. Наряду с `LContent`, `LFooter` и `LSidebar` является дочерним компонентом для `LContainer`.

**Внимание:**

- При наличии на странице хедера, информацию об этом в виде свойства `hasHeader` нужно **обязательно** передавать компоненту `LContainer` и **при необходимости** в `LSidebar`!
- При наличие на странице одного или двух компонентов `LSidebar`, информацию об этом в виде свойства `sidebarSide` нужно **обязательно** передавать компоненту `LContainer` и **при необходимости** `LHeader`!

**Usage:**

```jsx
<LContainer hasHeader>
    <LHeader>Хедер</LHeader>
    <LContent>Контент</LContent>
</LContainer>

// Хедер на всю ширину:
// передаем hasHeader и sidebarSide в LContainer и НЕ передаем sidebarSide в LHeader
<LContainer hasHeader sidebarSide="left">
    <LHeader>Хедер</LHeader>
    <LSidebar hasHeader>Сайдбар</LSidebar>
    <LContent>Контент</LContent>
</LContainer>

// Хедер справа от сайдбара:
// передаем hasHeader и sidebarSide в LContainer и также передаем sidebarSide в LHeader
<LContainer hasHeader sidebarSide="left">
    <LHeader sidebarSide="left">Хедер</LHeader>
    <LSidebar>Сайдбар</LSidebar>
    <LContent>Контент</LContent>
</LContainer>
```

Prop name | Prop type | Default value
--- | --- | ---
*sidebarSide*|`none`, `left`, `right`, `both`|`none`

**CSS Selectors:**

```css
.LHeader {}
.LHeader_sidebar-side_left {}
.LHeader_sidebar-side_right {}
.LHeader_sidebar-side_both {}
```

## LSidebar

Используется для разметки места под "сайдбар" и располагается слева (`left`) или справа (`right`) в зависимости от значения свойства `side`. Для планшетной и мобильной версий "сайдбар" сворачивается в "гамбургер". Иконка для "гамбургера" берется из свойства `toggleIcon`. Наряду с `LHeader`, `LFooter` и `LContent` является дочерним компонентом для `LContainer`. 

**Внимание:**

- При наличии на странице хедера, информацию об этом в виде свойства `hasHeader` нужно **обязательно** передавать компоненту `LContainer` и **при необходимости** в `LSidebar`!
- При наличие на странице одного или двух компонентов `LSidebar`, информацию об этом в виде свойства `sidebarSide` нужно **обязательно** передавать компоненту `LContainer` и **при необходимости** `LHeader`!

**Usage:**

```jsx
// Сайдбар слева (по умолчанию, можно не передавать side)
<LContainer hasHeader>
    <LSidebar>Сайдбар</LSidebar>
    <LContent>Контент</LContent>
</LContainer>

// Сайдбар слева с кастомной иконкой
<LContainer hasHeader>
    <LSidebar toggleIcon={<FeatherIcon name="menu" />}>Сайдбар</LSidebar>
    <LContent>Контент</LContent>
</LContainer>

// Сайдбар под хедером:
// передаем hasHeader и sidebarSide в LContainer и НЕ передаем sidebarSide в LHeader
<LContainer hasHeader sidebarSide="left">
    <LHeader>Хедер</LHeader>
    <LSidebar hasHeader>Сайдбар</LSidebar>
    <LContent>Контент</LContent>
</LContainer>

// Сайдбар слева от хедера:
// передаем hasHeader и sidebarSide в LContainer и также передаем sidebarSide в LHeader
<LContainer hasHeader sidebarSide="left">
    <LHeader sidebarSide="left">Хедер</LHeader>
    <LSidebar>Сайдбар</LSidebar>
    <LContent>Контент</LContent>
</LContainer>
```

**Props:**

Prop name | Prop type | Default value
--- | --- | ---
*hasHeader*|bool|`false`
*side*|`left`, `right`|`left`
*toggleIcon*|React component, string|`=`

**CSS Selectors:**

```css
/* Видимая область "сайдбара" */
.LSidebar {}
.LSidebar_has-header {}
.LSidebar_side_left {}
.LSidebar_side_right {}
.LSidebar_opened {}

/* "Overlay" на странице при открытии "сайдбара" в планшетной или мобильной версиях */
.LSidebar__Background {}
.LSidebar__Background_opened {}

/* "Гамбургер" */
.LSidebar__Toggler {}
.LSidebar__Toggler_side_left {}
.LSidebar__Toggler_side_right {}
```
